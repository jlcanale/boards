//
//  SpotlightController.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 4/13/22.
//

import Foundation
import CoreSpotlight
import MobileCoreServices

public struct SpotlightController {
    public static let shared = SpotlightController()

    private let persistence = PersistenceController.shared

    public func index(_ items: [CSSearchableItem]) {
        guard !items.isEmpty else { return }

        CSSearchableIndex.default().indexSearchableItems(items) { error in
            if let error = error {
                print("Indexing error: \(error.localizedDescription)")
            } else {
                print("Successfully indexed \(items.count) items.")
            }
        }
    }

    public func unIndex(_ ids: [String]) {
        guard !ids.isEmpty else { return }
        CSSearchableIndex.default().deleteSearchableItems(withIdentifiers: ids) { error in
            if let error = error {
                print("Indexing error: \(error.localizedDescription)")
            } else {
                print("Successfully unindexed \(ids.count) items.")
            }
        }
    }

    public func rebuild() {
        let fetchRequest = Boardgame.makeFetchRequest()
        let context = persistence.container.viewContext
        if let boardgames  = try? context.fetch(fetchRequest) {
            unIndex(boardgames.map { $0.bggID })
            index(boardgames.map { $0.spotlightSearchableItem })
        }
    }
}
