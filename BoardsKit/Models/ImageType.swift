//
//  ImageType.swift
//  Boards
//
//  Created by Joseph Canale on 10/7/21.
//

import Foundation

public enum ImageType: String, Codable {
    case square30
    case square100
    case mediaCard = "mediacard"
    case unknown
}
