//
//  Currency.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation

// swiftlint: disable type_body_length
public enum Currency: String, Codable, Sendable {
    case ALL
    case AFN
    case ARS
    case AWG
    case AUD
    case AZN
    case BSD
    case BBD
    case BYN
    case BZD
    case BMD
    case BOB
    case BAM
    case BWP
    case BGN
    case BRL
    case BND
    case KHR
    case CAD
    case KYD
    case CLP
    case CNY
    case COP
    case CRC
    case HRK
    case CUP
    case CZK
    case DKK
    case DOP
    case XCD
    case EGP
    case SVC
    case EUR
    case FKP
    case FJD
    case GHS
    case GIP
    case GTQ
    case GGP
    case GYD
    case HNL
    case HKD
    case HUF
    case ISK
    case INR
    case IDR
    case IRR
    case IMP
    case ILS
    case JMD
    case JPY
    case JEP
    case KZT
    case KPW
    case KRW
    case KGS
    case LAK
    case LBP
    case LRD
    case MKD
    case MYR
    case MUR
    case MXN
    case MNT
    case MZN
    case NAD
    case NPR
    case ANG
    case NZD
    case NIO
    case NGN
    case NOK
    case OMR
    case PKR
    case PAB
    case PYG
    case PEN
    case PHP
    case PLN
    case QAR
    case RON
    case RUB
    case SHP
    case SAR
    case RSD
    case SCR
    case SGD
    case SBD
    case SOS
    case ZAR
    case LKR
    case SEK
    case CHF
    case SRD
    case SYP
    case TWD
    case THB
    case TTD
    case TRY
    case TVD
    case UAH
    case GBP
    case USD
    case UYU
    case UZS
    case VEF
    case VND
    case YER
    case ZWD

    public var symbol: String {
        switch self {
            case .ALL: return "Lek"
            case .AFN: return "؋"
            case .ARS: return "$"
            case .AWG: return "ƒ"
            case .AUD: return "A$"
            case .AZN: return "₼"
            case .BSD: return "BS$"
            case .BBD: return "BB$"
            case .BYN: return "Br"
            case .BZD: return "BZ$"
            case .BMD: return "BM$"
            case .BOB: return "$b"
            case .BAM: return "KM"
            case .BWP: return "P"
            case .BGN: return "лв"
            case .BRL: return "R$"
            case .BND: return "BN$"
            case .KHR: return "៛"
            case .CAD: return "CA$"
            case .KYD: return "KY$"
            case .CLP: return "CL$"
            case .CNY: return "¥"
            case .COP: return "CO$"
            case .CRC: return "₡"
            case .HRK: return "kn"
            case .CUP: return "₱"
            case .CZK: return "Kč"
            case .DKK: return "kr"
            case .DOP: return "RD$"
            case .XCD: return "XC$"
            case .EGP: return "£"
            case .SVC: return "SV$"
            case .EUR: return "€"
            case .FKP: return "£"
            case .FJD: return "FJ$"
            case .GHS: return "¢"
            case .GIP: return "£"
            case .GTQ: return "Q"
            case .GGP: return "£"
            case .GYD: return "GY$"
            case .HNL: return "L"
            case .HKD: return "HK$"
            case .HUF: return "Ft"
            case .ISK: return "kr"
            case .INR: return "₹"
            case .IDR: return "Rp"
            case .IRR: return "﷼"
            case .IMP: return "£"
            case .ILS: return "₪"
            case .JMD: return "J$"
            case .JPY: return "¥"
            case .JEP: return "£"
            case .KZT: return "лв"
            case .KPW: return "₩"
            case .KRW: return "₩"
            case .KGS: return "лв"
            case .LAK: return "₭"
            case .LBP: return "£"
            case .LRD: return "LR$"
            case .MKD: return "ден"
            case .MYR: return "RM"
            case .MUR: return "₨"
            case .MXN: return "MX$"
            case .MNT: return "₮"
            case .MZN: return "MT"
            case .NAD: return "NA$"
            case .NPR: return "₨"
            case .ANG: return "ƒ"
            case .NZD: return "NZ$"
            case .NIO: return "C$"
            case .NGN: return "₦"
            case .NOK: return "kr"
            case .OMR: return "﷼"
            case .PKR: return "₨"
            case .PAB: return "B/."
            case .PYG: return "Gs"
            case .PEN: return "S/."
            case .PHP: return "₱"
            case .PLN: return "zł"
            case .QAR: return "﷼"
            case .RON: return "lei"
            case .RUB: return "₽"
            case .SHP: return "£"
            case .SAR: return "﷼"
            case .RSD: return "Дин."
            case .SCR: return "₨"
            case .SGD: return "SG$"
            case .SBD: return "SB$"
            case .SOS: return "S"
            case .ZAR: return "R"
            case .LKR: return "₨"
            case .SEK: return "kr"
            case .CHF: return "CHF"
            case .SRD: return "SR$"
            case .SYP: return "£"
            case .TWD: return "NT$"
            case .THB: return "฿"
            case .TTD: return "TT$"
            case .TRY: return "₺"
            case .TVD: return "TV$"
            case .UAH: return "₴"
            case .GBP: return "£"
            case .USD: return "$"
            case .UYU: return "$U"
            case .UZS: return "лв"
            case .VEF: return "Bs"
            case .VND: return "₫"
            case .YER: return "﷼"
            case .ZWD: return "Z$"
        }
    }

    public var description: String {
        switch self {
            case .ALL: return "Albania Lek"
            case .AFN: return "Afghanistan Afghani"
            case .ARS: return "Argentina Peso"
            case .AWG: return "Aruba Guilder"
            case .AUD: return "Australia Dollar"
            case .AZN: return "Azerbaijan Manat"
            case .BSD: return "Bahamas Dollar"
            case .BBD: return "Barbados Dollar"
            case .BYN: return "Belarus Ruble"
            case .BZD: return "Belize Dollar"
            case .BMD: return "Bermuda Dollar"
            case .BOB: return "Bolivia Bolíviano"
            case .BAM: return "Bosnia and Herzegovina Convertible Mark"
            case .BWP: return "Botswana Pula"
            case .BGN: return "Bulgaria Lev"
            case .BRL: return "Brazil Real"
            case .BND: return "Brunei Darussalam Dollar"
            case .KHR: return "Cambodia Riel"
            case .CAD: return "Canada Dollar"
            case .KYD: return "Cayman Islands Dollar"
            case .CLP: return "Chile Peso"
            case .CNY: return "China Yuan Renminbi"
            case .COP: return "Colombia Peso"
            case .CRC: return "Costa Rica Colon"
            case .HRK: return "Croatia Kuna"
            case .CUP: return "Cuba Peso"
            case .CZK: return "Czech Republic Koruna"
            case .DKK: return "Denmark Krone"
            case .DOP: return "Dominican Republic Peso"
            case .XCD: return "East Caribbean Dollar"
            case .EGP: return "Egypt Pound"
            case .SVC: return "El Salvador Colon"
            case .EUR: return "Euro Member Countries"
            case .FKP: return "Falkland Islands (Malvinas) Pound"
            case .FJD: return "Fiji Dollar"
            case .GHS: return "Ghana Cedi"
            case .GIP: return "Gibraltar Pound"
            case .GTQ: return "Guatemala Quetzal"
            case .GGP: return "Guernsey Pound"
            case .GYD: return "Guyana Dollar"
            case .HNL: return "Honduras Lempira"
            case .HKD: return "Hong Kong Dollar"
            case .HUF: return "Hungary Forint"
            case .ISK: return "Iceland Krona"
            case .INR: return "India Rupee"
            case .IDR: return "Indonesia Rupiah"
            case .IRR: return "Iran Rial"
            case .IMP: return "Isle of Man Pound"
            case .ILS: return "Israel Shekel"
            case .JMD: return "Jamaica Dollar"
            case .JPY: return "Japan Yen"
            case .JEP: return "Jersey Pound"
            case .KZT: return "Kazakhstan Tenge"
            case .KPW: return "Korea (North) Won"
            case .KRW: return "Korea (South) Won"
            case .KGS: return "Kyrgyzstan Som"
            case .LAK: return "Laos Kip"
            case .LBP: return "Lebanon Pound"
            case .LRD: return "Liberia Dollar"
            case .MKD: return "Macedonia Denar"
            case .MYR: return "Malaysia Ringgit"
            case .MUR: return "Mauritius Rupee"
            case .MXN: return "Mexico Peso"
            case .MNT: return "Mongolia Tughrik"
            case .MZN: return "Mozambique Metical"
            case .NAD: return "Namibia Dollar"
            case .NPR: return "Nepal Rupee"
            case .ANG: return "Netherlands Antilles Guilder"
            case .NZD: return "New Zealand Dollar"
            case .NIO: return "Nicaragua Cordoba"
            case .NGN: return "Nigeria Naira"
            case .NOK: return "Norway Krone"
            case .OMR: return "Oman Rial"
            case .PKR: return "Pakistan Rupee"
            case .PAB: return "Panama Balboa"
            case .PYG: return "Paraguay Guarani"
            case .PEN: return "Peru Sol"
            case .PHP: return "Philippines Peso"
            case .PLN: return "Poland Zloty"
            case .QAR: return "Qatar Riyal"
            case .RON: return "Romania Leu"
            case .RUB: return "Russia Ruble"
            case .SHP: return "Saint Helena Pound"
            case .SAR: return "Saudi Arabia Riyal"
            case .RSD: return "Serbia Dinar"
            case .SCR: return "Seychelles Rupee"
            case .SGD: return "Singapore Dollar"
            case .SBD: return "Solomon Islands Dollar"
            case .SOS: return "Somalia Shilling"
            case .ZAR: return "South Africa Rand"
            case .LKR: return "Sri Lanka Rupee"
            case .SEK: return "Sweden Krona"
            case .CHF: return "Switzerland Franc"
            case .SRD: return "Suriname Dollar"
            case .SYP: return "Syria Pound"
            case .TWD: return "Taiwan New Dollar"
            case .THB: return "Thailand Baht"
            case .TTD: return "Trinidad and Tobago Dollar"
            case .TRY: return "Turkey Lira"
            case .TVD: return "Tuvalu Dollar"
            case .UAH: return "Ukraine Hryvnia"
            case .GBP: return "United Kingdom Pound"
            case .USD: return "United States Dollar"
            case .UYU: return "Uruguay Peso"
            case .UZS: return "Uzbekistan Som"
            case .VEF: return "Venezuela Bolívar"
            case .VND: return "Viet Nam Dong"
            case .YER: return "Yemen Rial"
            case .ZWD: return "Zimbabwe Dollar"
        }
    }
}
