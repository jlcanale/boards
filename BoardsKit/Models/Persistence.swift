//
//  Persistence.swift
//  Shared
//
//  Created by Joseph Canale on 9/21/21.
//

import CoreData
import CoreSpotlight

public struct PersistenceController {
    public static let shared = PersistenceController()

    public let container: NSPersistentCloudKitContainer
    private let managedModelRootName = "Boards"
    private let managedModelExtension = "momd"

    public static var preview: PersistenceController = {
        let controller = PersistenceController(inMemory: true)
        let context = controller.container.viewContext
        for i in 1...10 {
            let boardgame = Boardgame(from: .sampleBoardgame, in: context)
        }
        try? context.save()
        return controller
    }()

    public func save() {
        do {
            try container.viewContext.save()

            var searchableItems = [CSSearchableItem]()
            for item in container.viewContext.insertedObjects {
                if let item = item as? Boardgame {
                    searchableItems.append(item.spotlightSearchableItem)
                }
            }
            SpotlightController.shared.index(searchableItems)

            var deletedIDs = [String]()
            for item in container.viewContext.deletedObjects {
                if let item = item as? Boardgame {
                    deletedIDs.append(item.bggID)
                }
            }
            SpotlightController.shared.unIndex(deletedIDs)

        } catch {
            let nsError = error as NSError
            print(nsError.userInfo)
        }
    }

    public init(inMemory: Bool = false) {
        let bundle = Bundle.main
        let modelURL = bundle.url(forResource: managedModelRootName, withExtension: managedModelExtension)!

        container = NSPersistentCloudKitContainer(name: "Boards", managedObjectModel: NSManagedObjectModel(contentsOf: modelURL)!)

        let storeURL = URL.storeURL(for: "group.canalecoding.boards.data", databaseName: "Boards")
        let storeDescription = NSPersistentStoreDescription(url: storeURL)
        storeDescription.setOption(true as NSNumber, forKey: NSPersistentHistoryTrackingKey)
        storeDescription.setOption(true as NSNumber, forKey: NSPersistentStoreRemoteChangeNotificationPostOptionKey)

        storeDescription.cloudKitContainerOptions = NSPersistentCloudKitContainerOptions(containerIdentifier: "iCloud.CanaleCoding.Boards")
        container.persistentStoreDescriptions = [storeDescription]

        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                Typical reasons for an error here include:
                * The parent directory does not exist, cannot be created, or disallows writing.
                * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                * The device is out of space.
                * The store could not be migrated to the current model version.
                Check the error message to determine what the actual problem was.
                */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
}

public extension URL {

    /// Returns a URL for the given app group and database pointing to the sqlite database.
    static func storeURL(for appGroup: String, databaseName: String) -> URL {
        guard let fileContainer = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: appGroup) else {
            fatalError("Shared file container could not be created.")
        }

        return fileContainer.appendingPathComponent("\(databaseName).sqlite")
    }
}
