//
//  Units.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import Foundation

public struct Units {

  public let bytes: Int64

  public var kilobytes: Double {
    return Double(bytes) / 1000
  }

  public var megabytes: Double {
    return kilobytes / 1000
  }

  public var gigabytes: Double {
    return megabytes / 1000
  }

  public init(bytes: Int64) {
    self.bytes = bytes
  }

  public func getReadableUnit() -> String {

    switch bytes {
    case 0..<1000:
      return "\(bytes) bytes"
    case 1000..<(1000 * 1000):
      return "\(String(format: "%.0f", kilobytes)) KB"
    case 1000..<(1000 * 1000 * 1000):
      return "\(String(format: "%.0f", megabytes)) MB"
    case (1000 * 1000 * 1000)...Int64.max:
      return "\(String(format: "%.0f", gigabytes)) GB"
    default:
      return "\(bytes) bytes"
    }
  }
}
