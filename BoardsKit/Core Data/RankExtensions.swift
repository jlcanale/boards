//
//  RankExtensions.swift
//  Boards
//
//  Created by Joseph Canale on 10/8/21.
//

import Foundation
import CoreData

extension Rank {
    convenience init(from geekRank: GeekRank, in context: NSManagedObjectContext) {
        self.init(context: context)
        self.bayesAverage = geekRank.bayesAverage
        self.prettyName = geekRank.prettyName
        self.rank = geekRank.rank
        self.shortPrettyName = geekRank.shortPrettyName
        self.subdomain = geekRank.subdomain
        self.veryShortPrettyName = geekRank.veryShortPrettyName
    }
}
