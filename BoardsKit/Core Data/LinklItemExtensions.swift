//
//  LinkItemExtensions.swift
//  Boards
//
//  Created by Joseph Canale on 10/8/21.
//

import Foundation
import CoreData

extension LinkItem: Metadata {}

extension LinkItem {

    static func from(geekMetadataItem: GeekItemMetadata, in context: NSManagedObjectContext) -> LinkItem {
        let item = context.fetchBy(bggID: geekMetadataItem.objectID, geekItemType: geekMetadataItem.objectType) ?? LinkItem(context: context)
        item.updateValues(from: geekMetadataItem)
        return item
    }

    static func from(geekMetadataItems: [GeekItemMetadata], in context: NSManagedObjectContext) -> Set<LinkItem> {
        var newItems = Set<LinkItem>()
        for metadataItem in geekMetadataItems {
            let newItem = LinkItem.from(geekMetadataItem: metadataItem, in: context)
            newItems.insert(newItem)
        }
        return newItems
    }

    func updateValues(from geekMetadataItem: GeekItemMetadata) {
        self.name = geekMetadataItem.name
        self.bggID = geekMetadataItem.objectID
        self.urlReference = geekMetadataItem.urlReference ?? ""
        self.geekItemType = geekMetadataItem.objectType
    }
}

extension Set where Element == LinkItem {
    var names: [String] {
        return self.map { $0.name }
    }
}
