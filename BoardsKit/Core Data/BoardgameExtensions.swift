//
//  BoardgameExtensions.swift
//  Boards
//
//  Created by Joseph Canale on 10/7/21.
//

import Foundation
import CoreData

extension Rank: RankRepresentable {}

extension Boardgame: Identifiable {}

// swiftlint: disable cyclomatic_complexity
// MARK: - Initializers
extension Boardgame {
    public var geekItemType: GeekItemType { .thing }

    public static func from(geekItem: GeekItem, in context: NSManagedObjectContext) -> Boardgame {
        return context.fetchBy(bggID: "\(geekItem.objectID)") ?? Boardgame(from: geekItem, in: context)
    }

    public convenience init(from geekItem: GeekItem, in context: NSManagedObjectContext) {
        self.init(context: context)
        self.update(geekItem: geekItem)
    }

    // swiftlint: disable function_body_length
    private func createMetadataItems(geekMetadataItems: [GeekItemMetadata], for link: GeekItemLink) {
        guard let context = self.managedObjectContext else { return }
        let newSet = LinkItem.from(geekMetadataItems: geekMetadataItems, in: context)
        switch link {
            case .designer:
                self.designers = []
                self.addToDesigners(newSet)
            case .soloDesigner:
                self.soloDesigners = []
                self.addToSoloDesigners(newSet)
            case .artist:
                self.soloDesigners = []
                self.addToArtists(newSet)
            case .developer:
                self.developers = []
                self.addToDevelopers(newSet)
            case .graphicDesigner:
                self.graphicDesigners = []
                self.addToGraphicDesigners(newSet)
            case .sculptor:
                self.sculptors = []
                self.addToSculptors(newSet)
            case .editor:
                self.editors = []
                self.addToEditors(newSet)
            case .writer:
                self.writers = []
                self.addToWriters(newSet)
            case .insertDesigner:
                self.soloDesigners = []
                self.addToInsertDesigners(newSet)
            case .publisher:
                self.publishers = []
                self.addToPublishers(newSet)
            case .honor:
                self.honors = []
                self.addToHonors(newSet)
            case .category:
                self.categories = []
                self.addToCategories(newSet)
            case .mechanic:
                self.mechanics = []
                self.addToMechanics(newSet)
            case .expansion:
                self.expansions = []
                self.addToExpansions(newSet)
            case .version:
                self.versions = []
                self.addToVersions(newSet)
            case .expands:
                self.expands = []
                self.addToExpands(newSet)
            case .integratesWith:
                self.integratesWith = []
                self.addToIntegratesWith(newSet)
            case .contains:
                self.contains = []
                self.addToContains(newSet)
            case .containedIn:
                self.containedIn = []
                self.addToContainedIn(newSet)
            case .reimplementedBy:
                self.reimplementedBy = []
                self.addToReimplementedBy(newSet)
            case .reimplements:
                self.reimplements = []
                self.addToReimplements(newSet)
            case .family:
                self.families = []
                self.addToFamilies(newSet)
            case .videoGameBG:
                self.videoGames = []
                self.addToVideoGames(newSet)
            case .subdomain:
                self.subdomains = []
                self.addToSubdomains(newSet)
            case .accessory:
                self.accessories = []
                self.addToAccessories(newSet)
            default:
                break
        }
    }

    public func update(geekItem: GeekItem) {
        guard let context = self.managedObjectContext else { return }

        self.name = geekItem.name
        self.bggID = String(geekItem.objectID)
        self.yearPublished = geekItem.yearPublished
        self.minPlayers = Int32(geekItem.minPlayers?.int)
        self.maxPlayers = Int32(geekItem.maxPlayers?.int)
        self.minPlaytime = Int32(geekItem.minPlaytime?.int)
        self.maxPlaytime = Int32(geekItem.maxPlaytime?.int)
        self.abbreviatedDescription = geekItem.shortDescription
        self.fullDescription = geekItem.fullDescription

        self.minimumAge = geekItem.minimumAge
        self.imageURL = geekItem.imageURL
        self.ranks = []

        if let dynamicInfo = geekItem.dynamicInfo {
            self.update(dynamicInfo: dynamicInfo)
        }

        for (key, value) in geekItem.links.decoded {
            createMetadataItems(geekMetadataItems: value, for: key)
        }

        self.imageSets = []
        for (key, value) in geekItem.imageSets {
            if let imageType = ImageType(rawValue: key) {
                let imageSet = ImageSet(context: context)
                imageSet.imageType = imageType
                imageSet.source = value.sourceURL
                imageSet.source2x = value.source2xURL
                self.addToImageSets(imageSet)
            }
        }
    }

    public func update(dynamicInfo: DynamicInfo) {
        self.averageWeight = dynamicInfo.stats.averageWeight
        self.update(ranks: dynamicInfo.ranks ?? [])
        self.update(stats: dynamicInfo.stats)
    }

    public func update(stats: Stats) {
        self.totalRatings = stats.totalRatings
        self.averageRating = NSDecimalNumber(string: stats.averageRating, locale: Locale.current)
        self.bayesAverageRating = NSDecimalNumber(string: stats.bayesAverageRating, locale: Locale.current)
        self.standardDeviation = stats.standardDeviation
        self.averageWeight = stats.averageWeight
        self.totalWeights = stats.totalWeights
        self.totalGeekLists = stats.totalGeekLists
        self.totalForTrade = stats.totalForTrade
        self.totalWantInTrade = stats.totalWantInTrade
        self.totalWishlist = stats.totalWishlist
        self.totalOwned = stats.totalOwned
        self.totalPreviouslyOwned = stats.totalPreviouslyOwned
        self.totalComments = stats.totalComments
        self.totalWishlistComments = stats.totalWishlistComments
        self.totalHasParts = stats.totalHasParts
        self.totalWantsParts = stats.totalWantsParts
        self.totalViews = stats.totalViews
        self.totalPlays = stats.totalPlays
        self.totalPlaysThisMonth = stats.totalPlaysThisMonth
        self.totalFans = Int32(stats.totalFans)
    }

    public func update(ranks: [GeekRank]) {
        guard let context = self.managedObjectContext else { return }
        if let currentRanks = self.ranks {
            self.removeFromRanks(currentRanks)
        }
        for rank in ranks {
            let newRank = Rank(from: rank, in: context)
            self.addToRanks(newRank)
        }
    }

    public static var sample: Boardgame {
        let context = PersistenceController.preview.container.viewContext
        let sample = Boardgame(from: .sampleBoardgame, in: context)
        // try! context.save()
        return sample
    }
}

extension Boardgame {
    public func update(status: GeekCollectionItemStatus) {
        self.owned = status.own.boolValue
        self.previouslyOwned = status.previouslyOwned.boolValue
        self.forTrade = status.forTrade.boolValue
        self.wantToPlay = status.wantToPlay.boolValue
        self.wantInTrade = status.wantInTrade.boolValue
        self.wantToBuy = status.wantToBuy.boolValue
        self.preordered = status.preOrdered.boolValue
        self.onWishlist = status.onWishlist.boolValue
    }
}

// MARK: - UI Helpers
extension Boardgame {

    public var playerCountText: String {
        if let minPlayers = minPlayers, let maxPlayers = maxPlayers, maxPlayers != minPlayers {
            return "\(minPlayers)-\(maxPlayers)"
        } else if let minPlayers = minPlayers {
            return "\(minPlayers)"
        } else if let maxPlayers = maxPlayers {
            return "\(maxPlayers)"
        } else {
            return ""
        }
    }

    public var playTimeText: String {
        if let minPlaytime = minPlaytime, let maxPlaytime = maxPlaytime, maxPlaytime != minPlaytime {
            return "\(minPlaytime)-\(maxPlaytime)"
        } else if let minPlaytime = minPlaytime {
            return "\(minPlaytime)"
        } else if let maxPlayers = maxPlaytime {
            return "\(maxPlayers)"
        } else {
            return ""
        }
    }

    public var overallRatingText: String {
        if let rank = ranks?.first(where: { $0.veryShortPrettyName == "Overall" }) {
            return String(rank.bayesAverage.prefix(4))
        } else {
            return "N/A"
        }
    }

    public var averageRatingText: String {
        String(self.averageRating.stringValue.prefix(4))
    }

    public var myRatingText: String {
        guard myRating > 0 else { return "N/A" }
        return "\(myRating) / 10"
    }

    public var minimumAgeText: String {
        "\(minimumAge ?? "12")+"
    }

    public var weightText: String {
        String(averageWeight.prefix(4))
    }

    // swiftlint: disable function_body_length
    public func metadataFor(geekItemLink: GeekItemLink) -> [Metadata] {
        switch geekItemLink {
            case .designer:
                return Array(designers ?? [])
            case .soloDesigner:
                return Array(soloDesigners ?? [])
            case .artist:
                return Array(artists ?? [])
            case .developer:
                return Array(developers ?? [])
            case .graphicDesigner:
                return Array(graphicDesigners ?? [])
            case .sculptor:
                return Array(sculptors ?? [])
            case .editor:
                return Array(editors ?? [])
            case .writer:
                return Array(writers ?? [])
            case .insertDesigner:
                return Array(insertDesigners ?? [])
            case .publisher:
                return Array(publishers ?? [])
            case .honor:
                return Array(honors ?? [])
            case .category:
                return Array(categories ?? [])
            case .mechanic:
                return Array(mechanics ?? [])
            case .expansion:
                return Array(expansions ?? [])
            case .version:
                return Array(versions ?? [])
            case .expands:
                return Array(expands ?? [])
            case .integratesWith:
                return Array(integratesWith ?? [])
            case .contains:
                return Array(contains ?? [])
            case .containedIn:
                return Array(containedIn ?? [])
            case .reimplementedBy:
                return Array(reimplementedBy ?? [])
            case .reimplements:
                return Array(reimplements ?? [])
            case .family:
                return Array(families ?? [])
            case .videoGameBG:
                return Array(videoGames ?? [])
            case .subdomain:
                return Array(subdomains ?? [])
            case .accessory:
                return Array(accessories ?? [])
            default:
                return []
        }
    }

    public func itemsFor(geekItemLink: GeekItemLink) -> [String] {
        return metadataFor(geekItemLink: geekItemLink).map { $0.name }
    }

    public func imageURLFor(imageType: ImageType) -> URL? {
        self.imageSets?.first { $0.imageType == imageType }?.source2x
    }

    public var yearPublishedText: String {
        if let yearPublished = yearPublished {
            return "(\(yearPublished))"
        } else {
            return ""
        }
    }
}

// MARK: - Spotlight Search

import CoreSpotlight
import MobileCoreServices

extension Boardgame {
    public var spotlightSearchableItem: CSSearchableItem {
        let attributeSet = CSSearchableItemAttributeSet(contentType: UTType.text)
        attributeSet.title = self.name
        attributeSet.contentDescription = self.abbreviatedDescription
        attributeSet.thumbnailData = self.imageData
        return CSSearchableItem(uniqueIdentifier: self.bggID, domainIdentifier: "com.CanaleCoding", attributeSet: attributeSet)
    }

    public func index() {
        SpotlightController.shared.index([spotlightSearchableItem])
    }

    public func deindex() {
        SpotlightController.shared.unIndex([self.bggID])
    }
}

#if canImport(UIKit)
import UIKit

extension Boardgame {

    public var image: UIImage? {
        guard let imageData = imageData else { return nil }
        return UIImage(data: imageData)
    }

    public func imageFor(imageType: ImageType) -> UIImage? {
        if let imageSet = self.imageSets?.first(where: { $0.imageType == imageType }) {
            if let imageData = imageSet.source2xData {
                return UIImage(data: imageData)
            } else if let imageURL = imageSet.source2x {
                Task {
                    let data = try? await URLSession.shared.data(from: imageURL)
                    if let imageData = data?.0 {
                        imageSet.source2xData = imageData
                        try? managedObjectContext?.save()
                    }
                }
            }
        }
        return nil
    }
}
#endif
