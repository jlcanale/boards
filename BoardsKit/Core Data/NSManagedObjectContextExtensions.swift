//
//  NSManagedObjectContextExtensions.swift
//  Boards
//
//  Created by Joseph Canale on 10/8/21.
//

import CoreData

extension NSManagedObjectContext {
    public func fetchBy<T: NSManagedObject>(_ type: T.Type = T.self, bggID: String, geekItemType: GeekItemType? = nil) -> T? {
        let fetchRequest: NSFetchRequest<T> = NSFetchRequest(entityName: T.entity().name ?? "LinkItem")
        var predicate = NSPredicate(format: "bggID == %@", bggID)
        if let geekItemType = geekItemType {
            predicate = predicate.and(NSPredicate(format: "geekItemType == %@", geekItemType.rawValue))
        }
        fetchRequest.predicate = predicate

        do {
            return try self.fetch(fetchRequest).first
        } catch {
            return nil
        }
    }
}
