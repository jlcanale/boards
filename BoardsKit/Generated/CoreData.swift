// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable superfluous_disable_command implicit_return
// swiftlint:disable sorted_imports
import CoreData
import Foundation

// swiftlint:disable attributes file_length vertical_whitespace_closing_braces
// swiftlint:disable identifier_name line_length type_body_length

// MARK: - Boardgame

@objc(Boardgame)
public class Boardgame: NSManagedObject {
  public class var entityName: String {
    return "Boardgame"
  }

  public class func entity(in managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
    return NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)
  }

  @available(*, deprecated, renamed: "makeFetchRequest", message: "To avoid collisions with the less concrete method in `NSManagedObject`, please use `makeFetchRequest()` instead.")
  @nonobjc public class func fetchRequest() -> NSFetchRequest<Boardgame> {
    return NSFetchRequest<Boardgame>(entityName: entityName)
  }

  @nonobjc public class func makeFetchRequest() -> NSFetchRequest<Boardgame> {
    return NSFetchRequest<Boardgame>(entityName: entityName)
  }

  // swiftlint:disable discouraged_optional_boolean discouraged_optional_collection
  @NSManaged public var abbreviatedDescription: String
  @NSManaged public var averageRating: NSDecimalNumber
  @NSManaged public var averageWeight: String
  @NSManaged public var bayesAverageRating: NSDecimalNumber
  @NSManaged public var bggID: String
  @NSManaged public var forTrade: Bool
  @NSManaged public var fullDescription: String
  @NSManaged public var imageData: Data?
  @NSManaged public var imageURL: URL?
  public var maxPlayers: Int32? {
    get {
      let key = "maxPlayers"
      willAccessValue(forKey: key)
      defer { didAccessValue(forKey: key) }

      return primitiveValue(forKey: key) as? Int32
    }
    set {
      let key = "maxPlayers"
      willChangeValue(forKey: key)
      defer { didChangeValue(forKey: key) }

      setPrimitiveValue(newValue, forKey: key)
    }
  }
  public var maxPlaytime: Int32? {
    get {
      let key = "maxPlaytime"
      willAccessValue(forKey: key)
      defer { didAccessValue(forKey: key) }

      return primitiveValue(forKey: key) as? Int32
    }
    set {
      let key = "maxPlaytime"
      willChangeValue(forKey: key)
      defer { didChangeValue(forKey: key) }

      setPrimitiveValue(newValue, forKey: key)
    }
  }
  @NSManaged public var minimumAge: String?
  public var minPlayers: Int32? {
    get {
      let key = "minPlayers"
      willAccessValue(forKey: key)
      defer { didAccessValue(forKey: key) }

      return primitiveValue(forKey: key) as? Int32
    }
    set {
      let key = "minPlayers"
      willChangeValue(forKey: key)
      defer { didChangeValue(forKey: key) }

      setPrimitiveValue(newValue, forKey: key)
    }
  }
  public var minPlaytime: Int32? {
    get {
      let key = "minPlaytime"
      willAccessValue(forKey: key)
      defer { didAccessValue(forKey: key) }

      return primitiveValue(forKey: key) as? Int32
    }
    set {
      let key = "minPlaytime"
      willChangeValue(forKey: key)
      defer { didChangeValue(forKey: key) }

      setPrimitiveValue(newValue, forKey: key)
    }
  }
  @NSManaged public var myRating: Int32
  @NSManaged public var name: String
  @NSManaged public var onWishlist: Bool
  @NSManaged public var owned: Bool
  @NSManaged public var preordered: Bool
  @NSManaged public var previouslyOwned: Bool
  @NSManaged public var standardDeviation: String
  @NSManaged public var totalComments: String
  @NSManaged public var totalFans: Int32
  @NSManaged public var totalForTrade: String
  @NSManaged public var totalGeekLists: String
  @NSManaged public var totalHasParts: String
  @NSManaged public var totalOwned: String
  @NSManaged public var totalPlays: String
  @NSManaged public var totalPlaysThisMonth: String
  @NSManaged public var totalPreviouslyOwned: String
  @NSManaged public var totalRatings: String
  @NSManaged public var totalViews: String
  @NSManaged public var totalWantInTrade: String
  @NSManaged public var totalWantsParts: String
  @NSManaged public var totalWeights: String
  @NSManaged public var totalWishlist: String
  @NSManaged public var totalWishlistComments: String
  @NSManaged public var wantInTrade: Bool
  @NSManaged public var wantToBuy: Bool
  @NSManaged public var wantToPlay: Bool
  @NSManaged public var yearPublished: String?
  @NSManaged public var accessories: Set<LinkItem>?
  @NSManaged public var artists: Set<LinkItem>?
  @NSManaged public var categories: Set<LinkItem>?
  @NSManaged public var containedIn: Set<LinkItem>?
  @NSManaged public var contains: Set<LinkItem>?
  @NSManaged public var designers: Set<LinkItem>?
  @NSManaged public var developers: Set<LinkItem>?
  @NSManaged public var editors: Set<LinkItem>?
  @NSManaged public var expands: Set<LinkItem>?
  @NSManaged public var expansions: Set<LinkItem>?
  @NSManaged public var families: Set<LinkItem>?
  @NSManaged public var graphicDesigners: Set<LinkItem>?
  @NSManaged public var honors: Set<LinkItem>?
  @NSManaged public var imageSets: Set<ImageSet>?
  @NSManaged public var insertDesigners: Set<LinkItem>?
  @NSManaged public var integratesWith: Set<LinkItem>?
  @NSManaged public var mechanics: Set<LinkItem>?
  @NSManaged public var publishers: Set<LinkItem>?
  @NSManaged public var ranks: Set<Rank>?
  @NSManaged public var reimplementedBy: Set<LinkItem>?
  @NSManaged public var reimplements: Set<LinkItem>?
  @NSManaged public var sculptors: Set<LinkItem>?
  @NSManaged public var soloDesigners: Set<LinkItem>?
  @NSManaged public var subdomains: Set<LinkItem>?
  @NSManaged public var versions: Set<LinkItem>?
  @NSManaged public var videoGames: Set<LinkItem>?
  @NSManaged public var writers: Set<LinkItem>?
  // swiftlint:enable discouraged_optional_boolean discouraged_optional_collection
}

// MARK: Relationship Accessories

extension Boardgame {
  @objc(addAccessoriesObject:)
  @NSManaged public func addToAccessories(_ value: LinkItem)

  @objc(removeAccessoriesObject:)
  @NSManaged public func removeFromAccessories(_ value: LinkItem)

  @objc(addAccessories:)
  @NSManaged public func addToAccessories(_ values: Set<LinkItem>)

  @objc(removeAccessories:)
  @NSManaged public func removeFromAccessories(_ values: Set<LinkItem>)
}

// MARK: Relationship Artists

extension Boardgame {
  @objc(addArtistsObject:)
  @NSManaged public func addToArtists(_ value: LinkItem)

  @objc(removeArtistsObject:)
  @NSManaged public func removeFromArtists(_ value: LinkItem)

  @objc(addArtists:)
  @NSManaged public func addToArtists(_ values: Set<LinkItem>)

  @objc(removeArtists:)
  @NSManaged public func removeFromArtists(_ values: Set<LinkItem>)
}

// MARK: Relationship Categories

extension Boardgame {
  @objc(addCategoriesObject:)
  @NSManaged public func addToCategories(_ value: LinkItem)

  @objc(removeCategoriesObject:)
  @NSManaged public func removeFromCategories(_ value: LinkItem)

  @objc(addCategories:)
  @NSManaged public func addToCategories(_ values: Set<LinkItem>)

  @objc(removeCategories:)
  @NSManaged public func removeFromCategories(_ values: Set<LinkItem>)
}

// MARK: Relationship ContainedIn

extension Boardgame {
  @objc(addContainedInObject:)
  @NSManaged public func addToContainedIn(_ value: LinkItem)

  @objc(removeContainedInObject:)
  @NSManaged public func removeFromContainedIn(_ value: LinkItem)

  @objc(addContainedIn:)
  @NSManaged public func addToContainedIn(_ values: Set<LinkItem>)

  @objc(removeContainedIn:)
  @NSManaged public func removeFromContainedIn(_ values: Set<LinkItem>)
}

// MARK: Relationship Contains

extension Boardgame {
  @objc(addContainsObject:)
  @NSManaged public func addToContains(_ value: LinkItem)

  @objc(removeContainsObject:)
  @NSManaged public func removeFromContains(_ value: LinkItem)

  @objc(addContains:)
  @NSManaged public func addToContains(_ values: Set<LinkItem>)

  @objc(removeContains:)
  @NSManaged public func removeFromContains(_ values: Set<LinkItem>)
}

// MARK: Relationship Designers

extension Boardgame {
  @objc(addDesignersObject:)
  @NSManaged public func addToDesigners(_ value: LinkItem)

  @objc(removeDesignersObject:)
  @NSManaged public func removeFromDesigners(_ value: LinkItem)

  @objc(addDesigners:)
  @NSManaged public func addToDesigners(_ values: Set<LinkItem>)

  @objc(removeDesigners:)
  @NSManaged public func removeFromDesigners(_ values: Set<LinkItem>)
}

// MARK: Relationship Developers

extension Boardgame {
  @objc(addDevelopersObject:)
  @NSManaged public func addToDevelopers(_ value: LinkItem)

  @objc(removeDevelopersObject:)
  @NSManaged public func removeFromDevelopers(_ value: LinkItem)

  @objc(addDevelopers:)
  @NSManaged public func addToDevelopers(_ values: Set<LinkItem>)

  @objc(removeDevelopers:)
  @NSManaged public func removeFromDevelopers(_ values: Set<LinkItem>)
}

// MARK: Relationship Editors

extension Boardgame {
  @objc(addEditorsObject:)
  @NSManaged public func addToEditors(_ value: LinkItem)

  @objc(removeEditorsObject:)
  @NSManaged public func removeFromEditors(_ value: LinkItem)

  @objc(addEditors:)
  @NSManaged public func addToEditors(_ values: Set<LinkItem>)

  @objc(removeEditors:)
  @NSManaged public func removeFromEditors(_ values: Set<LinkItem>)
}

// MARK: Relationship Expands

extension Boardgame {
  @objc(addExpandsObject:)
  @NSManaged public func addToExpands(_ value: LinkItem)

  @objc(removeExpandsObject:)
  @NSManaged public func removeFromExpands(_ value: LinkItem)

  @objc(addExpands:)
  @NSManaged public func addToExpands(_ values: Set<LinkItem>)

  @objc(removeExpands:)
  @NSManaged public func removeFromExpands(_ values: Set<LinkItem>)
}

// MARK: Relationship Expansions

extension Boardgame {
  @objc(addExpansionsObject:)
  @NSManaged public func addToExpansions(_ value: LinkItem)

  @objc(removeExpansionsObject:)
  @NSManaged public func removeFromExpansions(_ value: LinkItem)

  @objc(addExpansions:)
  @NSManaged public func addToExpansions(_ values: Set<LinkItem>)

  @objc(removeExpansions:)
  @NSManaged public func removeFromExpansions(_ values: Set<LinkItem>)
}

// MARK: Relationship Families

extension Boardgame {
  @objc(addFamiliesObject:)
  @NSManaged public func addToFamilies(_ value: LinkItem)

  @objc(removeFamiliesObject:)
  @NSManaged public func removeFromFamilies(_ value: LinkItem)

  @objc(addFamilies:)
  @NSManaged public func addToFamilies(_ values: Set<LinkItem>)

  @objc(removeFamilies:)
  @NSManaged public func removeFromFamilies(_ values: Set<LinkItem>)
}

// MARK: Relationship GraphicDesigners

extension Boardgame {
  @objc(addGraphicDesignersObject:)
  @NSManaged public func addToGraphicDesigners(_ value: LinkItem)

  @objc(removeGraphicDesignersObject:)
  @NSManaged public func removeFromGraphicDesigners(_ value: LinkItem)

  @objc(addGraphicDesigners:)
  @NSManaged public func addToGraphicDesigners(_ values: Set<LinkItem>)

  @objc(removeGraphicDesigners:)
  @NSManaged public func removeFromGraphicDesigners(_ values: Set<LinkItem>)
}

// MARK: Relationship Honors

extension Boardgame {
  @objc(addHonorsObject:)
  @NSManaged public func addToHonors(_ value: LinkItem)

  @objc(removeHonorsObject:)
  @NSManaged public func removeFromHonors(_ value: LinkItem)

  @objc(addHonors:)
  @NSManaged public func addToHonors(_ values: Set<LinkItem>)

  @objc(removeHonors:)
  @NSManaged public func removeFromHonors(_ values: Set<LinkItem>)
}

// MARK: Relationship ImageSets

extension Boardgame {
  @objc(addImageSetsObject:)
  @NSManaged public func addToImageSets(_ value: ImageSet)

  @objc(removeImageSetsObject:)
  @NSManaged public func removeFromImageSets(_ value: ImageSet)

  @objc(addImageSets:)
  @NSManaged public func addToImageSets(_ values: Set<ImageSet>)

  @objc(removeImageSets:)
  @NSManaged public func removeFromImageSets(_ values: Set<ImageSet>)
}

// MARK: Relationship InsertDesigners

extension Boardgame {
  @objc(addInsertDesignersObject:)
  @NSManaged public func addToInsertDesigners(_ value: LinkItem)

  @objc(removeInsertDesignersObject:)
  @NSManaged public func removeFromInsertDesigners(_ value: LinkItem)

  @objc(addInsertDesigners:)
  @NSManaged public func addToInsertDesigners(_ values: Set<LinkItem>)

  @objc(removeInsertDesigners:)
  @NSManaged public func removeFromInsertDesigners(_ values: Set<LinkItem>)
}

// MARK: Relationship IntegratesWith

extension Boardgame {
  @objc(addIntegratesWithObject:)
  @NSManaged public func addToIntegratesWith(_ value: LinkItem)

  @objc(removeIntegratesWithObject:)
  @NSManaged public func removeFromIntegratesWith(_ value: LinkItem)

  @objc(addIntegratesWith:)
  @NSManaged public func addToIntegratesWith(_ values: Set<LinkItem>)

  @objc(removeIntegratesWith:)
  @NSManaged public func removeFromIntegratesWith(_ values: Set<LinkItem>)
}

// MARK: Relationship Mechanics

extension Boardgame {
  @objc(addMechanicsObject:)
  @NSManaged public func addToMechanics(_ value: LinkItem)

  @objc(removeMechanicsObject:)
  @NSManaged public func removeFromMechanics(_ value: LinkItem)

  @objc(addMechanics:)
  @NSManaged public func addToMechanics(_ values: Set<LinkItem>)

  @objc(removeMechanics:)
  @NSManaged public func removeFromMechanics(_ values: Set<LinkItem>)
}

// MARK: Relationship Publishers

extension Boardgame {
  @objc(addPublishersObject:)
  @NSManaged public func addToPublishers(_ value: LinkItem)

  @objc(removePublishersObject:)
  @NSManaged public func removeFromPublishers(_ value: LinkItem)

  @objc(addPublishers:)
  @NSManaged public func addToPublishers(_ values: Set<LinkItem>)

  @objc(removePublishers:)
  @NSManaged public func removeFromPublishers(_ values: Set<LinkItem>)
}

// MARK: Relationship Ranks

extension Boardgame {
  @objc(addRanksObject:)
  @NSManaged public func addToRanks(_ value: Rank)

  @objc(removeRanksObject:)
  @NSManaged public func removeFromRanks(_ value: Rank)

  @objc(addRanks:)
  @NSManaged public func addToRanks(_ values: Set<Rank>)

  @objc(removeRanks:)
  @NSManaged public func removeFromRanks(_ values: Set<Rank>)
}

// MARK: Relationship ReimplementedBy

extension Boardgame {
  @objc(addReimplementedByObject:)
  @NSManaged public func addToReimplementedBy(_ value: LinkItem)

  @objc(removeReimplementedByObject:)
  @NSManaged public func removeFromReimplementedBy(_ value: LinkItem)

  @objc(addReimplementedBy:)
  @NSManaged public func addToReimplementedBy(_ values: Set<LinkItem>)

  @objc(removeReimplementedBy:)
  @NSManaged public func removeFromReimplementedBy(_ values: Set<LinkItem>)
}

// MARK: Relationship Reimplements

extension Boardgame {
  @objc(addReimplementsObject:)
  @NSManaged public func addToReimplements(_ value: LinkItem)

  @objc(removeReimplementsObject:)
  @NSManaged public func removeFromReimplements(_ value: LinkItem)

  @objc(addReimplements:)
  @NSManaged public func addToReimplements(_ values: Set<LinkItem>)

  @objc(removeReimplements:)
  @NSManaged public func removeFromReimplements(_ values: Set<LinkItem>)
}

// MARK: Relationship Sculptors

extension Boardgame {
  @objc(addSculptorsObject:)
  @NSManaged public func addToSculptors(_ value: LinkItem)

  @objc(removeSculptorsObject:)
  @NSManaged public func removeFromSculptors(_ value: LinkItem)

  @objc(addSculptors:)
  @NSManaged public func addToSculptors(_ values: Set<LinkItem>)

  @objc(removeSculptors:)
  @NSManaged public func removeFromSculptors(_ values: Set<LinkItem>)
}

// MARK: Relationship SoloDesigners

extension Boardgame {
  @objc(addSoloDesignersObject:)
  @NSManaged public func addToSoloDesigners(_ value: LinkItem)

  @objc(removeSoloDesignersObject:)
  @NSManaged public func removeFromSoloDesigners(_ value: LinkItem)

  @objc(addSoloDesigners:)
  @NSManaged public func addToSoloDesigners(_ values: Set<LinkItem>)

  @objc(removeSoloDesigners:)
  @NSManaged public func removeFromSoloDesigners(_ values: Set<LinkItem>)
}

// MARK: Relationship Subdomains

extension Boardgame {
  @objc(addSubdomainsObject:)
  @NSManaged public func addToSubdomains(_ value: LinkItem)

  @objc(removeSubdomainsObject:)
  @NSManaged public func removeFromSubdomains(_ value: LinkItem)

  @objc(addSubdomains:)
  @NSManaged public func addToSubdomains(_ values: Set<LinkItem>)

  @objc(removeSubdomains:)
  @NSManaged public func removeFromSubdomains(_ values: Set<LinkItem>)
}

// MARK: Relationship Versions

extension Boardgame {
  @objc(addVersionsObject:)
  @NSManaged public func addToVersions(_ value: LinkItem)

  @objc(removeVersionsObject:)
  @NSManaged public func removeFromVersions(_ value: LinkItem)

  @objc(addVersions:)
  @NSManaged public func addToVersions(_ values: Set<LinkItem>)

  @objc(removeVersions:)
  @NSManaged public func removeFromVersions(_ values: Set<LinkItem>)
}

// MARK: Relationship VideoGames

extension Boardgame {
  @objc(addVideoGamesObject:)
  @NSManaged public func addToVideoGames(_ value: LinkItem)

  @objc(removeVideoGamesObject:)
  @NSManaged public func removeFromVideoGames(_ value: LinkItem)

  @objc(addVideoGames:)
  @NSManaged public func addToVideoGames(_ values: Set<LinkItem>)

  @objc(removeVideoGames:)
  @NSManaged public func removeFromVideoGames(_ values: Set<LinkItem>)
}

// MARK: Relationship Writers

extension Boardgame {
  @objc(addWritersObject:)
  @NSManaged public func addToWriters(_ value: LinkItem)

  @objc(removeWritersObject:)
  @NSManaged public func removeFromWriters(_ value: LinkItem)

  @objc(addWriters:)
  @NSManaged public func addToWriters(_ values: Set<LinkItem>)

  @objc(removeWriters:)
  @NSManaged public func removeFromWriters(_ values: Set<LinkItem>)
}

// MARK: - ImageSet

@objc(ImageSet)
public class ImageSet: NSManagedObject {
  public class var entityName: String {
    return "ImageSet"
  }

  public class func entity(in managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
    return NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)
  }

  @available(*, deprecated, renamed: "makeFetchRequest", message: "To avoid collisions with the less concrete method in `NSManagedObject`, please use `makeFetchRequest()` instead.")
  @nonobjc public class func fetchRequest() -> NSFetchRequest<ImageSet> {
    return NSFetchRequest<ImageSet>(entityName: entityName)
  }

  @nonobjc public class func makeFetchRequest() -> NSFetchRequest<ImageSet> {
    return NSFetchRequest<ImageSet>(entityName: entityName)
  }

  // swiftlint:disable discouraged_optional_boolean discouraged_optional_collection
  public var imageType: ImageType? {
    get {
      let key = "imageType"
      willAccessValue(forKey: key)
      defer { didAccessValue(forKey: key) }

      guard let value = primitiveValue(forKey: key) as? ImageType.RawValue else {
        return nil
      }
      return ImageType(rawValue: value)
    }
    set {
      let key = "imageType"
      willChangeValue(forKey: key)
      defer { didChangeValue(forKey: key) }

      setPrimitiveValue(newValue?.rawValue, forKey: key)
    }
  }
  @NSManaged public var source: URL?
  @NSManaged public var source2x: URL?
  @NSManaged public var source2xData: Data?
  @NSManaged public var sourceData: Data?
  @NSManaged public var boardgame: Boardgame?
  // swiftlint:enable discouraged_optional_boolean discouraged_optional_collection
}

// MARK: - LinkItem

@objc(LinkItem)
public class LinkItem: NSManagedObject {
  public class var entityName: String {
    return "LinkItem"
  }

  public class func entity(in managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
    return NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)
  }

  @available(*, deprecated, renamed: "makeFetchRequest", message: "To avoid collisions with the less concrete method in `NSManagedObject`, please use `makeFetchRequest()` instead.")
  @nonobjc public class func fetchRequest() -> NSFetchRequest<LinkItem> {
    return NSFetchRequest<LinkItem>(entityName: entityName)
  }

  @nonobjc public class func makeFetchRequest() -> NSFetchRequest<LinkItem> {
    return NSFetchRequest<LinkItem>(entityName: entityName)
  }

  // swiftlint:disable discouraged_optional_boolean discouraged_optional_collection
  @NSManaged public var bggID: String
  public var geekItemType: GeekItemType? {
    get {
      let key = "geekItemType"
      willAccessValue(forKey: key)
      defer { didAccessValue(forKey: key) }

      guard let value = primitiveValue(forKey: key) as? GeekItemType.RawValue else {
        return nil
      }
      return GeekItemType(rawValue: value)
    }
    set {
      let key = "geekItemType"
      willChangeValue(forKey: key)
      defer { didChangeValue(forKey: key) }

      setPrimitiveValue(newValue?.rawValue, forKey: key)
    }
  }
  @NSManaged public var name: String
  @NSManaged public var urlReference: String
  @NSManaged public var accessoryFor: Set<Boardgame>?
  @NSManaged public var artistFor: Set<Boardgame>?
  @NSManaged public var categoryFor: Set<Boardgame>?
  @NSManaged public var containedIn: Set<Boardgame>?
  @NSManaged public var contains: Set<Boardgame>?
  @NSManaged public var designerFor: Set<Boardgame>?
  @NSManaged public var developerFor: Set<Boardgame>?
  @NSManaged public var editorFor: Set<Boardgame>?
  @NSManaged public var expandedBy: Set<Boardgame>?
  @NSManaged public var expansionFor: Set<Boardgame>?
  @NSManaged public var familyFor: Set<Boardgame>?
  @NSManaged public var graphicDesignerFor: Set<Boardgame>?
  @NSManaged public var honorFor: Set<Boardgame>?
  @NSManaged public var insertDesignerFor: Set<Boardgame>?
  @NSManaged public var integratesWith: Set<Boardgame>?
  @NSManaged public var mechanicFor: Set<Boardgame>?
  @NSManaged public var publisherFor: Set<Boardgame>?
  @NSManaged public var reimplementedBy: Set<Boardgame>?
  @NSManaged public var reimplements: Set<Boardgame>?
  @NSManaged public var sculptorFor: Set<Boardgame>?
  @NSManaged public var soloDesignerFor: Set<Boardgame>?
  @NSManaged public var subdomainFor: Set<Boardgame>?
  @NSManaged public var versionFor: Set<Boardgame>?
  @NSManaged public var videoGameOf: Set<Boardgame>?
  @NSManaged public var writerFor: Set<Boardgame>?
  // swiftlint:enable discouraged_optional_boolean discouraged_optional_collection
}

// MARK: Relationship AccessoryFor

extension LinkItem {
  @objc(addAccessoryForObject:)
  @NSManaged public func addToAccessoryFor(_ value: Boardgame)

  @objc(removeAccessoryForObject:)
  @NSManaged public func removeFromAccessoryFor(_ value: Boardgame)

  @objc(addAccessoryFor:)
  @NSManaged public func addToAccessoryFor(_ values: Set<Boardgame>)

  @objc(removeAccessoryFor:)
  @NSManaged public func removeFromAccessoryFor(_ values: Set<Boardgame>)
}

// MARK: Relationship ArtistFor

extension LinkItem {
  @objc(addArtistForObject:)
  @NSManaged public func addToArtistFor(_ value: Boardgame)

  @objc(removeArtistForObject:)
  @NSManaged public func removeFromArtistFor(_ value: Boardgame)

  @objc(addArtistFor:)
  @NSManaged public func addToArtistFor(_ values: Set<Boardgame>)

  @objc(removeArtistFor:)
  @NSManaged public func removeFromArtistFor(_ values: Set<Boardgame>)
}

// MARK: Relationship CategoryFor

extension LinkItem {
  @objc(addCategoryForObject:)
  @NSManaged public func addToCategoryFor(_ value: Boardgame)

  @objc(removeCategoryForObject:)
  @NSManaged public func removeFromCategoryFor(_ value: Boardgame)

  @objc(addCategoryFor:)
  @NSManaged public func addToCategoryFor(_ values: Set<Boardgame>)

  @objc(removeCategoryFor:)
  @NSManaged public func removeFromCategoryFor(_ values: Set<Boardgame>)
}

// MARK: Relationship ContainedIn

extension LinkItem {
  @objc(addContainedInObject:)
  @NSManaged public func addToContainedIn(_ value: Boardgame)

  @objc(removeContainedInObject:)
  @NSManaged public func removeFromContainedIn(_ value: Boardgame)

  @objc(addContainedIn:)
  @NSManaged public func addToContainedIn(_ values: Set<Boardgame>)

  @objc(removeContainedIn:)
  @NSManaged public func removeFromContainedIn(_ values: Set<Boardgame>)
}

// MARK: Relationship Contains

extension LinkItem {
  @objc(addContainsObject:)
  @NSManaged public func addToContains(_ value: Boardgame)

  @objc(removeContainsObject:)
  @NSManaged public func removeFromContains(_ value: Boardgame)

  @objc(addContains:)
  @NSManaged public func addToContains(_ values: Set<Boardgame>)

  @objc(removeContains:)
  @NSManaged public func removeFromContains(_ values: Set<Boardgame>)
}

// MARK: Relationship DesignerFor

extension LinkItem {
  @objc(addDesignerForObject:)
  @NSManaged public func addToDesignerFor(_ value: Boardgame)

  @objc(removeDesignerForObject:)
  @NSManaged public func removeFromDesignerFor(_ value: Boardgame)

  @objc(addDesignerFor:)
  @NSManaged public func addToDesignerFor(_ values: Set<Boardgame>)

  @objc(removeDesignerFor:)
  @NSManaged public func removeFromDesignerFor(_ values: Set<Boardgame>)
}

// MARK: Relationship DeveloperFor

extension LinkItem {
  @objc(addDeveloperForObject:)
  @NSManaged public func addToDeveloperFor(_ value: Boardgame)

  @objc(removeDeveloperForObject:)
  @NSManaged public func removeFromDeveloperFor(_ value: Boardgame)

  @objc(addDeveloperFor:)
  @NSManaged public func addToDeveloperFor(_ values: Set<Boardgame>)

  @objc(removeDeveloperFor:)
  @NSManaged public func removeFromDeveloperFor(_ values: Set<Boardgame>)
}

// MARK: Relationship EditorFor

extension LinkItem {
  @objc(addEditorForObject:)
  @NSManaged public func addToEditorFor(_ value: Boardgame)

  @objc(removeEditorForObject:)
  @NSManaged public func removeFromEditorFor(_ value: Boardgame)

  @objc(addEditorFor:)
  @NSManaged public func addToEditorFor(_ values: Set<Boardgame>)

  @objc(removeEditorFor:)
  @NSManaged public func removeFromEditorFor(_ values: Set<Boardgame>)
}

// MARK: Relationship ExpandedBy

extension LinkItem {
  @objc(addExpandedByObject:)
  @NSManaged public func addToExpandedBy(_ value: Boardgame)

  @objc(removeExpandedByObject:)
  @NSManaged public func removeFromExpandedBy(_ value: Boardgame)

  @objc(addExpandedBy:)
  @NSManaged public func addToExpandedBy(_ values: Set<Boardgame>)

  @objc(removeExpandedBy:)
  @NSManaged public func removeFromExpandedBy(_ values: Set<Boardgame>)
}

// MARK: Relationship ExpansionFor

extension LinkItem {
  @objc(addExpansionForObject:)
  @NSManaged public func addToExpansionFor(_ value: Boardgame)

  @objc(removeExpansionForObject:)
  @NSManaged public func removeFromExpansionFor(_ value: Boardgame)

  @objc(addExpansionFor:)
  @NSManaged public func addToExpansionFor(_ values: Set<Boardgame>)

  @objc(removeExpansionFor:)
  @NSManaged public func removeFromExpansionFor(_ values: Set<Boardgame>)
}

// MARK: Relationship FamilyFor

extension LinkItem {
  @objc(addFamilyForObject:)
  @NSManaged public func addToFamilyFor(_ value: Boardgame)

  @objc(removeFamilyForObject:)
  @NSManaged public func removeFromFamilyFor(_ value: Boardgame)

  @objc(addFamilyFor:)
  @NSManaged public func addToFamilyFor(_ values: Set<Boardgame>)

  @objc(removeFamilyFor:)
  @NSManaged public func removeFromFamilyFor(_ values: Set<Boardgame>)
}

// MARK: Relationship GraphicDesignerFor

extension LinkItem {
  @objc(addGraphicDesignerForObject:)
  @NSManaged public func addToGraphicDesignerFor(_ value: Boardgame)

  @objc(removeGraphicDesignerForObject:)
  @NSManaged public func removeFromGraphicDesignerFor(_ value: Boardgame)

  @objc(addGraphicDesignerFor:)
  @NSManaged public func addToGraphicDesignerFor(_ values: Set<Boardgame>)

  @objc(removeGraphicDesignerFor:)
  @NSManaged public func removeFromGraphicDesignerFor(_ values: Set<Boardgame>)
}

// MARK: Relationship HonorFor

extension LinkItem {
  @objc(addHonorForObject:)
  @NSManaged public func addToHonorFor(_ value: Boardgame)

  @objc(removeHonorForObject:)
  @NSManaged public func removeFromHonorFor(_ value: Boardgame)

  @objc(addHonorFor:)
  @NSManaged public func addToHonorFor(_ values: Set<Boardgame>)

  @objc(removeHonorFor:)
  @NSManaged public func removeFromHonorFor(_ values: Set<Boardgame>)
}

// MARK: Relationship InsertDesignerFor

extension LinkItem {
  @objc(addInsertDesignerForObject:)
  @NSManaged public func addToInsertDesignerFor(_ value: Boardgame)

  @objc(removeInsertDesignerForObject:)
  @NSManaged public func removeFromInsertDesignerFor(_ value: Boardgame)

  @objc(addInsertDesignerFor:)
  @NSManaged public func addToInsertDesignerFor(_ values: Set<Boardgame>)

  @objc(removeInsertDesignerFor:)
  @NSManaged public func removeFromInsertDesignerFor(_ values: Set<Boardgame>)
}

// MARK: Relationship IntegratesWith

extension LinkItem {
  @objc(addIntegratesWithObject:)
  @NSManaged public func addToIntegratesWith(_ value: Boardgame)

  @objc(removeIntegratesWithObject:)
  @NSManaged public func removeFromIntegratesWith(_ value: Boardgame)

  @objc(addIntegratesWith:)
  @NSManaged public func addToIntegratesWith(_ values: Set<Boardgame>)

  @objc(removeIntegratesWith:)
  @NSManaged public func removeFromIntegratesWith(_ values: Set<Boardgame>)
}

// MARK: Relationship MechanicFor

extension LinkItem {
  @objc(addMechanicForObject:)
  @NSManaged public func addToMechanicFor(_ value: Boardgame)

  @objc(removeMechanicForObject:)
  @NSManaged public func removeFromMechanicFor(_ value: Boardgame)

  @objc(addMechanicFor:)
  @NSManaged public func addToMechanicFor(_ values: Set<Boardgame>)

  @objc(removeMechanicFor:)
  @NSManaged public func removeFromMechanicFor(_ values: Set<Boardgame>)
}

// MARK: Relationship PublisherFor

extension LinkItem {
  @objc(addPublisherForObject:)
  @NSManaged public func addToPublisherFor(_ value: Boardgame)

  @objc(removePublisherForObject:)
  @NSManaged public func removeFromPublisherFor(_ value: Boardgame)

  @objc(addPublisherFor:)
  @NSManaged public func addToPublisherFor(_ values: Set<Boardgame>)

  @objc(removePublisherFor:)
  @NSManaged public func removeFromPublisherFor(_ values: Set<Boardgame>)
}

// MARK: Relationship ReimplementedBy

extension LinkItem {
  @objc(addReimplementedByObject:)
  @NSManaged public func addToReimplementedBy(_ value: Boardgame)

  @objc(removeReimplementedByObject:)
  @NSManaged public func removeFromReimplementedBy(_ value: Boardgame)

  @objc(addReimplementedBy:)
  @NSManaged public func addToReimplementedBy(_ values: Set<Boardgame>)

  @objc(removeReimplementedBy:)
  @NSManaged public func removeFromReimplementedBy(_ values: Set<Boardgame>)
}

// MARK: Relationship Reimplements

extension LinkItem {
  @objc(addReimplementsObject:)
  @NSManaged public func addToReimplements(_ value: Boardgame)

  @objc(removeReimplementsObject:)
  @NSManaged public func removeFromReimplements(_ value: Boardgame)

  @objc(addReimplements:)
  @NSManaged public func addToReimplements(_ values: Set<Boardgame>)

  @objc(removeReimplements:)
  @NSManaged public func removeFromReimplements(_ values: Set<Boardgame>)
}

// MARK: Relationship SculptorFor

extension LinkItem {
  @objc(addSculptorForObject:)
  @NSManaged public func addToSculptorFor(_ value: Boardgame)

  @objc(removeSculptorForObject:)
  @NSManaged public func removeFromSculptorFor(_ value: Boardgame)

  @objc(addSculptorFor:)
  @NSManaged public func addToSculptorFor(_ values: Set<Boardgame>)

  @objc(removeSculptorFor:)
  @NSManaged public func removeFromSculptorFor(_ values: Set<Boardgame>)
}

// MARK: Relationship SoloDesignerFor

extension LinkItem {
  @objc(addSoloDesignerForObject:)
  @NSManaged public func addToSoloDesignerFor(_ value: Boardgame)

  @objc(removeSoloDesignerForObject:)
  @NSManaged public func removeFromSoloDesignerFor(_ value: Boardgame)

  @objc(addSoloDesignerFor:)
  @NSManaged public func addToSoloDesignerFor(_ values: Set<Boardgame>)

  @objc(removeSoloDesignerFor:)
  @NSManaged public func removeFromSoloDesignerFor(_ values: Set<Boardgame>)
}

// MARK: Relationship SubdomainFor

extension LinkItem {
  @objc(addSubdomainForObject:)
  @NSManaged public func addToSubdomainFor(_ value: Boardgame)

  @objc(removeSubdomainForObject:)
  @NSManaged public func removeFromSubdomainFor(_ value: Boardgame)

  @objc(addSubdomainFor:)
  @NSManaged public func addToSubdomainFor(_ values: Set<Boardgame>)

  @objc(removeSubdomainFor:)
  @NSManaged public func removeFromSubdomainFor(_ values: Set<Boardgame>)
}

// MARK: Relationship VersionFor

extension LinkItem {
  @objc(addVersionForObject:)
  @NSManaged public func addToVersionFor(_ value: Boardgame)

  @objc(removeVersionForObject:)
  @NSManaged public func removeFromVersionFor(_ value: Boardgame)

  @objc(addVersionFor:)
  @NSManaged public func addToVersionFor(_ values: Set<Boardgame>)

  @objc(removeVersionFor:)
  @NSManaged public func removeFromVersionFor(_ values: Set<Boardgame>)
}

// MARK: Relationship VideoGameOf

extension LinkItem {
  @objc(addVideoGameOfObject:)
  @NSManaged public func addToVideoGameOf(_ value: Boardgame)

  @objc(removeVideoGameOfObject:)
  @NSManaged public func removeFromVideoGameOf(_ value: Boardgame)

  @objc(addVideoGameOf:)
  @NSManaged public func addToVideoGameOf(_ values: Set<Boardgame>)

  @objc(removeVideoGameOf:)
  @NSManaged public func removeFromVideoGameOf(_ values: Set<Boardgame>)
}

// MARK: Relationship WriterFor

extension LinkItem {
  @objc(addWriterForObject:)
  @NSManaged public func addToWriterFor(_ value: Boardgame)

  @objc(removeWriterForObject:)
  @NSManaged public func removeFromWriterFor(_ value: Boardgame)

  @objc(addWriterFor:)
  @NSManaged public func addToWriterFor(_ values: Set<Boardgame>)

  @objc(removeWriterFor:)
  @NSManaged public func removeFromWriterFor(_ values: Set<Boardgame>)
}

// MARK: - PreviewItemStatusLink

@objc(PreviewItemStatusLink)
public class PreviewItemStatusLink: NSManagedObject {
  public class var entityName: String {
    return "PreviewItemStatusLink"
  }

  public class func entity(in managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
    return NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)
  }

  @available(*, deprecated, renamed: "makeFetchRequest", message: "To avoid collisions with the less concrete method in `NSManagedObject`, please use `makeFetchRequest()` instead.")
  @nonobjc public class func fetchRequest() -> NSFetchRequest<PreviewItemStatusLink> {
    return NSFetchRequest<PreviewItemStatusLink>(entityName: entityName)
  }

  @nonobjc public class func makeFetchRequest() -> NSFetchRequest<PreviewItemStatusLink> {
    return NSFetchRequest<PreviewItemStatusLink>(entityName: entityName)
  }

  // swiftlint:disable discouraged_optional_boolean discouraged_optional_collection
  @NSManaged public var geekItemId: String?
  @NSManaged public var notes: String
  public var postStatus: PreviewItemStatus? {
    get {
      let key = "postStatus"
      willAccessValue(forKey: key)
      defer { didAccessValue(forKey: key) }

      guard let value = primitiveValue(forKey: key) as? PreviewItemStatus.RawValue else {
        return nil
      }
      return PreviewItemStatus(rawValue: value)
    }
    set {
      let key = "postStatus"
      willChangeValue(forKey: key)
      defer { didChangeValue(forKey: key) }

      setPrimitiveValue(newValue?.rawValue, forKey: key)
    }
  }
  public var preStatus: PreviewItemStatus? {
    get {
      let key = "preStatus"
      willAccessValue(forKey: key)
      defer { didAccessValue(forKey: key) }

      guard let value = primitiveValue(forKey: key) as? PreviewItemStatus.RawValue else {
        return nil
      }
      return PreviewItemStatus(rawValue: value)
    }
    set {
      let key = "preStatus"
      willChangeValue(forKey: key)
      defer { didChangeValue(forKey: key) }

      setPrimitiveValue(newValue?.rawValue, forKey: key)
    }
  }
  @NSManaged public var seen: Bool
  // swiftlint:enable discouraged_optional_boolean discouraged_optional_collection
}

// MARK: - Rank

@objc(Rank)
public class Rank: NSManagedObject {
  public class var entityName: String {
    return "Rank"
  }

  public class func entity(in managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
    return NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)
  }

  @available(*, deprecated, renamed: "makeFetchRequest", message: "To avoid collisions with the less concrete method in `NSManagedObject`, please use `makeFetchRequest()` instead.")
  @nonobjc public class func fetchRequest() -> NSFetchRequest<Rank> {
    return NSFetchRequest<Rank>(entityName: entityName)
  }

  @nonobjc public class func makeFetchRequest() -> NSFetchRequest<Rank> {
    return NSFetchRequest<Rank>(entityName: entityName)
  }

  // swiftlint:disable discouraged_optional_boolean discouraged_optional_collection
  @NSManaged public var bayesAverage: String
  @NSManaged public var prettyName: String
  @NSManaged public var rank: String
  @NSManaged public var shortPrettyName: String
  @NSManaged public var subdomain: String?
  @NSManaged public var veryShortPrettyName: String
  @NSManaged public var boardgame: Boardgame?
  // swiftlint:enable discouraged_optional_boolean discouraged_optional_collection
}

// swiftlint:enable identifier_name line_length type_body_length
