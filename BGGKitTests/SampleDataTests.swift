//
//  SampleDataTests.swift
//  BoardsTests
//
//  Created by Joseph Canale on 9/27/21.
//

import XCTest
import CoreData
@testable 

class SampleDataTests: XCTestCase {

    func test() throws {
        let _ = GeekCollection.sample

        print("Hello")
    }

    func testBoardgameSample() throws {
        _ = GeekItem.sampleBoardgame
        XCTAssert(true)
    }

    func testNewReleasesSample() throws {
        _ = NewReleaseItem.sample
        XCTAssert(true)
    }

    func testDecodeCompany() throws {
        _ = GeekItem.sampleCompany
        XCTAssert(true)
    }

    func testDecodeForum() throws {
        let _ = Forum.sample
        XCTAssert(true)
    }

    func testDecodeThread() throws {
        let _ = GeekThread.sample
        XCTAssert(true)
    }

    func testDecodeArticle() throws {
        let _  = Article.sample
        XCTAssert(true)
    }

    func testDecodeUser() throws {
        let _  = GeekUser.sample
        XCTAssert(true)
    }

    func testDecodeImages() throws {
        let _ = GeekImage.sample
        XCTAssertTrue(true)
    }

    func testDecodeFiles() throws {
        let _ = GeekFile.sample
        XCTAssertTrue(true)
    }

    func testDecodeFilePages() throws {
        let _ = GeekFilePage.sample
        XCTAssertTrue(true)
    }

    func testDecodeBoardgameExpansion() throws {
        let _ = BoardgameExpansion.sample
        XCTAssert(true)
    }

    func testDecodeSearch() throws {
        let _ = SearchResultItem.sampleSearch
        XCTAssert(true)
    }
    
    func testGeekPreviewListing() throws {
        let _ = GeekPreviewListingContainer.sample
        XCTAssert(true)
    }
}
