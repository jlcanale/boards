//
//  BGGAPITests.swift
//  BoardsTests
//
//  Created by Joseph Canale on 9/30/21.
//

import XCTest
@testable

class BGGAPITests: XCTestCase {

    var api: BGGAPI! = nil

    override func setUpWithError() throws {
        api = BGGAPI.shared
    }

    func testGeekItemSearch() async throws {
        let results = try await api.searchForGeekItem(query: "Spirit Island")
        XCTAssertNotEqual(results.count, 0, "Unable to fetch search results.")
    }

    func testHotness() async throws {
        let hotness = try await api.fetchHotness()
        XCTAssertNotEqual(hotness.count, 0, "Unable to fetch hotness")
    }

    func testForumsFetch() async throws {
        let forums = try await api.fetchForums(forID: "174430")
        XCTAssertNotEqual(forums.count, 0, "Unable to fetch forums")
    }

    func testArticlesFetch() async throws {
        let articles = try await api.fetchArticles(forThreadID: "2733579")
        XCTAssertNotEqual(articles.count, 0, "Unable to fetch articles")
    }

    func testUserFetch() async throws {
        let user = try await api.fetchGeekUser(forID: "772566")
        XCTAssertEqual(user.username, "jlcanale")
    }

    func testImagesFetch() async throws {
        let images = try await api.fetchGeekImages(objectID: "174430", gallery: .all, pageNumber: 1)
        XCTAssertEqual(images.count, 36)
    }

    func testFilesFetch() async throws {
        let files = try await api.fetchGeekFiles(forID: "174430")
        XCTAssertEqual(files.count, 25)
    }

    func testCollectionFetch() async throws {
        let collection = try await api.fetchCollection(forUser: "jlcanale")
        XCTAssertNotEqual(collection.items.count, 0)
    }

    func testLoginUser() async throws {
        _ = try await api.login(using: BGGCredential(username: "jlcanale", password: "pajtu3-gemzUw-cihcur"))
        XCTAssert(true)
    }

    func testLoggedIn() async throws {
        let result = try await api.loginStatus(sessionID: "46ed9d5d4e99e6e566931c478cc13edcf3e230ffu772566")
        XCTAssertEqual(true, result)
    }

    func test() throws {
        _ = "Filename.pdf".pathExtension
        print("hello")
    }
}
