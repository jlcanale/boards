//
//  DawgTests.swift
//  Tests iOS
//
//  Created by Joe Canale on 5/24/22.
//

import XCTest
@testable import Boards

class DawgTests: XCTestCase {
    var dawg: Dawg!

    override func setUp() {
        dawg = Dawg()
    }

    func testLoadingWordList() throws {
        let url = Bundle.main.url(forResource: "words", withExtension: "txt")!
        let contents = try! String(contentsOf: url)
        let words = contents.components(separatedBy: .newlines).map { $0.lowercased() }.sorted()
        words.forEach { dawg.insert(word: $0) }
        XCTAssert(true)
    }

    func testLoadingDuplicateWord() throws {
        dawg.insert(word: "cat")
        dawg.insert(word: "cat")
        XCTAssert(true)
    }

    func testFindingWordsFromPattern() throws {
    }

    func testDawgFinishPerformance() throws {
        let url = Bundle.main.url(forResource: "words", withExtension: "txt")!
        let contents = try! String(contentsOf: url)
        let words = contents.components(separatedBy: .newlines).map { $0.lowercased() }.sorted()
        words.forEach { dawg.insert(word: $0) }
        self.measure {
            dawg.finish()
        }
    }

}
