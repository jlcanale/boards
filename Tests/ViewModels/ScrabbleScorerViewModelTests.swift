//
//  ScrabbleScorerViewModelTests.swift
//  Boards
//
//  Created by Joe Canale on 5/18/22.
//

import XCTest
@testable import Boards

class ScrabbleScorerViewModelTests: XCTestCase {
    
    var viewModel: ScrabbleScorerViewModel!
    
    override func setUp() {
        viewModel = ScrabbleScorerViewModel()
    }
    
    func testSetWord() {
        viewModel.inputWord = "PHONE"
        XCTAssertTrue(viewModel.word.string == "PHONE")
    }
    
    func testStandardScoring() {
        viewModel.inputWord = "PHONE"
        Task {
            await viewModel.calculateScore()
            XCTAssertTrue(viewModel.score == 10)
        }
    }
    
    func test() {
        let inputBoard: [[SudokuValue]] = [
            [nil, nil, nil,  7 ,  4 ,  6 , nil, nil, nil],
            [nil, nil,  1 , nil, nil, nil,  2 , nil, nil],
            [nil,  5 , nil, nil, nil, nil, nil,  8 , nil],
            [ 7 , nil, nil,  9 , nil,  5 , nil, nil,  2 ],
            [ 2 , nil, nil, nil, nil, nil, nil, nil,  9 ],
            [ 1 , nil, nil,  3 , nil,  7 , nil, nil,  8 ],
            [nil,  3 , nil, nil, nil, nil, nil,  6 , nil],
            [nil, nil,  8 , nil, nil, nil,  1 , nil, nil],
            [nil, nil, nil,  5 ,  7 ,  3 , nil, nil, nil]
        ]
        
        let solution: [[SudokuValue]] = [
            [8,2,9,7,4,6,5,3,1],
            [3,6,1,8,5,9,2,4,7],
            [4,5,7,1,3,2,9,8,6],
            [7,4,3,9,8,5,6,1,2],
            [2,8,5,4,6,1,3,7,9],
            [1,9,6,3,2,7,4,5,8],
            [9,3,4,2,1,8,7,6,5],
            [5,7,8,6,9,4,1,2,3],
            [6,1,2,5,7,3,8,9,4]
        ]
        
        
        var solver = SudokuSolver(board: inputBoard)
        solver.solve()
        
        XCTAssertEqual(solver.board, solution)
    }
    
    //TODO: Fix these tests
//    func testDoubleLetterScoring() {
//        viewModel.word = [("P", .normal), ("H", .doubleLetter), ("O", .normal), ("N", .doubleLetter), ("E", .normal)]
//        viewModel.calculateScore()
//        XCTAssertTrue(viewModel.score == 15)
//    }
//
//    func testTripleLetterScoring() {
//        viewModel.word = [("P", .normal), ("H", .tripleLetter), ("O", .normal), ("N", .normal), ("E", .normal)]
//        viewModel.calculateScore()
//        XCTAssertTrue(viewModel.score == 18)
//    }
//
//    func testDoubleWordScoring() {
//        viewModel.word = [("P", .normal), ("H", .doubleWord), ("O", .normal), ("N", .normal), ("E", .normal)]
//        viewModel.calculateScore()
//        XCTAssertTrue(viewModel.score == 20)
//    }
//
//    func testTripleWordScoring() {
//        viewModel.word = [("P", .normal), ("H", .tripleWord), ("O", .normal), ("N", .normal), ("E", .normal)]
//        viewModel.calculateScore()
//        XCTAssertTrue(viewModel.score == 30)
//    }
//
//    func testDoubleTripleWordScoring() {
//        viewModel.word = [("P", .normal), ("H", .tripleWord), ("O", .doubleWord), ("N", .normal), ("E", .normal)]
//        viewModel.calculateScore()
//        XCTAssertTrue(viewModel.score == 60)
//    }
}
