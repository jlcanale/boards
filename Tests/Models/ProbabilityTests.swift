//
//  ProbabilityTests.swift
//  BoardsTests
//
//  Created by Joe Canale on 6/2/22.
//

import XCTest
@testable import Boards

class ProbabilityTests: XCTestCase {

    func testExpectedValueForOneDiceType() {
        XCTAssertEqual(Probability.expectedValue(rolling: 1, .d6), 3.5)
        XCTAssertEqual(Probability.expectedValue(rolling: 2, .d6), 7)
        XCTAssertEqual(Probability.expectedValue(rolling: 3, .d6), 10.5)

        XCTAssertEqual(Probability.expectedValue(rolling: 1, .d20), 10.5)
        XCTAssertEqual(Probability.expectedValue(rolling: 2, .d20), 21)
        XCTAssertEqual(Probability.expectedValue(rolling: 3, .d20), 31.5)
    }
    
    func testFactorial() {
        XCTAssertEqual(Probability.factorial(0), 1)
        XCTAssertEqual(Probability.factorial(1), 1)
        XCTAssertEqual(Probability.factorial(2), 2)
        XCTAssertEqual(Probability.factorial(3), 6)
        XCTAssertEqual(Probability.factorial(4), 24)
    }
    
    func testCombinations() {
        XCTAssertEqual(Probability.combinations(n: 5, choose: 2), 10)
        XCTAssertEqual(Probability.combinations(n: 10, choose: 3), 120)
        XCTAssertEqual(Probability.combinations(n: 25, choose: 3), 2300)
    }
    
    func testProbabilityOfRollingAtLeastSum() {
        XCTAssertEqual(Probability.probabilityOfRolling(atLeast: 9, using: 2, .d6), 0.2778, accuracy: 0.1)
        XCTAssertEqual(Probability.probabilityOfRolling(atLeast: 31, using: 10, .d6), 0.79, accuracy: 0.1)
    }
    
    func testProbabilityOfRollingAtLeastDieFace() {
        XCTAssertEqual(Probability.probabilityOfRolling(atLeast: 2, equalTo: [5,6], using: 2, .d6), 0.111, accuracy: 0.1)
        XCTAssertEqual(Probability.probabilityOfRolling(atLeast: 2, equalTo: [5,6], using: 6, .d6), 0.64, accuracy: 0.1)
    }
    
    func testBinomialProbability() {
        XCTAssertEqual(Probability.binomialProbability(events: 3, successes: 1, successProbability: 0.1666), 0.34714, accuracy: 0.1)
        XCTAssertEqual(Probability.binomialProbability(events: 6, successes: 2, successProbability: 0.1666), 0.20093, accuracy: 0.1)

        
    }

}
