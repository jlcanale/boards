//
//  MockBGGAPI.swift
//  Boards
//
//  Created by Joseph Canale on 11/3/21.
//

import Foundation

public class MockBGGAPI: BGGAPIService {
    public static let shared = MockBGGAPI()

    public func searchForGeekItem(query: String) async throws -> [SearchResultItem] { SearchResultItem.sampleSearch }

    public func fetchGeekItem(with id: String, type: GeekItemType, fetchDynamicInfo: Bool) async throws -> GeekItem { .sampleBoardgame }

    public func fetchDynamicInfo(forID id: String, type: GeekItemType) async throws -> DynamicInfo { DynamicInfo.sample }

    public func fetchHotness() async throws -> [HotnessItem] { HotnessItem.sample }

    public func fetchCrowdfunding() async throws -> [CrowdfundingItem] { CrowdfundingItem.sample }

    public func fetchNewReleases() async throws -> [NewReleaseItem] { NewReleaseItem.sample }

    public func fetchVersions(forID id: String, type: GeekItemType, pageNumber: Int, itemsPerPage: Int) async throws -> [BoardgameVersion] { BoardgameVersion.sample }

    public func fetchExpansions(forID id: String, type: GeekItemType, pageNumber: Int, itemsPerPage: Int) async throws -> [BoardgameExpansion] { BoardgameExpansion.sample }

    public func fetchForums(forID id: String, type: GeekItemType) async throws -> [Forum] { Forum.sample }

    public func fetchGeekThreads(forumID: String, objectID: String, type: GeekItemType, pageNumber: Int, itemsPerPage: Int) async throws -> [GeekThread] { GeekThread.sample }

    public func fetchArticles(forThreadID threadID: String) async throws -> [Article] { Article.sample }

    public func fetchGeekImages(objectID: String, type: GeekItemType, gallery: ImageGalleryType, pageNumber: Int, itemsPerPage: Int) async throws -> [GeekImage] { GeekImage.sample }

    public func fetchGeekVideos(objectID: String, type: GeekItemType, gallery: VideoGalleryType, pageNumber: Int, itemsPerPage: Int) async throws -> [GeekVideo] { GeekVideo.sample }

    public func fetchGeekUser(forID userID: String) async throws -> GeekUser { GeekUser.sample }

    public func fetchGeekFiles(forID objectID: String, type: GeekItemType, pageNumber: Int, itemsPerPage: Int) async throws -> [GeekFile] { GeekFile.sample }

    public func fetchGeekFilePages(forID fileID: String) async throws -> [GeekFilePage] { GeekFilePage.sample }

    public func fetchCollection(forUser username: String) async throws -> GeekCollection { GeekCollection.sample }
}
