//
//  BGGAPIService.swift
//  Boards
//
//  Created by Joseph Canale on 11/3/21.
//

import Foundation

public protocol BGGAPIService {

    func searchForGeekItem(query: String) async throws -> [SearchResultItem]

    func fetchGeekItem(with id: String, type: GeekItemType, fetchDynamicInfo: Bool) async throws -> GeekItem

    func fetchDynamicInfo(forID id: String, type: GeekItemType) async throws -> DynamicInfo

    func fetchHotness() async throws ->  [HotnessItem]

    func fetchCrowdfunding() async throws -> [CrowdfundingItem]

    func fetchNewReleases() async throws -> [NewReleaseItem]

    func fetchVersions(forID id: String, type: GeekItemType, pageNumber: Int, itemsPerPage: Int) async throws -> [BoardgameVersion]

    func fetchExpansions(forID id: String, type: GeekItemType, pageNumber: Int, itemsPerPage: Int) async throws -> [BoardgameExpansion]

    func fetchForums(forID id: String, type: GeekItemType) async throws -> [Forum]

    func fetchGeekThreads(forumID: String, objectID: String, type: GeekItemType, pageNumber: Int, itemsPerPage: Int) async throws -> [GeekThread]

    func fetchArticles(forThreadID threadID: String) async throws -> [Article]

    func fetchGeekImages(objectID: String, type: GeekItemType, gallery: ImageGalleryType, pageNumber: Int, itemsPerPage: Int) async throws -> [GeekImage]

    func fetchGeekVideos(objectID: String, type: GeekItemType, gallery: VideoGalleryType, pageNumber: Int, itemsPerPage: Int) async throws -> [GeekVideo]

    func fetchGeekUser(forID userID: String) async throws -> GeekUser

    func fetchGeekFiles(forID objectID: String, type: GeekItemType, pageNumber: Int, itemsPerPage: Int) async throws -> [GeekFile]

    func fetchGeekFilePages(forID fileID: String) async throws -> [GeekFilePage]

    func fetchCollection(forUser username: String) async throws -> GeekCollection
}
