//
//  BGGLinkable.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import Foundation

public protocol BGGLinkable {
    var urlReference: String { get }
}

extension BGGLinkable {
    public var bggURL: URL {
        URL(string: "https://boardgamegeek.com\(urlReference)") ?? URL(string: "https://boardgamegeek.com")!
    }
}
