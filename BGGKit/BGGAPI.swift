//
//  BGGAPI.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation
import XMLCoder
import OSLog

// swiftlint: disable file_length
public struct BGGAPI: BGGAPIService {
    public static let shared = BGGAPI()
    private init() {
        let config = URLSessionConfiguration.default
        config.httpCookieAcceptPolicy = .always
        self.session = URLSession(configuration: config)
    }

    private var cache = URLCache.shared
    private let session: URLSession

    private var jsonDecoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }

    private var xmlDecoder: XMLDecoder { XMLDecoder() }
    private var logger = Logger()

    /// BoardGameGeek URL Scheme
    private let scheme = "https"

    /// BoardGameGeek Base URL
    private let bggURL = URL(string: "https://www.boardgamegeek.com")!

    /// BoardGameGeek API Root
    private let host = "api.geekdo.com"

    /// BoardGameGeek API Endpoints
    private struct Endpoints {
        static let hotness = "/api/hotness"
        static let crowdfunding = "/api/ending_preorders"
        static let newReleases = "/api/newreleases"
        static let geekPreview = "/api/geekpreviews"
        static let geekPreviewParentItems = "/api/geekpreviewparentitems"
        static let geekPreviewItems = "/api/geekpreviewitems"
        static let images = "/api/images"
        static let videos = "/api/videos"
        static let geekItems = "/api/geekitems"
        static let dynamicInfo = "/api/dynamicinfo"
        static let linkedItems = "/api/geekitem/linkeditems"
        static let forums = "/api/forums"
        static let threads = "/api/forums/threads"
        static let articles = "/api/articles"
        static let search = "/search/boardgame"
        static let users = "/api/users"
        static let geekItemPoll = "/geekitempoll.php"
        static let files = "/api/files"
        static let filePages = "/api/filepages"
        static let geekPoll = "/geekpoll.php"
        static let collection = "/xmlapi2/collection"
        static let login = "/login/api/v1"
        static let currentUser = "/api/users/current"
        static let logout = "/logout"
    }

    /// BoardGameGeek Forum Page Size
    private let forumPageSize = 100

    /// BoardGameGeek API Query Items
    enum QueryItemName: String {
        case noSession = "nosession"
        case objectType = "objecttype"
        case objectID = "objectid"
        case previewID = "previewid"
        case pageID = "pageid"
        case fields = "fields"
        case search = "search"
        case query = "q"
        case itemPollType = "itempolltype"
        case action = "action"
        case pollID = "pollid"
        case ajax = "ajax"
        case filterForums = "filterforums"
        case forumID = "forumid"
        case sort = "sort"
        case showCount = "showcount"
        case threadID = "threadid"
        case geekSite = "geeksite"
        case gallery = "gallery"
        case languageID = "languageid"
        case fileID = "fileid"
        case linkDataIndex = "linkdata_index"
        case subtype = "subtype"
        case username = "username"
        case description = "description"
    }

    /// BoardGameGeek API Sort Types
    enum SortTypes: String {
        case top = "hot"
        case recent
        case active
    }

    enum DecoderType {
        case json
        case xml
    }

    /// BoardGameGeek URL Components
    private var components: URLComponents {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.queryItems = [URLQueryItem(name: .noSession, value: "1")]
        return components
    }
}

// MARK: - Async
extension BGGAPI {

    // MARK: - Geek Items

    public func searchForGeekItem(query: String) async throws -> [SearchResultItem] {
        var components = self.components
        components.host = "boardgamegeek.com"
        components.path = Endpoints.search
        components.queryItems?.append(URLQueryItem(name: .query, value: query))

        return try await fetchAndDecode(SearchResponse.self, from: components, headerFields: ["accept": "application/json", "accept-encoding": "gzip, deflate, br"]).items
    }

    public func fetchGeekItem(with id: String, type: GeekItemType = .thing, fetchDynamicInfo: Bool = false) async throws -> GeekItem {
        var components = self.components
        components.path = Endpoints.geekItems
        components.queryItems?.append(URLQueryItem(name: .objectID, value: id))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))

        if !fetchDynamicInfo {
            return try await fetchAndDecode(GeekItemResponse.self, from: components).item
        } else {
            var geekItem = try await fetchAndDecode(GeekItemResponse.self, from: components).item
            let dynamicInfo = try await self.fetchDynamicInfo(forID: id)

            geekItem.dynamicInfo = dynamicInfo

            return geekItem
        }
    }

    public func fetchDynamicInfo(forID id: String, type: GeekItemType = .thing) async throws -> DynamicInfo {
        var components = self.components
        components.path = Endpoints.dynamicInfo
        components.queryItems?.append(URLQueryItem(name: .objectID, value: id))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))

        return try await fetchAndDecode(DynamicInfoResponse.self, from: components).item
    }

    public func fetchDescription(forID id: String, type: GeekItemType = .thing) async throws -> String {
        var components = self.components
        components.path = Endpoints.geekItems
        components.queryItems?.append(URLQueryItem(name: .objectID, value: id))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))
        components.queryItems?.append(URLQueryItem(name: .description, value: id))

        return try await fetchAndDecode(GeekItemDescription.Container.self, from: components).item.description
    }

    public func fetchHotness() async throws ->  [HotnessItem] {
        var components = self.components
        components.path = Endpoints.hotness
        components.queryItems?.append(URLQueryItem(name: .geekSite, value: "boardgame"))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: "thing"))

        return try await fetchAndDecode(HotnessResponse.self, from: components).items
    }

    public func fetchCrowdfunding() async throws -> [CrowdfundingItem] {
        var components = self.components
        components.path = Endpoints.crowdfunding

        return try await fetchAndDecode(from: components)
    }

    public func fetchNewReleases() async throws -> [NewReleaseItem] {
        var components = self.components
        components.path = Endpoints.newReleases

        return try await fetchAndDecode(from: components)
    }

    public func fetchVersions(forID id: String, type: GeekItemType = .thing, pageNumber: Int = 1, itemsPerPage: Int = 10) async throws -> [BoardgameVersion] {
        var components = self.components
        components.path = Endpoints.linkedItems

        components.queryItems?.append(URLQueryItem(name: .linkDataIndex, value: "boardgameversion"))
        components.queryItems?.append(URLQueryItem(name: .objectID, value: id))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))
        components.queryItems?.append(URLQueryItem(name: .pageID, value: pageNumber))
        components.queryItems?.append(URLQueryItem(name: .showCount, value: itemsPerPage))
        components.queryItems?.append(URLQueryItem(name: .sort, value: "yearpublished"))
        components.queryItems?.append(URLQueryItem(name: .subtype, value: "boardgameversion"))

        return try await fetchAndDecode(BoardgameVersionResponse.self, from: components).items
    }

    public func fetchExpansions(forID id: String, type: GeekItemType = .thing, pageNumber: Int = 1, itemsPerPage: Int = 10) async throws -> [BoardgameExpansion] {
        var components = self.components
        components.path = Endpoints.linkedItems

        components.queryItems?.append(URLQueryItem(name: .linkDataIndex, value: "boardgameexpansion"))
        components.queryItems?.append(URLQueryItem(name: .objectID, value: id))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))
        components.queryItems?.append(URLQueryItem(name: .pageID, value: pageNumber))
        components.queryItems?.append(URLQueryItem(name: .showCount, value: itemsPerPage))
        components.queryItems?.append(URLQueryItem(name: .sort, value: "yearpublished"))
        components.queryItems?.append(URLQueryItem(name: .subtype, value: "boardgameexpansion"))

        return try await fetchAndDecode(BoardgameExpansionResponse.self, from: components).items
    }

    // MARK: - Forums
    public func fetchForums(forID id: String, type: GeekItemType = .thing) async throws -> [Forum] {
        var components = self.components
        components.path = Endpoints.forums
        components.queryItems?.append(URLQueryItem(name: .objectID, value: id))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))

        return try await fetchAndDecode(ForumResponse.self, from: components).forums
    }

    public func fetchGeekThreads(forumID: String, objectID: String, type: GeekItemType = .thing, pageNumber: Int = 1, itemsPerPage: Int = 100) async throws -> [GeekThread] {
        var components = self.components
        components.path = Endpoints.threads

        components.queryItems?.append(URLQueryItem(name: .forumID, value: forumID))
        components.queryItems?.append(URLQueryItem(name: .objectID, value: objectID))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))
        components.queryItems?.append(URLQueryItem(name: .pageID, value: pageNumber))
        components.queryItems?.append(URLQueryItem(name: .showCount, value: itemsPerPage))
        components.queryItems?.append(URLQueryItem(name: .sort, value: "recent"))

        return try await fetchAndDecode(GeekThreadResponse.self, from: components).threads
    }

    public func fetchArticles(forThreadID threadID: String) async throws -> [Article] {
        var components = self.components
        components.path = Endpoints.articles
        components.queryItems?.append(URLQueryItem(name: .threadID, value: threadID))

        var articles = try await fetchAndDecode(ArticleResponse.self, from: components).articles

        for (index, article) in articles.enumerated() {
            if let authorID = article.authorID {
                articles[index].user = try? await fetchGeekUser(forID: "\(authorID)")
            }
        }

        return articles
    }

    // MARK: - Images
    public func fetchGeekImages(objectID: String, type: GeekItemType = .thing, gallery: ImageGalleryType = .all, pageNumber: Int = 1, itemsPerPage: Int = 36) async throws -> [GeekImage] {
        var components = self.components
        components.path = Endpoints.images

        components.queryItems?.append(URLQueryItem(name: .objectID, value: objectID))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))
        components.queryItems?.append(URLQueryItem(name: .pageID, value: pageNumber))
        components.queryItems?.append(URLQueryItem(name: .showCount, value: itemsPerPage))
        components.queryItems?.append(URLQueryItem(name: .sort, value: "hot"))
        components.queryItems?.append(URLQueryItem(name: .gallery, value: gallery.rawValue))

        return try await fetchAndDecode(GeekImageResponse.self, from: components).images
    }

    // MARK: - Videos
    public func fetchGeekVideos(objectID: String, type: GeekItemType = .thing, gallery: VideoGalleryType = .all, pageNumber: Int = 1, itemsPerPage: Int = 36) async throws -> [GeekVideo] {
        var components = self.components
        components.path = Endpoints.videos

        components.queryItems?.append(URLQueryItem(name: .objectID, value: objectID))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))
        components.queryItems?.append(URLQueryItem(name: .pageID, value: pageNumber))
        components.queryItems?.append(URLQueryItem(name: .showCount, value: itemsPerPage))
        components.queryItems?.append(URLQueryItem(name: .sort, value: "hot"))
        components.queryItems?.append(URLQueryItem(name: .gallery, value: gallery.rawValue))

        return try await fetchAndDecode(GeekVideoResponse.self, from: components).videos
    }

    // MARK: - Users
    public func fetchGeekUser(forID userID: String) async throws -> GeekUser {
        var components = self.components
        components.path = Endpoints.users
        components.path.append("/\(userID)")

        return try await fetchAndDecode(GeekUser.self, from: components)
    }

    // MARK: - Files
    public func fetchGeekFiles(forID objectID: String, type: GeekItemType = .thing, pageNumber: Int = 1, itemsPerPage: Int = 25) async throws -> [GeekFile] {
        var components = self.components
        components.path = Endpoints.files

        components.queryItems?.append(URLQueryItem(name: .objectID, value: objectID))
        components.queryItems?.append(URLQueryItem(name: .objectType, value: type.rawValue))
        components.queryItems?.append(URLQueryItem(name: .pageID, value: pageNumber))
        components.queryItems?.append(URLQueryItem(name: .showCount, value: itemsPerPage))
        components.queryItems?.append(URLQueryItem(name: .languageID, value: 0))
        components.queryItems?.append(URLQueryItem(name: .sort, value: "hot"))

        return try await fetchAndDecode(GeekFileResponse.self, from: components).files
    }

    public func fetchGeekFilePages(forID fileID: String) async throws -> [GeekFilePage] {
        var components = self.components
        components.path = Endpoints.filePages
        components.path.append("/\(fileID)/files")

        components.queryItems?.append(URLQueryItem(name: .fileID, value: fileID))

        return try await fetchAndDecode(GeekFilePageResponse.self, from: components).files
    }

    // MARK: - Collections
    public func fetchCollection(forUser username: String) async throws -> GeekCollection {
        var components = self.components
        components.path = Endpoints.collection
        components.queryItems?.append(URLQueryItem(name: .username, value: username))

        guard let url = components.url else { throw APIError.badURL }

        var retry = false
        var sessionReturn: (data: Data, response: URLResponse)
        repeat {
            sessionReturn = try await session.data(from: url)

            guard let response = sessionReturn.response as? HTTPURLResponse else { throw APIError.networkError }

            switch response.statusCode {
                case 202:
                    retry = true
                    try await Task.sleep(nanoseconds: 3_000_000)
                case 200...299:
                    continue
                default:
                    throw APIError.invalidServerResponse(response.statusCode)
            }
        } while retry

#if DEBUG
        // swiftlint: disable force_try
        return try! xmlDecoder.decode(GeekCollection.self, from: sessionReturn.data)
        // swiftlint: enable force_try
#else
        return try xmlDecoder.decode(GeekCollection.self, from: sessionReturn.data)
#endif

    }

    // MARK: - Previews
    public func invalidatePreviewCache() {
        var components = self.components
        components.path = Endpoints.geekPreview
        components.queryItems?.append(URLQueryItem(name: "partial", value: "listing"))
        invalidate(components: components)
    }

    public func invalidateCaches() {
        cache.removeAllCachedResponses()
    }

    public func fetchPreviews(withFullDetails: Bool = false) async throws -> [GeekPreview] {
        var components = self.components
        components.path = Endpoints.geekPreview
        components.queryItems?.append(URLQueryItem(name: "partial", value: "listing"))

        let previewListings = try await fetchAndDecode(GeekPreviewListingContainer.self, from: components).data.filter { !$0.name.lowercased().contains("test") }

        guard withFullDetails else { return previewListings.map(GeekPreview.init) }

        var previews = [GeekPreview]()

        for previewListing in previewListings {
            previews.append(try await fetchPreview(with: previewListing.id))
        }

        return previews
    }

    public func fetchPreview(with id: String) async throws -> GeekPreview {
        var components = self.components
        components.path = Endpoints.geekPreview
        components.queryItems?.append(URLQueryItem(name: .previewID, value: id))

        return try await fetchAndDecode(GeekPreview.self, from: components)
    }

    public func fetchPreviewParentItemsForPreview(with previewID: String, pageID: Int? = nil, cacheMode: CacheMode = .normal) async throws -> [GeekPreviewParentItem] {
        var components = self.components
        components.path = Endpoints.geekPreviewParentItems
        components.queryItems?.append(URLQueryItem(name: .previewID, value: previewID))

        if let pageID = pageID {
            components.queryItems?.append(URLQueryItem(name: .pageID, value: pageID))
            return try await fetchAndDecode(from: components, cacheMode: cacheMode)
        } else {
            var results = [GeekPreviewParentItem]()
            var page = 1
            while true {
                do {
                    let items = try await fetchPreviewParentItemsForPreview(with: previewID, pageID: page, cacheMode: cacheMode)
                    if items.isEmpty {
                        break
                    }
                    results.append(contentsOf: items)
                } catch {
                    break
                }
                page += 1
            }
            return results
        }
    }

    public func fetchPreviewItemsForPreview(with previewID: String, pageID: Int? = nil, cacheMode: CacheMode = .normal) async throws -> [GeekPreviewItem] {
        var components = self.components
        components.path = Endpoints.geekPreviewItems
        components.queryItems?.append(URLQueryItem(name: .previewID, value: previewID))

        if let pageID = pageID {
            components.queryItems?.append(URLQueryItem(name: .pageID, value: pageID))
            return try await fetchAndDecode(from: components, cacheMode: cacheMode)
        } else {
            var results = [GeekPreviewItem]()
            var page = 1
            while true {
                do {
                    let items = try await fetchPreviewItemsForPreview(with: previewID, pageID: page, cacheMode: cacheMode)
                    if items.isEmpty {
                        break
                    }
                    results.append(contentsOf: items)
                } catch {
                    break
                }
                page += 1
            }
            return results
        }
    }

    // MARK: - User State

    public typealias SessionID = String
    public func login(using credentials: BGGCredential) async throws -> SessionID {
        var components = self.components
        components.host = "boardgamegeek.com"
        components.path = Endpoints.login
        components.queryItems = []

        guard let url = components.url else { throw APIError.badURL }
        var request = URLRequest(url: url)
        request.httpBody = try JSONEncoder().encode(BGGCredentialContainer(credential: credentials))
        request.httpMethod = "POST"

        let (_, response) = try await session.data(for: request)
        guard let response = response as? HTTPURLResponse else { throw APIError.networkError }
        guard let headerFields = response.allHeaderFields as? [String: String] else { throw APIError.networkError }

        print(headerFields)

        let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: response.url!)
        guard let sessionID = cookies.first(where: { $0.name == "SessionID" }) else { throw APIError.networkError }

        return sessionID.value
    }

    public func loginStatus(sessionID: SessionID) async throws -> Bool {
        var components = self.components
        components.host = "boardgamegeek.com"
        components.path = Endpoints.currentUser
        components.queryItems = []

        return try await fetchAndDecode(BGGUserStatus.self, from: components, headerFields: ["Authorization": "GeekAuth \(sessionID)"]).loggedIn
    }

    public func logout(sessionID: SessionID) async throws {
        var components = self.components
        components.host = "boardgamegeek.com"
        components.path = Endpoints.logout
        components.queryItems = []

        guard let url = components.url else { throw APIError.badURL }
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = ["Authorization": "GeekAuth \(sessionID)"]

        let (_, response) = try await session.data(for: request)
        guard let response = response as? HTTPURLResponse else { throw APIError.networkError }
        switch response.statusCode {
            case 200...299:
                return
            default:
                throw APIError.invalidServerResponse(response.statusCode)
        }
    }

    // MARK: - Helper Methods
    private func invalidate(components: URLComponents, headerFields: [String: String]? = nil) {
        guard let url = components.url else { return }
        var request = URLRequest(url: url)
        if let headerFields = headerFields {
            request.allHTTPHeaderFields = [:]
            for (key, value) in headerFields {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }

        cache.removeCachedResponse(for: request)
    }

    // swiftlint: disable cyclomatic_complexity
    private func fetchAndDecode<T: Decodable>(_ type: T.Type = T.self, from components: URLComponents, headerFields: [String: String]? = nil, decoderType: DecoderType = .json, cacheMode: CacheMode = .noCache) async throws -> T {
        guard let url = components.url else { throw APIError.badURL }
        var request = URLRequest(url: url)
        if let headerFields = headerFields {
            request.allHTTPHeaderFields = [:]
            for (key, value) in headerFields {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }

        switch cacheMode {
            case .invalidate:
                logger.info("Invalidating Cache!")
                invalidate(components: components, headerFields: headerFields)
            case .normal:
                if let cachedData = cache.cachedResponse(for: request) {
                    logger.info("Cache Hit!")
                    return decoderType == .json ? try jsonDecoder.decode(T.self, from: cachedData.data) : try xmlDecoder.decode(T.self, from: cachedData.data)
                }
                logger.info("Cache Miss!")
            default:
                break
        }

        let (data, response) = try await session.data(for: request)
        guard let response = response as? HTTPURLResponse else { throw APIError.networkError }

        if cacheMode != .noCache {
            let cachedResponse = CachedURLResponse(response: response, data: data)
            cache.storeCachedResponse(cachedResponse, for: request)
        }

        switch response.statusCode {
            case 200...299:
#if DEBUG
                // swiftlint: disable force_try
                return decoderType == .json ? try! jsonDecoder.decode(T.self, from: data) : try! xmlDecoder.decode(T.self, from: data)
                // swiftlint: enable force_try
#else
                return decoderType == .json ? try jsonDecoder.decode(T.self, from: data) : try xmlDecoder.decode(T.self, from: data)
#endif
            default:
                throw APIError.invalidServerResponse(response.statusCode)
        }
    }
    // swiftlint: enable cyclomatic_complexity

}

// MARK: - Error Specification
extension BGGAPI {
    /// Enum representing the types of Error that the `BGGAPI` can throw
    public enum APIError: Error, LocalizedError {

        /// Error when a malformed URL is used.
        case badURL

        /// Error when the server returns a status code other than 200.
        ///
        /// The associated value contains the actual error from the server.
        case invalidServerResponse(Int)

        /// Error when the server returns no data.
        case noDataReturned

        /// Error when the server does not reutrn enough data to parse.
        case insufficientDataReturned

        /// Error when the server was unreachable
        case networkError

        /// Error when the JSON could not be parsed.
        ///
        /// The associated value contains the actual error from the `JSONDecoder`
        case jsonFailure(Error)

        /// Generic error to be used when none of the other error cases are valid.
        case genericError(Error)

        /// Retreive the localized description for this error.
        public var errorDescription: String? {
            switch self {
                case .badURL:
                    return "There was a problem forming the URL."
                case .invalidServerResponse(let statusCode):
                    return "The server responded with an invalid status code: \(statusCode)"
                case .noDataReturned:
                    return "The API call was successful, but no data was returned."
                case .insufficientDataReturned:
                    return """
                The API call was successful, and data was returned,
                but the data returned wasn't sufficient to complete the call.
                """
                case .jsonFailure(let error):
                    return "There was a problem decoding the JSON data: \(error.localizedDescription)"
                case .genericError(let error):
                    return "An Error Occured: \(error.localizedDescription)"
                case .networkError:
                    return "A network error occured."
            }
        }
    }
}

extension BGGAPI {
    public enum CacheMode {
        case noCache
        case normal
        case invalidate
    }
}
