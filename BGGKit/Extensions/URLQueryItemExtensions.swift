//
//  URLQueryItemExtensions.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation

extension URLQueryItem {
    init(name: BGGAPI.QueryItemName, value: String?) {
        self.init(name: name.rawValue, value: value)
    }

    init(name: BGGAPI.QueryItemName, value: Int) {
        self.init(name: name.rawValue, value: String(value))
    }
}
