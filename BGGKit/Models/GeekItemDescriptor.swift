//
//  GeekItemDescriptor.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation

public struct GeekItemDescriptor: Codable, Equatable, Sendable {
    public let name: String
    public let displayValue: String
}
