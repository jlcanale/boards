//
//  GeekThread.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import Foundation
@preconcurrency import BetterCodable

public struct GeekThread: Codable, Identifiable, Equatable, BGGLinkable, Sendable {
    public static func == (lhs: GeekThread, rhs: GeekThread) -> Bool {
        lhs.id == rhs.id
    }

    public var id: String { threadID }

    public let threadID: String
    public let forumID: String
    public let objectType: GeekItemType
    public let objectID: String
    public let userID: String
    @DateValue<YMDTDecodingStrategy> public var postDate: Date
    @LosslessValue public var postCount: String
    @DateValue<YMDTDecodingStrategy> public var lastPostDate: Date
    public let lastPostUserID: String
    public let lastPostArticleID: String
    public let hidden: String
    public let pin: String
    public let locked: String
    public let subject: String
    @DateValue<YMDTDecodingStrategy> public var threadPostDate: Date
    @DateValue<YMDTDecodingStrategy> public var threadLastPostDate: Date
    @LosslessValue public var thumbCount: String
    public let forumTitle: String
    public let forumUserID: String
    public let linkName: String
    public let urlReference: String
    public let user: SimpleGeekUser?

    public var threadCreatedRelativeString: String {
        let formatter = RelativeDateTimeFormatter()
        let relativeDateString = formatter.localizedString(for: postDate, relativeTo: Date())
        return relativeDateString
    }

    enum CodingKeys: String, CodingKey {
        case threadID = "threadid"
        case forumID = "forumid"
        case objectType = "objecttype"
        case objectID = "objectid"
        case userID = "userid"
        case postDate = "postdate"
        case postCount = "numposts"
        case lastPostDate = "lastpostdate"
        case lastPostUserID = "lastpostuserid"
        case lastPostArticleID = "lastpostarticleid"
        case hidden
        case pin
        case locked
        case subject
        case threadPostDate = "thread_postdate"
        case threadLastPostDate = "thread_lastpostdate"
        case thumbCount = "numrecommend"
        case forumTitle = "forumtitle"
        case forumUserID = "forumuid"
        case linkName = "linkname"
        case urlReference = "href"
        case user
    }

    public static var sample: [GeekThread] {
        Bundle.main.decode(GeekThreadResponse.self, from: "GeekThread.json").threads
    }
}
