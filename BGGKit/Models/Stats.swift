//
//  Stats.swift
//  Boards
//
//  Created by Joseph Canale on 9/29/21.
//

import Foundation
import BetterCodable

public struct Stats: Codable, Equatable, Hashable {
    public let totalRatings: String
    public let averageRating: String
    public let bayesAverageRating: String
    public let standardDeviation: String
    public let averageWeight: String
    public let totalWeights: String
    public let totalGeekLists: String
    public let totalForTrade: String
    public let totalWantInTrade: String
    public let totalWishlist: String
    public let totalOwned: String
    public let totalPreviouslyOwned: String
    public let totalComments: String
    public let totalWishlistComments: String
    public let totalHasParts: String
    public let totalWantsParts: String
    public let totalViews: String
    public let playMonth: String
    @LosslessValue public var totalPlays: String
    @LosslessValue public var totalPlaysThisMonth: String
    public let totalFans: Int

    enum CodingKeys: String, CodingKey {
        case totalRatings = "usersrated"
        case averageRating = "average"
        case bayesAverageRating = "baverage"
        case standardDeviation = "stddev"
        case averageWeight = "avgweight"
        case totalWeights = "numweights"
        case totalGeekLists = "numgeeklists"
        case totalForTrade = "numtrading"
        case totalWantInTrade = "numwanting"
        case totalWishlist = "numwish"
        case totalOwned = "numowned"
        case totalPreviouslyOwned = "numprevowned"
        case totalComments = "numcomments"
        case totalWishlistComments = "numwishlistcomments"
        case totalHasParts = "numhasparts"
        case totalWantsParts = "numwantparts"
        case totalViews = "views"
        case playMonth = "playmonth"
        case totalPlays = "numplays"
        case totalPlaysThisMonth = "numplays_month"
        case totalFans = "numfans"
    }

    public static var sample: Stats {
        Bundle.main.decode(from: "Stats.json")
    }
}
