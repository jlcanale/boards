//
//  GeekItemType.swift
//  Boards
//
//  Created by Joseph Canale on 9/29/21.
//

import Foundation

public enum GeekItemType: String, Codable, Sendable, Hashable {
    case thing
    case person
    case company
    case family
    case subtype
    case property
    case version
    case weblink
}
