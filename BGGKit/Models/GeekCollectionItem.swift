//
//  GeekCollectionItem.swift
//  Boards
//
//  Created by Joseph Canale on 10/15/21.
//

import Foundation

public struct GeekCollectionItem: Codable {
    public var objectID: String
    public var status: GeekCollectionItemStatus

    enum CodingKeys: String, CodingKey {
        case objectID = "objectid"
        case status = "status"
    }
}
