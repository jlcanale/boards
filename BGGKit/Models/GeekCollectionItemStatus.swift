//
//  GeekCollectionItemStatus.swift
//  Boards
//
//  Created by Joseph Canale on 10/15/21.
//

import Foundation
import BetterCodable

public struct GeekCollectionItemStatus: Codable {
    @LosslessValue public var own: Int
    @LosslessValue public var previouslyOwned: Int
    @LosslessValue public var forTrade: Int
    @LosslessValue public var wantInTrade: Int
    @LosslessValue public var wantToPlay: Int
    @LosslessValue public var wantToBuy: Int
    @LosslessValue public var onWishlist: Int
    @LosslessValue public var preOrdered: Int

    enum CodingKeys: String, CodingKey {
        case own
        case previouslyOwned = "prevowned"
        case forTrade = "fortrade"
        case wantInTrade = "want"
        case wantToPlay = "wanttoplay"
        case wantToBuy = "wanttobuy"
        case onWishlist = "wishlist"
        case preOrdered = "preordered"
    }
}
