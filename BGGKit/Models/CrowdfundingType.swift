//
//  CrowdfundingType.swift
//  Boards
//
//  Created by Joseph Canale on 10/2/21.
//

import Foundation

public enum CrowdfundingType: String, Codable, Sendable {
    case kickstarter
    case gamefound
    case unknown

    public init?(rawValue: String) {
        switch rawValue {
            case "kickstarter": self = .kickstarter
            case "gamefound": self = .gamefound
            default: self = .unknown
        }
    }
}
