//
//  FetchTaskToken.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import Foundation

public protocol FetchTokenable {
    var token: Date { get }
    var key: String { get }
}

public struct BrowseFetchTaskToken: Equatable, FetchTokenable {
    public var key: String { category.rawValue }
    public var category: BrowseCategory
    public var token: Date
}

public struct FetchTaskToken: Equatable, FetchTokenable {
    public var token: Date
    public var key: String
}
