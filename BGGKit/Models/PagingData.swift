//
//  PagingData.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import Foundation

public actor PagingData {
    private(set) var currentPage = 1
    private(set) var hasReachedEnd = false

    public let itemsPerPage: Int
    public let maxPageLimit: Int
    public let userInfo: [String: Sendable]?

    public typealias DataFetchProvider<T> = @Sendable (Int, [String: Sendable]?) async throws -> [T]

    public init(itemsPerPage: Int, maxPageLimit: Int, userInfo: [String: Sendable]? = nil) {
        assert(itemsPerPage > 0 && maxPageLimit > 0, "Items per page must be greater than zero.")
        assert(maxPageLimit > 0, "Max page limit  and must be greater than zero.")

        self.itemsPerPage = itemsPerPage
        self.maxPageLimit = maxPageLimit
        self.userInfo = userInfo
    }

    public var nextPage: Int { currentPage + 1}

    public var shouldLoadNextPage: Bool {
        !hasReachedEnd && nextPage <= maxPageLimit
    }

    public func loadNextPage<T>(dataFetchProvider: @escaping DataFetchProvider<T>) async throws -> [T] {
        guard !Task.isCancelled else { return [] }
        print("PAGING: Current Page \(currentPage), next page: \(nextPage)")
        guard shouldLoadNextPage else { return [] }

        let nextPage = self.nextPage
        let items = try await dataFetchProvider(nextPage, userInfo)

        if Task.isCancelled || nextPage != self.nextPage { return [] }

        currentPage = nextPage
        hasReachedEnd = items.count < itemsPerPage

        print("PAGING: Fetch \(items.count) item(s) successfully, current page: \(currentPage)")

        return items
    }

    public func reset() {
        print("PAGING: RESET")
        currentPage = 0
        hasReachedEnd = false
    }
}
