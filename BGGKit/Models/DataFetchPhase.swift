//
//  DataFetchPhase.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation

enum DataFetchPhase<T> {
    case initial
    case empty
    case success(T)
    case fetchingNextPage(T)
    case failure(Error)

    var value: T? {
        if case .success(let value) = self {
            return value
        } else if case .fetchingNextPage(let value) = self {
            return value
        }
        return nil
    }
}
