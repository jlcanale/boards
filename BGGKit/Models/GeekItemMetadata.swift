//
//  GeekItemMetadata.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import Foundation

public struct GeekItemMetadata: Codable, Equatable, Identifiable, BGGLinkable, Metadata {
    public var id: String { objectID }
    public let name: String
    public let objectType: GeekItemType
    public let objectID: String
    public let primaryLink: Int
    public let urlReference: String

    enum CodingKeys: String, CodingKey {
        case name
        case objectType = "objecttype"
        case objectID = "objectid"
        case primaryLink = "primarylink"
        case urlReference = "href"
    }
}
