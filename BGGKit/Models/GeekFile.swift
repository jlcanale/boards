//
//  GeekFile.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import Foundation
@preconcurrency  import BetterCodable

public struct GeekFile: Codable, Identifiable, BGGLinkable, Equatable, Sendable {
    public var id: String { fileID }
    public let filePageID: String
    public let fileID: String
    public let fileName: String
    public let size: String
    public let description: GeekFileDescription
    public let userID: String
    public let user: SimpleGeekUser
    public let title: String
    @DateValue<YMDTDecodingStrategy> public var postDate: Date
    public let thumbsCount: String
    public let commentsCount: String
    public let urlReference: String
    public let language: String?

    enum CodingKeys: String, CodingKey {
        case filePageID = "filepageid"
        case fileID = "fileid"
        case fileName = "filename"
        case size
        case description
        case userID = "userid"
        case title
        case postDate = "postdate"
        case thumbsCount = "numpositive"
        case commentsCount = "numcomments"
        case urlReference = "href"
        case user
        case language
    }

    public var postDateRelativeString: String {
        RelativeDateTimeFormatter().localizedString(for: postDate, relativeTo: Date())
    }

    public static func == (lhs: GeekFile, rhs: GeekFile) -> Bool {
        lhs.id == rhs.id
    }

    public static var sample: [GeekFile] {
        Bundle.main.decode(GeekFileResponse.self, from: "GeekFile.json").files
    }
}

public struct GeekFileDescription: Codable, Sendable {
    public let rendered: String
}
