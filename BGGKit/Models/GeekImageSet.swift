//
//  GeekImageSet.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation

public struct GeekImageSet: Codable, Equatable, Sendable, Hashable {
    public enum GeekImageType: String, Codable, CaseIterable {
        case square30
        case square100
        case square200
        case mediaCard = "mediacard"
        case thumb
        case micro
        case square
        case squareFit = "squarefit"
        case tallThumb = "tallthumb"
        case previewThumb = "previewthumb"
        case original = "orig"
        case unknown

        public init(rawValue: String) {
            let cases = GeekImageType.allCases
            self = cases.first { $0.rawValue == rawValue } ?? .unknown
        }
    }

    public let source: String
    public let source2x: String

    public var sourceURL: URL? {
        URL(string: source)
    }

    public var source2xURL: URL? {
        URL(string: source2x)
    }

    enum CodingKeys: String, CodingKey {
        case source = "src"
        case source2x = "src@2x"
    }
}
