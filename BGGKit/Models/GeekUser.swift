//
//  GeekUser.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import Foundation
import BetterCodable

public struct GeekUser: Codable, Identifiable {
    public var id: String { username }
    public let userID: Int?
    public let username: String
    public let avatarURL: URL?
    public let urlReference: String
    public let firstName: String?
    public let lastName: String?
    public let city: String?
    public let state: String?
    public let country: String?
    public let isoCountry: String?
    @DateValue<YearMonthDayStrategy> public var registrationDate: Date
    public let designerID: Int?
    public let publisehrID: Int?
    public let hideName: Bool?

    enum CodingKeys: String, CodingKey {
        case userID = "userid"
        case username
        case avatarURL = "avatarurl_md"
        case urlReference = "href"
        case firstName = "firstname"
        case lastName = "lastname"
        case city
        case state
        case country
        case isoCountry = "isocountry"
        case registrationDate = "regdate"
        case designerID = "designerid"
        case publisehrID = "publisherid"
        case hideName
    }

    public static var sample: GeekUser {
        Bundle.main.decode(GeekUser.self, from: "GeekUser.json")
    }
}
