//
//  VideoGalleryType.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import Foundation

public enum VideoGalleryType: String, CaseIterable, Identifiable {
    public var id: Self { self }
    case all
    case review
    case session
    case instructional
    case unboxing
    case humor
    case other
}
