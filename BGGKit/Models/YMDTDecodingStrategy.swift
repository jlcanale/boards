//
//  YMDTDecodingStrategy.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import Foundation
import BetterCodable

public struct YMDTDecodingStrategy: DateValueCodableStrategy {
     private static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()

    public static func decode(_ value: String) throws -> Date {
        if let date = Self.dateFormatter.date(from: value) {
            return date
        } else {
            throw DecodingError.dataCorrupted(.init(codingPath: [], debugDescription: "Invalid Date Format!"))
        }
    }

    public static func encode(_ date: Date) -> String {
        return Self.dateFormatter.string(from: date)
    }
}
