//
//  GeekImage.swift
//  Boards
//
//  Created by Joseph Canale on 10/2/21.
//

import Foundation
@preconcurrency import BetterCodable

public struct GeekImage: Codable, Hashable, Sendable {
    public let imageID: String
    public let largeImageURL: URL
    public let imageURL: URL
    public let name: String?
    public let caption: String
    @DefaultCodable<ZeroStringDefault> public var thumbCount: String
    @DefaultCodable<ZeroStringDefault> public var commentCount: String
    public let uploadUser: SimpleGeekUser
    public let geekItemURLReference: String

    enum CodingKeys: String, CodingKey {
        case imageID = "imageid"
        case largeImageURL = "imageurl_lg"
        case imageURL = "imageurl"
        case name
        case caption
        case thumbCount = "numrecommend"
        case commentCount = "numcomments"
        case uploadUser = "user"
        case geekItemURLReference = "href"
    }

    public static var sample: [GeekImage] {
        Bundle.main.decode(GeekImageResponse.self, from: "GeekImage.json").images
    }
}
