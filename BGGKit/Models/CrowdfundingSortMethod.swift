//
//  CrowdfundingSortMethod.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import Foundation

public enum CrowdfundingSortMethod: String, CaseIterable, Identifiable, Equatable {
    public var id: Self { self }
    case endingSoon = "Ending Soon"
    case backersCount = "Backers Count"
    case pledgedAmount = "Pledged Amount"
    case percentBacked = "Percent Backed"
}
