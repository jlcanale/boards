//
//  GeekFilePage.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import Foundation
import BetterCodable

public struct GeekFilePage: Codable, Identifiable {
    public let id: String
    public let fileName: String
    @DateValue<ISO8601Strategy> public var postDate: Date
    public let downloadCount: Int
    public let size: Int
    public let description: String

    enum CodingKeys: String, CodingKey {
        case id
        case fileName = "filename"
        case postDate = "postdate"
        case downloadCount
        case size
        case description
    }

    public var sizeString: String {
        Units(bytes: Int64(size)).getReadableUnit()
    }

    public static var sample: [GeekFilePage] {
        Bundle.main.decode(GeekFilePageResponse.self, from: "GeekFilePage.json").files
    }
}
