//
//  GeekPreviewUserInfo.swift
//  Boards
//
//  Created by Joe Canale on 7/7/22.
//

import Foundation

struct GeekPreviewUserInfo: Codable {
    let username: String
    let userID: String
    let savedItemCount: Int
    let items: [String: UserInfo]

    enum CodingKeys: String, CodingKey {
        case username
        case userID = "userid"
        case savedItemCount = "num_saved_items"
        case items
    }
}

extension GeekPreviewUserInfo {
    struct UserInfo: Codable {
        let userInfoID: String
        let itemID: String
        let notes: String
        let priority: GeekPreviewItem.Priority

        enum CodingKeys: String, CodingKey {
            case userInfoID = "userinfoid"
            case itemID = "itemid"
            case notes
            case priority
        }
    }
}
