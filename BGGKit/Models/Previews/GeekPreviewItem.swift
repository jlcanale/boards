//
//  GeekPreviewItem.swift
//  BGGKit
//
//  Created by Joe Canale on 6/10/22.
//

import Foundation
import BetterCodable

public struct GeekPreviewItem: Codable, Identifiable, BGGLinkable {
    public var id: String { geekPreviewItemID }
    public var geekPreviewItemID: String
    public var previewID: String
    public var userID: String
    public var versionID: String
    public var objectType: GeekItemType
    public var objectID: String
    public var msrpCurrency: Currency
    public var msrp: Double
    public var showPriceCurrency: Currency
    public var showPrice: Double
    public var videoID: String
    public var location: String
    public var availabilityStatus: String
    public var prettyAvailibilityStatus: String?
    public var itemDescription: String
    @DateValue<ISO8601Strategy> public var dateCreated: Date
    public var dateUpdated: Date?
    public var urlReference: String
    public var reactions: Reactions
    public var geekItem: GeekPreviewItem.GeekItemContainer
    public var stats: GeekPreviewItem.Stats
    public var versionContainer: GeekPreviewItem.VersionContainer?

    enum CodingKeys: String, CodingKey {
        case geekPreviewItemID = "itemid"
        case previewID = "previewid"
        case userID = "userid"
        case versionID = "versionid"
        case objectType = "objecttype"
        case objectID = "objectid"
        case msrpCurrency = "msrp_currency"
        case msrp
        case showPriceCurrency = "showprice_currency"
        case showPrice = "showprice"
        case videoID = "videoid"
        case location
        case availabilityStatus = "availability_status"
        case prettyAvailibilityStatus = "pretty_availability_status"
        case itemDescription = "description"
        case dateCreated = "date_created"
        case dateUpdated = "date_updated"
        case urlReference = "href"
        case reactions
        case geekItem = "geekitem"
        case stats
        case versionContainer = "version"
    }

    static var sample: GeekPreviewItem {
        Bundle.main.decode(GeekPreviewItem.self, from: "GeekPreviewItem.json")
    }
}

extension GeekPreviewItem {
    public struct Reactions: Codable {
        public var thumbs: Int
    }

    public struct Stats: Codable {
        var mustHave: Int
        var interested: Int
        var undecided: Int

        enum CodingKeys: String, CodingKey {
            case mustHave = "musthave"
            case interested
            case undecided
        }
    }

    public struct GeekItemContainer: Codable {
        public var item: GeekPreviewItem.GeekItem
    }

    public struct DynamicInfoContainer: Codable {
        public var item: DynamicInfo
    }

    public struct GeekItem: Codable, BGGLinkable, GameInfo {
        let optionalUrlReference: String?
        public let subtypes: [String]?
        public let yearPublished: String?
        public let minPlayers: String?
        public let maxPlayers: String?
        public let minPlaytime: String?
        public let maxPlaytime: String?
        public let minAge: String?
        public let shortDescription: String?
        public let links: CodableDictionary<GeekItemLink, [GeekItemMetadata]>?
        public let primaryName: GeekName
        // @LosslessValue public var imageID: String
        public let images: [String: URL]?
        public let dynamicinfo: DynamicInfoContainer?

        public var urlReference: String {
            optionalUrlReference ?? "https://boardgamegeek.com"
        }

        public var minAgeText: String {
            guard let minAge = minAge else { return "NA"}
            return "\(minAge)+"
        }

        enum CodingKeys: String, CodingKey {
            case optionalUrlReference = "href"
            case subtypes = "subtypes"
            case yearPublished = "yearpublished"
            case minPlayers = "minplayers"
            case maxPlayers = "maxplayers"
            case minPlaytime = "minplaytime"
            case maxPlaytime = "maxplaytime"
            case minAge = "minage"
            case shortDescription = "short_description"
            case links = "links"
            case primaryName = "primaryname"
            // case imageID = "imageid"
            case images = "images"
            case dynamicinfo = "dynamicinfo"
        }
    }

    public struct VersionContainer: Codable {
        public var item: Version
    }

    public struct Version: Codable {
        public var releaseDate: String

        enum CodingKeys: String, CodingKey {
            case releaseDate = "releasedate"
        }

        public var releaseDateText: String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            guard let releaseDate = dateFormatter.date(from: releaseDate) else { return "NA"}

            dateFormatter.dateFormat = "MMM yyyy"
            return dateFormatter.string(from: releaseDate)
        }
    }

    public enum Priority: Int, Codable {
        case mustHave = 1
        case interested = 2
        case undecided = 3
        case notInterested = 4
    }
}
