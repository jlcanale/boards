//
//  GeekPreview.swift
//  BGGKit
//
//  Created by Joe Canale on 6/7/22.
//

import Foundation

public struct GeekPreviewListingContainer: Codable {
    public let data: [GeekPreviewListing]

    public static var sample: [GeekPreviewListing] {
        Bundle.main.decode(GeekPreviewListingContainer.self, from: "GeekPreviewListing.json").data
    }
}

public struct GeekPreviewListing: Codable, Identifiable, Hashable, BGGLinkable {
    public let id: String
    public let urlReference: String
    public let name: String
    public let location: String
    public var externalURL: URL?
    public let shortDescription: String
    public var startDate: Date?
    public var endDate: Date?
    public var images: [GeekImageSet]

    private enum CodingKeys: String, CodingKey {
        case id
        case urlReference = "href"
        case name
        case location
        case externalURL = "externalUrl"
        case shortDescription = "description"
        case startDate
        case endDate
        case images
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let dateFormatter = DateFormatter.yearMonthDateFormatter

        self.id = try container.decode(String.self, forKey: .id)
        self.urlReference = try container.decode(String.self, forKey: .urlReference)
        self.name = try container.decode(String.self, forKey: .name)
        self.location = try container.decode(String.self, forKey: .location)
        self.externalURL = try? container.decode(URL.self, forKey: .externalURL)
        self.shortDescription = try container.decode(String.self, forKey: .shortDescription)

        let startDateString = (try? container.decode(String.self, forKey: .startDate)) ?? ""
        self.startDate = dateFormatter.date(from: startDateString)

        let endDateString = (try? container.decode(String.self, forKey: .endDate)) ?? ""
        self.endDate = dateFormatter.date(from: endDateString)

        self.images = try container.decode([GeekImageSet].self, forKey: .images)
    }

}
