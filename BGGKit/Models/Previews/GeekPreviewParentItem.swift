//
//  GeekPreviewParentItem.swift
//  BGGKit
//
//  Created by Joe Canale on 6/10/22.
//

import Foundation

public struct GeekPreviewParentItem: Codable, Identifiable, Hashable {
    public var id: String { self.parentItemID }
    public var parentItemID: String
    public var previewID: String
    public var objectType: GeekItemType
    public var objectID: String
    public var body: String
    public var location: String
    public var geekItem: GeekPreviewParentItem.GeekItem.Container
    public var previewItemIDs: [String]

    public var sectionTitle: String {
        "\(geekItem.item.primaryName.name) - Location: \(locationString)"
    }

    public var locationString: String {
        "\(location.isEmpty ? "Unknown" : location)"
    }

    enum CodingKeys: String, CodingKey {
        case parentItemID = "parentitemid"
        case previewID = "previewid"
        case objectType = "objecttype"
        case objectID = "objectid"
        case body
        case location
        case geekItem = "geekitem"
        case previewItemIDs = "previewitemids"
    }
}

extension GeekPreviewParentItem {
    public struct GeekItem: Codable, BGGLinkable, Hashable {
        public var urlReference: String
        public var primaryName: GeekName

        // public var imageID: String
        public var images: [GeekImageSet.GeekImageType: URL]

        enum CodingKeys: String, CodingKey {
            case urlReference = "href"
            case primaryName = "primaryname"
            // case imageID = "imageid"
            case images
        }

        public init(from decoder: Decoder) throws {
            let container: KeyedDecodingContainer<GeekPreviewParentItem.GeekItem.CodingKeys> = try decoder.container(keyedBy: GeekPreviewParentItem.GeekItem.CodingKeys.self)
            self.urlReference = try container.decode(String.self, forKey: GeekPreviewParentItem.GeekItem.CodingKeys.urlReference)
            self.primaryName = try container.decode(GeekName.self, forKey: GeekPreviewParentItem.GeekItem.CodingKeys.primaryName)
            // self.imageID = try container.decode(String.self, forKey: GeekPreviewParentItem.GeekItem.CodingKeys.imageID)

            let imageDictionary = try container.decodeIfPresent([String: URL].self, forKey: .images) ?? [:]
            self.images = imageDictionary.mapKeysAndValues { (GeekImageSet.GeekImageType(rawValue: $0.key), $0.value) }
        }
    }
}

extension GeekPreviewParentItem.GeekItem {
    public struct Container: Codable, Hashable {
        var item: GeekPreviewParentItem.GeekItem
    }
}
