//
//  GeekPreview.swift
//  BGGKit
//
//  Created by Joe Canale on 6/7/22.
//

import Foundation

public struct GeekPreview: Codable, Hashable, Identifiable, BGGLinkable {
    public var id: String { previewID }
    public let previewID: String
    public let userID: String
    public let imageID: String
    public let backgroundImageID: String
    public let title: String
    public let shortDescription: String
    public let location: String
    public let externalUrl: URL?
    public let startDate: Date?
    public let endDate: Date?
    public let dateCreated: Date?
    public let hidden: Bool
    public let images: [GeekImageSet.GeekImageType: URL]
    public let backgroundImages: [GeekImageSet.GeekImageType: URL]

    public let urlReference: String

    public var dateRangeString: String {
        var returnString = ""
        let dateFormatter = DateFormatter.mediumDateFormatter

        if let startDate = startDate {
            returnString += "\(dateFormatter.string(from: startDate))"
        }

        if let endDate = endDate {
            returnString += " - \(dateFormatter.string(from: endDate))"
        }

        return returnString
    }

    public var daysUntilEventString: String {
        guard let startDate = startDate else { return "" }
        let dateFormatter = RelativeDateTimeFormatter()
        return dateFormatter.localizedString(for: startDate, relativeTo: Date())
    }

    public var daysUntilEvent: Int {
        guard let startDate = startDate else { return -9999 }
        let currentCalendar = Calendar.current
        let start = currentCalendar.startOfDay(for: startDate)

        return Calendar.current.dateComponents([.day], from: Date(), to: start).day!
    }

    public init(geekPreviewListing: GeekPreviewListing) {
        self.previewID = geekPreviewListing.id
        self.userID = "0"
        self.imageID = "0"
        self.backgroundImageID = "0"
        self.title = geekPreviewListing.name
        self.shortDescription = geekPreviewListing.shortDescription
        self.location = geekPreviewListing.location
        self.externalUrl = geekPreviewListing.externalURL
        self.startDate = geekPreviewListing.startDate
        self.endDate = geekPreviewListing.endDate
        self.dateCreated = Date(timeIntervalSince1970: 0)
        self.hidden = false
        self.images = [:]
        self.urlReference = geekPreviewListing.urlReference
        self.backgroundImages = [:]

    }

    enum CodingKeys: String, CodingKey {
        case previewID = "previewid"
        case userID = "userid"
        case imageID = "imageid"
        case backgroundImageID = "background_imageid"
        case title
        case shortDescription = "description"
        case location
        case externalUrl = "url"
        case startDate = "start_date"
        case endDate = "end_date"
        case dateCreated = "date_created"
        case hidden
        case images
        case urlReference = "href"
        case backgroundImages = "background_images"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dateFormatter = DateFormatter.yearMonthDateFormatter

        self.previewID = try container.decode(String.self, forKey: .previewID)
        self.userID = try container.decode(String.self, forKey: .userID)
        self.imageID = try container.decode(String.self, forKey: .imageID)
        self.backgroundImageID = try container.decode(String.self, forKey: .backgroundImageID)
        self.title = try container.decode(String.self, forKey: .title)
        self.shortDescription = try container.decode(String.self, forKey: .shortDescription)
        self.location = try container.decode(String.self, forKey: .location)
        self.externalUrl = try? container.decode(URL.self, forKey: .externalUrl)

        let startDateString = (try? container.decode(String.self, forKey: .startDate)) ?? ""
        self.startDate = dateFormatter.date(from: startDateString)

        let endDateString = (try? container.decode(String.self, forKey: .endDate)) ?? ""
        self.endDate = dateFormatter.date(from: endDateString)

        let dateCreatedString = (try? container.decode(String.self, forKey: .dateCreated)) ?? ""
        self.dateCreated = dateFormatter.date(from: dateCreatedString)

        self.hidden = try container.decode(Bool.self, forKey: .hidden)

        let imageDictionary = try container.decodeIfPresent([String: URL].self, forKey: .images) ?? [:]
        self.images = imageDictionary.mapKeysAndValues { (GeekImageSet.GeekImageType(rawValue: $0.key), $0.value) }

        let backgroundImageDictoinary = try container.decodeIfPresent([String: URL].self, forKey: .backgroundImages) ?? [:]
        self.backgroundImages = backgroundImageDictoinary.mapKeysAndValues { (GeekImageSet.GeekImageType(rawValue: $0.key), $0.value) }

        self.urlReference = try container.decode(String.self, forKey: .urlReference)
    }

    public static var sample: GeekPreview {
        Bundle.main.decode(GeekPreview.self, from: "GeekPreview.json")
    }
}
