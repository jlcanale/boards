//
//  ImageGalleryType.swift
//  Boards
//
//  Created by Joseph Canale on 10/3/21.
//

import Foundation

public enum ImageGalleryType: String {
    case all
    case game
    case people
    case creative
}
