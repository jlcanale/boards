//
//  BGGUserStatus.swift
//  Boards
//
//  Created by Joseph Canale on 10/15/21.
//

import Foundation

struct BGGUserStatus: Codable {
    var loggedIn: Bool
}
