//
//  CodableDictionary.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import Foundation

public struct CodableDictionary<Key: Hashable, Value: Codable>: Codable, Equatable where Key: CodingKey {
    public static func == (lhs: CodableDictionary<Key, Value>, rhs: CodableDictionary<Key, Value>) -> Bool {
        lhs.decoded.keys == rhs.decoded.keys
    }

    public let decoded: [Key: Value]

    public init(_ decoded: [Key: Value]) {
        self.decoded = decoded
    }

    public init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: Key.self)

        decoded = Dictionary(uniqueKeysWithValues:
            try container.allKeys.lazy.map {
                (key: $0, value: try container.decode(Value.self, forKey: $0))
            }
        )
    }

    public subscript(key: Key) -> Value? {
        return decoded[key]
    }

    public func encode(to encoder: Encoder) throws {

        var container = encoder.container(keyedBy: Key.self)

        for (key, value) in decoded {
            try container.encode(value, forKey: key)
        }
    }
}
