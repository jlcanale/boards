//
//  GeekCollection.swift
//  Boards
//
//  Created by Joseph Canale on 10/15/21.
//

import Foundation
import XMLCoder
import BetterCodable

public struct GeekCollection: Codable {
    public let items: [GeekCollectionItem]

    enum CodingKeys: String, CodingKey {
        case items = "item"
    }

    public static var sample: GeekCollection {
        guard let url = Bundle.main.url(forResource: "Collection", withExtension: "xml") else {
            fatalError("Failed to locate Collection.xml in bundle.")
        }

        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load Collection.xml from bundle.")
        }
        // swiftlint: disable force_try
        return try! XMLDecoder().decode(GeekCollection.self, from: data)
        // swiftlint: enable force_try
    }
}
