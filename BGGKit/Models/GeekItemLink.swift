//
//  GeekItemLink.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import Foundation

public enum GeekItemLink: String, Codable, CaseIterable, Equatable, CodingKey, Identifiable {
    public var id: Self { self }
    case boardgame = "boardgame"
    case designer = "boardgamedesigner"
    case soloDesigner = "boardgamesolodesigner"
    case artist = "boardgameartist"
    case publisher = "boardgamepublisher"
    case developer = "boardgamedeveloper"
    case graphicDesigner = "boardgamegraphicdesigner"
    case sculptor = "boardgamesculptor"
    case editor = "boardgameeditor"
    case writer = "boardgamewriter"
    case insertDesigner = "boardgameinsertdesigner"
    case honor = "boardgamehonor"
    case category = "boardgamecategory"
    case mechanic = "boardgamemechanic"
    case expansion = "boardgameexpansion"
    case version = "boardgameversion"
    case expands = "expandsboardgame"
    case integratesWith = "boardgameintegration"
    case contains = "contains"
    case containedIn = "containedin"
    case reimplementedBy = "reimplementation"
    case reimplements = "reimplements"
    case family = "boardgamefamily"
    case videoGameBG = "videogamebg"
    case subdomain = "boardgamesubdomain"
    case accessory = "boardgameaccessory"
    case commerceWebLink = "commerceweblink"

    public static var usefulCases: [GeekItemLink] {
        var cases = GeekItemLink.allCases
        cases.removeAll([.commerceWebLink, .videoGameBG])
        return cases
    }

    public static var topCredits: [GeekItemLink] {
        [.designer, .artist, .publisher]
    }

    public static var fullCredits: [GeekItemLink] {
        [.designer, .soloDesigner, .artist, .publisher,
            .developer, .graphicDesigner, .sculptor, .editor,
            .writer, .insertDesigner, .category, .mechanic, .family]
    }

    public static var classifications: [GeekItemLink] {
        [.subdomain, .category, .mechanic, .family, .integratesWith, .reimplementedBy, .reimplements]
    }

    public var pluralTitle: String {
        switch self {
            case .boardgame:
                return "Boardgames"
            case .designer:
                return "Designers"
            case .soloDesigner:
                return "Solo Designers"
            case .artist:
                return "Artists"
            case .publisher:
                return "Publishers"
            case .developer:
                return "Developers"
            case .graphicDesigner:
                return "Graphic Designers"
            case .sculptor:
                return "Sculptors"
            case .editor:
                return "Editors"
            case .writer:
                return "Writers"
            case .insertDesigner:
                return "Insert Designers"
            case .honor:
                return "Honors"
            case .category:
                return "Categories"
            case .mechanic:
                return "Mechanics"
            case .expansion:
                return "Expansions"
            case .version:
                return "Versions"
            case .expands:
                return "Expands"
            case .integratesWith:
                return "Integrates With"
            case .contains:
                return "Contains"
            case .containedIn:
                return "Contained In"
            case .reimplementedBy:
                return "Reimplemented By"
            case .reimplements:
                return "Reimplements"
            case .family:
                return "Families"
            case .videoGameBG:
                return "Video Games"
            case .subdomain:
                return "Type"
            case .accessory:
                return "Accessories"
            case .commerceWebLink:
                return "Commerce Links"
        }
    }
}
