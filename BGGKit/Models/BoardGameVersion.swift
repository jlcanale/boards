//
//  BoardGameVersion.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import Foundation
import BetterCodable

public struct BoardgameVersion: Codable, Identifiable, Equatable {
    public var id: String { linkID }
    public let linkID: String
    public let name: String
    public let yearPublished: String
    public let releaseStatus: String
    public let releaseDate: String
    public let productCode: String
    public let images: [String: URL]?

    enum CodingKeys: String, CodingKey {
        case linkID = "linkid"
        case name
        case yearPublished = "yearpublished"
        case releaseStatus = "releasestatus"
        case releaseDate = "releasedate"
        case productCode = "productcode"
        case images
    }

    public var releaseDateText: String {
        releaseDate == "0000-00-00" ? "Unknown" : releaseDate
    }

    public var releaseStatusText: String {
        releaseStatus == "" ? "Unknown" : releaseStatus
    }

    public static var sample: [BoardgameVersion] {
        Bundle.main.decode(BoardgameVersionResponse.self, from: "BoardgameVersion.json").items
    }
}
