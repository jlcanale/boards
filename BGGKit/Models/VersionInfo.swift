//
//  VersionInfo.swift
//  Boards
//
//  Created by Joseph Canale on 10/2/21.
//

import Foundation
import BetterCodable

public struct VersionInfo: Codable, Equatable {
    public let orderURL: URL?
    public let orderType: CrowdfundingType?
    // @DateValue<YearMonthDayStrategy> var orderStartDate: Date?
    // @DateValue<YearMonthDayStrategy> var orderEndDate: Date?

    enum CodingKeys: String, CodingKey {
        case orderURL = "orderurl"
        case orderType = "ordertype"
    }
}
