//
//  Article.swift
//  Boards
//
//  Created by Joseph Canale on 10/1/21.
//

import Foundation
import BetterCodable

public struct Article: Codable, Identifiable {
    public let id: String
    public let firstPost: Bool
    public let urlReference: String
    public let collapsed: Bool
    public let rollsEnabled: Bool
    public let authorID: Int?
    @DateValue<ISO8601Strategy> public var postDate: Date
    @DateValue<ISO8601Strategy> public var lastEditDate: Date
    public let body: String
    public let rollsCount: Int
    public var user: GeekUser?

    enum CodingKeys: String, CodingKey {
        case id
        case firstPost
        case urlReference = "href"
        case collapsed
        case rollsEnabled
        case authorID = "author"
        case postDate = "postdate"
        case lastEditDate = "editdate"
        case body
        case rollsCount
    }

    public var postDateText: String {
        if Date().daysSince(postDate) < 7 {
            let formatter = RelativeDateTimeFormatter()
            return formatter.localizedString(for: postDate, relativeTo: Date())
        } else {
            let formatter = DateFormatter()
            if Date().year != postDate.year {
                formatter.dateFormat = "MMM dd ''yy"
            } else {
                formatter.dateFormat = "MMM dd"
            }
            return formatter.string(from: postDate)
        }
    }

    public static var sample: [Article] {
        Bundle.main.decode(ArticleResponse.self, from: "Article.json").articles
    }
}
