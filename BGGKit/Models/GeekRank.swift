//
//  GeekRank.swift
//  Boards
//
//  Created by Joseph Canale on 9/29/21.
//

import Foundation

public struct GeekRank: Codable, Equatable, Identifiable, Hashable, RankRepresentable {
    public var id: Int { objectID }
    public let prettyName: String
    public let shortPrettyName: String
    public let veryShortPrettyName: String
    public let subdomain: String?
    public let objectType: GeekItemType
    public let objectID: Int
    public let rank: String
    public let bayesAverage: String

    public var bayesAverageNumber: Int {
        let formatter = NumberFormatter()
        return formatter.number(from: bayesAverage)?.intValue ?? 0
    }

    enum CodingKeys: String, CodingKey {
        case prettyName = "prettyname"
        case shortPrettyName = "shortprettyname"
        case veryShortPrettyName = "veryshortprettyname"
        case subdomain = "subdomain"
        case objectType = "rankobjecttype"
        case objectID = "rankobjectid"
        case rank = "rank"
        case bayesAverage = "baverage"
    }
}
