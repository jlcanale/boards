//
//  GeekVideo.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import Foundation
import BetterCodable

public struct GeekVideo: Codable, Identifiable, Equatable {

    public var id: String { videoID }
    public let videoID: String
    public let videoHost: String
    public let externalVideoID: String
    public let title: String
    public let gallery: String
    public let images: [String: URL]
    public let user: SimpleGeekUser
    @DateValue<YMDTDecodingStrategy> public var postDate: Date

    enum CodingKeys: String, CodingKey {
        case videoID = "videoid"
        case videoHost = "videohost"
        case externalVideoID = "extvideoid"
        case title
        case images
        case gallery
        case postDate = "postdate"
        case user
    }

    public var postDateRelaviveText: String {
        RelativeDateTimeFormatter().localizedString(for: postDate, relativeTo: Date())
    }

    public static func == (lhs: GeekVideo, rhs: GeekVideo) -> Bool {
        lhs.id == rhs.id
    }

    public static var sample: [GeekVideo] {
        Bundle.main.decode(GeekVideoResponse.self, from: "GeekVideo.json").videos
    }
}
