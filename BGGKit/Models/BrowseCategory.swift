//
//  BrowseCategory.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation
import SFSafeSymbols

public enum BrowseCategory: String, CaseIterable, Identifiable {
    public var id: Self { self }
    case hotness
    case crowdfunding
    case newReleases

    public var text: String {
        switch self {
            case .hotness:
                return "Hotness"
            case .crowdfunding:
                return "Crowdfunding"
            case .newReleases:
                return "New Releases"
        }
    }

    public var systemSymbol: SFSymbol {
        switch self {
            case .hotness:
                return .flame
            case .crowdfunding:
                return .dollarsignCircle
            case .newReleases:
                return .calendar
        }
    }

    public static var menuItems: [MenuItem] {
        allCases.map { .browse($0) }
    }
}
