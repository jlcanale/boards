//
//  BetterCodableDefaults.swift
//  Boards
//
//  Created by Joseph Canale on 9/29/21.
//

import Foundation
import BetterCodable

public struct EmptyStringDefault: DefaultCodableStrategy {
    public static var defaultValue: String { return ""}
}

public struct ZeroStringDefault: DefaultCodableStrategy {
    public static var defaultValue: String { return "0"}
}
