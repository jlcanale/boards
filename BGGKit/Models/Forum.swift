//
//  Forum.swift
//  Boards
//
//  Created by Joseph Canale on 9/29/21.
//

import Foundation
import SwifterSwift

public struct Forum: Codable, Identifiable {
    public var id: String { forumID }
    public let title: String
    public let forumID: String
    public let postCount: Int
    public let threadCount: Int

    public init(title: String, threadCount: Int) {
        self.title = title
        self.forumID = "0"
        self.postCount = 0
        self.threadCount = threadCount
    }

    enum CodingKeys: String, CodingKey {
        case title
        case forumID = "forumid"
        case postCount = "numposts"
        case threadCount = "numthreads"
    }

    public static var sample: [Forum] {
        Bundle.main.decode(ForumResponse.self, from: "Forum.json").forums
    }
}

extension Array where Element == Forum {
    public var allForum: Forum {
        return Forum(title: "All", threadCount: self.sum(for: \.threadCount))
    }
}
