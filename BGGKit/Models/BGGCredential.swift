//
//  BGGCredentials.swift
//  Boards
//
//  Created by Joseph Canale on 10/15/21.
//

import Foundation

public struct BGGCredentialContainer: Codable {
    public let credentials: BGGCredential

    public init(credential: BGGCredential) {
        self.credentials = credential
    }
}

public struct BGGCredential: Codable {
    public let username: String
    public let password: String

    public init(username: String, password: String) {
        self.username = username
        self.password = password
    }
}
