//
//  DynamicInfo.swift
//  Boards
//
//  Created by Joseph Canale on 9/29/21.
//

import Foundation

public struct DynamicInfo: Codable, Equatable {
    public let ranks: [GeekRank]?
    public let stats: Stats

    enum CodingKeys: String, CodingKey {
        case ranks = "rankinfo"
        case stats
    }

    public static var sample: DynamicInfo {
        Bundle.main.decode(from: "DynamicInfo.json")
    }
}
