//
//  BoardgameExpansion.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import Foundation

public struct BoardgameExpansion: Codable, Identifiable, Equatable {
    public var id: String { objectID }
    public let objectID: String
    public let name: String
    public let yearPublished: String
    public let ratingsCount: String
    public let averageWeight: String
    public let commentsCount: String
    public let ownedCount: String
    public let previouslyOwnedCount: String
    public let forTradeCount: String
    public let wantInTradeCount: String
    public let wishlistCount: String
    public let images: [String: URL]?

    enum CodingKeys: String, CodingKey {
        case name
        case objectID = "objectid"
        case yearPublished = "yearpublished"
        case ratingsCount = "usersrated"
        case averageWeight = "avgweight"
        case commentsCount = "numcomments"
        case ownedCount = "numowned"
        case previouslyOwnedCount = "numprevowned"
        case forTradeCount = "numtrading"
        case wantInTradeCount = "numwanting"
        case wishlistCount = "numwish"
        case images
    }

    public static var sample: [BoardgameExpansion] {
        Bundle.main.decode(BoardgameExpansionResponse.self, from: "BoardgameExpansion.json").items
    }
}
