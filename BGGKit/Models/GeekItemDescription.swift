//
//  GeekItemDescription.swift
//  Boards
//
//  Created by Joe Canale on 7/5/22.
//

import Foundation

public struct GeekItemDescription: Codable {
    let description: String
}

extension GeekItemDescription {
    public struct Container: Codable {
        let item: GeekItemDescription
    }
}
