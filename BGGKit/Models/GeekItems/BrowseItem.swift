//
//  BrowseItem.swift
//  Boards
//
//  Created by Joseph Canale on 10/25/21.
//

import Foundation

extension URL: @unchecked Sendable {}

public struct BrowseItem: Codable, Sendable {
    public let id: String
    public let objectID: String?
    public let name: String
    public let yearPublished: String?
    public let description: String
    public let imageSets: [String: GeekImageSet]
    public let imageURL: URL?
    public let imageID: String?
    public let delta: Int?
    public let urlReference: String?

    public let descriptors: [GeekItemDescriptor]?
    public let publisherName: String?

    public let ends: String?
    public let progress: Double?
    public let currency: Currency?
    public let pledged: Double?
    public let backersCount: Int?
    public let orderURL: URL?
    public let orderType: CrowdfundingType?
    public let newReleaseItem: NewReleaseGeekItem?

    public init(_ hotnessItem: HotnessItem) {
        self.id = hotnessItem.id
        self.objectID = hotnessItem.objectID
        self.name = hotnessItem.name
        self.yearPublished = hotnessItem.yearPublished
        self.description = hotnessItem.description
        self.imageSets = hotnessItem.imageSets
        self.imageURL = hotnessItem.imageURL
        self.imageID = hotnessItem.imageID
        self.delta = hotnessItem.delta
        self.urlReference = hotnessItem.urlReference

        self.descriptors = nil
        self.publisherName = nil

        self.ends = nil
        self.progress = nil
        self.currency = nil
        self.pledged = nil
        self.backersCount = nil
        self.orderURL = nil
        self.orderType = nil
        self.newReleaseItem = nil
    }

    public init(_ crowdfundingItem: CrowdfundingItem) {
        self.id = crowdfundingItem.id
        self.objectID = crowdfundingItem.id
        self.name = crowdfundingItem.name
        self.yearPublished = crowdfundingItem.item?.yearPublishedString
        self.description = crowdfundingItem.description
        self.imageSets = crowdfundingItem.images
        self.imageURL = nil
        self.imageID = nil
        self.delta = nil
        self.urlReference = nil

        self.descriptors = crowdfundingItem.item?.descriptors
        self.publisherName = nil

        self.ends = crowdfundingItem.ends
        self.progress = crowdfundingItem.progress
        self.currency = crowdfundingItem.currency
        self.pledged = crowdfundingItem.pledged
        self.backersCount = crowdfundingItem.backersCount
        self.orderURL = crowdfundingItem.orderURL
        self.orderType = crowdfundingItem.orderType
        self.newReleaseItem = crowdfundingItem.item
    }

    public init(_ newReleaseItem: NewReleaseItem) {
        self.id = "\(newReleaseItem.id)"
        self.objectID = "\(newReleaseItem.id)"
        self.name = newReleaseItem.itemName
        self.yearPublished = newReleaseItem.item?.yearPublishedString
        self.description = newReleaseItem.description
        self.imageSets = [GeekImageSet.GeekImageType.square100.rawValue: newReleaseItem.image]
        self.imageURL = nil
        self.imageID = nil
        self.delta = nil
        self.urlReference = nil

        self.descriptors = newReleaseItem.item?.descriptors
        self.publisherName = newReleaseItem.publisherName

        self.ends = nil
        self.progress = nil
        self.currency = nil
        self.pledged = nil
        self.backersCount = nil
        self.orderURL = nil
        self.orderType = nil
        self.newReleaseItem = newReleaseItem.item
    }
}
