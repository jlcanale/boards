//
//  HotnessItem.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation
import BetterCodable

public struct HotnessItem: Codable, Equatable, Identifiable {
    public let id: String
    public let objectID: String
    public let name: String
    @DefaultCodable<EmptyStringDefault> public var yearPublished: String
    public let description: String
    public let imageSets: [String: GeekImageSet]
    public let imageURL: URL
    public let imageID: String
    public let delta: Int?
    public let urlReference: String?

    enum CodingKeys: String, CodingKey {
        case id
        case objectID = "objectid"
        case name
        case yearPublished = "yearpublished"
        case description
        case imageSets = "images"
        case imageURL = "imageurl"
        case imageID = "rep_imageid"
        case delta
        case urlReference = "href"
    }

    public init(_ browseItem: BrowseItem) {
        self.id = browseItem.id
        self.objectID = browseItem.objectID ?? browseItem.id
        self.name = browseItem.name
        self.yearPublished = browseItem.yearPublished ?? ""
        self.description = browseItem.description
        self.imageSets = browseItem.imageSets
        self.imageURL = browseItem.imageURL ?? URL(string: "https://boardgamegeek.com")!
        self.imageID = browseItem.imageID ?? ""
        self.delta = browseItem.delta
        self.urlReference = browseItem.urlReference ?? ""
    }

    public var bggURL: URL {
        URL(string: "https://boardgamegeek.com/\(urlReference ?? "")") ?? URL(string: "https://boardgamegeek.com")!
    }

    public static var sample: [HotnessItem] {
        Bundle.main.decode(HotnessResponse.self, from: "Hotness.json").items
    }
}
