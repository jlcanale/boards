//
//  GeekItem.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation
import BetterCodable

public struct GeekItem: Codable, Equatable, Identifiable, BGGLinkable {
    public let id: String?
    public let objectID: Int
    public let name: String
    public var yearPublished: String?
    public let imageID: String
    public let imageURL: URL
    public let imageSets: [String: GeekImageSet]
    public let urlReference: String
    public let fullDescription: String
    public let primaryName: GeekName?
    public let versionInfo: VersionInfo?
    @DefaultCodable<EmptyStringDefault> public var shortDescription: String
    public let minPlayers: String?
    public let maxPlayers: String?
    public let minPlaytime: String?
    public let maxPlaytime: String?
    public let minimumAge: String?
    public let links: CodableDictionary<GeekItemLink, [GeekItemMetadata]>
    public var dynamicInfo: DynamicInfo?

    enum CodingKeys: String, CodingKey {
        case id
        case objectID = "objectid"
        case name
        case yearPublished = "yearpublished"
        case imageID = "imageid"
        case imageURL = "imageurl"
        case imageSets
        case urlReference = "href"
        case fullDescription = "description"
        case primaryName = "primaryname"
        case versionInfo = "versioninfo"
        case shortDescription = "short_description"
        case links
        case minPlayers = "minplayers"
        case maxPlayers = "maxplayers"
        case minPlaytime = "minplaytime"
        case maxPlaytime = "maxplaytime"
        case minimumAge = "minage"
        case dynamicInfo
    }

    public var yearPublishedText: String {
        yearPublished ?? ""
    }

    public var playerCountText: String {
        if let minPlayers = minPlayers, let maxPlayers = maxPlayers, maxPlayers != minPlayers {
            return "\(minPlayers)-\(maxPlayers)"
        } else if let minPlayers = minPlayers {
            return "\(minPlayers)"
        } else if let maxPlayers = maxPlayers {
            return "\(maxPlayers)"
        } else {
            return ""
        }
    }

    public var playTimeText: String {
        if let minPlaytime = minPlaytime, let maxPlaytime = maxPlaytime, maxPlaytime != minPlaytime {
            return "\(minPlaytime)-\(maxPlaytime)"
        } else if let minPlaytime = minPlaytime {
            return "\(minPlaytime)"
        } else if let maxPlayers = maxPlaytime {
            return "\(maxPlayers)"
        } else {
            return ""
        }
    }

    public var weightText: String {
        String(dynamicInfo?.stats.averageWeight.prefix(4) ?? "")
    }

    public var rankText: String {
        if let rank = dynamicInfo?.ranks?.first(where: { $0.veryShortPrettyName == "Overall" }) {
            return rank.rank
        } else {
            return "N/A"
        }
    }

    public var overallRatingText: String {
        if let rank = dynamicInfo?.ranks?.first(where: { $0.veryShortPrettyName == "Overall" }) {
            return String(rank.bayesAverage.prefix(4))
        } else {
            return "N/A"
        }
    }

    public var attributedDescription: AttributedString {
        guard let data = fullDescription.data(using: .utf8) else { return ""}
        let attributedString = (try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)) ?? NSAttributedString()
        return AttributedString(attributedString)
    }

    public var minimumAgeText: String {
        "\(minimumAge ?? "12")+"
    }

    public static var sampleBoardgame: GeekItem {
        var geekItem = Bundle.main.decode(GeekItemResponse.self, from: "Boardgame.json").item
        let dynamicInfo = Bundle.main.decode(DynamicInfoResponse.self, from: "DynamicInfo.json").item
        geekItem.dynamicInfo = dynamicInfo
        return geekItem
    }

    public static var sampleCompany: GeekItem {
        Bundle.main.decode(GeekItemResponse.self, from: "Company.json").item
    }
}
