//
//  NewReleaseItem.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation

public struct NewReleaseItem: Codable, Identifiable {
    public var id: Int {
        var hasher = Hasher()
        hasher.combine(itemName)
        hasher.combine(publisherName)
        hasher.combine(description)
        return hasher.finalize()
    }

    public let item: NewReleaseGeekItem?
    public let itemName: String
    public let publisherName: String
    public let image: GeekImageSet
    public let description: String

    public init(_ browseItem: BrowseItem) {
        self.item = browseItem.newReleaseItem
        self.itemName = browseItem.name
        self.publisherName = browseItem.publisherName ?? ""
        self.image = browseItem.imageSets.first!.value
        self.description = browseItem.description
    }

    public static var sample: [NewReleaseItem] {
        Bundle.main.decode(from: "NewReleases.json")
    }
}
