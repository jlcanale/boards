//
//  CrowdfundingItem.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation

public struct CrowdfundingItem: Codable, Identifiable, Equatable {
    public var id: String { name }

    public let name: String
    public let ends: String
    public let progress: Double
    public let currency: Currency
    public let pledged: Double
    public let images: [String: GeekImageSet]
    public let backersCount: Int
    public let orderURL: URL
    public let orderType: CrowdfundingType
    public let description: String
    public let item: NewReleaseGeekItem?

    enum CodingKeys: String, CodingKey {
        case name
        case ends = "endDate"
        case progress
        case currency
        case pledged
        case images
        case backersCount
        case orderURL = "orderUrl"
        case orderType
        case description
        case item
    }

    public init(_ browseItem: BrowseItem) {
        self.name = browseItem.name
        self.ends = browseItem.ends ?? ""
        self.progress = browseItem.progress ?? 0.0
        self.currency = browseItem.currency ?? .USD
        self.pledged = browseItem.pledged ?? 0.0
        self.images = browseItem.imageSets
        self.backersCount = browseItem.backersCount ?? 0
        self.orderURL = browseItem.orderURL ?? URL(string: "https://boardgamegeek.com")!
        self.orderType = browseItem.orderType ?? .unknown
        self.description = browseItem.description
        self.item = browseItem.newReleaseItem
    }

    public static var sample: [CrowdfundingItem] {
        Bundle.main.decode(from: "Crowdfunding.json")
    }

    public var endingSoon: Bool {
        let interval = endDate.timeIntervalSince(Date())
        return (interval < 86400 && interval > 0) ? true : false
    }

    public var ended: Bool {
        endDate.timeIntervalSince(Date()) < 0 ? true : false
    }

    public var pledgedText: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        return "\(currency.symbol)\(formatter.string(from: pledged as NSNumber) ?? "0")"
    }

    public var endDate: Date {
        let formatter = ISO8601DateFormatter()
        return formatter.date(from: ends) ?? Date()
    }

    public var endsInText: String {
        let formatter = RelativeDateTimeFormatter()
        let relativeDateString = formatter.localizedString(for: endDate, relativeTo: Date())
        return "\(ended ? "Ended" : "Ends") \(relativeDateString)"
    }
}
