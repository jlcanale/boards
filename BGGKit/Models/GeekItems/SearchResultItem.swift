//
//  SearchResultItem.swift
//  Boards
//
//  Created by Joseph Canale on 10/6/21.
//

import Foundation

public struct SearchResultItem: Codable, Equatable, Identifiable, BGGLinkable {
    public var id: String
    public let objectID: String
    public let name: String
    public var yearPublished: Int
    public let imageID: Int
    public let urlReference: String

    enum CodingKeys: String, CodingKey {
        case id
        case objectID = "objectid"
        case name
        case yearPublished = "yearpublished"
        case imageID = "rep_imageid"
        case urlReference = "href"
    }

    public static var sampleSearch: [SearchResultItem] {
        Bundle.main.decode(SearchResponse.self, from: "Search.json").items
    }
}
