//
//  NewReleaseGeekItem.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import Foundation

public struct NewReleaseGeekItem: Codable, Equatable, Identifiable, Sendable {
    public let id: String
    public let name: String
    public let descriptors: [GeekItemDescriptor]?
    public let imageID: Int
    public let imageSets: [String: GeekImageSet]
    public let urlReference: String

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case descriptors
        case imageSets
        case imageID = "imageid"
        case urlReference = "href"
    }

    public var yearPublishedString: String {
        if let yearPublished = descriptors?.first(where: { $0.name == "yearpublished" }) {
            return yearPublished.displayValue
        } else {
            return ""
        }
    }

    public var bggURL: URL {
        URL(string: "https://boardgamegeek.com/\(urlReference)") ?? URL(string: "https://boardgamegeek.com")!
    }
}
