//
//  GeekName.swift
//  BGGKit
//
//  Created by Joe Canale on 6/10/22.
//

import Foundation

public struct GeekName: Codable, Equatable, Hashable {
    public var nameID: String
    public var name: String
    public var sortIndex: String
    public var primaryName: String
    public var translit: String

    enum CodingKeys: String, CodingKey {
        case nameID = "nameid"
        case name
        case sortIndex = "sortindex"
        case primaryName = "primaryname"
        case translit
    }
}
