//
//  GeekThreadUser.swift
//  Boards
//
//  Created by Joseph Canale on 10/1/21.
//

import Foundation

public struct SimpleGeekUser: Codable, Identifiable, Hashable, Sendable {
    public var id: String { username }
    public let username: String
    public let avatarURL: URL?

    enum CodingKeys: String, CodingKey {
        case username
        case avatarURL = "avatarurl_md"
    }

    public static var sample: SimpleGeekUser {
        Bundle.main.decode(from: "GeekUser.json")
    }
}
