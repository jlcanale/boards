//
//  GeekThreadResponse.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import Foundation

struct GeekThreadResponse: Codable {
    let threads: [GeekThread]
}

struct ForumResponse: Codable {
    let forums: [Forum]
}

struct HotnessResponse: Codable {
    let items: [HotnessItem]
}

struct GeekItemResponse: Codable {
    let item: GeekItem
}

struct SearchResponse: Codable {
    let items: [SearchResultItem]
}

struct DynamicInfoResponse: Codable {
    let item: DynamicInfo
}

struct ArticleResponse: Codable {
    let articles: [Article]
}

struct GeekImageResponse: Codable {
    let images: [GeekImage]
}

struct GeekFileResponse: Codable {
    let files: [GeekFile]
}

struct GeekFilePageResponse: Codable {
    let files: [GeekFilePage]
}

struct BoardgameVersionResponse: Codable {
    let items: [BoardgameVersion]
}

struct BoardgameExpansionResponse: Codable {
    let items: [BoardgameExpansion]
}

struct GeekVideoResponse: Codable {
    let videos: [GeekVideo]
}
