//
//  MainView.swift
//  Boards (macOS)
//
//  Created by Joseph Canale on 10/14/21.
//

import SwiftUI

struct MainView: View {

    @SceneStorage("item_selection") private var selectedMenuItemId: MenuItem.ID?
    private var selection: Binding<MenuItem.ID?> {
        Binding {
            selectedMenuItemId ?? MenuItem.library.id
        } set: { newValue in
            if let newValue = newValue {
                selectedMenuItemId = newValue
            }
        }
    }

    var body: some View {
        NavigationView {
            SidebarList(selection: selection)
                .toolbar {
                    ToolbarItem(placement: .navigation) {
                        Button(action: toggleSidebar) {
                            Image(systemSymbol: .sidebarLeft)
                        }

                    }
                }
        }
        .frame(minWidth: 1200, minHeight: 386)

    }

    private func toggleSidebar() {
        NSApp.keyWindow?.firstResponder?.tryToPerform(#selector(NSSplitViewController.toggleSidebar(_:)), with: nil)
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
