//
//  SidebarListView.swift
//  Boards (macOS)
//
//  Created by Joseph Canale on 10/20/21.
//

import SwiftUI

struct SidebarList: View {

    @Binding var selection: MenuItem.ID?
    @StateObject var browseViewModel = BrowseViewModel()

    var body: some View {
        ZStack {
            navigationLink
                .opacity(0)
            List(selection: $selection) {
                Section {
                    listRow(MenuItem.library)
                        .tag(MenuItem.library.id)
                } header: {
                    Text("Boards")
                }
                .collapsible(false)

                Section {
                    ForEach(BrowseCategory.menuItems) { menuItem in
                        listRow(menuItem)
                            .tag(menuItem.id)
                            .onTapGesture {
                                browseViewModel.fetchTaskToken.category = BrowseCategory(rawValue: menuItem.id)!
                                selection = menuItem.id
                            }
                    }
                } header: {
                    Text("Browse")
                }
                .collapsible(false)

            }
            .listStyle(.sidebar)
            .frame(minWidth: 220)
            .padding(.top)
        }
    }

    @ViewBuilder
    private func viewFor(menuItem: MenuItem) -> some View {
        Group {
            switch menuItem {
                case .library:
                    LibraryList()
                case .search:
                    Text("Search")
                case .browse(let browseType):
                    switch browseType {
                        case .hotness:
                            NavigationView {
                                HotnessItemList(geekItems: browseViewModel.hotnessItems)
                                    .frame(minWidth: 375)
                            }
                        case .crowdfunding:
                            NavigationView {
                                CrowdfundingItemList(crowdfundingItems: browseViewModel.crowdfundingItems)
                                    .frame(minWidth: 375)
                            }
                        case .newReleases:
                            NavigationView {
                                NewReleasesItemList(items: browseViewModel.newReleaseItems)
                                    .frame(minWidth: 375)
                            }
                    }
            }
        }
        .overlay(overlayView)
        .id(menuItem.id)
        .task(id: browseViewModel.fetchTaskToken, loadTask)
    }

    @Sendable
    func loadTask() async {
        await browseViewModel.loadData()
    }

    @ViewBuilder
    private var navigationLink: some View {
        if let selectedMenuItem = MenuItem(id: selection) {
            NavigationLink(destination: viewFor(menuItem: selectedMenuItem), tag: selectedMenuItem.id, selection: $selection) {
                EmptyView()
            }
        }
    }

    @ViewBuilder
    private var overlayView: some View {
        if browseViewModel.fetchTaskToken.category == .hotness {
            switch browseViewModel.hotnessPhase {
                case .empty:
                    ProgressView()
                case .success(let geekItems) where geekItems.isEmpty:
                    EmptyPlaceholderView(text: "No Items", image: nil)
                case .failure(let error):
                    RetryView(text: error.localizedDescription, retryAction: {})
                default:
                    EmptyView()
            }
        } else if browseViewModel.fetchTaskToken.category == .crowdfunding {
            switch browseViewModel.crowdfundingPhase {
                case .empty:
                    ProgressView()
                case .success(let items) where items.isEmpty:
                    EmptyPlaceholderView(text: "No Items", image: nil)
                case .failure(let error):
                    RetryView(text: error.localizedDescription, retryAction: {})
                default:
                    EmptyView()
            }
        } else if browseViewModel.fetchTaskToken.category == .newReleases {
            switch browseViewModel.newReleasesPhase {
                case .empty:
                    ProgressView()
                case .success(let items) where items.isEmpty:
                    EmptyPlaceholderView(text: "No new releases this week", image: nil)
                case .failure(let error):
                    RetryView(text: error.localizedDescription, retryAction: {})
                default:
                    EmptyView()
            }
        }

    }

    private func listRow(_ item: MenuItem) -> some View {
        Label {
            Text(item.text)
                .padding(.leading, 4)
        } icon: {
            Image(systemSymbol: item.systemSymbol)
        }
        .padding(.vertical, 4)
    }
}

struct SidebarList_Previews: PreviewProvider {
    static var previews: some View {
        SidebarList(selection: .constant(nil))
    }
}
