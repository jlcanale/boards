//
//  AppState.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 4/14/22.
//

import Foundation
import Intents

class AppState: ObservableObject {
    enum AppStateView: EquatableEnumCase {
        case boardgameDetails(String)
    }

    static let shared = AppState()
    @Published var pageToNavigateTo: AppStateView?
}
