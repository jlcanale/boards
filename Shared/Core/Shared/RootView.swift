//
//  RootView.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 6/7/22.
//

import SwiftUI

struct RootView: View {

    @State private var selection: MenuItem? = .library

    var body: some View {
        NavigationSplitView {
            Text("Hello")
        } detail: {
            LibraryList()
        }
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
