//
//  CheckmarkRow.swift
//  Boards (iOS)
//
//  Created by Joseph Canale on 10/12/21.
//

import SwiftUI

struct CheckmarkRow: View {
    @Binding var isChecked: Bool
    var text: String
    var body: some View {
        Button {
            isChecked.toggle()
        } label: {
            HStack {
                Text(text)
                Spacer()
                if isChecked {
                    Image(systemSymbol: .checkmark)
                        .opacity(0.5)

                }
            }
            .contentShape(Rectangle())
        }
        .buttonStyle(.plain)
    }
}

struct CheckmarkRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            CheckmarkRow(isChecked: .constant(true), text: "Owned")
            CheckmarkRow(isChecked: .constant(false), text: "Owned")
        }
    }
}
