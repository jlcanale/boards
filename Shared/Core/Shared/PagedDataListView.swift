//
//  PagedDataList.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

struct PagedDataList<Model: Identifiable & Equatable, ViewModel: PagedDataListViewModel<Model>, Content: View>: View {
    @StateObject var viewModel: ViewModel
    let content: (Model) -> Content

    init(viewModel: ViewModel, @ViewBuilder content: @escaping (Model) -> Content) {
        _viewModel = StateObject(wrappedValue: viewModel)
        self.content = content
    }

    var body: some View {
        List {
            ForEach(viewModel.items) { item in
                if item == viewModel.items.last {
                    content(item)
                        .task { await viewModel.loadNextPage() }
                    if viewModel.isFetchingNextPage {
                        bottomProgressView
                    }
                } else {
                    content(item)
                }
            }
        }
        .overlay(overlayView)
        .task { await viewModel.loadFirstPage() }
        #if os(iOS)
        .navigationBarTitleDisplayMode(.inline)
        #endif
    }

        @ViewBuilder
        private var overlayView: some View {
            switch viewModel.phase {
                case .empty:
                    ProgressView()
                case .success(let items) where items.isEmpty:
                    EmptyPlaceholderView(text: viewModel.noDataText, image: nil)
                case .failure(let error):
                    RetryView(text: error.localizedDescription, retryAction: {})
                default:
                    EmptyView()
            }
        }

        @ViewBuilder
        private var bottomProgressView: some View {
            Divider()
            HStack {
                Spacer()
                ProgressView()
                Spacer()
            }.padding()
        }
}

// struct PagedDataList_Previews: PreviewProvider {
//    static var previews: some View {
//        PagedDataList()
//    }
// }
