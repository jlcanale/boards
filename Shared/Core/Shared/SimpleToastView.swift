//
//  SimpleToastView.swift
//  Boards
//
//  Created by Joseph Canale on 10/11/21.
//

import SwiftUI
import SFSafeSymbols

struct SimpleToastView: View {
    let text: String
    let icon: SFSymbol

    #if os(iOS)
    let feedbackType: UINotificationFeedbackGenerator.FeedbackType?

    init(text: String, icon: SFSymbol, feedbackType: UINotificationFeedbackGenerator.FeedbackType? = nil) {
        self.text = text
        self.icon = icon
        self.feedbackType = feedbackType
    }
    #endif

    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 8)
                .frame(width: 125, height: 125)
                .background(.ultraThinMaterial)
            VStack {
                Image(systemSymbol: icon)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 30, height: 30)
                    .foregroundColor(.white.opacity(0.7))
                Text(text)
                    .lineLimit(2)
                    .multilineTextAlignment(.center)
                    .frame(width: 110)
                    .foregroundColor(.white.opacity(0.7))
            }
        }
        #if os(iOS)
        .onAppear {
            if let feedbackType = feedbackType {
                let feedback = UINotificationFeedbackGenerator()
                feedback.prepare()
                feedback.notificationOccurred(feedbackType)
            }
        }
        #endif
    }

}

struct SimpleToastView_Previews: PreviewProvider {
    static var previews: some View {
        SimpleToastView(text: "Hello", icon: .checkmark)
        // SimpleToastView(text: "Added to library", icon: .checkmark, feedbackType: .success)
    }
}
