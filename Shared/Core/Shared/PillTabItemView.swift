//
//  PillTabItemView.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI

struct PillTabItemView<T: PillTabItem>: View {
    var item: T
    let isSelected: Bool
    var body: some View {
        Text(item.title)
            .font(.headline)
            .padding(.horizontal, 10)
            .background(isSelected ? .orange.opacity(0.9) : .gray.opacity(0.5))
            .background(in: Capsule())
    }
}

// struct PillTabItemView_Previews: PreviewProvider {
//    static var previews: some View {
//        PillTabItemView(item: PillTabItem(title: "Expansions"), isSelected: false)
//    }
// }
