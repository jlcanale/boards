//
//  SpinnerSlice.swift
//  Boards
//
//  Created by Joe Canale on 5/17/22.
//

import SwiftUI

struct SpinnerSlice: View {

    let segmentAngle: Double
    let segmentIndex: Int
    let color: Color

    var body: some View {
        GeometryReader { geometry in
            let center = geometry.frame(in: .local).center
            Path { path in
                path.move(to: center)
                path.addArc(center: center, radius: geometry.size.width / 2.1, startAngle: .degrees(Double(segmentIndex) * segmentAngle), endAngle: .degrees(Double(segmentIndex + 1) * segmentAngle), clockwise: false)
            }
            .fill(color)
        }
    }

}

struct SpinnerSlice_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            SpinnerSlice(segmentAngle: 90, segmentIndex: 0, color: .red)
        }
    }
}
