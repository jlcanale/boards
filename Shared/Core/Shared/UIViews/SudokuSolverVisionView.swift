//
//  SudokuSolverVisionView.swift
//  Boards
//
//  Created by Joe Canale on 5/23/22.
//

import SwiftUI
import VisionKit
import Vision

struct SudokuSolverVisionView: UIViewControllerRepresentable {
    @Binding var scannedData: [ValueData]?

    func makeUIViewController(context: Context) -> VNDocumentCameraViewController {
        let viewController = VNDocumentCameraViewController()
        viewController.delegate = context.coordinator
        return viewController
    }

    func updateUIViewController(_ uiViewController: VNDocumentCameraViewController, context: Context) { }

    func makeCoordinator() -> Coordinator {
        Coordinator(parent: self)
    }
}

extension SudokuSolverVisionView {
    struct ValueData {
        let value: String
        let column: Int
        let row: Int

        init(value: String, column: Int, row: Int) {
           self.value = (1...9).map({String($0)}).contains(value) ? value : "1"
           self.column = column
           self.row = row
        }
    }

    class Coordinator: NSObject, VNDocumentCameraViewControllerDelegate {

        var parent: SudokuSolverVisionView

        var textRecognitionRequest = VNRecognizeTextRequest(completionHandler: nil)
        private let textRecognitionWorkQueue = DispatchQueue(label: "BoardsVisionScannerQueue", qos: .userInitiated, attributes: [], autoreleaseFrequency: .workItem)

        init(parent: SudokuSolverVisionView) {
            self.parent = parent
            super.init()

            setupVision()
        }

        func closedRangeForElementsCount(_ count: Int, reversedIndex: Bool = false) -> [(ClosedRange<CGFloat>, Int)] {
            var ranges: [(ClosedRange<CGFloat>, Int)] = []

            let absoluteLength: CGFloat = 1.0/CGFloat(count)

            for i in 0..<count {
                let leftBound = CGFloat(i) * absoluteLength
                let rightBound = leftBound + absoluteLength
                ranges.append((leftBound...rightBound, reversedIndex ? count - (i + 1) : i))
            }
            return ranges
        }

        private func setupVision() {
            let horizontalRange = closedRangeForElementsCount(9)
            let verticalRange = closedRangeForElementsCount(9, reversedIndex: true)

            textRecognitionRequest = VNRecognizeTextRequest { [weak self] (request, _) in
                guard let observations = request.results as? [VNRecognizedTextObservation] else { return }

                var data: [ValueData] = []

                for observation in observations {
                    guard let topCandidate = observation.topCandidates(1).first else { return }

                    let candidateCenter = observation.boundingBox.absoluteCenter
                    var text = topCandidate.string

                    if topCandidate.string == "7" {
                        var oneCount = 0
                        for candidate in observation.topCandidates(9) where candidate.string == "1" {
                            oneCount += 1
                        }
                        if oneCount > 5 {
                            text = "1"
                        }
                    }

                    if text.count > 1 {
                        let trimmedText = text.trimmingCharacters(in: .whitespaces).replacingOccurrences(of: " ", with: "")
                        let width = observation.boundingBox.width / CGFloat(trimmedText.count)
                        let centerY = observation.boundingBox.origin.y + observation.boundingBox.height / 2
                        let originX = observation.boundingBox.origin.y

                        for (index, character) in trimmedText.enumerated() {
                            let centerX = originX + CGFloat(index) * (width/CGFloat(text.count))
                            let center = CGPoint(x: centerX, y: centerY)

                            if let horizontalIndex = horizontalRange.first(where: { $0.0.contains(center.x) })?.1,
                                let verticalIndex = verticalRange.first(where: {$0.0.contains(center.y) })?.1 {
                                data.append(ValueData(value: String(character), column: horizontalIndex, row: verticalIndex))
                            }
                        }
                    } else {
                        if let horizontalIndex = horizontalRange.first(where: { $0.0.contains(candidateCenter.x) })?.1,
                            let verticalIndex = verticalRange.first(where: {$0.0.contains(candidateCenter.y) })?.1 {
                            data.append(ValueData(value: String(text), column: horizontalIndex, row: verticalIndex))
                        }
                    }
                }

                DispatchQueue.main.async { [weak self] in
                    self?.processScannedData(data)
                }
            }

            textRecognitionRequest.recognitionLevel = .accurate
            // textRecognitionRequest.usesLanguageCorrection = false
            textRecognitionRequest.customWords = (1...9).map { String($0) }
        }

        private func processScannedData(_ scannedData: [ValueData]) {
            parent.scannedData = scannedData
        }

        private func process(image: UIImage) {
            recognizeTextIn(image: image)
        }

        private func recognizeTextIn(image: UIImage) {
            guard let cgImage = image.cgImage else { return }

            textRecognitionWorkQueue.async { [weak self] in
                guard let textRecognitionRequest = self?.textRecognitionRequest else { return }
                let requestHandler = VNImageRequestHandler(cgImage: cgImage, options: [:])
                do {
                    try requestHandler.perform([textRecognitionRequest])
                } catch {
                    print(error)
                }
            }
        }

        func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
            guard scan.pageCount >= 1 else {
                controller.dismiss(animated: true)
                return
            }

            let originalImage = scan.imageOfPage(at: 0)
            let newImage = compress(image: originalImage)
            controller.dismiss(animated: true)

            process(image: newImage)
        }

        func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
            print(error)
            controller.dismiss(animated: true)
        }

        func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
            controller.dismiss(animated: true)
        }

        func compress(image: UIImage) -> UIImage {
            guard let imageData = image.jpegData(compressionQuality: 1), let reloadedImage = UIImage(data: imageData) else { return image }
            return reloadedImage
        }
    }
}

extension CGRect {
    var absoluteCenter: CGPoint {
        return CGPoint(x: origin.x + width/2, y: origin.y + height/2)
    }
}
