//
//  Keypad.swift
//  Boards
//
//  Created by Joe Canale on 5/24/22.
//

import SwiftUI

struct Keypad: View {

    var onNumberPress: ((Int) -> Void)?
    var onClearPress: (() -> Void)?

    var body: some View {
        VStack {
            HStack {
                Button(action: { buttonTapped(1) }, label: { Text("1") })
                Button(action: { buttonTapped(2) }, label: { Text("2") })
                Button(action: { buttonTapped(3) }, label: { Text("3") })
            }
            HStack {
                Button(action: { buttonTapped(4) }, label: { Text("4") })
                Button(action: { buttonTapped(5) }, label: { Text("5") })
                Button(action: { buttonTapped(6) }, label: { Text("6") })
            }
            HStack {
                Button(action: { buttonTapped(7) }, label: { Text("7") })
                Button(action: { buttonTapped(8) }, label: { Text("8") })
                Button(action: { buttonTapped(9) }, label: { Text("9") })
            }
            Button(action: { buttonTapped(nil) }, label: { Text("C") })
        }

        .buttonStyle(.bordered)
    }

    private func buttonTapped(_ value: Int?) {
        if let value = value {
            onNumberPress?(value)
        } else {
            onClearPress?()
        }
    }
}

struct Keypad_Previews: PreviewProvider {
    static var previews: some View {
        Keypad()
    }
}
