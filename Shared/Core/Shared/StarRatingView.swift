//
//  StarRatingView.swift
//  Boards
//
//  Created by Joseph Canale on 9/29/21.
//

import SwiftUI

struct StarRatingView: View {
    @Binding var rating: Int

    var offImage: Image?
    var onImage = Image(systemSymbol: .starFill)
    var offColor = Color.gray
    var onColor = Color.yellow

    var body: some View {
        HStack {
            ForEach(1..<10 + 1) { number in
                self.image(for: number)
                    .foregroundColor(number > self.rating ? self.offColor : self.onColor)
                    .onTapGesture {
                        self.rating = number
                    }
            }
        }
    }

    func image(for number: Int) -> Image {
        if number > rating {
            return offImage ?? onImage
        } else {
            return onImage
        }
    }
}

struct StarRatingView_Previews: PreviewProvider {
    static var previews: some View {
        StarRatingView(rating: .constant(4), offImage: Image(systemSymbol: .star))
    }
}
