//
//  PillTabView.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI

struct PillTabView<T: PillTabItem>: View {
    let tabs: [T]
    @Binding var selectedTab: T

    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(tabs) { tab in
                    PillTabItemView(item: tab, isSelected: tab == selectedTab)
                        .onTapGesture {
                            selectedTab = tab
                        }
                }
            }.padding(.horizontal)
        }
    }
}

// struct PillTabView_Previews: PreviewProvider {
//    static var previews: some View {
//        PillTabView<TestPillTabItem>(tabs: [PillTabItem(title: "Overview")], selectedTab: .constant(nil))
//
//    }
// }
