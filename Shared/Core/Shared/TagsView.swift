//
//  TagsView.swift
//  Boards
//
//  Created by Joe Canale on 7/5/22.
//

import SwiftUI

struct TagsView: View {

    var tags: [String]
    var separator: String = " · "

    var body: some View {
        Text(tagsString)
            .multilineTextAlignment(.center)
    }

    private var tagsString: String {
        tags.joined(separator: separator)
    }
}

struct TagsView_Previews: PreviewProvider {
    static var previews: some View {
        TagsView(tags: ["Action Points", "Contracts", "Economic", "Farming"])
    }
}
