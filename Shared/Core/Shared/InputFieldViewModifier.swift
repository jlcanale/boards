//
//  InputFieldViewModifier.swift
//  Boards
//
//  Created by Joe Canale on 5/20/22.
//

import Foundation
import SwiftUI

struct InputFieldViewModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding(.all, 10)
            .background(.gray.opacity(0.4), in: RoundedRectangle(cornerRadius: 6, style: .continuous))
    }
}

extension View {
    func inputField() -> some View {
        self.modifier(InputFieldViewModifier())
    }
}
