//
//  TabContentView.swift
//  Boards
//
//  Created by Joseph Canale on 10/26/21.
//

import SwiftUI
import SFSafeSymbols

struct TabContentView: View {
    @Binding var selectedMenuItemId: MenuItem.ID?
    private var selection: Binding<TabMenuItem> {
        Binding {
            TabMenuItem(menuItem: selectedMenuItemId)
        } set: { newValue in
            selectedMenuItemId = newValue.menuItemId(category: selectedCategory ?? .hotness)
        }
    }

    private var selectedCategory: BrowseCategory? {
        let menuItem = MenuItem(id: selectedMenuItemId)
        if case .browse(let browseCategory) = menuItem {
            return browseCategory
        } else {
            return nil
        }
    }

    var body: some View {
        TabView(selection: selection) {
            ForEach(TabMenuItem.allCases) { item in
                NavigationStack {
                    viewFor(tabItem: item)
                }
                .tabItem {
                    Label(item.text, systemSymbol: item.systemSymbol)
                }
                .tag(item)
            }
        }
    }

    @MainActor @ViewBuilder
    private func viewFor(tabItem: TabMenuItem) -> some View {
        switch tabItem {
            case .library:
                LibraryList()
            case .search:
                SearchTab()
            case .previews:
                GeekPreviewTab()
            case .tools:
                ToolsList()
            case .browse:
                BrowseTab(browseViewModel: BrowseViewModel(selectedCategory: selectedCategory ?? .hotness))
                // BrowseTab(category: selectedCategory ?? .hotness)
            case .settings:
                SettingsTab()
        }
    }
}

struct TabContentView_Previews: PreviewProvider {
    static var previews: some View {
        TabContentView(selectedMenuItemId: .constant(nil))
    }
}
