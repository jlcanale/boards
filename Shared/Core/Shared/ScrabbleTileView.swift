//
//  ScrabbleTileView.swift
//  Boards
//
//  Created by Joe Canale on 5/18/22.
//

import SwiftUI

struct ScrabbleTileView: View {

    let scrabbleTile: ScrabbleTile
    let spaceType: ScrabbleSpaceType

    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 2, style: .continuous)
                .stroke(lineWidth: 2)
                .background(spaceType.color)
                .aspectRatio(contentMode: .fit)
                .overlay(GeometryReader { geometry in
                    Text(scrabbleTile.description)
                        .font(.system(size: geometry.size.height * 0.70, weight: .thin, design: .monospaced))
                        .frame(geometry.size)
                    Text(scrabbleTile.value == 0 ? "" : "\(scrabbleTile.value)")
                        .font(.system(size: geometry.size.height * 0.15, weight: .regular, design: .monospaced))
                        .offset(x: geometry.size.width * 0.75, y: geometry.size.height * 0.75)
                    Text(spaceType.abbreviation)
                        .font(.system(size: geometry.size.height * 0.15, weight: .regular, design: .monospaced))
                        .offset(x: geometry.size.width * 0.05, y: geometry.size.height * 0.05)
                })

        }
    }
}

struct ScrabbleTileView_Previews: PreviewProvider {
    static var previews: some View {
        ScrabbleTileView(scrabbleTile: "A", spaceType: .tripleWord).preferredColorScheme(.dark).width(200)
    }
}
