//
//  SettingsTab.swift
//  Boards
//
//  Created by Joseph Canale on 10/16/21.
//

import SwiftUI
import CoreData

struct SettingsTab: View {
    @EnvironmentObject var settingsStore: SettingsStore
    @State var showingDeleteConfirmation = false

    var body: some View {
        Form {
            Section {
                TextField("BGG Username", text: $settingsStore.geekUserManager.username, prompt: nil)
                    .textContentType(.username)
                    .autocorrectionDisabled()
                SecureField("BGG Password", text: $settingsStore.geekUserManager.password, prompt: nil)
                    .textContentType(.password)
                    .autocorrectionDisabled()
                Button("Login") {
                    Task {
                        let credentials = BGGCredential(username: settingsStore.geekUserManager.username, password: settingsStore.geekUserManager.password)
                        _ = try? await BGGAPI.shared.login(using: credentials)
                    }
                }
            } header: {
                Text("Profile")
            }

            Section {
                Picker("Method", selection: $settingsStore.collectionSyncMethod) {
                    ForEach(CollectionSyncMethod.allCases) { Text($0.rawValue.capitalized) }
                }
            } header: {
                Text("Collection Sync")
            } footer: {
                Text(settingsStore.collectionSyncMethod.explanation)
            }

            Section {
                Button("Rebuild Spotlight Index", action: SpotlightController.shared.rebuild)
                Button("Delete all boardgames", role: .destructive) {
                    showingDeleteConfirmation = true
                }
                .confirmationDialog("Confirm", isPresented: $showingDeleteConfirmation) {
                    Button("Delete all boardgames", role: .destructive, action: deleteAllBoardgames)
                    Button("Cancel", role: .cancel, action: {})
                } message: {
                    Text("Are you sure you want to delete all boardgames in your library?  This action cannot be undone.")
                }
            }

        }
        .navigationTitle("Settings")
    }

    private func deleteAllBoardgames() {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Boardgame.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        let context = PersistenceController.shared.container.viewContext

        _ = try? context.execute(deleteRequest)
    }
}

struct SettingsTab_Previews: PreviewProvider {
    static var previews: some View {
        SettingsTab()
            .environmentObject(SettingsStore.shared)

    }
}
