//
//  OpenOnBGGButton.swift
//  Boards
//
//  Created by Joseph Canale on 10/2/21.
//

import SwiftUI

struct OpenInSafariButton: View {
    let urlToOpen: URL
    let buttonText: String

    init(urlToOpen: URL, buttonText: String? = nil) {
        self.urlToOpen = urlToOpen
        self.buttonText = buttonText ?? "Open in Safari"
    }

    var body: some View {
        Button {
            UIApplication.shared.open(urlToOpen)
        } label: {
            Label(buttonText, systemSymbol: .safari)
        }
    }
}

struct OpenOnBGGButton_Previews: PreviewProvider {
    static var previews: some View {
        OpenInSafariButton(urlToOpen: URL(string: "https://boardgamegeek.com")!)
    }
}
