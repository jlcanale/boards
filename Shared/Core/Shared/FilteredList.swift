//
//  FilteredList.swift
//  Boards
//
//  Created by Joseph Canale on 10/11/21.
//

import SwiftUI
import CoreData

struct FilteredList<T: NSManagedObject, Content: View>: View {
    var fetchRequest: FetchRequest<T>
    var items: FetchedResults<T> { fetchRequest.wrappedValue }

    let content: (T) -> Content
    let onDelete: (IndexSet) -> Void

    init(predicate: NSPredicate, sortDescriptors: [NSSortDescriptor] = [], onDelete: @escaping ((IndexSet) -> Void) = { _ in }, @ViewBuilder content: @escaping (T) -> Content) {
        fetchRequest = FetchRequest<T>(entity: T.entity(), sortDescriptors: sortDescriptors, predicate: predicate)
        self.content = content
        self.onDelete = onDelete
    }

    var body: some View {
        List {
            ForEach(fetchRequest.wrappedValue, id: \.self) { item in
                self.content(item)
            }
            .onDelete(perform: onDelete)
        }
    }
}
