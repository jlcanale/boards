//
//  CenterStepper.swift
//  Boards
//
//  Created by Joe Canale on 5/26/22.
//

import SwiftUI
import SFSafeSymbols

struct CenterStepper: View {

    var value: Int = 0

    var minValue: Int?
    var maxValue: Int?

    var onDecrease: () -> Void = {}
    var onIncrease: () -> Void = {}

    var label: (Int) -> String = { value in  "\(value)" }

    var body: some View {
        HStack {
            Button(action: onDecrease) {
                ZStack {
                    Image(systemSymbol: .minusCircle)
                }
            }
            .disabled(value == minValue)

            Text(label(value))
                .padding(.horizontal)
                .frame(maxWidth: .infinity)

            Button(action: onIncrease) {
                ZStack {
                    Image(systemSymbol: .plusCircle)
                }
            }
            .disabled(value == maxValue)
        }
    }
}

struct CenterStepper_Previews: PreviewProvider {
    static var previews: some View {
        CenterStepper(label: { value in "\(value) Players"})
    }
}
