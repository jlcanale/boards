//
//  IconView.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI
import SFSafeSymbols

enum IconViewType {
    case playerCount
    case playTime
    case weight
    case geekRating
    case averageRating
    case myRating
    case rank
    case minimumAge
    case status
    case custom(Image)
}

struct IconView: View {

    let iconType: IconViewType
    let text: String

    var body: some View {
        VStack(spacing: 0) {
            image
                .scaledToFit()
                .frame(width: 50, height: 50)
            Text(text)
                .font(.system(size: 16))
                .fixedSize(horizontal: true, vertical: false)
        }
        .frame(width: 50, height: 70)
    }

    @ViewBuilder
    private var image: some View {
        switch iconType {
            case .playerCount:
                Image(systemSymbol: .person2)
                    .resizable()
            case .playTime:
                Image(systemSymbol: .stopwatch)
                    .resizable()
            case .weight:
                Image(systemName: "brain")
                    .resizable()
            case .geekRating:
                ZStack {
                    Image(systemSymbol: .eyeglasses)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 30, height: 30)
                    Image(systemSymbol: .star)
                        .resizable()
                }
            case .averageRating:
                Image(systemSymbol: .star)
                    .resizable()
            case .myRating:
                ZStack {
                    Image(systemSymbol: .person)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                    Image(systemSymbol: .star)
                        .resizable()
                }
            case .rank:
                Image(systemSymbol: .numberSquare)
                    .resizable()
            case .minimumAge:
                Image(systemName: "person.text.rectangle")
                    .resizable()
            case .status:
                Image(systemSymbol: .infoCircle)
                    .resizable()
            case .custom(let image):
                image
                    .resizable()
        }
    }
}

struct IconView_Previews: PreviewProvider {
    static var previews: some View {
        IconView(iconType: .myRating, text: "30-90 Mins")
    }
}
