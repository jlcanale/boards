//
//  StringsDisplayView.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI

struct StringsDisplayView: View {
    #if os(iOS)
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    #endif
    var title: String
    var items: [String]
    var emptyText: String?

    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(title)
                    .font(.headline)
                Text(joinedItems)
                    .font(.callout)
                    .lineLimit(nil)
            }
            Spacer()
        }
    }

    var numberToDisplay: Int {
        #if os(iOS)
        switch horizontalSizeClass {
            case .regular:
                return 15
            default:
                return 5
        }
        #else
        return 100
        #endif
    }

    var joinedItems: String {
        guard !items.isEmpty else { return emptyText ?? "None."}
        if items.count <= numberToDisplay {
            return items.joined(separator: ", ")
        } else {
            let overflowCount = items.count - numberToDisplay
            let shortenedArray = items.dropLast(overflowCount)
            let joinedString = shortenedArray.joined(separator: ", ")
            return "\(joinedString), and \(overflowCount) more"
        }
    }
}

struct StringsDisplayView_Previews: PreviewProvider {
    static var previews: some View {
        StringsDisplayView(title: "Category", items: ["Adventure"])
    }
}
