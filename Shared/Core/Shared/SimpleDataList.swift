//
//  SimpleDataList.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

struct SimpleDataList<Model: Identifiable, ViewModel: SimpleListViewModel<Model>, Content: View>: View {

    @StateObject var viewModel: ViewModel
    let content: (Model) -> Content

    init(viewModel: ViewModel, @ViewBuilder content: @escaping (Model) -> Content) {
        _viewModel = StateObject(wrappedValue: viewModel)
        self.content = content
    }

    var body: some View {
        List {
            ForEach(viewModel.items) { item in
                content(item)
            }
        }
        .task { await viewModel.loadData() }
        .listStyle(PlainListStyle())
        #if os(iOS)
        .navigationBarTitleDisplayMode(.inline)
        #endif
        .overlay(overlayView)
    }

    @ViewBuilder
    private var overlayView: some View {
        switch viewModel.phase {
            case .empty:
                ProgressView()
            case .success where viewModel.items.isEmpty:
                EmptyPlaceholderView(text: viewModel.noDataText, image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription, retryAction: retryAction)
            default:
                EmptyView()
        }
    }

    private func retryAction() {
        Task {
            await viewModel.loadData()
        }
    }

}

struct SimpleDataList_Preview: PreviewProvider {
    static var previews: some View {
        SimpleDataList(viewModel: GeekFilePageListViewModel(items: GeekFilePage.sample)) { _ in
            Text("Hello")
        }
    }
}
