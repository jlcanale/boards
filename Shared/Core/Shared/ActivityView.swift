//
//  ActivityView.swift
//  Boards
//
//  Created by Joseph Canale on 9/22/21.
//

import SwiftUI

struct ActivityView: View {
    var isShowing = false

    var body: some View {
        if isShowing {
            ZStack {
                RoundedRectangle(cornerRadius: 10)
                    .frame(width: 100, height: 100)
                    .opacity(0.2)
                ProgressView()
                    .scaleEffect(3)
                    .frame(width: 100, height: 100, alignment: .center)
            }
        }
    }
}

struct ActivityView_Previews: PreviewProvider {
    static var previews: some View {
        ActivityView()
    }
}
