//
//  FilteredVGrid.swift
//  Boards (iOS)
//
//  Created by Joseph Canale on 10/12/21.
//

import SwiftUI
import CoreData

struct FilteredVGrid<T: NSManagedObject, Content: View>: View {
    var fetchRequest: FetchRequest<T>
    var items: FetchedResults<T> { fetchRequest.wrappedValue }

    let content: (T) -> Content
    let onDelete: (IndexSet) -> Void

    init(predicate: NSPredicate, sortDescriptors: [NSSortDescriptor] = [], onDelete: @escaping ((IndexSet) -> Void) = { _ in }, @ViewBuilder content: @escaping (T) -> Content) {
        fetchRequest = FetchRequest<T>(entity: T.entity(), sortDescriptors: sortDescriptors, predicate: predicate)
        self.content = content
        self.onDelete = onDelete
    }

    var body: some View {
        ScrollView {
            LazyVGrid(columns: [GridItem(.adaptive(minimum: 260), spacing: 8)]) {
                ForEach(items, id: \.self) { item in
                    content(item)
                }
            }
            .buttonStyle(.plain)
            .padding()
        }
        .background(Color(uiColor: .secondarySystemGroupedBackground))
    }
}
