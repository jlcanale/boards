//
//  MainView.swift
//  Boards
//
//  Created by Joseph Canale on 9/21/21.
//

import SwiftUI
import SFSafeSymbols

struct MainView: View {

    @AppStorage("item_selection") var selectedMenuItemId: MenuItem.ID?
    @Environment(\.horizontalSizeClass) private var horizontalSizeClass

    var body: some View {
        switch horizontalSizeClass {
            case .regular:
                SidebarContentView(selectedMenuItemId: $selectedMenuItemId)
            default:
                TabContentView(selectedMenuItemId: $selectedMenuItemId)
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
