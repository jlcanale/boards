//
//  ExpandableTextView.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI
import SwifterSwift

struct ExpandableTextView: View {
    var text: String
    @State private var isDescriptionExpanded = false

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Description")
                    .font(.headline)
                Button {
                    withAnimation {
                        isDescriptionExpanded.toggle()
                    }
                } label: {
                    Text("\(isDescriptionExpanded ? "Collapse" : "Expand")")
                        .font(.subheadline)
                }
            }
            Text(text)
                .font(.callout)
                .lineLimit(isDescriptionExpanded ? nil : 4)
                .frame(maxWidth: .infinity)
        }
    }
}

struct ExpandableTextView_Previews: PreviewProvider {
    static var previews: some View {
        ExpandableTextView(text: String.loremIpsum())
    }
}
