//
//  StackLabelView.swift
//  Boards
//
//  Created by Joe Canale on 6/29/22.
//

import SwiftUI

struct StackLabelView: View {

    enum Size {
        case small
        case medium
        case large
        case custom(Font)
    }

    var topLabel: String
    var bottomLabel: String
    var size: StackLabelView.Size = .small

    var body: some View {
        VStack {
            Text(topLabel)
                .font(fontFor(size))
            Text(bottomLabel)
                .font(fontFor(size))
                .bold()
        }

        .padding(.horizontal, 2)
    }

    func fontFor(_ size: StackLabelView.Size) -> Font {
        switch size {
            case .small:
                return .caption
            case .medium:
                return .body
            case .large:
                return .title
            case .custom(let font):
                return font
        }
    }
}

struct StackLabelView_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 40) {
            StackLabelView(topLabel: "Players", bottomLabel: "1-4", size: .small)
            StackLabelView(topLabel: "Players", bottomLabel: "1-4", size: .medium)
            StackLabelView(topLabel: "Players", bottomLabel: "1-4", size: .large)
            StackLabelView(topLabel: "Players", bottomLabel: "1-4", size: .custom(.headline))

        }
    }
}
