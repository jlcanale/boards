//
//  FileExtensionView.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import SwiftUI

struct FileExtensionView: View {
    let fileExtension: String
    var body: some View {
        Text(fileExtension.uppercased())
            .font(.caption2.bold())
            .padding(4)
            .border(Color.primary, width: 1, cornerRadius: 4)
            .opacity(0.6)
    }
}

struct FileExtensionView_Previews: PreviewProvider {
    static var previews: some View {
        FileExtensionView(fileExtension: "pdf")
    }
}
