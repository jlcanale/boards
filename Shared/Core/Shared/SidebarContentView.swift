//
//  SidebarContentView.swift
//  Boards (iOS)
//
//  Created by Joseph Canale on 10/25/21.
//

import SwiftUI

struct SidebarContentView: View {

    @Binding var selectedMenuItemId: MenuItem.ID?
    private var selection: Binding<MenuItem.ID?> {
            Binding {
                selectedMenuItemId ?? MenuItem.browse(.hotness).id
            } set: { newValue in
                if let menuItemId = newValue {
                    selectedMenuItemId = menuItemId
                }
            }
        }

    var body: some View {
        NavigationView {
            ZStack {
                navigationLink

                List(selection: selection) {
                    ForEach([MenuItem.library, MenuItem.search]) {
                        listRow($0)
                    }

                    Section {
                        ForEach(BrowseCategory.menuItems) {
                            listRow($0)
                        }
                    } header: {
                        Text("Browse")
                    }
                    .navigationTitle("Boards")

                    listRow(.settings)
                }
                .listStyle(.sidebar)
            }
        }
    }

    @ViewBuilder
    private func listRow(_ menuItem: MenuItem) -> some View {
        let isSelected = menuItem.id == selection.wrappedValue
        Button {
            self.selection.wrappedValue = menuItem.id
        } label: {
            Label(menuItem.text, systemSymbol: menuItem.systemSymbol)
        }
        .foregroundColor(isSelected ? .white : nil)
        .listRowBackground((isSelected ? Color.accentColor : Color.clear).mask(RoundedRectangle(cornerRadius: 8)))
    }

    @MainActor @ViewBuilder
    private var navigationLink: some View {
        if let selectedMenuItem = MenuItem(id: selection.wrappedValue) {
            NavigationLink(destination: viewFor(menuItem: selectedMenuItem), tag: selectedMenuItem.id, selection: selection) { EmptyView() }
        }
    }

    @MainActor private func navigationLinkFor(menuItem: MenuItem) -> some View {
        NavigationLink(destination: viewFor(menuItem: menuItem), tag: menuItem.id, selection: selection) {
            Label(menuItem.text, systemSymbol: menuItem.systemSymbol)
        }
    }

    @MainActor @ViewBuilder
    private func viewFor(menuItem: MenuItem) -> some View {
        switch menuItem {
            case .search:
                SearchTab()
            case .library:
                LibraryList()
            case .previews:
                GeekPreviewTab()
            case .tools:
                ToolsList()
            case .settings:
                SettingsTab()
            case .browse(let browseCategory):
                BrowseTab(browseViewModel: BrowseViewModel(selectedCategory: browseCategory))
        }
    }
}

struct SidebarContentView_Previews: PreviewProvider {
    static var previews: some View {
        SidebarContentView(selectedMenuItemId: .constant(nil))
    }
}
