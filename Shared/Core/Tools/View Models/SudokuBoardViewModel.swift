//
//  SudokuBoardViewModel.swift
//  Boards
//
//  Created by Joe Canale on 5/24/22.
//

import UIKit

class SudokuBoardViewModel: ObservableObject {
    @Published var selectedBoxIndex: Int?
    @Published var selectedCellIndex: Int?
}
