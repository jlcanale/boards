//
//  SpinnerViewModel.swift
//  Boards
//
//  Created by Joe Canale on 5/17/22.
//

import Foundation
import SwiftUI

class SpinnerViewModel: ObservableObject {
    let colors: [Color] = [.red, .blue, .yellow, .orange, .indigo, .green, .cyan, .mint, .purple, .pink]

    @Published var segmentCount = 4
    @Published var selectedSegment: Int = -1
    @Published var spinnerAngle: Double = 0
    var segmentAngle: Double { 360/Double(segmentCount) }

    @MainActor
    func spin() {
        spinnerAngle += Double.random(in: 720...3960)
        selectedSegment = Int(spinnerAngle.truncatingRemainder(dividingBy: 360) / segmentAngle)
    }
}
