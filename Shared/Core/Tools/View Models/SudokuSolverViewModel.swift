//
//  SudokuSolverViewModel.swift
//  Boards
//
//  Created by Joe Canale on 5/23/22.
//

import Foundation
import UIKit

class SudokuSolverViewModel: ObservableObject {
    @Published var scannedData: [SudokuSolverVisionView.ValueData]?
    @Published var sudokuValues: [[SudokuValue]] = Array(repeating: Array(repeating: nil, count: 9), count: 9)
    @Published var showingScannerView = false

    @Published var selectedBoxIndex: Int?
    @Published var selectedCellIndex: Int?

    private var sudokuSolver = SudokuSolver()

    var isSolveButtonEnabled: Bool {
        !sudokuValues.flatMap { $0 }.compactMap { $0.value }.isEmpty
    }

    func buildSudokuValues() {
        guard let scannedData = scannedData else {
            sudokuValues = []
            return
        }
        var returnArray: [[SudokuValue]] = []

        let centerValues = [(row: 1, column: 1), (row: 1, column: 4), (row: 1, column: 7),
                            (row: 4, column: 1), (row: 4, column: 4), (row: 4, column: 7),
                            (row: 7, column: 1), (row: 7, column: 4), (row: 7, column: 7)]

        for centerValue in centerValues {
            var boxArray: [SudokuValue] = []
            for i in -1...1 {
                for j in -1...1 {
                    if let scannedValue = scannedData.first(where: { $0.row == centerValue.row + i && $0.column == centerValue.column + j }) {
                        boxArray.append(SudokuValue(value: scannedValue.value.int, isStartingValue: true))
                    } else {
                        boxArray.append(nil)
                    }
                }
            }
            returnArray.append(boxArray)
        }

        self.sudokuValues = returnArray
    }

    func reset() {
        scannedData = nil
        sudokuValues = []
    }

    func solve() {
        // guard scannedData != nil else { return }
        // buildSudokuValues()
        sudokuSolver.board = sudokuValues
        sudokuSolver.solve()
        self.sudokuValues = sudokuSolver.board
    }
}
