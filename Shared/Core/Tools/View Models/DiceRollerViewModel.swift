//
//  DiceRollerViewModel.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 5/14/22.
//

import Foundation
import AVKit

class DiceRollerViewModel: ObservableObject {

    struct RollResult: Identifiable {
        var id: Date { rollDate }

        let description: String
        let value: Int
        let rollDate = Date()
    }

    @Published var value: Int = 1
    @Published var dieType: DieType = .d4 {
        didSet {
            value = dieType.rawValue
        }
    }
    @Published var dieCount = 1
    @Published var modifier = 0

    @Published var history: [RollResult] = []

    var modifierSymbol: String {
        return modifier >= 0 ? "+" : ""
    }

    private let player: AVAudioPlayer?

    init() {
        guard let soundFileURL = Bundle.main.url(forResource: "roll", withExtension: "mp3") else { fatalError("Roll sound Effect is missing!") }

        do {
            try AVAudioSession.sharedInstance().setCategory(.ambient, options: .mixWithOthers)
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVAudioPlayer(contentsOf: soundFileURL)
        } catch {
            print("Problem initializing AVAudioSession")
            player = nil
        }
    }

    @MainActor
    func roll() {
        var rolls: [Int] = []

        for _ in 1...dieCount {
            rolls.append(Int.random(in: dieType.rollRange))
        }

        value = rolls.reduce(0, +) + modifier
        player?.play()

        history.prepend(RollResult(description: "\(dieCount)\(dieType.description)\(modifierSymbol)\(modifier)", value: value))
    }

    func clearHistory() {
        history = []
    }
}
