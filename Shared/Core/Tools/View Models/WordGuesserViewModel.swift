//
//  WordGuesserViewModel.swift
//  Boards
//
//  Created by Joe Canale on 5/24/22.
//

import Foundation

class WordGuesserViewModel: ObservableObject {
    @Published var inputWord: String = ""
    @Published var guesses: [Word] = []
    @Published var matchWholeWord = true {
        didSet {
            generateGuesses()
        }
    }
    @Published var selectedSortMethod = SortMethod.frequency {
        didSet {
            guesses = sort(guesses: guesses)
        }
    }

    private var allWords: [Word]

    init() {
        let fileURL = Bundle.main.url(forResource: "wordsWithFrequency", withExtension: "txt")!
        let rawWords = (try? String(contentsOf: fileURL))?.components(separatedBy: .newlines) ?? []
        allWords = rawWords.map {
            let components = $0.split(separator: "\t")
            return Word(text: String(components[0]), frequency: Int(components[1])!)
        }
    }

    func generateGuesses() {
        var guessRegex = inputWord.replacingOccurrences(of: "?", with: ".").lowercased()

        if matchWholeWord {
            guessRegex = "\\b\(guessRegex)\\b"
        }

        var filteredGuesses = allWords.filter { $0.text.range(of: guessRegex, options: .regularExpression) != nil }

        filteredGuesses = sort(guesses: filteredGuesses)

        DispatchQueue.main.async {
            self.guesses = filteredGuesses
        }
    }

    func sort(guesses: [Word]) -> [Word] {
        switch selectedSortMethod {
            case .frequency:
                return guesses.sorted { $0.frequency > $1.frequency }
            case .alphabetical:
                return guesses.sorted { $0.text < $1.text }
            case .length:
                return guesses.sorted { $0.text.count > $1.text.count }
        }
    }
}

extension WordGuesserViewModel {

    enum SortMethod: CaseIterable, Identifiable, CustomStringConvertible {
        var id: Self { self }

        case frequency
        case alphabetical
        case length

        var description: String {
            switch self {
                case .frequency:
                    return "Frequency"
                case .alphabetical:
                    return "Alphabetical"
                case .length:
                    return "Length"
            }
        }
    }

    struct Word: Identifiable {

        var id: String { "\(text)-\(frequency)" }

        var text: String
        var frequency: Int

        var normalizedFrequency: Double {
            let maximumFrequency = 23135851162
            let minimumFrequency = 12711
            let range = Double(maximumFrequency - minimumFrequency)

            return Double(frequency - minimumFrequency) / range
        }
    }
}
