//
//  ToolsListViewModel.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 4/18/22.
//

import Foundation
import SwiftUI

class ToolsListViewModel: ObservableObject {
    var tools: [Tool] = [DiceRollerTool(), RandomizerTool(), ScorerTool(),
                         ScrabbleScorerTool(), ScrabbleSolverTool(), SpinnerTool(),
                         SudokuSolverTool(), WordGuesserTool()].sorted(by: \.id)

}
