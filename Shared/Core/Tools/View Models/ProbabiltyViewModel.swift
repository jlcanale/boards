//
//  ProbabiltyViewModel.swift
//  Boards
//
//  Created by Joe Canale on 5/28/22.
//

import Foundation

class ProbabilityViewModel: ObservableObject {
    enum ProbabilityType {
        case expectedValue
    }

    @Published var dieCount = 1
    @Published var dieType = DieType.d6

    private let probability = Probability()

    func expectedValue() -> Double {

        return 0
    }
}
