//
//  ScrabbleScorerViewModel.swift
//  Boards
//
//  Created by Joe Canale on 5/18/22.
//

import Foundation

class ScrabbleScorerViewModel: ObservableObject {

    @Published var word: ScrabbleWord = ScrabbleWord(string: "")
    @Published private(set) var score: Int = 0
    @Published var inputWord: String = "" {
        willSet {
            self.word = ScrabbleWord(string: newValue)
        }
    }

    lazy var words: [String] = {
        let fileURL = Bundle.main.url(forResource: "words", withExtension: "txt")!
        let wordsString = (try? String(contentsOf: fileURL)) ?? ""
        return wordsString.components(separatedBy: .newlines)
    }()

    @MainActor
    func calculateScore() {
        score = word.score()
    }

    func scrabblePlacement(at index: Int) -> ScrabbleWord.Placement? { word[index] }

    func set(placement: ScrabbleWord.Placement, at index: Int) { word[index] = placement }

    func nextSpaceTypeForPlacement(at index: Int) {
        if let placement = word[index] {
            word[index]?.spaceType = placement.spaceType.next
        }
    }
}
