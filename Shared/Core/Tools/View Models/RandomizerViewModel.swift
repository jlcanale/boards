//
//  RandomizerViewModel.swift
//  Boards
//
//  Created by Joe Canale on 5/17/22.
//

import Foundation
import SwiftUI

class RandomizerViewModel: ObservableObject {
    @Published var selectedRandomType = RandomType.number {
        didSet {
            result = ""
            colorResult = .clear
        }
    }

    @Published var minimumNumber = 1
    @Published var maximumNumber = 10

    @Published var result: String = ""
    @Published var colorResult: Color = .clear
    @Published var validationError = ""

    @Published var includeProperNouns = false
    @Published var limitMinimumWordLength = false
    @Published var limitMaximumWordLength = false

    @Published var allowRepeatLetters = true
    @Published var allowUppercaseLetters = true
    @Published var allowLowercaseLetters = true
    @Published var allowNumbers = true

    lazy var words: [String] = {
        let fileURL = Bundle.main.url(forResource: "words", withExtension: "txt")!
        let wordsString = (try? String(contentsOf: fileURL)) ?? ""
        return wordsString.components(separatedBy: .newlines)
    }()

    var generateDisabled: Bool {
        if selectedRandomType == .letters {
            return !(allowNumbers || allowLowercaseLetters || allowUppercaseLetters)
        }
        return false
    }

    var colorRBGString: String {
        guard colorResult != .clear else { return ""}
        let rgb = colorResult.cgColor?.uiColor?.rgbComponents ?? (red: 0, green: 0, blue: 0)

        return "Red: \(rgb.red), Green: \(rgb.green), Blue: \(rgb.blue)"
    }

    var colorHexString: String {
        guard colorResult != .clear else { return ""}
        return colorResult.cgColor?.uiColor?.hexString ?? "#000000"
    }

    @MainActor
    func generate() {
        switch selectedRandomType {
            case .number:
                generateNumber()
            case .word:
                generateWord()
            case .letters:
                generateLetter()
            case .color:
                generateColor()
        }
    }

    private func generateNumber() {
        guard minimumNumber < maximumNumber else {
            validationError = "Minimum number must be less than the maximum number!"
            return
        }
        self.validationError = ""
        result = "\(Int.random(in: minimumNumber...maximumNumber))"
    }

    private func generateWord() {
        if limitMinimumWordLength && limitMaximumWordLength {
            guard minimumNumber < maximumNumber else {
                validationError = "Minimum word length must be less than maximum word length!"
                return
            }
        } else if limitMinimumWordLength {
            guard minimumNumber > 0 else {
                validationError = "Minimum word length must be greater than 0!"
                return
            }
        } else if limitMaximumWordLength {
            guard maximumNumber > 0 else {
                validationError = "Maximum word length must be greater than 0!"
                return
            }
        }
        validationError = ""

        var filteredWords = words.filter {
            if limitMinimumWordLength && limitMaximumWordLength {
                return (minimumNumber...maximumNumber).contains($0.count)
            } else if limitMinimumWordLength {
                return $0.count >= minimumNumber
            } else if limitMaximumWordLength {
                return $0.count <= maximumNumber
            } else {
                return true
            }
        }

        if !includeProperNouns {
            filteredWords = filteredWords.filter { $0.charactersArray.first?.isLowercase ?? false}
        }

        result = filteredWords.randomElement() ?? ""

    }

    private func generateLetter() {
        let distintCharacterCount = (allowUppercaseLetters ? 26 : 0) + (allowLowercaseLetters ? 26 : 0) + (allowNumbers ? 10 : 0)
        var randomResult = ""
        for _ in 0..<minimumNumber {
            var randomCharacter = Character.randomAlphanumeric()
            var validCharacter = false
            while !validCharacter {
                randomCharacter = Character.randomAlphanumeric()
                if !allowUppercaseLetters && randomCharacter.isUppercase { continue }
                if !allowLowercaseLetters && randomCharacter.isLowercase { continue }
                if !allowNumbers && randomCharacter.isNumber { continue }
                if !allowRepeatLetters && randomResult.contains(randomCharacter) { continue }

                validCharacter = true
            }
            randomResult.append(randomCharacter)
            if !allowRepeatLetters && randomResult.count == distintCharacterCount { break }

        }
        result = randomResult
    }

    private func generateColor() {
        colorResult = Color.random()
    }

}
