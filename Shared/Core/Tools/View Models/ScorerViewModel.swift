//
//  ScorerViewModel.swift
//  Boards
//
//  Created by Joe Canale on 5/26/22.
//

import Foundation

class ScorerViewModel: ObservableObject {

    @Published var playersScores: [[Int]]
    @Published var showingSettings = false

    private var settingsStore = SettingsStore.shared

    var playerCount: Int { playersScores.count }
    var scoringCategoryCount: Int { playersScores.first?.count ?? 0 }

    func totalScore(for player: Int) -> Int {
        guard player < playerCount && player >= 0 else { return 0}

        return playersScores[player].sum()
    }

    init() {
        let defaultPlayerCount = settingsStore.toolScorerDefaultPlayerCount
        let defaultScoreCategoryCount = settingsStore.toolScorerDefaultScoreCategoryCount

        playersScores = .init(repeating: .init(repeating: 0, count: defaultScoreCategoryCount), count: defaultPlayerCount)
    }

    func finalScoreEmoji(for player: Int) -> String {
        guard player < playerCount && player >= 0 else { return ""}

        var totalScores = playersScores.map { $0.sum() }
        let lowestScore = totalScores.min()
        let playerScore = totalScores[player]

        totalScores.removeDuplicates()
        totalScores.sort()
        totalScores.reverse()

        while totalScores.count < 3 {
            totalScores.append(0)
        }

        if playerScore == totalScores[0] {
            return "🥇"
        } else if playerScore == totalScores[1] {
            return "🥈"
        } else if playerScore == totalScores[2] {
            return "🥉"
        } else if playerScore == lowestScore {
            return "💩"
        } else {
            return ""
        }

    }

    func increasePlayers() {
        playersScores.append(.init(repeating: 0, count: scoringCategoryCount))
    }

    func decreasePlayers() {
        playersScores = playersScores.dropLast()

    }

    func increaseScoringCategories() {
        for index in playersScores.indices {
            playersScores[index].append(0)
        }
    }

    func decreaseScoringCategories() {
        for index in playersScores.indices {
            playersScores[index] = playersScores[index].dropLast()
        }
    }
}

extension ScorerViewModel {

    public struct ScorePoint: Hashable {
        let player: Int
        let scoringCategory: Int

        enum ScorePointIncrementMethod {
            case player
            case scoringCategory
        }
    }

    var lastField: ScorePoint { ScorePoint(player: playerCount - 1, scoringCategory: scoringCategoryCount - 1) }

    func next(_ scorePoint: ScorePoint?, by incrementMethod: ScorePoint.ScorePointIncrementMethod = .player) -> ScorePoint {
        guard let scorePoint = scorePoint else { return ScorePoint(player: 0, scoringCategory: 0)}

        switch incrementMethod {
            case .player:
                let category = scorePoint.player == (playerCount - 1) ? scorePoint.scoringCategory + 1 : scorePoint.scoringCategory
                return ScorePoint(player: (scorePoint.player + 1) % playerCount, scoringCategory: category)
            case .scoringCategory:
                let player = scorePoint.scoringCategory == (scoringCategoryCount - 1) ? scorePoint.player + 1 : scorePoint.player
                return ScorePoint(player: player, scoringCategory: (scorePoint.scoringCategory + 1) % scoringCategoryCount)
        }
    }

}
