//
//  ScrabbleSolverViewModel.swift
//  Boards
//
//  Created by Joe Canale on 5/19/22.
//

import Foundation

class ScrabbleSolverViewModel: ObservableObject {
    @Published var selectedWordList: WordList = .NWL2020
    @Published var wordListReady = true
    @Published var findingWords = false
    @Published var tileRackInputString = ""
    @Published var possibleWords: [String: Int] = [:]

    @MainActor
    public func prepare() {
        _ = dawg.validWords(using: [])
    }

    private var dawg: Dawg { selectedWordList == .NWL2020 ? nwlDawg : cswDawg }

    private func dawg(for wordList: WordList) -> Dawg {
        DispatchQueue.main.async {
            self.wordListReady = false
        }
        let url = Bundle.main.url(forResource: wordList.filename, withExtension: "txt")!
        let contents = (try? String(contentsOf: url)) ?? ""
        let words = contents.components(separatedBy: .newlines).sorted()

        let dawg = Dawg()

        words.forEach { dawg.insert(word: $0) }
        dawg.finish()

        DispatchQueue.main.async {
            self.wordListReady = true
        }
        return dawg
    }

    private lazy var cswDawg: Dawg = { dawg(for: .CSW2021) }()

    private lazy var nwlDawg: Dawg = { dawg(for: .NWL2020)}()

    public var possibleWordsString: String { "\(possibleWords.count == 0 ? "" : "\(possibleWords.count)") Possible Words" }

    public func findPossibleWords() {
        DispatchQueue.main.async {
            self.findingWords = true
            self.possibleWords.removeAll()
        }

        let validatedString = tileRackInputString.uppercased().replacingOccurrences(of: "[^A-Z?]", with: "", options: .regularExpression)
        guard !validatedString.isEmpty else { return }
        var foundWords = [String: Int]()
        let tileRack = validatedString.charactersArray
        let unscoredWords = dawg.validWords(using: tileRack)

        for word in unscoredWords {
            var wordCharacters = word.charactersArray
            for i in 0..<wordCharacters.count {
                if !tileRack.contains(wordCharacters[i]) {
                    wordCharacters[i] = "?"
                }
            }
            foundWords[word] = ScrabbleWord(string: String(wordCharacters)).score()
        }
        DispatchQueue.main.async {
            self.findingWords = false
            self.possibleWords = foundWords
        }
    }
}

extension ScrabbleSolverViewModel {
    enum WordList: CaseIterable, Identifiable, CustomStringConvertible, Equatable {
        case NWL2020
        case CSW2021

        var id: Self { self }

        var description: String {
            switch self {
                case .CSW2021:
                    return "CSW 2021"
                case .NWL2020:
                    return "NWL 2020"
            }
        }

        var filename: String {
            switch self {
                case .CSW2021:
                    return "CSW21"
                case .NWL2020:
                    return "NWL2020"
            }
        }
    }
}
