//
//  SudokuBoard.swift
//  Boards
//
//  Created by Joe Canale on 5/21/22.
//

import SwiftUI

struct SudokuBoard: View {

    var size: CGFloat
    @Binding var values: [[SudokuValue]]
    @StateObject var viewModel = SudokuBoardViewModel()

    var onSelectedBoxChange: ((Int?, Int?) -> Void)?

    var body: some View {
        LazyVGrid(columns: [GridItem(.fixed(size), spacing: size * 2), GridItem(.fixed(size), spacing: size * 2), GridItem(.fixed(size), spacing: size * 2)], alignment: .center, spacing: 0) {
            ForEach(0..<9) { i in
                SudokuBox(boxIndex: i, size: size, values: $values[i])
                    .environmentObject(viewModel)
                    .overlay(Rectangle()
                        .stroke(lineWidth: 3)
                    )
            }
        }
        .onChange(of: viewModel.selectedBoxIndex) { _ in
            onSelectedBoxChange?(viewModel.selectedBoxIndex, viewModel.selectedCellIndex)
        }
        .onChange(of: viewModel.selectedCellIndex) { _ in
            onSelectedBoxChange?(viewModel.selectedBoxIndex, viewModel.selectedCellIndex)
        }
    }
}

struct SudokuBoardView_Previews: PreviewProvider {
    static var previews: some View {
        SudokuBoard(size: 30, values: .constant(Array(repeating: Array(repeating: nil, count: 9), count: 9)))
    }
}
