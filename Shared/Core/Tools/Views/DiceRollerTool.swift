//
//  DiceRollerView.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 5/14/22.
//

import Foundation
import SwiftUI

struct DiceRollerTool: Tool {

    static var title: String { "Dice Roller" }
    static var shortDescription: String { "Let Boards roll dice for you" }

    static var content: AnyView { AnyView(ContentView()) }

    struct ContentView: View {
        @StateObject private var viewModel = DiceRollerViewModel()

        var body: some View {
            VStack {
                HStack {
                    Picker("Die Count", selection: $viewModel.dieCount) {
                        ForEach(1..<21) { index in
                            Text("\(index)").tag(index)
                        }
                    }
                    .pickerStyle(.wheel)
                    .width(70)
                    .clipped()

                    Picker("Die Type", selection: $viewModel.dieType) {
                        ForEach(DieType.allCases) { dieType in
                            Text("\(dieType.description)")
                        }
                    }
                    .pickerStyle(.wheel)
                    .width(70)
                    .clipped()

                    Picker("Die Modifier", selection: $viewModel.modifier) {
                        ForEach(-10..<11) { index in
                            Text("\(index > 0 ? "+" : "")\(index)").tag(index)
                        }
                    }
                    .pickerStyle(.wheel)
                    .width(70)
                    .clipped()
                }

                Spacer()
                Divider()

                DieView(dieType: viewModel.dieType, value: $viewModel.value)
                    .onTapGesture {
                        viewModel.roll()
                    }
                    .onShake {
                        viewModel.roll()
                    }
                    .padding()

                HStack {
                    Text("History")
                        .font(.subheadline.bold())

                    Spacer()

                    Button {
                        viewModel.clearHistory()
                    } label: {
                        Text("Clear")
                    }
                }
                .padding(.horizontal)

                List(viewModel.history) { history in
                    HStack {
                        Text("Roll:").font(.body.bold())
                        Text(history.description)
                        Spacer()
                        Text("Result:").font(.body.bold())
                        Text("\(history.value)")
                    }
                    .padding(.trailing)
                }
                .listStyle(PlainListStyle())
                .listRowInsets(.none)
            }
            .navigationBarTitle(title)
            .navigationBarTitleDisplayMode(.inline)

        }
    }
}

extension UIPickerView {
    open override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: super.intrinsicContentSize.height)
    }
}

struct DiceRoller_Previews: PreviewProvider {
    static var previews: some View {
        DiceRollerTool.content
    }
}
