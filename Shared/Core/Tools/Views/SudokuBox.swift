//
//  SudokuBox.swift
//  Boards
//
//  Created by Joe Canale on 5/21/22.
//

import SwiftUI

struct SudokuBox: View {
    var boxIndex: Int
    var size: CGFloat

    @EnvironmentObject var viewModel: SudokuBoardViewModel

    @Binding var values: [SudokuValue]
    @State private var selectedIndex: Int?

    var body: some View {
        LazyVGrid(columns: [GridItem(.fixed(size), spacing: 0), GridItem(.fixed(size), spacing: 0), GridItem(.fixed(size), spacing: 0)], alignment: .center, spacing: 0) {
            ForEach(0..<9) { i in
                SudokuCell(boxIndex: boxIndex, cellIndex: i, value: $values[i])
                    .frame(width: size, height: size)
            }
        }
    }
}

struct SudokuBox_Previews: PreviewProvider {
    static var previews: some View {
        SudokuBox(boxIndex: 0, size: 50, values: .constant(Array(repeating: nil, count: 9)))
    }
}
