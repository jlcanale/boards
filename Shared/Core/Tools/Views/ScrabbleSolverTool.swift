//
//  ScrabbleSolver.swift
//  Boards
//
//  Created by Joe Canale on 5/19/22.
//

import SwiftUI

struct ScrabbleSolverTool: Tool {
    static var title: String = "Scrabble Solver"
    static var shortDescription: String = ""

    static var content: AnyView { AnyView(ContentView()) }

    struct ContentView: View {
        @StateObject private var viewModel = ScrabbleSolverViewModel()

        var body: some View {
            VStack {

                VStack {
                    Text("Word List")
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .font(.headline)

                    Picker("Word List", selection: $viewModel.selectedWordList) {
                        ForEach(ScrabbleSolverViewModel.WordList.allCases) { wordList in
                            Text(wordList.description)
                        }
                    }
                    .pickerStyle(.segmented)

                    TextField("Rack letters", text: $viewModel.tileRackInputString)
                        .inputField()
                        .disableAutocorrection(true)

                    Text("Use ? for blank tiles")
                        .font(.subheadline)
                }
                .padding()

                Button {
                    DispatchQueue.global(qos: .userInitiated).async {
                        viewModel.findPossibleWords()
                    }
                } label: {
                    if !viewModel.wordListReady {
                        HStack {
                            Text("Building Word List")
                            ProgressView()
                                .padding(.leading)
                        }
                    } else if viewModel.findingWords {
                        HStack {
                            Text("Finding Possible Words")
                            ProgressView()
                                .padding(.leading)
                        }
                    } else {
                        Text("Find Possible Words")
                    }
                }
                .buttonStyle(.bordered)
                .disabled(!viewModel.wordListReady || viewModel.findingWords)

                Text(viewModel.possibleWordsString)
                    .font(.headline)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding()

                if !viewModel.possibleWords.isEmpty {
                    List(viewModel.possibleWords.sorted(by: \.value, and: \.key).reversed(), id: \.key) { keyValuePair in
                        Text("\(keyValuePair.key) - \(keyValuePair.value) points").font(.monospaced(.body)())
                    }
                    .listStyle(PlainListStyle())
                } else {
                    Spacer()
                    Text("Enter rack above to find the possible words.")
                        .multilineTextAlignment(.center)
                        .font(.system(size: 24, weight: .light, design: .rounded))
                    Spacer()
                }

                Spacer()
            }
            .navigationBarTitle(title)
            .navigationBarTitleDisplayMode(.inline)
            .onAppear(perform: prepareWordList)
            .onChange(of: viewModel.selectedWordList) { _ in
                prepareWordList()
            }

        }

        private func prepareWordList() {
            DispatchQueue.global(qos: .userInitiated).async {
                viewModel.prepare()
            }
        }
    }
}

struct ScrabbleSolver_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ScrabbleSolverTool.content
        }
    }
}
