//
//  SudokuCell.swift
//  Boards
//
//  Created by Joe Canale on 5/21/22.
//

import SwiftUI

struct SudokuCell: View {
    @EnvironmentObject var viewModel: SudokuBoardViewModel

    var boxIndex: Int
    var cellIndex: Int
    @Binding var value: SudokuValue

    var valueLabel: String {
        value.value != nil ? "\(value.value!)" : ""
    }

    var selected: Bool {
        viewModel.selectedBoxIndex == boxIndex && viewModel.selectedCellIndex == cellIndex
    }

    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Rectangle()
                    .stroke()
                    .aspectRatio(contentMode: .fit)
                    .background(selected ? .gray.opacity(0.5) : .clearInteractable)
                Text(valueLabel)
                    .font(.system(size: geometry.size.width * 0.8, weight: .bold, design: .monospaced))
                    .foregroundColor(value.isStartingValue ? .primary : .blue)

            }
            .onTapGesture {
                viewModel.selectedBoxIndex = boxIndex
                viewModel.selectedCellIndex = cellIndex
            }
        }
    }
}

struct SudokuCell_Previews: PreviewProvider {
    static var previews: some View {
        SudokuCell(boxIndex: 0, cellIndex: 0, value: .constant(1)).environmentObject(SudokuBoardViewModel())
    }
}
