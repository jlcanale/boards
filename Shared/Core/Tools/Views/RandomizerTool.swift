//
//  Randomizer.swift
//  Boards
//
//  Created by Joe Canale on 5/17/22.
//

import SwiftUI
import Combine

struct RandomizerTool: Tool {
    static var title: String = "Randomizer"
    static var shortDescription: String = "Your one stop shop for all things random"

    static var content: AnyView { AnyView(ContentView()) }

    struct ContentView: View {
        @StateObject private var viewModel = RandomizerViewModel()

        var body: some View {
            VStack {
                Group {
                    Picker("Type", selection: $viewModel.selectedRandomType) {
                        ForEach(RandomType.allCases) { randomType in
                            Text(randomType.description).tag(randomType)
                        }
                    }
                    .padding(.bottom)
                    .pickerStyle(.segmented)

                        switch viewModel.selectedRandomType {
                            case .number:
                                HStack {
                                    VStack {
                                        Text("Minimum Number")
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                        TextField("Minimum Number", value: $viewModel.minimumNumber, formatter: NumberFormatter())
                                            .keyboardType(.numberPad)
                                            .inputField()
                                    }

                                    VStack {
                                        Text("Maximum Number")
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                        TextField("Minimum Number", value: $viewModel.maximumNumber, formatter: NumberFormatter())
                                            .keyboardType(.numberPad)
                                            .inputField()
                                    }

                                }
                            case .word:
                                Toggle("Include Proper Nouns", isOn: $viewModel.includeProperNouns)
                                Toggle("Limit Minimum Word Length", isOn: $viewModel.limitMinimumWordLength.animation())
                                TextField("Minimum Word Length", value: $viewModel.minimumNumber, formatter: NumberFormatter())
                                    .keyboardType(.numberPad)
                                    .inputField()
                                    .opacity(viewModel.limitMinimumWordLength ? 1 : 0)
                                    .frame(maxHeight: viewModel.limitMinimumWordLength ? nil : 0)
                                Toggle("Limit Maximum Word Length", isOn: $viewModel.limitMaximumWordLength.animation())
                                TextField("Maximum Word Length", value: $viewModel.maximumNumber, formatter: NumberFormatter())
                                    .keyboardType(.numberPad)
                                    .inputField()
                                    .opacity(viewModel.limitMaximumWordLength ? 1 : 0)
                                    .frame(maxHeight: viewModel.limitMaximumWordLength ? nil : 0)
                                    .animation(.default, value: viewModel.limitMaximumWordLength)

                            case .letters:
                                VStack {
                                    Text("Letter Count")
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                    TextField("Letter Count", value: $viewModel.minimumNumber, formatter: NumberFormatter())
                                        .keyboardType(.numberPad)
                                        .inputField()

                                    Toggle("Allow Repeat Letters", isOn: $viewModel.allowRepeatLetters)
                                    Toggle("Allow Uppercase Letters", isOn: $viewModel.allowUppercaseLetters)
                                    Toggle("Allow Lowercase Letters", isOn: $viewModel.allowLowercaseLetters)
                                    Toggle("Allow Numbers", isOn: $viewModel.allowNumbers)

                                }
                            case .color:
                                EmptyView()
                        }
                    Text(viewModel.validationError)
                        .font(.caption2)
                        .foregroundColor(.red)

                    Button("Generate") {
                        viewModel.generate()
                    }
                    .disabled(viewModel.generateDisabled)
                    .padding()
                    .buttonStyle(BorderedButtonStyle())

                }
                .padding(.horizontal)

                Spacer()

                Text(viewModel.result)
                    .font(Font.system(size: 72, weight: .thin, design: .rounded))
                    .lineLimit(viewModel.selectedRandomType == .word ? 1 : 2)
                    .minimumScaleFactor(0.25)
                    .padding(.horizontal)
                    .onTapGesture {
                        if UIPasteboard.general.string != viewModel.result {
                            UIPasteboard.general.string = viewModel.result
                        }
                    }

                VStack {
                    Text("Color Details")
                    Text(viewModel.colorRBGString)
                        .onTapGesture {
                            if UIPasteboard.general.string != viewModel.colorRBGString {
                                UIPasteboard.general.string = viewModel.colorRBGString
                            }
                        }
                    Text(viewModel.colorHexString)
                        .onTapGesture {
                            if UIPasteboard.general.string != viewModel.colorHexString {
                                UIPasteboard.general.string = viewModel.colorHexString
                            }
                        }

                    Circle()
                        .strokeBorder(lineWidth: 5, antialiased: true)
                        .background(Circle().fill(viewModel.colorResult))
                        .padding()
                        .padding(.top, -100)

                }.hidden(if: viewModel.selectedRandomType != .color)

                Spacer()
            }
            .onShake {
                viewModel.generate()
            }
            .navigationBarTitle(title)
            .navigationBarTitleDisplayMode(.inline)
        }
    }

}

struct Randomizer_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            RandomizerTool.content
        }
    }
}
