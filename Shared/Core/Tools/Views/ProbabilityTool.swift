//
//  ProbabilityTool.swift
//  Boards
//
//  Created by Joe Canale on 5/28/22.
//

import Foundation
import SwiftUI

struct ProbabilityTool: Tool {
    static var title: String = "Scorer"
    static var shortDescription: String = ""

    static var content: AnyView { AnyView(ContentView()) }

    struct ContentView: View {

        var body: some View {
            return Text("Hello")
        }

    }
}
