//
//  SudokuSolverView.swift
//  Boards
//
//  Created by Joe Canale on 5/23/22.
//

import SwiftUI

struct SudokuSolverTool: Tool {
    static var title: String = "Sudoku Solver"
    static var shortDescription: String = ""

    static var content: AnyView { return AnyView(ContentView()) }

    struct ContentView: View {
        @StateObject private var viewModel = SudokuSolverViewModel()

        var body: some View {
            VStack {
                SudokuBoard(size: 35, values: $viewModel.sudokuValues) { boxIndex, cellIndex in
                    viewModel.selectedBoxIndex = boxIndex
                    viewModel.selectedCellIndex = cellIndex
                }

                Keypad(onNumberPress: { value in
                    guard let boxIndex = viewModel.selectedBoxIndex, let cellIndex = viewModel.selectedCellIndex else { return }
                    viewModel.sudokuValues[boxIndex][cellIndex].value = value
                }, onClearPress: {
                    guard let boxIndex = viewModel.selectedBoxIndex, let cellIndex = viewModel.selectedCellIndex else { return }
                    viewModel.sudokuValues[boxIndex][cellIndex] = nil
                })
                .padding(.top)

                HStack {
                    Button {
                        viewModel.reset()
                        viewModel.showingScannerView = true
                    } label: {
                        Text("Scan")
                    }

                    Button {
                        viewModel.solve()
                    } label: {
                        Text("Solve")
                    }
                    .enabled(viewModel.isSolveButtonEnabled)
                }
                .buttonStyle(.bordered)
                .padding()

            }
            .navigationBarTitle(title)
            .navigationBarTitleDisplayMode(.inline)
            .sheet(isPresented: $viewModel.showingScannerView) {
                SudokuSolverVisionView(scannedData: $viewModel.scannedData)
                    .onDisappear(perform: viewModel.buildSudokuValues)
            }
        }
    }
}

struct SudokuSolverView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SudokuSolverTool.ContentView()
        }
    }
}
