//
//  ToolsList.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 4/18/22.
//

import SwiftUI

struct ToolsList: View {

    @StateObject var viewModel = ToolsListViewModel()

    var body: some View {
        List(viewModel.tools, id: \.id) { tool in
            let toolType = type(of: tool)
            NavigationLink(toolType.title, destination: toolType.content)
        }
        .navigationTitle("Tools")
    }
}

struct ToolsList_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ToolsList()
        }
    }
}
