//
//  WordGuesser.swift
//  Boards
//
//  Created by Joe Canale on 5/24/22.
//

import SwiftUI

struct WordGuesserTool: Tool {
    static var title: String = "Word Guesser"
    static var shortDescription: String = ""

    static var content: AnyView { AnyView(ContentView()) }

    struct ContentView: View {
        @StateObject private var viewModel = WordGuesserViewModel()

        var body: some View {
            VStack {
                TextField("Input Word", text: $viewModel.inputWord)
                    .inputField()
                    .disableAutocorrection(true)
                    .padding([.top, .horizontal])
                Text("Use ? for unknown letters.")
                    .font(.subheadline)

                VStack(spacing: 10) {
                    Text("Sort Method")
                        .frame(maxWidth: .infinity, alignment: .leading)
                    Picker("Sort Method", selection: $viewModel.selectedSortMethod) {
                        ForEach(WordGuesserViewModel.SortMethod.allCases) { sortMethod  in
                            Text(sortMethod.description)
                        }
                    }
                    .pickerStyle(.segmented)
                }
                .padding()

                Toggle("Match Whole Word Only", isOn: $viewModel.matchWholeWord)
                    .padding()

                Button(action: viewModel.generateGuesses) {
                    Text("Generate Guesses")
                }
                .buttonStyle(.bordered)
                .padding()

                Text("Guesses")
                    .font(.headline)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.horizontal)

                if viewModel.guesses.isEmpty {
                    Spacer()

                    if viewModel.inputWord.count < 3 {
                        Text("Enter at least 3 letters above and select generate guesses.")
                            .font(.system(size: 28, weight: .light, design: .rounded))
                            .multilineTextAlignment(.center)
                            .padding()
                    } else {
                        Text("No Matches found!")
                            .font(.system(size: 28, weight: .light, design: .rounded))
                            .multilineTextAlignment(.center)
                            .padding()
                    }
                    Spacer()

                } else {
                    List(viewModel.guesses) { guess in
                        HStack {
                            Text("\(guess.normalizedFrequency)")
                                .frame(minWidth: 20)
                                .padding(.horizontal, 7)
                                .background(.gray)
                                .background(in: Capsule())
                                .foregroundColor(.white)
                                .font(.footnote)
                            Text(guess.text)
                        }
                    }
                    .listStyle(PlainListStyle())
                }

            }
            .navigationTitle(title)
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct WordGuesser_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            WordGuesserTool.content
        }
    }
}
