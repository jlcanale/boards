//
//  DieView.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 5/14/22.
//

import SwiftUI
import PolyKit

struct DieView: View {

    var dieType: DieType
    var strokeWidth: CGFloat = 5
    var cornerRadius: CGFloat = 20

    private let pipScaleFactor = 0.17
    private let pipPositionFactor = 0.25
    private let fontScaleFactor = 0.3
    private let fontPositionFactor = 0.1
    @Binding var value: Int

    var body: some View {
        VStack {
            GeometryReader { geometry in

                ZStack {
                    switch dieType {
                        case .d4:
                            d4(geometry)
                        case .d6:
                            d6(geometry)
                        case .d8:
                            d8(geometry)
                        case .d10:
                            d10(geometry)
                        case .d12:
                            d12(geometry)
                        case .d20:
                            d20(geometry)
                        case .d100:
                            d10(geometry)
                    }
                }
            }
            .aspectRatio(contentMode: .fit)
        }
    }

    @ViewBuilder
    func d4(_ geometry: GeometryProxy) -> some View {
        ZStack {
            Poly(count: 3, cornerRadius: cornerRadius)
                .stroke(lineWidth: strokeWidth)
            Text("\(value)")
                .font(.system(size: geometry.size.width * fontScaleFactor, weight: .semibold, design: .rounded))
        }
    }

    @ViewBuilder
    func d6(_ geometry: GeometryProxy) -> some View {
        Poly(count: 4, cornerRadius: cornerRadius)
            .stroke(lineWidth: strokeWidth)
            .rotationEffect(.degrees(-45))
        Text("\(value)")
            .font(.system(size: geometry.size.width * fontScaleFactor, weight: .semibold, design: .rounded))
    }

    @ViewBuilder
    func d8(_ geometry: GeometryProxy) -> some View {
        ZStack {
            Poly(count: 10, cornerRadius: cornerRadius)
                .stroke(lineWidth: strokeWidth)
            Text("\(value)")
                .font(.system(size: geometry.size.width * fontScaleFactor, weight: .semibold, design: .rounded))
        }
    }

    @ViewBuilder
    func d10(_ geometry: GeometryProxy) -> some View {
        ZStack {
            Poly(count: 4, cornerRadius: cornerRadius - 15)
                .stroke(lineWidth: strokeWidth)
            Text("\(value)")
                .font(.system(size: geometry.size.width * fontScaleFactor, weight: .semibold, design: .rounded))
        }
    }

    @ViewBuilder
    func d12(_ geometry: GeometryProxy) -> some View {
        ZStack {
            Poly(count: 5, cornerRadius: cornerRadius)
                .stroke(lineWidth: strokeWidth)
            Text("\(value)")
                .font(.system(size: geometry.size.width * fontScaleFactor, weight: .semibold, design: .rounded))
        }
    }

    @ViewBuilder
    func d20(_ geometry: GeometryProxy) -> some View {
        ZStack {
            Poly(count: 6, cornerRadius: cornerRadius)
                .stroke(lineWidth: strokeWidth)
            Text("\(value)")
                .font(.system(size: geometry.size.width * fontScaleFactor, weight: .semibold, design: .rounded))
        }
    }
}

struct DieView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            DieView(dieType: .d6, value: .constant(5)).frame(width: 200)
        }
    }
}
