//
//  Spinner.swift
//  Boards
//
//  Created by Joe Canale on 5/17/22.
//

import SwiftUI

struct SpinnerTool: Tool {
    static var title: String = "Spinner"
    static var shortDescription: String = "Do some spinning"

    static var content: AnyView { AnyView(ContentView()) }

    struct ContentView: View {
        @StateObject private var viewModel = SpinnerViewModel()

        @State private var segmentOpacity = 1.0

        var body: some View {
            VStack {
                Stepper("Segments", value: $viewModel.segmentCount, in: 2...10)
                    .padding(.horizontal)
                Spacer()
                ZStack {
                    ForEach(0..<viewModel.segmentCount, id: \.self) { index in
                        SpinnerSlice(segmentAngle: viewModel.segmentAngle, segmentIndex: index, color: viewModel.colors[index].opacity(index == viewModel.selectedSegment ? segmentOpacity : 1))
                            .animation(.easeInOut(duration: 0.25).repeatCount(7, autoreverses: true), value: segmentOpacity)
                            .onAnimationCompleted(for: segmentOpacity) {
                                segmentOpacity = 1
                            }
                    }

                    Image("SpinnerArrow")
                        .foregroundColor(.black.opacity(0.8))
                        .rotationEffect(.degrees(viewModel.spinnerAngle))
                        .onShake(perform: spin)
                        .onAnimationCompleted(for: viewModel.spinnerAngle) {
                            withAnimation {
                                segmentOpacity = 0.5
                            }
                        }

                }
                .onTapGesture(perform: spin)
                .frame(maxHeight: 100)
                Spacer()
            }
            .navigationBarTitle(title)
            .navigationBarTitleDisplayMode(.inline)
        }

        private func spin() {
            withAnimation(.spring()) {
                viewModel.spin()
            }
        }
    }

}

struct Spinner_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SpinnerTool.content
        }
    }
}
