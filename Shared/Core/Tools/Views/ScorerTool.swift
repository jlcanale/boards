//
//  Scorer.swift
//  Boards
//
//  Created by Joe Canale on 5/26/22.
//

import SwiftUI
import SFSafeSymbols

struct ScorerTool: Tool {
    static var title: String = "Scorer"
    static var shortDescription: String = ""

    static var content: AnyView { AnyView(ContentView()) }

    struct ContentView: View {
        @StateObject private var viewModel = ScorerViewModel()
        @FocusState private var focus: ScorerViewModel.ScorePoint?

        var body: some View {
            ScrollView {
                CenterStepper(value: viewModel.playerCount, minValue: 1, maxValue: 6, onDecrease: viewModel.decreasePlayers, onIncrease: viewModel.increasePlayers) { value in
                    "\(value) players"
                }
                .frame(maxWidth: 275)
                .padding()

                CenterStepper(value: viewModel.scoringCategoryCount, minValue: 1, onDecrease: viewModel.decreaseScoringCategories, onIncrease: viewModel.increaseScoringCategories) { value in
                    "\(value) scoring categories"
                }
                    .frame(maxWidth: 275)
                    .padding()

                HStack {
                    ForEach(0..<viewModel.playerCount, id: \.self) { playerIndex in
                        VStack {
                            ForEach(-1..<viewModel.scoringCategoryCount, id: \.self) { scoreIndex in
                                if scoreIndex == -1 {
                                    HStack(spacing: 0) {
                                        Image(systemSymbol: .person)
                                            .imageScale(.large)
                                        Text("\(playerIndex + 1)")
                                    }
                                } else {
                                    TextField("", value: $viewModel.playersScores[playerIndex][scoreIndex], formatter: NumberFormatter())
                                        .inputField()
                                        .keyboardType(.numberPad)
                                        .focused($focus, equals: ScorerViewModel.ScorePoint(player: playerIndex, scoringCategory: scoreIndex))
                                }
                            }
                            Divider()
                            HStack {
                                Text("\(viewModel.finalScoreEmoji(for: playerIndex))\(viewModel.totalScore(for: playerIndex)) ")
                            }

                        }

                    }
                }
                .toolbar {
                    ToolbarItemGroup(placement: .keyboard) {
                        Button {
                            focus = viewModel.next(focus, by: .scoringCategory)
                        } label: {
                            Image(systemSymbol: .arrowDown)
                        }
                        .disabled(focus == viewModel.lastField)

                        Button {
                            focus = viewModel.next(focus, by: .player)
                        } label: {
                            Image(systemSymbol: .arrowRight)
                        }
                        .disabled(focus == viewModel.lastField)

                        Spacer()
                        Text("Category \((focus?.scoringCategory ?? 0) + 1)")
                        Spacer()

                        Button("Done") { focus = nil }
                    }

                    ToolbarItemGroup(placement: .automatic) {
                        Button {
                            viewModel.showingSettings = true
                        } label: {
                            Image(systemSymbol: .gear)
                                .imageScale(.large)
                        }

                    }
                }
                .padding()
                .multilineTextAlignment(.center)
                .sheet(isPresented: $viewModel.showingSettings) {
                    ContentView.Settings()
                }
            }
            .navigationBarTitle("Scorer")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

extension ScorerTool.ContentView {
    struct Settings: View {

        @StateObject var viewModel = SettingsStore.shared
        @Environment(\.dismiss) private var dismiss

        var body: some View {
            NavigationView {
                Form {
                    Stepper("Default Player Count: \(viewModel.toolScorerDefaultPlayerCount)", value: $viewModel.toolScorerDefaultPlayerCount)
                    Stepper("Default Category Count: \(viewModel.toolScorerDefaultScoreCategoryCount)", value: $viewModel.toolScorerDefaultScoreCategoryCount)
                }
                .navigationTitle("Settings")
                .toolbar {
                    ToolbarItemGroup(placement: .automatic) {
                        Button {
                            dismiss()
                        } label: {
                            Text("Done")
                        }
                    }
                }
            }
        }
    }
}

struct Scorer_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ScorerTool.content
        }
}
}
