//
//  ScrabbleScorer.swift
//  Boards
//
//  Created by Joe Canale on 5/18/22.
//

import SwiftUI

struct ScrabbleScorerTool: Tool {
    static var title: String = "Scrabble Scorer"
    static var shortDescription: String = ""

    static var content: AnyView { AnyView(ContentView()) }

    struct ContentView: View {
        @StateObject var viewModel = ScrabbleScorerViewModel()
        @State private var wordInput = ""

        var body: some View {
            VStack {
                TextField("Input Word", text: $wordInput)
                    .inputField()
                    .padding(.horizontal)
                    .onChange(of: wordInput) { newValue in
                        viewModel.word = ScrabbleWord(string: newValue)
                        viewModel.calculateScore()
                    }
                Text("Use ? for blank tiles")
                    .font(.subheadline)

                HStack {
                    ForEach(0..<wordInput.count, id: \.self) { index in
                        if let placement = viewModel.scrabblePlacement(at: index) {
                            ScrabbleTileView(scrabbleTile: placement.scrabbleTile, spaceType: placement.spaceType)
                                .frame(maxWidth: 50)
                                .onTapGesture {
                                    viewModel.nextSpaceTypeForPlacement(at: index)
                                    viewModel.calculateScore()
                                }
                        }
                    }
                }
                .padding()

                Text("\(viewModel.score) points")
                    .font(.system(size: 72, weight: .thin, design: .rounded))

                Spacer()
            }
            .navigationBarTitle(title)
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct ScrabbleScorer_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ScrabbleScorerTool.content
        }
    }
}
