//
//  GeekFileRow.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import SwiftUI

struct GeekFileRow: View {
    let file: GeekFile
    var body: some View {
        VStack(alignment: .leading) {

            HStack {
                FileExtensionView(fileExtension: file.fileName.pathExtension)
                Text(file.title)
                    .font(.headline)
            }

            HStack(spacing: 4) {
                Image(systemSymbol: .handThumbsup)
                Text(file.thumbsCount)
                Text("|")
                Image(systemSymbol: .bubbleRight)
                Text(file.commentsCount)
            }
            .font(.caption)
            .padding(.bottom, 4)

            Text(file.description.rendered.stripHTML())
                .lineLimit(3)
                .font(.subheadline)
                .padding(.bottom, 3)
            HStack(spacing: 5) {
                Text(file.user.username)
                Text("·")
                Text(file.postDateRelativeString)
                Text("·")
                Text(file.language ?? "")
            }
            .font(.subheadline)
            .opacity(0.7)
        }

    }
}

struct GeekFileRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            GeekFileRow(file: .sample[0])
        }.listStyle(PlainListStyle())
    }
}
