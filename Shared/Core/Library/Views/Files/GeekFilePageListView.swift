//
//  GeekFilePageList.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import SwiftUI

struct GeekFilePageList: View {
    @StateObject var geekFilePageListVM: GeekFilePageListViewModel

    var body: some View {
        List {
            ForEach(geekFilePageListVM.items) { geekFilePage in
                GeekFilePageRow(geekFilePage: geekFilePage)
            }
        }
        .task { await geekFilePageListVM.loadData() }
        .navigationTitle("Files")
        .navigationBarTitleDisplayMode(.inline)
        .listStyle(PlainListStyle())
    }
}

struct GeekFilePageList_Previews: PreviewProvider {
    static var previews: some View {
        GeekFilePageList(geekFilePageListVM: GeekFilePageListViewModel(items: GeekFilePage.sample))
    }
}
