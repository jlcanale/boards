//
//  GeekFilePageRow.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import SwiftUI
import SwiftUIKit

struct GeekFilePageRow: View {
    let geekFilePage: GeekFilePage

    @State private var showingSafariSheet = false

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text(geekFilePage.fileName.deletingPathExtension)
                    .font(.headline)
                FileExtensionView(fileExtension: geekFilePage.fileName.pathExtension)
                Button {
                    print("Download File")
                    showingSafariSheet = true
                } label: {
                    Image(systemSymbol: .arrowDownToLine)
                }.buttonStyle(.borderless)

            }.padding(.bottom, -5)
            HStack {
                Text(geekFilePage.postDate.dateString())
                Text("·")
                Text(geekFilePage.sizeString)
                Text("·")
                Text("\(geekFilePage.downloadCount.thousandsFormatted) Downloads")
            }.padding(.bottom, 5)
            .font(.caption2)
            Text(geekFilePage.description)
        }
        #if os(iOS)
        .sheet(isPresented: $showingSafariSheet, onDismiss: nil) {
            // TODO: Check to see if the user is logged in then get the URL
            SafariView(url: URL(string: "https://boardgamegeek.com")!).edgesIgnoringSafeArea(.bottom)
        }
        #endif
    }
}

struct GeekFilePageRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            GeekFilePageRow(geekFilePage: .sample[0])
        }
        .listStyle(PlainListStyle())
    }
}
