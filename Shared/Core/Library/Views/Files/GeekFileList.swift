//
//  GeekFileList.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import SwiftUI

struct GeekFileList: View {
    @StateObject var geekFileListVM: GeekFileListViewModel

    var body: some View {
        List {
            ForEach(geekFileListVM.files) { file in
                if file == geekFileListVM.files.last {
                    navigationLink(for: file)
                        .task { await geekFileListVM.loadNextPage() }
                    if geekFileListVM.isFetchingNextPage {
                        bottomProgressView
                    }
                } else {
                    navigationLink(for: file)
                }
            }
        }
        .overlay(overlayView)
        .listStyle(PlainListStyle())
        .task { await loadTask() }
    }

    private func navigationLink(for file: GeekFile) -> some View {
        NavigationLink(
            destination:
                SimpleDataList(viewModel: GeekFilePageListViewModel(objectID: file.filePageID)) { item in
                    GeekFilePageRow(geekFilePage: item)
                }.navigationTitle("Files")
        ) {
            GeekFileRow(file: file)
        }
    }

    private func loadTask() async {
        await geekFileListVM.loadFirstPage()
    }

    @ViewBuilder
    private var overlayView: some View {
        switch geekFileListVM.phase {
            case .empty:
                ProgressView()
            case .success where geekFileListVM.files.isEmpty:
                EmptyPlaceholderView(text: "No Files", image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription, retryAction: {})
            default:
                EmptyView()
        }
    }

    @ViewBuilder
    private var bottomProgressView: some View {
        Divider()
        HStack {
            Spacer()
            ProgressView()
            Spacer()
        }.padding()
    }
}

struct GeekFileList_Previews: PreviewProvider {
    static var previews: some View {
        GeekFileList(geekFileListVM: GeekFileListViewModel(geekFiles: GeekFile.sample))
    }
}
