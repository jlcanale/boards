//
//  BoardgameVersionList.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

struct BoardgameVersionList: View {
    @StateObject var boardgameVersionListVM: BoardgameVersionListViewModel
    var body: some View {
        List {
            ForEach(boardgameVersionListVM.items) { item in
                if item == boardgameVersionListVM.items.last {
                    BoardgameVersionRow(boardgameVersion: item)
                        .task { await boardgameVersionListVM.loadNextPage() }
                    if boardgameVersionListVM.isFetchingNextPage {
                        bottomProgressView
                    }
                } else {
                    BoardgameVersionRow(boardgameVersion: item)
                }
            }
        }
        .task { await boardgameVersionListVM.loadFirstPage() }
        .overlay(overlayView)
        .listStyle(PlainListStyle())
    }

    @ViewBuilder
    private var overlayView: some View {
        switch boardgameVersionListVM.phase {
            case .empty:
                ProgressView()
            case .success(let items) where items.isEmpty:
                EmptyPlaceholderView(text: "No Versions", image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription, retryAction: {})
            default:
                EmptyView()
        }
    }

    @ViewBuilder
    private var bottomProgressView: some View {
        Divider()
        HStack {
            Spacer()
            ProgressView()
            Spacer()
        }.padding()
    }
}

struct BoardgameVersionList_Previews: PreviewProvider {
    static var previews: some View {
        BoardgameVersionList(boardgameVersionListVM: BoardgameVersionListViewModel(items: BoardgameVersion.sample))
    }
}
