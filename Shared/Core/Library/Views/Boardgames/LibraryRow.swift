//
//  LibraryRow.swift
//  Boards
//
//  Created by Joseph Canale on 10/7/21.
//

import SwiftUI

struct LibraryRow: View {
    let boardgame: Boardgame
    var body: some View {
        HStack {
            AsyncImage(url: boardgame.imageURL) { image in
                image
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 6))
                    .frame(width: 110, height: 110)
            } placeholder: {
                Image(systemSymbol: .photo)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 110, height: 110)
                    .foregroundColor(.secondary)
            }
            VStack(alignment: .leading) {
                Text("\(boardgame.name) \(boardgame.yearPublishedText)")
                    .font(.headline)
                HStack {
                    Image(systemSymbol: .person2)
                    Text(boardgame.playerCountText)
                    Image(systemSymbol: .stopwatch)
                    Text(boardgame.playTimeText)
                }
                .font(.subheadline)
            }
        }
    }
}

struct LibraryRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            LibraryRow(boardgame: .sample)
        }
        #if os(iOS)
        .listStyle(PlainListStyle())
        #endif
    }
}
