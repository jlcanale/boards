//
//  RankList.swift
//  Boards
//
//  Created by Joseph Canale on 9/29/21.
//

import SwiftUI

struct RankList: View {
    let ranks: [RankRepresentable]
    var body: some View {
        if ranks.isEmpty {
            Text("No rank data.")
        } else {
            VStack(alignment: .leading) {
                ForEach(ranks, id: \.prettyName) { rank in
                    Text("\(rank.prettyName): #\(rank.rank) (\(rank.bayesAverage))")
                        .padding(.bottom, 3)
                    StarRatingView(rating: .constant(rank.bayesAverageInt))
                        .padding(.bottom)

                }
            }
        }
    }
}

struct RankList_Previews: PreviewProvider {
    static var previews: some View {
        RankList(ranks: GeekItem.sampleBoardgame.dynamicInfo?.ranks ?? [])
    }
}
