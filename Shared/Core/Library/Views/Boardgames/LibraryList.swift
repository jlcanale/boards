//
//  LibraryList.swift
//  Boards
//
//  Created by Joseph Canale on 10/7/21.
//

import SwiftUI
import Combine
import SwiftUIKit

struct LibraryList: View {

    @Environment(\.managedObjectContext) private var viewContext
    #if os(iOS)
    @Environment(\.horizontalSizeClass) private var horizontalSizeClass
    #endif

    @EnvironmentObject var settingsStore: SettingsStore
    @ObservedObject private var appState = AppState.shared
    @StateObject private var sheetContext = SheetContext()
    @StateObject private var alertContext = AlertContext()
    @State private var selectedBoardgameBGGID: String?
    @StateObject var filterVM = LibraryFilterViewModel()
    @StateObject var libraryVM: LibraryViewModel
    @State private var showingMoreOptions = false

    init() {
        self._libraryVM = StateObject(wrappedValue: LibraryViewModel())
    }

    var body: some View {
        rootView
            .toolbar(content: toolbarContent)
            .navigationTitle("Library")
            .alert(alertContext)
            .sheet(sheetContext)
            .overlay(overlayView)
            .overlay(progressOverlay)
            .searchable(text: $filterVM.searchText)
            .task {
                libraryVM.fetchData()
            }
            .task(id: filterVM.predicate) {
                libraryVM.predicate = filterVM.predicate
            }
            .alert("Error Occurred!", isPresented: $libraryVM.showError) {
                Button(action: {}, label: { Text("OK") })
            }
            #if os(macOS)
            .frame(minWidth: 300)
            #endif
    }

    @ViewBuilder
    private var progressOverlay: some View {
        if libraryVM.syncing {
            VStack(alignment: .center) {
                Text(libraryVM.syncMessage)
                    .padding(.vertical)
                ProgressView(libraryVM.syncProgress)
                    .progressViewStyle(.linear)
                    .padding(.horizontal)
            }.frame(width: 200, height: 200)
                .background(.thinMaterial)
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .shadow(radius: 10)
        } else {
            EmptyView()
        }
    }

    private func toolbarContent() -> some ToolbarContent {
        Group {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    sheetContext.present(LibraryFilters(filterVM: filterVM))
                } label: {
                    Image(filterVM.filtersActive ? "FilterSolid": "Filter")
                        .imageScale(.small)
                }
            }

            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    showingMoreOptions = true
                } label: {
                    Image(systemSymbol: .ellipsis)
                }
                .confirmationDialog("", isPresented: $showingMoreOptions, titleVisibility: .hidden) {
                    confirmationDialogActionsView
                }
            }
        }
    }

    @ViewBuilder
    private var confirmationDialogActionsView: some View {
        Button("Sync library with BGG Collection") {
            Task {
                await libraryVM.syncCollection()
            }
        }

        Button("Refresh Dynamic Info") {
            Task {
                await libraryVM.refreshDynamicInfo()
            }
        }
    }

    private var gridView: some View {
        LazyVGrid(columns: [GridItem(.adaptive(minimum: 260), spacing: 8)]) {
            ForEach(libraryVM.boardgames) { boardgame in
                NavigationLink {
                    BoardgameDetail(boardgame: boardgame)
                } label: {
                    LibraryCard(boardgame: boardgame)
                }
            }
        }
    }

    private var listView: some View {
        ScrollViewReader { proxy in
            List {
                ForEach(libraryVM.boardgames) { boardgame in
                    NavigationLink(tag: boardgame.bggID, selection: $selectedBoardgameBGGID) {
                        BoardgameDetail(boardgame: boardgame)
                    } label: {
                        LibraryRow(boardgame: boardgame)
                    }.id(boardgame.bggID)
                }
                .onDelete(perform: deleteItems)
            }
            .listStyle(PlainListStyle())
            .onReceive(appState.$pageToNavigateTo) { nav in
                if let nav = nav {
                    switch nav {
                        case .boardgameDetails(let bggID):
                            proxy.scrollTo(bggID)
                            selectedBoardgameBGGID = bggID
                    }
                }
            }
        }

    }

    @ViewBuilder
    private var rootView: some View {
        #if os(iOS)
        switch horizontalSizeClass {
            default:
                listView
        }
        #else
        listView
        #endif
    }

    @ViewBuilder
    private var overlayView: some View {
        if libraryVM.boardgames.isEmpty {
            EmptyPlaceholderView(text: "There are no games in your library.", image: Image(systemSymbol: .questionmark))
        } else {
            EmptyView()
        }
    }

    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { libraryVM.boardgames[$0] }.forEach(viewContext.delete)
            PersistenceController.shared.save()
        }
    }
}

struct LibraryList_Previews: PreviewProvider {
    static var previews: some View {
        return LibraryList()
            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
        #if os(iOS)
            .navigationViewStyle(.stack)
        #endif
    }
}
