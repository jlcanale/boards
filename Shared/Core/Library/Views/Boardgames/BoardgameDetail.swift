//
//  BoardgameDetail.swift
//  Boards
//
//  Created by Joseph Canale on 10/8/21.
//

import SwiftUI

struct BoardgameDetail: View {
    @ObservedObject var boardgame: Boardgame
    @State private var selectedTab: BoardgameTab = .overview
    @State private var showActionSheet: Bool = false
    @State private var refreshID = UUID()

    var body: some View {
        ScrollView {
            VStack {
                AsyncImage(url: boardgame.imageURL) { image in
                    image
                        .geekItemDetail()
                } placeholder: {
                    Image(systemSymbol: .photo)
                        .geekItemDetail()
                        .foregroundColor(.secondary)
                }
                Text("\(boardgame.name) \(boardgame.yearPublishedText)")
                    .font(.headline)

                Text(boardgame.abbreviatedDescription)
                    .font(.subheadline)
                    .multilineTextAlignment(.center)
            }
            BoardgameIcons(boardgame: boardgame)
            PillTabView(tabs: tabs, selectedTab: $selectedTab)
            tabs(for: boardgame)
        }
        .id(refreshID)
        .navigationTitle("Boardgame")
        #if os(iOS)
        .navigationBarTitleDisplayMode(.inline)
        #endif
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                Button {
                    showActionSheet = true
                } label: {
                    Image(systemSymbol: .ellipsis)
                }

            }
        }
        .confirmationDialog("More Actions", isPresented: $showActionSheet) {
            Button("Refresh from BGG") {
                Task {
                    do {
                        let geekItem = try await BGGAPI.shared.fetchGeekItem(with: boardgame.bggID, fetchDynamicInfo: true)
                        boardgame.update(geekItem: geekItem)
                        try boardgame.managedObjectContext?.save()
                        // This is to get the UI to refresh
                        self.refreshID = UUID()
                        self.selectedTab = .overview
                    } catch {
                        // TODO: Better error handling
                        print("ERROR!")
                    }
                }
            }
        }
    }

    private var tabs: [BoardgameTab] {
        #if os(iOS)
        return BoardgameTab.allCases
        #elseif os(macOS)
        return [.overview, .ranks, .stats, .versions, .expansions]
        #endif
    }

    @ViewBuilder
    private func tabs(for boardgame: Boardgame) -> some View {
        switch selectedTab {
            case .overview:
                BoardgameOverview(boardgame: boardgame)
                    .padding(.horizontal)
            case .ranks:
                RankList(ranks: Array(boardgame.ranks ?? []))
            case .forums:
                ForumsList(forumListVM: ForumListViewModel(objectID: boardgame.bggID))
            case .images:
                #if os(iOS)
                GeekImageGallery(gameImagesVM: GeekImageGalleryViewModel(objectID: boardgame.bggID))
                    .frame(height: 900)
                #else
                EmptyView()
                #endif
            case .videos:
                GeekVideoGalleryList(objectID: boardgame.bggID)
            case .files:
                GeekFileList(geekFileListVM: GeekFileListViewModel(objectID: boardgame.bggID))
                    .frame(height: 500)
            case .stats:
                GeekItemStatsList(statsVM: StatsViewModel(boardgame: boardgame))
            case .versions:
                PagedDataList(viewModel: BoardgameVersionListViewModel(objectID: boardgame.bggID, itemsPerPage: 10)) { item in
                    BoardgameVersionRow(boardgameVersion: item)
                }
                .listStyle(PlainListStyle())
                .frame(height: 500)
            case .expansions:
                PagedDataList(viewModel: BoardgameExpansionListViewModel(objectID: boardgame.bggID, itemsPerPage: 10)) { item in
                    BoardgameExpansionRow(boardgameExpansion: item)
                }
                .listStyle(PlainListStyle())
                .frame(height: 500)
        }
    }
}

struct BoardgameDetail_Previews: PreviewProvider {
    static var previews: some View {
        BoardgameDetail(boardgame: .sample)
    }
}
