//
//  BoardgameExpansionRow.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

struct BoardgameExpansionRow: View {
    let boardgameExpansion: BoardgameExpansion
    var body: some View {
        HStack {
            AsyncImage(url: boardgameExpansion.images?["thumb"]) { image in
                image
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 6))
                    .frame(width: 100, height: 100)
            } placeholder: {
                Image(systemSymbol: .photo)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100, height: 100)
                    .foregroundColor(.secondary)
            }
            VStack(alignment: .leading) {
                Text("\(boardgameExpansion.name) (\(boardgameExpansion.yearPublished))")
                    .font(.headline)
                    .lineLimit(2)
                    .minimumScaleFactor(0.7)
                HStack {
                    VStack(alignment: .leading) {
                        Text("Ratings: \(boardgameExpansion.ratingsCount)")
                            .font(.caption2)
                        Text("Weight: \(boardgameExpansion.averageWeight)")
                            .font(.caption2)
                        Text("Comments: \(boardgameExpansion.commentsCount)")
                            .font(.caption2)
                    }

                    Spacer()

                    VStack(alignment: .leading) {
                        Text("Own: \(boardgameExpansion.ownedCount)")
                            .font(.caption2)
                        Text("Prev. Own: \(boardgameExpansion.previouslyOwnedCount)")
                            .font(.caption2)
                        Text("For Trade: \(boardgameExpansion.forTradeCount)")
                            .font(.caption2)
                    }

                    Spacer()

                    VStack(alignment: .leading) {
                        Text("Want in Trade: \(boardgameExpansion.wantInTradeCount)")
                            .font(.caption2)
                        Text("Wishlist: \(boardgameExpansion.wishlistCount)")
                            .font(.caption2)
                    }

                }
            }
        }
    }
}

struct BoardgameExpansionRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            BoardgameExpansionRow(boardgameExpansion: .sample[0])
        }.listStyle(PlainListStyle())
    }
}
