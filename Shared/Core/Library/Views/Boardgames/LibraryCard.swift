//
//  LibraryCard.swift
//  Boards (iOS)
//
//  Created by Joseph Canale on 10/12/21.
//

import SwiftUI

struct LibraryCard: View {
    @Environment(\.managedObjectContext) private var viewContext
    let boardgame: Boardgame
    @State private var showingPopover = false
    var body: some View {
            VStack {
                asyncImage
                Text("\(boardgame.name) \(boardgame.yearPublishedText)")
                    .font(.headline)
                    .multilineTextAlignment(.center)
                HStack {
                    Image(systemSymbol: .person2)
                    Text(boardgame.playerCountText)
                    Image(systemSymbol: .stopwatch)
                    Text(boardgame.playTimeText)
                    Button {
                        showingPopover = true
                    } label: {
                        Image(systemSymbol: .ellipsis)
                            .imageScale(.large)
                    }
                    .confirmationDialog("", isPresented: $showingPopover) {
                        Button(role: .destructive) {
                            withAnimation {
                                viewContext.delete(boardgame)
                                PersistenceController.shared.save()
                            }
                        } label: {
                            Text("Remove from library")
                        }
                    }

                }
                .font(.subheadline)
                Text(boardgame.abbreviatedDescription)
                    .frame(width: 230)
                    .font(.caption)
                Spacer()
            }
            .padding()
            .frame(height: 300)
            .background(.thinMaterial)
            .mask(RoundedRectangle(cornerRadius: 8))
            .shadow(radius: 4)
            .padding(.bottom, 4)
    }

    var asyncImage: some View {
        AsyncImage(url: boardgame.imageURL) { phase in
            switch phase {
                case .empty:
                    HStack {
                        Spacer()
                        ProgressView()
                        Spacer()
                    }
                    .background(Color.gray.opacity(0.6))
                    .clipShape(RoundedRectangle(cornerRadius: 8))
                    .frame(width: 180, height: 180)

                case .success(let image):
                    image
                        .resizable()
                        .scaledToFit()
                        .clipShape(RoundedRectangle(cornerRadius: 8))
                        .frame(width: 150, height: 150)

                case .failure:
                    HStack {
                        Spacer()
                        Image(systemSymbol: .photo)
                            .imageScale(.large)
                        Spacer()
                    }
                    .frame(width: 180, height: 180)

                @unknown default:
                    fatalError()
            }
        }
    }
}

struct LibraryCard_Previews: PreviewProvider {
    static var previews: some View {
        LibraryCard(boardgame: .sample)
    }
}
