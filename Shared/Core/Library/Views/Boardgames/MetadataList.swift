//
//  MetadataList.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI

struct MetadataList: View {
    @Environment(\.dismiss) var dismiss
    var title: String
    var items: [Metadata]?

    var body: some View {
        #if os(iOS)
        NavigationView {
            contentView
                .navigationBarTitleDisplayMode(.inline)

        }
        #else
        contentView
            .frame(width: 200, height: 400)
        #endif
    }

    @ViewBuilder
    private var contentView: some View {
        Group {
            if let items = items, !items.isEmpty {
                List(items, id: \.name) { item in
                    Text(item.name)
                }
                .listStyle(PlainListStyle())

            } else {
                Text("No \(title)")
            }
        }
        .navigationTitle(title)
        .toolbar {
            ToolbarItem(placement: .cancellationAction) {
                Button {
                    dismiss()
                } label: {
                    Text("Close")
                }
            }
        }
    }
}

struct GeekItemMetadataList_Previews: PreviewProvider {
    static var previews: some View {
        MetadataList(title: GeekItemLink.designer.pluralTitle, items: GeekItem.sampleBoardgame.links[.designer])
    }
}
