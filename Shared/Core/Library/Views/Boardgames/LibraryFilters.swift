//
//  LibraryFilters.swift
//  Boards
//
//  Created by Joseph Canale on 10/11/21.
//

import SwiftUI

struct LibraryFilters: View {

    @ObservedObject var filterVM: LibraryFilterViewModel
    @Environment(\.dismiss) var dismiss

    var body: some View {
        #if os(iOS)
        NavigationView {
            filterForm
        }
        #else
        // TODO: This needs a lot of work on MacOS.  Maybe it's own form
        filterForm
            .padding()
            .frame(width: 400, height: 600)
        #endif
    }

    @ViewBuilder
    private var filterForm: some View {
        Form {
            Section(header: Text("Status")) {
                CheckmarkRow(isChecked: $filterVM.showingOwned, text: "Owned")
                CheckmarkRow(isChecked: $filterVM.showingPreviouslyOwned, text: "Previously Owned")
                CheckmarkRow(isChecked: $filterVM.showingForTrade, text: "For Trade")
                CheckmarkRow(isChecked: $filterVM.showingWantToPlay, text: "Want to Play")
                CheckmarkRow(isChecked: $filterVM.showingWantInTrade, text: "Want in Trade")
                CheckmarkRow(isChecked: $filterVM.showingWantToBuy, text: "Want to Buy")
                CheckmarkRow(isChecked: $filterVM.showingPreOrdered, text: "Pre-Ordered")
                CheckmarkRow(isChecked: $filterVM.showingOnWishlist, text: "On Wishlist")
            }

            Section(header: Text("Player Count")) {
                VStack(alignment: .leading) {
                    Toggle("Enable", isOn: $filterVM.filteringPlayerCount)
                    if filterVM.filteringPlayerCount {
                        Text(filterVM.playerCountText)
                        Slider(value: $filterVM.playerCount, in: filterVM.playerCountRange, step: 1.0)
                    }
                }

            }

            Section(header: Text("Ratings")) {
                VStack(spacing: 10) {
                    Toggle("Geek Rating", isOn: $filterVM.filteringGeekRating)
                    if filterVM.filteringGeekRating {
                        Picker("Show if Rating is", selection: $filterVM.geekRatingComparisonMode) {
                            ForEach(ComparisonMode.allCases) { comparisonMode in
                                Text(comparisonMode.text)
                            }
                        }
                        StarRatingView(rating: $filterVM.geekRating)
                    }
                }

                VStack(spacing: 10) {
                    Toggle("My Rating", isOn: $filterVM.filteringMyRating)
                    if filterVM.filteringMyRating {
                        Picker("Show if My Rating is", selection: $filterVM.myRatingComparisonMode) {
                            ForEach(ComparisonMode.allCases) { comparisonMode in
                                Text(comparisonMode.text)
                            }
                        }
                        StarRatingView(rating: $filterVM.myRating)
                    }
                }

            }

            Section(header: Text("Play Time")) {
                VStack(alignment: .leading) {
                    Toggle("Enable", isOn: $filterVM.filteringPlaytime)
                    if filterVM.filteringPlaytime {
                        Text(filterVM.playtimeText)
                        Slider(value: $filterVM.playtime, in: filterVM.playtimeRange, step: 15)
                    }
                }
            }
        }
        .navigationTitle("Filters")
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                Button {
                    filterVM.buildPredicate()
                    dismiss()
                } label: {
                    Text("Apply")
                }

            }
            ToolbarItem(placement: .navigation) {
                Button {
                    filterVM.clearFilters()
                } label: {
                    Text("Clear")
                }
            }
            ToolbarItem(placement: .navigation) {
                Button {
                    dismiss()
                } label: {
                    Text("Cancel")
                }
            }
        }
    }
}

struct LibraryFilters_Previews: PreviewProvider {
    static var previews: some View {
        LibraryFilters(filterVM: LibraryFilterViewModel())
    }
}
