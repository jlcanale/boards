//
//  BoardgameVersionRow.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

struct BoardgameVersionRow: View {
    let boardgameVersion: BoardgameVersion

    var body: some View {
        HStack {
            AsyncImage(url: boardgameVersion.images?["thumb"]) { image in
                image
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 6))
                    .frame(width: 100, height: 100)
            } placeholder: {
                Image(systemSymbol: .photo)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100, height: 100)
                    .foregroundColor(.secondary)
            }

            VStack(alignment: .leading) {
                Text("\(boardgameVersion.name) (\(boardgameVersion.yearPublished))")
                    .font(.headline)
                    .lineLimit(2)
                Text("Status: \(boardgameVersion.releaseStatusText.capitalized)")
                    .font(.caption)
                Text("Release Date: \(boardgameVersion.releaseDateText)")
                    .font(.caption)
            }
        }
    }
}

struct BoardgameVersionRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            BoardgameVersionRow(boardgameVersion: .sample[0])
        }
        .listStyle(PlainListStyle())
    }
}
