//
//  StatusView.swift
//  Boards
//
//  Created by Joseph Canale on 10/11/21.
//

import SwiftUI

struct StatusView: View {
    @StateObject var statusVM: StatusViewModel
    @Environment(\.dismiss) var dismiss

    var body: some View {
        #if os(iOS)
        NavigationView {
            contentView
                .toggleStyle(.button)

        }
        #else
        contentView
            .padding()
            .frame(width: 400, height: 125)
        #endif
    }

    @ViewBuilder
    private var contentView: some View {
        HStack {
            VStack(alignment: .leading) {
                Toggle("Owned", isOn: $statusVM.owned)
                Toggle("Previously Owned", isOn: $statusVM.previouslyOwned)
                Toggle("For Trade", isOn: $statusVM.forTrade)
                Toggle("Want to Play", isOn: $statusVM.wantToPlay)
                Spacer()
            }
            .padding(.horizontal, 20)

            Spacer()

            VStack(alignment: .leading) {
                Toggle("Want in Trade", isOn: $statusVM.wantInTrade)
                Toggle("Want to Buy", isOn: $statusVM.wantToBuy)
                Toggle("Pre-Ordered", isOn: $statusVM.preordered)
                Toggle("On Wishlist", isOn: $statusVM.onWishlist)
                Spacer()
            }
            .padding(.horizontal, 20)
        }
        .navigationTitle("Status")
        .toolbar {
            ToolbarItem(placement: .confirmationAction) {
                Button {
                    statusVM.save()
                    dismiss()
                } label: {
                    Text("Save")
                }
            }

            ToolbarItem(placement: .cancellationAction) {
                Button {
                    dismiss()
                } label: {
                    Text("Cancel")
                }
            }
        }
    }
}

struct StatusView_Previews: PreviewProvider {
    static var previews: some View {
        StatusView(statusVM: StatusViewModel(geekItem: .sampleBoardgame))
    }
}
