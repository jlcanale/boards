//
//  BoardgameOverview.swift
//  Boards
//
//  Created by Joseph Canale on 10/8/21.
//

import SwiftUI

struct BoardgameOverview: View {
    let boardgame: Boardgame
    @State private var selectedLink: GeekItemLink?

    var body: some View {
        VStack(spacing: 10) {
            ExpandableTextView(text: boardgame.fullDescription.stripHTML())
            ForEach(GeekItemLink.topCredits + GeekItemLink.classifications) { geekItemLink in
                StringsDisplayView(title: geekItemLink.pluralTitle, items: boardgame.itemsFor(geekItemLink: geekItemLink))
                    .onTapGesture {
                        selectedLink = geekItemLink
                    }
            }
        }
        .sheet(item: $selectedLink, onDismiss: nil) { link in
            MetadataList(title: link.pluralTitle, items: boardgame.metadataFor(geekItemLink: link))
        }
    }
}

struct BoardgameOverview_Previews: PreviewProvider {
    static var previews: some View {
        BoardgameOverview(boardgame: .sample)
    }
}
