//
//  BoardgameIcons.swift
//  Boards
//
//  Created by Joseph Canale on 10/8/21.
//

import SwiftUI
import SwiftUIKit

struct BoardgameIcons: View {
    let boardgame: Boardgame
    @StateObject private var sheetContext = SheetContext()
    @State private var showingGeekRating = true

    var body: some View {
        VStack {
            HStack(spacing: 75) {
                IconView(iconType: .playerCount, text: boardgame.playerCountText)
                IconView(iconType: .playTime, text: boardgame.playTimeText)
                IconView(iconType: .weight, text: boardgame.weightText)
            }
            HStack(spacing: 75) {
                IconView(iconType: showingGeekRating ? .geekRating : .averageRating, text: showingGeekRating ? boardgame.overallRatingText : boardgame.averageRatingText)
                    .onTapGesture {
                        showingGeekRating.toggle()
                    }
                IconView(iconType: .myRating, text: boardgame.myRatingText)
                    .onTapGesture {
                        sheetContext.present(MyRating(myRatingVM: MyRatingViewModel(boardgame: boardgame)))
                    }
                IconView(iconType: .status, text: "Status")
                    .onTapGesture {
                        sheetContext.present(StatusView(statusVM: StatusViewModel(boardgame: boardgame)))
                    }
            }
        }
        .sheet(sheetContext)
    }
}

struct BoardgameIcons_Previews: PreviewProvider {
    static var previews: some View {
        BoardgameIcons(boardgame: .sample)
    }
}
