//
//  MyRating.swift
//  Boards (iOS)
//
//  Created by Joseph Canale on 10/12/21.
//

import SwiftUI

struct MyRating: View {
    @StateObject var myRatingVM: MyRatingViewModel
    var body: some View {
        NavigationView {
            VStack {
                Text(myRatingVM.ratingText)
                    .multilineTextAlignment(.center)
                    .font(.subheadline)
                    .padding(.bottom)
                StarRatingView(rating: $myRatingVM.myRating, offImage: Image(systemSymbol: .star), onImage: Image(systemSymbol: .starFill), offColor: .yellow, onColor: .yellow)
                Spacer()
            }
            .padding()
            .onDisappear {
                myRatingVM.save()
            }
            .navigationTitle("My Rating")
        }

    }
}

struct MyRating_Previews: PreviewProvider {
    static var previews: some View {
        MyRating(myRatingVM: MyRatingViewModel(boardgame: .sample))
    }
}
