//
//  GeekVideoGalleryList.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

struct GeekVideoGalleryList: View {
    let objectID: String

    var body: some View {
        VStack {
            ForEach(VideoGalleryType.allCases) { gallery in
                NavigationLink {
                    PagedDataList(viewModel: GeekVideoGalleryViewModel(objectID: objectID, gallery: gallery)) { item in
                        GeekVideoRow(geekVideo: item)
                    }
                    .listStyle(PlainListStyle())
                    .navigationTitle(gallery.rawValue.capitalized)
                } label: {
                    HStack {
                        Text(gallery.rawValue.capitalized)
                            .font(.headline)
                        Spacer()
                        Image(systemSymbol: .chevronForward)
                            .foregroundColor(.secondary)
                    }
                    .contentShape(Rectangle())

                }
                Divider()
            }
            .padding(.horizontal)
            .padding(.vertical, 3)
        }
        .buttonStyle(.plain)
    }
}

struct GeekVideoGalleryList_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            GeekVideoGalleryList(objectID: "\(GeekItem.sampleBoardgame.objectID)")
        }
    }
}
