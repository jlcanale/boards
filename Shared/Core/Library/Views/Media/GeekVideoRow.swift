//
//  GeekVideoRow.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI
import YouTubePlayerKit
import SwiftUIKit

struct GeekVideoRow: View {
    let geekVideo: GeekVideo
    private let youtubePlayer: YouTubePlayer
    @State private var showingVideo = false
    @State private var showingActionSheet = false
    @StateObject private var sheetContext = SheetContext()

    init(geekVideo: GeekVideo) {
        self.geekVideo = geekVideo
        self.youtubePlayer = YouTubePlayer(source: .video(id: geekVideo.externalVideoID), configuration: .init(allowsPictureInPictureMediaPlayback: true, autoPlay: true, playInline: false))
    }

    var body: some View {
        GeometryReader { reader in
            VStack {
                ZStack {
                    AsyncImage(url: geekVideo.images["thumb"]) { image in
                        image
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: reader.size.width)
                    } placeholder: {
                        Image(systemSymbol: .photo)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .foregroundColor(.gray.opacity(0.7))
                            .frame(width: reader.size.width * 0.8)
                            .overlay(ProgressView())
                    }
                    .onTapGesture { showingVideo = true }

                    if showingVideo {
                        player
                            .frame(width: reader.size.width)
                    }

                }

                HStack {
                    Text(geekVideo.title)
                        .lineLimit(1)
                    Spacer()
                }
                .font(.headline)
                .padding(.bottom, -5)

                HStack {
                    Text(geekVideo.gallery.capitalized)
                        .padding(2)
                        .foregroundColor(.black.opacity(0.7))
                        .background(.gray.opacity(0.5))
                        .background(in: Rectangle())
                    Text("\(geekVideo.user.username) · \(geekVideo.postDateRelaviveText)")
                    Button {
                        showingActionSheet = true
                    } label: {
                        Image(systemSymbol: .ellipsis)
                            .imageScale(.large)
                    }
                    .confirmationDialog("", isPresented: $showingActionSheet, titleVisibility: .hidden) { actionSheet }

                    Spacer()
                }
                .font(.caption)
            }
        }
        .frame(height: 300)
        #if os(iOS)
        .listRowSeparator(.hidden)
        #endif
        .padding(.bottom)
        .sheet(sheetContext)
    }

    @ViewBuilder
    private var actionSheet: some View {
        if let videoURL = URL(string: "https://www.youtube.com/watch?v=\(geekVideo.externalVideoID)") {
            Button("Watch on Youtube") {
                #if os(iOS)
                UIApplication.shared.open(videoURL, options: [:], completionHandler: nil)
                #elseif os(macOS)
                NSWorkspace.shared.open(videoURL)
                #endif
            }
            #if os(iOS)
            Button("Share") {
                sheetContext.present(ShareSheet(activityItems: [videoURL]))
            }
            #endif
        }
    }

    @ViewBuilder
    private var player: some View {
        if showingVideo {
            YouTubePlayerView(self.youtubePlayer) { state in
                switch state {
                    case .idle:
                        ProgressView()
                    case .ready:
                        EmptyView()
                    case .error:
                        Text("Youtube player could not be loaded")
                }
            }
            .frame(height: 220)
        }
    }
}

struct GeekVideoRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            GeekVideoRow(geekVideo: .sample[0])
            GeekVideoRow(geekVideo: .sample[0])
        }
        .listStyle(PlainListStyle())
    }
}
