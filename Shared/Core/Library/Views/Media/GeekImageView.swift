//
//  GeekImageView.swift
//  Boards
//
//  Created by Joseph Canale on 10/2/21.
//

import SwiftUI

struct GeekImageView: View {
    let geekImage: GeekImage

    let imageWidth: CGFloat = 246
    let imageHeight: CGFloat = 205

    var body: some View {
        AsyncImage(url: geekImage.largeImageURL) { image in
            VStack(spacing: 0) {
                image
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 6))
                    .frame(width: imageWidth, height: imageHeight)

                HStack {
                    Label("\(geekImage.thumbCount)", systemSymbol: .handThumbsup)
                        .padding(.horizontal, 5)
                        .offset(x: -25, y: 0)
                    Label("\(geekImage.commentCount)", systemSymbol: .bubbleRight)
                        .padding(.horizontal, 5)
                        .offset(x: 25, y: 0)
                }
            }

        } placeholder: {
            Image(systemSymbol: .photo)
                .resizable()
                .scaledToFit()
                .frame(width: imageWidth, height: imageHeight)
                .foregroundColor(.secondary)
        }

    }
}

struct GeekImageView_Previews: PreviewProvider {
    static var previews: some View {
        GeekImageView(geekImage: .sample[1])

    }
}
