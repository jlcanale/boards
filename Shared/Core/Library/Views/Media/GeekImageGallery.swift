//
//  GeekImageGallery.swift
//  Boards
//
//  Created by Joseph Canale on 10/2/21.
//

import SwiftUI
import SwiftUIKit

struct GeekImageGallery: View {

    typealias Row = CollectionViewRow<ImageGalleryType, GeekImage>
    @StateObject var gameImagesVM: GeekImageGalleryViewModel
    // let peopleImages: [GeekImage]
    // let creativeImages: [GeekImage]

    var body: some View {
        CollectionView(
            rows: rows,
            layout: .shelves(itemSize: itemSize, headerHeight: 40, sectionInsets: sectionInsets, itemSpacing: 20),
            cell: cell,
            supplementaryView: sectionTitle)
            .edgesIgnoringSafeArea(.all)
            .task {
                await gameImagesVM.loadFirstPage(for: .all)
            }
    }

    private var rows: [Row] {
        [Row(section: .game, items: gameImagesVM.images(for: .game)),
         Row(section: .people, items: gameImagesVM.images(for: .people)),
         Row(section: .creative, items: gameImagesVM.images(for: .creative))]
    }

    @ViewBuilder
    private func cell(at indexPath: IndexPath, for item: GeekImage) -> some View {
        let gallery = rows[indexPath.section].section
        if gameImagesVM.images(for: gallery).last == item {
            GeekImageView(geekImage: item)
                .task { await gameImagesVM.loadNextPage(for: rows[indexPath.section].section) }
        } else {
            GeekImageView(geekImage: item)
        }
    }

    private var itemSize: CGSize {
        CGSize(width: 246, height: 230)
    }

    private var sectionInsets: NSDirectionalEdgeInsets {
        NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 0)
    }

    @ViewBuilder
    private func sectionTitle(for indexPath: IndexPath) -> some View {
        HStack {
            switch indexPath.section {
                case 0:
                    Text("Game")
                case 1:
                    Text("People")
                case 2:
                    Text("Creative")
                default:
                    Text("")
            }
            Spacer()
        }
        .font(.headline)
    }
}

struct GeekImageGallery_Previews: PreviewProvider {
    static var previews: some View {
        GeekImageGallery(gameImagesVM: GeekImageGalleryViewModel(geekImages: GeekImage.sample))
    }
}
