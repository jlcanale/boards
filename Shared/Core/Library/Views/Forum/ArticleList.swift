//
//  ArticleList.swift
//  Boards
//
//  Created by Joseph Canale on 10/1/21.
//

import SwiftUI
import SwiftUIKit

struct ArticleList: View {
    @StateObject var articleListVM: ArticleListViewModel
    @StateObject private var sheetContext = SheetContext()
    let threadURL: URL?

    var body: some View {
        List {
            ForEach(articles) { article in
                ArticleRow(article: article)
            }
        }
        .task { await articleListVM.loadData() }
        .listStyle(PlainListStyle())
        .navigationTitle("Posts")
        #if os(iOS)
        .navigationBarTitleDisplayMode(.inline)
        #endif
        .overlay(overlayView)
        .sheet(sheetContext)
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                if let threadURL = threadURL {
                    Button {
                        #if os(iOS)
                        sheetContext.present(AppSheet.safari(threadURL))
                        #elseif os(macOS)
                        NSWorkspace.shared.open(threadURL)
                        #endif
                    } label: {
                        Label("View on BGG", systemSymbol: .safari)
                    }
                }
            }
        }
    }

    @ViewBuilder
    private var overlayView: some View {
        switch articleListVM.phase {
            case .empty:
                ProgressView()
            case .success(let articles) where articles.isEmpty:
                EmptyPlaceholderView(text: "No Posts", image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription, retryAction: {})
            default:
                EmptyView()
        }
    }

    private var articles: [Article] {
        if case let .success(articles) = articleListVM.phase {
            return articles
        } else {
            return []
        }
    }

}

struct ArticleList_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ArticleList(articleListVM: ArticleListViewModel(articles: Article.sample), threadURL: nil)
        }
    }
}
