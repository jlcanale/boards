//
//  ForumsList.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import SwiftUI

struct ForumsList: View {
    @StateObject var forumListVM: ForumListViewModel

    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                NavigationLink {
                    GeekThreadList(geekThreadListVM: GeekThreadListViewModel(forumID: "0", objectID: forumListVM.objectID ?? ""), inAllForum: true)
                } label: {
                    HStack {
                        ForumRow(forum: forums.allForum)
                        Spacer()
                        Image(systemSymbol: .chevronForward)
                            .foregroundColor(.secondary)
                    }
                    .contentShape(Rectangle())
                }
                .padding(.horizontal)
                Divider()
                    .padding(.horizontal)

                ForEach(forums) { forum in
                    NavigationLink {
                        GeekThreadList(geekThreadListVM: GeekThreadListViewModel(forumID: forum.forumID, objectID: forumListVM.objectID ?? ""), inAllForum: false)
                    } label: {
                        HStack {
                            ForumRow(forum: forum)
                            Spacer()
                            Image(systemSymbol: .chevronForward)
                                .foregroundColor(.secondary)
                        }
                        .contentShape(Rectangle())

                    }
                    Divider()
                }
                .padding(.horizontal)
                .padding(.vertical, 3)

            }
            .buttonStyle(.plain)
            .task {
                await forumListVM.loadData()
            }
            Spacer()
        }
    }

    private var forums: [Forum] {
        if case let .success(forums) = forumListVM.phase {
            return forums
        } else {
            return []
        }
    }
}

struct ForumsList_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            ForumsList(forumListVM: ForumListViewModel(forums: Forum.sample))
            #if os(iOS)
                .navigationBarTitleDisplayMode(.inline)
            #endif
        }
    }
}
