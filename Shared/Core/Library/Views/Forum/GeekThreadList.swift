//
//  GeekThreadList.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import SwiftUI

struct GeekThreadList: View {

    @StateObject var geekThreadVM: GeekThreadListViewModel
    let inAllForum: Bool

    init(geekThreadListVM: GeekThreadListViewModel?, inAllForum: Bool = false) {
        self._geekThreadVM = StateObject(wrappedValue: geekThreadListVM ?? GeekThreadListViewModel())
        self.inAllForum = inAllForum
    }

    var body: some View {
        List {
            ForEach(geekThreadVM.threads) { thread in
                if thread == geekThreadVM.threads.last {
                    navigationLink(for: thread)
                        .task { await geekThreadVM.loadNextPage() }
                    if geekThreadVM.isFetchingNextPage {
                        bottomProgressView
                    }
                } else {
                    navigationLink(for: thread)
                }
            }
        }
        .overlay(overlayView)
        .task { await loadTask() }
        .listStyle(PlainListStyle())
        #if os(iOS)
        .navigationBarTitleDisplayMode(.inline)
        #endif
        .navigationTitle(navigationTitle)
    }

    private func navigationLink(for thread: GeekThread) -> some View {
        NavigationLink(destination: ArticleList(articleListVM: ArticleListViewModel(threadID: thread.threadID), threadURL: thread.bggURL)) {
            listRowView(for: thread)
        }
    }

    private func loadTask() async {
        await geekThreadVM.loadFirstPage()
    }

    private func listRowView(for thread: GeekThread) -> some View {
        GeekThreadRow(geekThread: thread, inAllForum: inAllForum)
    }

    @ViewBuilder
    private var overlayView: some View {
        switch geekThreadVM.phase {
            case .empty:
                ProgressView()
            case .success(let threads) where threads.isEmpty:
                EmptyPlaceholderView(text: "No Threads", image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription, retryAction: {})
            default:
                EmptyView()
        }
    }

    @ViewBuilder
    private var bottomProgressView: some View {
        Divider()
        HStack {
            Spacer()
            ProgressView()
            Spacer()
        }.padding()
    }

    private var navigationTitle: String {
        if inAllForum {
            return "All"
        } else {
            return geekThreadVM.threads.first?.forumTitle ?? ""
        }
    }
}

struct GeekThreadList_Previews: PreviewProvider {
    static var previews: some View {
        GeekThreadList(geekThreadListVM: GeekThreadListViewModel(geekThreads: GeekThread.sample), inAllForum: true)
    }
}
