//
//  GeekThreadRow.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import SwiftUI

struct GeekThreadRow: View {

    let geekThread: GeekThread
    let inAllForum: Bool

    var body: some View {
        HStack(spacing: -10) {
            HStack(spacing: 5) {
                Image(systemSymbol: .handThumbsupFill)
                Text(geekThread.thumbCount)
                Text("|")
                Image(systemSymbol: .bubbleRightFill)
                Text(geekThread.postCount)
                Spacer()
            }
            .frame(maxWidth: 100)

            .font(.caption2)

            VStack(alignment: .leading, spacing: 4) {
                Text(geekThread.subject)
                    .font(.headline)
                    .lineLimit(2)
                    .minimumScaleFactor(0.5)
                HStack {
                    if inAllForum {
                        Text(geekThread.forumTitle)
                            .font(.caption)
                            .padding(3)
                            .background(.gray.opacity(0.5))
                            .background(in: Capsule())
                    }
                    Text(geekThread.user?.username ?? "")
                        .font(.caption)
                        .lineLimit(1)
                        .multilineTextAlignment(.leading)

                    Text("·")
                    Text(geekThread.threadCreatedRelativeString)
                        .font(.caption)
                }
            }
        }
    }
}

struct GeekThreadRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            GeekThreadRow(geekThread: .sample[0], inAllForum: false)
            GeekThreadRow(geekThread: .sample[0], inAllForum: true)
        }
        .listStyle(PlainListStyle())
    }
}
