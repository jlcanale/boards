//
//  ArticleRow.swift
//  Boards
//
//  Created by Joseph Canale on 10/1/21.
//

import SwiftUI

struct ArticleRow: View {

    let article: Article
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("\(article.user?.firstName ?? "Unknown") \(article.user?.lastName ?? "")")
                    .font(.headline)
                if article.firstPost {
                    Image(systemSymbol: .crownFill)
                        .imageScale(.small)
                        .foregroundColor(.yellow)
                }
                Text("@\(article.user?.username ?? "") · \(article.postDateText)")
                    .font(.caption)
            }
            Text(article.body)
            Label("1", systemSymbol: .handThumbsup)
        }
    }
}

struct ArticleRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            ArticleRow(article: .sample[0])
        }
        .listStyle(PlainListStyle())
    }
}
