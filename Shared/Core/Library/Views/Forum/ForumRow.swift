//
//  ForumRow.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import SwiftUI

struct ForumRow: View {
    let forum: Forum
    var body: some View {
        HStack {
            Text(forum.title)
                .font(.headline)
            Text(forum.threadCount.thousandsFormatted)
                .padding(.horizontal, 7)
                .background(.gray)
                .background(in: Capsule())
                .foregroundColor(.white)
                .font(.subheadline)
        }
    }
}

struct ForumRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            List {
                ForumRow(forum: Forum.sample[0])
            }
            .listStyle(PlainListStyle())
        }
    }
}
