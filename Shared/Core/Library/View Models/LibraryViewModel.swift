//
//  LibraryViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/18/21.
//

import Foundation

@MainActor
class LibraryViewModel: ObservableObject {

    @Published var boardgames: [Boardgame] = []
    @Published var errorMessage: String?
    @Published var showError: Bool = false

    @Published var syncing = false
    @Published var syncProgress = Progress(totalUnitCount: 0)
    @Published var syncMessage = ""

    private var bggAPI = BGGAPI.shared
    private var settingsStore = SettingsStore.shared

    var predicate: NSPredicate? {
        didSet {
            fetchData()
        }
    }

    private let persistenceController = PersistenceController.shared

    init() {
        fetchData()
    }

    func fetchData() {
        let fetchRequest = Boardgame.makeFetchRequest()
        fetchRequest.predicate = self.predicate
        fetchRequest.sortDescriptors = [NSSortDescriptor(keyPath: \Boardgame.name, ascending: true)]
        self.boardgames = (try? persistenceController.container.viewContext.fetch(fetchRequest)) ?? []
    }

    func refreshDynamicInfo() async {
        self.syncing = true
        self.syncProgress = Progress(totalUnitCount: Int64(boardgames.count))
        self.syncMessage = "Refreshing Dynamic Info"

        defer {
            self.syncing = false
            self.persistenceController.save()
            self.fetchData()
        }

        for boardgame in boardgames {
            do {
                let dynamicInfo = try await bggAPI.fetchDynamicInfo(forID: boardgame.bggID)
                boardgame.update(dynamicInfo: dynamicInfo)
                self.syncProgress.completedUnitCount += 1
            } catch {
                self.errorMessage = "Error fetching dynamic info for: \(boardgame.name)"
                self.showError = true
            }
        }
    }

    func syncCollection() async {
        let username = settingsStore.geekUserManager.username
        guard !username.isEmpty else {
            self.errorMessage = "You have to complete your Profile in Settings before you can sync."
            self.showError = true
            return
        }

        self.syncing = true
        self.syncMessage = "Syncing Collection"
        defer {
            self.syncing = false
            self.persistenceController.save()
            self.fetchData()
        }

        if let collectionItems = try? await bggAPI.fetchCollection(forUser: username) {

            self.syncProgress = Progress(totalUnitCount: Int64(collectionItems.items.count))
            var geekItems = [(geekItem: GeekItem, status: GeekCollectionItemStatus)]()

            for item in collectionItems.items {
                if let geekItem = try? await bggAPI.fetchGeekItem(with: item.objectID, fetchDynamicInfo: true) {
                    geekItems.append((geekItem, item.status))
                    self.syncProgress.completedUnitCount += 1
                }
            }

            if settingsStore.collectionSyncMethod == .delete {
                self.boardgames.forEach(persistenceController.container.viewContext.delete)
            }

            for item in geekItems {
                let newBoardgame = Boardgame.from(geekItem: item.geekItem, in: persistenceController.container.viewContext)
                newBoardgame.update(status: item.status)
            }
        }
    }
}
