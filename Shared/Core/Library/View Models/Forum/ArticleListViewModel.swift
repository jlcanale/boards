//
//  ArticleListViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/1/21.
//

import Foundation

@MainActor
class ArticleListViewModel: ObservableObject {
    @Published var phase = DataFetchPhase<[Article]>.empty
    let threadID: String?

    private let bggAPI: BGGAPIService

    init(articles: [Article]? = nil, threadID: String? = nil, service: BGGAPIService = BGGAPI.shared) {
        self.bggAPI = service
        if let articles = articles {
            self.phase = .success(articles)
            self.threadID = nil
        } else {
            self.phase = .empty
            self.threadID = threadID
        }
    }

    func loadData() async {
        guard !Task.isCancelled else { return }
        guard let threadID = threadID else { return }
        phase = .empty
        do {
            let forums = try await bggAPI.fetchArticles(forThreadID: threadID)
            guard !Task.isCancelled else { return }
            phase = .success(forums)
        } catch {
            guard !Task.isCancelled else { return }
            phase = .failure(error)
        }
    }
}
