//
//  ForumListViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import Foundation

@MainActor
class ForumListViewModel: ObservableObject {
    @Published var phase = DataFetchPhase<[Forum]>.empty
    let objectID: String?
    let objectType: GeekItemType

    private let bggAPI = BGGAPI.shared

    init(forums: [Forum]? = nil, objectID: String? = nil, objectType: GeekItemType = .thing) {
        if let forums = forums {
            self.phase = .success(forums)
            self.objectID = nil
        } else {
            self.phase = .empty
            self.objectID = objectID
        }
        self.objectType = objectType
    }

    init(objectID: String, objectType: GeekItemType = .thing) {
        self.phase = .empty
        self.objectID = objectID
        self.objectType = objectType
    }

    func loadData() async {
        guard !Task.isCancelled else { return }
        guard let objectID = objectID else { return }
        phase = .empty
        do {
            let forums = try await bggAPI.fetchForums(forID: objectID)
            guard !Task.isCancelled else { return }
            phase = .success(forums)
        } catch {
            guard !Task.isCancelled else { return }
            phase = .failure(error)
        }
    }
}
