//
//  GeekThreadListViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 9/30/21.
//

import SwiftUI

@MainActor
class GeekThreadListViewModel: ObservableObject {
    @Published var phase = DataFetchPhase<[GeekThread]>.empty
    let forumID: String
    let objectID: String

    private let bggAPI = BGGAPI.shared
    private let pagingData = PagingData(itemsPerPage: 100, maxPageLimit: 1000)

    var isFetchingNextPage: Bool {
        if case .fetchingNextPage = phase {
            return true
        }
        return false
    }

    var threads: [GeekThread] {
        phase.value ?? []
    }

    init(geekThreads: [GeekThread]? = nil, forumID: String = "0", objectID: String = "0") {
        if let geekThreads = geekThreads {
            self.phase = .success(geekThreads)
        } else {
            self.phase = .empty
        }

        self.forumID = forumID
        self.objectID = objectID
    }

    func loadFirstPage() async {
        guard !Task.isCancelled else { return }
        phase = .empty
        do {
            await pagingData.reset()
            let threads = try await pagingData.loadNextPage(dataFetchProvider: loadThreads(page:userInfo:))

            if Task.isCancelled { return }
            phase = .success(threads)
        } catch {
            if Task.isCancelled { return }
            print(error.localizedDescription)
            phase = .failure(error)
        }
    }

    func loadNextPage() async {
        guard !Task.isCancelled else { return }

        let threads = self.phase.value ?? []
        phase = .fetchingNextPage(threads)

        do {
            let nextThreads = try await pagingData.loadNextPage(dataFetchProvider: loadThreads(page:userInfo:))
            if Task.isCancelled { return }

            phase = .success(threads + nextThreads)
        } catch {
            print(error.localizedDescription)
        }
    }

    @Sendable
    private func loadThreads(page: Int, userInfo: [String: Any]?) async throws -> [GeekThread] {
        let threads = try await bggAPI.fetchGeekThreads(forumID: forumID, objectID: objectID, pageNumber: page, itemsPerPage: pagingData.itemsPerPage)
        if Task.isCancelled { return [] }
        return threads
    }
}
