//
//  GeekVideoGalleryViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

@MainActor
class GeekVideoGalleryViewModel: PagedDataListViewModel<GeekVideo> {
    let gallery: VideoGalleryType
    override var noDataText: String { "No Videos" }

    init(objectID: String, itemsPerPage: Int = 36, gallery: VideoGalleryType) {
        self.gallery = gallery
        super.init(objectID: objectID, itemsPerPage: itemsPerPage, userInfo: nil)
    }

    @Sendable
    override func loadPage(page: Int, userInfo: [String: Sendable]?) async throws -> [GeekVideo] {
        guard let objectID = objectID else { return [] }
        let items = try await bggAPI.fetchGeekVideos(objectID: objectID, gallery: gallery, pageNumber: page)
        if Task.isCancelled { return [] }
        return items
    }
}
