//
//  GeekImageGalleryViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/3/21.
//

import Foundation

// swiftlint: disable cyclomatic_complexity
class GeekImageGalleryViewModel: ObservableObject {
    @Published var gamesPhase = DataFetchPhase<[GeekImage]>.empty
    @Published var peoplePhase = DataFetchPhase<[GeekImage]>.empty
    @Published var creativePhase = DataFetchPhase<[GeekImage]>.empty

    let objectID: String
    static private let galleryUserInfoKey = "gallery"

    private let bggAPI = BGGAPI.shared
    private let gamesPagingData = PagingData(itemsPerPage: 36, maxPageLimit: 1000, userInfo: [galleryUserInfoKey: ImageGalleryType.game.rawValue])
    private let peoplePagingData = PagingData(itemsPerPage: 36, maxPageLimit: 1000, userInfo: [galleryUserInfoKey: ImageGalleryType.people.rawValue])
    private let creativePagingData = PagingData(itemsPerPage: 36, maxPageLimit: 1000, userInfo: [galleryUserInfoKey: ImageGalleryType.creative.rawValue])

    func isFetchingNextPage(for gallery: ImageGalleryType) -> Bool {
        switch gallery {
            case .game:
                if case .fetchingNextPage = gamesPhase {
                    return true
                }
            case .people:
                if case .fetchingNextPage = peoplePhase {
                    return true
                }
            case .creative:
                if case .fetchingNextPage = creativePhase {
                    return true
                }
            default:
                return false
        }
        return false
    }

    func images(for gallery: ImageGalleryType) -> [GeekImage] {
        switch gallery {
            case .game:
                return gamesPhase.value ?? []
            case .people:
                return peoplePhase.value ?? []
            case .creative:
                return creativePhase.value ?? []
            default:
                return []
        }
    }

    init(geekImages: [GeekImage]? = nil, objectID: String = "0") {
        if let geekImages = geekImages {
            self.gamesPhase = .success(geekImages)
        } else {
            self.gamesPhase = .empty
        }
        self.creativePhase = .empty
        self.peoplePhase = .empty

        self.objectID = objectID

    }

    func loadFirstPage(for gallery: ImageGalleryType) async {
        guard !Task.isCancelled else { return }

        if gallery == .game || gallery == .all {
            gamesPhase = .empty
            do {
                await gamesPagingData.reset()
                let images = try await gamesPagingData.loadNextPage(dataFetchProvider: loadImages(page:userInfo:))

                if Task.isCancelled { return }
                gamesPhase = .success(images)
            } catch {
                if Task.isCancelled { return }
                print(error.localizedDescription)
                gamesPhase = .failure(error)
            }
        }

        if gallery == .people || gallery == .all {
            peoplePhase = .empty
            do {
                await peoplePagingData.reset()
                let images = try await peoplePagingData.loadNextPage(dataFetchProvider: loadImages(page:userInfo:))

                if Task.isCancelled { return }
                peoplePhase = .success(images)
            } catch {
                if Task.isCancelled { return }
                print(error.localizedDescription)
                peoplePhase = .failure(error)
            }
        }

        if gallery == .creative || gallery == .all {
            creativePhase = .empty
            do {
                await creativePagingData.reset()
                let images = try await creativePagingData.loadNextPage(dataFetchProvider: loadImages(page:userInfo:))

                if Task.isCancelled { return }
                creativePhase = .success(images)
            } catch {
                if Task.isCancelled { return }
                print(error.localizedDescription)
                creativePhase = .failure(error)
            }
        }

    }

    func loadNextPage(for gallery: ImageGalleryType) async {

        if gallery == .game || gallery == .all {
            guard !Task.isCancelled else { return }

            let images = self.gamesPhase.value ?? []
            gamesPhase = .fetchingNextPage(images)

            do {
                let nextImages = try await gamesPagingData.loadNextPage(dataFetchProvider: loadImages(page:userInfo:))
                if Task.isCancelled { return }

                gamesPhase = .success(images + nextImages)
            } catch {
                print(error.localizedDescription)
            }
        }

        if gallery == .people || gallery == .all {
            guard !Task.isCancelled else { return }

            let images = self.peoplePhase.value ?? []
            peoplePhase = .fetchingNextPage(images)

            do {
                let nextImages = try await peoplePagingData.loadNextPage(dataFetchProvider: loadImages(page:userInfo:))
                if Task.isCancelled { return }

                peoplePhase = .success(images + nextImages)
            } catch {
                print(error.localizedDescription)
            }
        }

        if gallery == .creative || gallery == .all {
            guard !Task.isCancelled else { return }

            let images = self.creativePhase.value ?? []
            creativePhase = .fetchingNextPage(images)

            do {
                let nextImages = try await creativePagingData.loadNextPage(dataFetchProvider: loadImages(page:userInfo:))
                if Task.isCancelled { return }

                creativePhase = .success(images + nextImages)
            } catch {
                print(error.localizedDescription)
            }
        }

    }

    @Sendable
    private func loadImages(page: Int, userInfo: [String: Any]?) async throws -> [GeekImage] {
        if let galleryRawValue = userInfo?[Self.galleryUserInfoKey] as? String, let gallery = ImageGalleryType(rawValue: galleryRawValue) {
            let images = try await bggAPI.fetchGeekImages(objectID: objectID, type: .thing, gallery: gallery, pageNumber: page, itemsPerPage: gamesPagingData.itemsPerPage)
            if Task.isCancelled { return [] }
            return images
        }
        return []
    }
}
