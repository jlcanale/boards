//
//  BoardgameVersionListViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import Foundation

@MainActor
class BoardgameVersionListViewModel: PagedDataListViewModel<BoardgameVersion> {
    override var noDataText: String { "No Versions" }

    @Sendable
    override func loadPage(page: Int, userInfo: [String: Sendable]?) async throws -> [BoardgameVersion] {
        guard let objectID = objectID else { return [] }
        let items = try await bggAPI.fetchVersions(forID: objectID, pageNumber: page, itemsPerPage: pagingData.itemsPerPage)
        if Task.isCancelled { return [] }
        return items
    }
}
