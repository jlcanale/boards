//
//  StatusViewModel.swift
//  Boards (iOS)
//
//  Created by Joseph Canale on 10/11/21.
//

import Foundation
import CoreData

class StatusViewModel: ObservableObject {
    @Published var owned: Bool
    @Published var previouslyOwned: Bool
    @Published var forTrade: Bool
    @Published var wantToPlay: Bool

    @Published var wantInTrade: Bool
    @Published var wantToBuy: Bool
    @Published var preordered: Bool
    @Published var onWishlist: Bool

    private(set) var boardgame: Boardgame?
    let geekItem: GeekItem?
    let persistence = PersistenceController.shared

    init(boardgame: Boardgame) {
        self.boardgame = boardgame
        self.geekItem = nil

        self.owned = boardgame.owned
        self.previouslyOwned = boardgame.previouslyOwned
        self.forTrade = boardgame.forTrade
        self.wantToPlay = boardgame.wantToPlay

        self.wantInTrade = boardgame.wantInTrade
        self.wantToBuy = boardgame.wantToBuy
        self.preordered = boardgame.preordered
        self.onWishlist = boardgame.onWishlist
    }

    init(geekItem: GeekItem) {
        self.geekItem = geekItem
        self.boardgame = persistence.container.viewContext.fetchBy(Boardgame.self, bggID: "\(geekItem.objectID)")

        self.owned = boardgame?.owned ?? false
        self.previouslyOwned = boardgame?.previouslyOwned ?? false
        self.forTrade = boardgame?.forTrade ?? false
        self.wantToPlay = boardgame?.wantToPlay ?? false

        self.wantInTrade = boardgame?.wantInTrade ?? false
        self.wantToBuy = boardgame?.wantToBuy ?? false
        self.preordered = boardgame?.preordered ?? false
        self.onWishlist = boardgame?.onWishlist ?? false
    }

    func save() {
        if let geekItem = geekItem, self.boardgame == nil {
            self.boardgame = Boardgame(from: geekItem, in: persistence.container.viewContext)
        }

        boardgame?.owned = owned
        boardgame?.previouslyOwned = previouslyOwned
        boardgame?.forTrade = forTrade
        boardgame?.wantToPlay = wantToPlay

        boardgame?.wantInTrade = wantInTrade
        boardgame?.wantToBuy = wantToBuy
        boardgame?.preordered = preordered
        boardgame?.onWishlist = onWishlist

        persistence.save()
    }
}
