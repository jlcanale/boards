//
//  MyRatingViewModel.swift
//  Boards (iOS)
//
//  Created by Joseph Canale on 10/12/21.
//

import Foundation

class MyRatingViewModel: ObservableObject {
    @Published var myRating: Int {
        didSet {
            self.boardgame.myRating = Int32(myRating)
        }
    }
    let boardgame: Boardgame

    init(boardgame: Boardgame) {
        self.boardgame = boardgame
        self.myRating = boardgame.myRating.int
    }

    var ratingText: String {
        switch myRating {
            case 0:
                return "Game not rated"
            case 1:
                return "Defies description of a game.  You won't catch me playing this.  Clearly broken."
            case 2:
                return "Extremely annoying game, won't ever play this again."
            case 3:
                return "Likely won't play this again, although could be convinced.  Bad."
            case 4:
                return "Not so good, it doesn't get me but could be talked into on occasion."
            case 5:
                return "Average game, slightly boring, take it or leave it."
            case 6:
                return "OK game, some fun or challenge at least, will play sporadically if in the right mood."
            case 7:
                return "Good game, usually willing to play"
            case 8:
                return "Very good game.  I like to play.  Probably I'll suggest it and will never turn down a game."
            case 9:
                return "Excellent game.  Always want to play it."
            case 10:
                return "Outstanding.  Always want to play and expect this will never change."
            default:
                return ""
        }
    }

    func save() {
        PersistenceController.shared.save()
    }
}
