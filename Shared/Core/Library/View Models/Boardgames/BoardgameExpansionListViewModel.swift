//
//  BoardgameExpansionListViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

@MainActor
class BoardgameExpansionListViewModel: PagedDataListViewModel<BoardgameExpansion> {
    override var noDataText: String { "No Expansions" }

    @Sendable
    override func loadPage(page: Int, userInfo: [String: Sendable]?) async throws -> [BoardgameExpansion] {
        guard let objectID = objectID else { return [] }
        let items = try await bggAPI.fetchExpansions(forID: objectID, pageNumber: page, itemsPerPage: pagingData.itemsPerPage)
        if Task.isCancelled { return [] }
        return items
    }
}
