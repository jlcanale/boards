//
//  StatsViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import Foundation

struct StatsViewModel {
    enum StatType: String, CaseIterable, Identifiable {
        var id: Self { self }
        case gameStats = "Game Stats"
        case playStats = "Play Stats"
        case collectionStats = "Collection Stats"
        case partsExchange = "Parts Exchange"
    }

    typealias StatLabel = (label: String, value: String)
    let stats: Stats?
    let boardgame: Boardgame?

    init(stats: Stats) {
        self.stats = stats
        self.boardgame = nil
    }

    init(boardgame: Boardgame) {
        self.stats = nil
        self.boardgame = boardgame
    }

    // swiftlint: disable function_body_length
    func stats(forType type: StatType) -> [Int: StatLabel] {
        if let stats = stats {
            switch type {
                case .gameStats:
                    return [1: (label: "Avg. Rating", value: stats.averageRating.thousandsFormatted),
                            2: (label: "No. of Ratings", value: stats.totalRatings.thousandsFormatted),
                            3: (label: "Std. Deviation", value: stats.standardDeviation.thousandsFormatted),
                            4: (label: "Weight", value: stats.averageWeight.thousandsFormatted),
                            5: (label: "Comments", value: stats.totalComments.thousandsFormatted),
                            6: (label: "Page Views", value: stats.totalViews.thousandsFormatted)]
                case .playStats:
                    return [1: (label: "Total Plays", value: stats.totalPlays.thousandsFormatted),
                            2: (label: "This Month", value: stats.totalPlaysThisMonth.thousandsFormatted)]
                case .collectionStats:
                    return [1: (label: "Own", value: stats.totalOwned.thousandsFormatted),
                            2: (label: "Prev. Owned", value: stats.totalPreviouslyOwned.thousandsFormatted),
                            3: (label: "For Trade", value: stats.totalForTrade.thousandsFormatted),
                            4: (label: "Want In Trade", value: stats.totalWantInTrade.thousandsFormatted),
                            5: (label: "Wishlist", value: stats.totalWishlist.thousandsFormatted)]
                case .partsExchange:
                    return [1: (label: "Has Parts", value: stats.totalHasParts.thousandsFormatted),
                            2: (label: "Wants Parts", value: stats.totalWantsParts.thousandsFormatted)]
            }
        } else {
            switch type {
                case .gameStats:
                    return [1: (label: "Avg. Rating", value: boardgame!.averageRating.decimalValue.string),
                            2: (label: "No. of Ratings", value: boardgame!.totalRatings.thousandsFormatted),
                            3: (label: "Std. Deviation", value: boardgame!.standardDeviation.thousandsFormatted),
                            4: (label: "Weight", value: boardgame!.averageWeight.thousandsFormatted),
                            5: (label: "Comments", value: boardgame!.totalComments.thousandsFormatted),
                            6: (label: "Page Views", value: boardgame!.totalViews.thousandsFormatted)]
                case .playStats:
                    return [1: (label: "Total Plays", value: boardgame!.totalPlays.thousandsFormatted),
                            2: (label: "This Month", value: boardgame!.totalPlaysThisMonth.thousandsFormatted)]
                case .collectionStats:
                    return [1: (label: "Own", value: boardgame!.totalOwned.thousandsFormatted),
                            2: (label: "Prev. Owned", value: boardgame!.totalPreviouslyOwned.thousandsFormatted),
                            3: (label: "For Trade", value: boardgame!.totalForTrade.thousandsFormatted),
                            4: (label: "Want In Trade", value: boardgame!.totalWantInTrade.thousandsFormatted),
                            5: (label: "Wishlist", value: boardgame!.totalWishlist.thousandsFormatted)]
                case .partsExchange:
                    return [1: (label: "Has Parts", value: boardgame!.totalHasParts.thousandsFormatted),
                            2: (label: "Wants Parts", value: boardgame!.totalWantsParts.thousandsFormatted)]
            }
        }
    }
}
