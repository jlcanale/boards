//
//  GeekFilePageListViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import Foundation

@MainActor
class GeekFilePageListViewModel: SimpleListViewModel<GeekFilePage> {
    override var noDataText: String { "No Files" }

    override func fetchData() async throws -> [GeekFilePage] {
        return try await bggAPI.fetchGeekFilePages(forID: objectID ?? "0")
    }
}
