//
//  GeekFileListViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import Foundation

@MainActor
class GeekFileListViewModel: ObservableObject {
    @Published var phase = DataFetchPhase<[GeekFile]>.empty
    let objectID: String

    private let bggAPI = BGGAPI.shared
    private let pagingData = PagingData(itemsPerPage: 25, maxPageLimit: 1000)

    var isFetchingNextPage: Bool {
        if case .fetchingNextPage = phase {
            return true
        }
        return false
    }

    var files: [GeekFile] {
        phase.value ?? []
    }

    init(geekFiles: [GeekFile]? = nil, objectID: String = "0") {
        if let geekFiles = geekFiles {
            self.phase = .success(geekFiles)
        } else {
            self.phase = .empty
        }
        self.objectID = objectID
    }

    func loadFirstPage() async {
        guard !Task.isCancelled else { return }
        phase = .empty
        do {
            await pagingData.reset()
            let files = try await pagingData.loadNextPage(dataFetchProvider: loadFiles(page:userInfo:))

            if Task.isCancelled { return }
            phase = .success(files)
        } catch {
            if Task.isCancelled { return }
            print(error.localizedDescription)
            phase = .failure(error)
        }
    }

    func loadNextPage() async {
        guard !Task.isCancelled else { return }

        let files = self.phase.value ?? []
        phase = .fetchingNextPage(files)

        do {
            let nextFiles = try await pagingData.loadNextPage(dataFetchProvider: loadFiles(page:userInfo:))
            if Task.isCancelled { return }

            phase = .success(files + nextFiles)
        } catch {
            print(error.localizedDescription)
        }
    }

    @Sendable
    private func loadFiles(page: Int, userInfo: [String: Sendable]?) async throws -> [GeekFile] {
        let files = try await bggAPI.fetchGeekFiles(forID: objectID, pageNumber: page, itemsPerPage: pagingData.itemsPerPage)
        if Task.isCancelled { return [] }
        return files
    }
}
