//
//  LibraryFilterViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/11/21.
//

import Foundation
import SwiftUI

class LibraryFilterViewModel: ObservableObject {

    let playerCountRange = 1.0...10.0
    let playtimeRange = 15.0...240.0

    @Published private(set) var predicate = NSPredicate(value: true)

    @Published var searchText = "" {
        didSet {
            buildPredicate()
        }
    }

    @Published var showingOwned = true
    @Published var showingPreviouslyOwned = false
    @Published var showingForTrade = false
    @Published var showingWantToPlay = false
    @Published var showingWantInTrade = false
    @Published var showingWantToBuy = false
    @Published var showingPreOrdered = false
    @Published var showingOnWishlist = false

    @Published var filteringGeekRating = false
    @Published var geekRating: Int = 0
    @Published var geekRatingComparisonMode: ComparisonMode = .greaterThan

    @Published var filteringMyRating = false
    @Published var myRating: Int = 0
    @Published var myRatingComparisonMode: ComparisonMode = .greaterThan

    @Published var filteringPlayerCount = false
    @Published var playerCount: Double = 3

    @Published var filteringPlaytime = false
    @Published var playtime: Double = 45

    init() {
        buildPredicate()
    }

    var filtersActive: Bool {
        filteringPlayerCount || filteringPlaytime || !showingOwned
    }

    var playerCountText: String {
        "Can play \(playerCount.int) Players"
    }

    var playtimeText: String {
        if playtime >= 60 {
            let hours = playtime.int / 60
            let minutes = playtime.int % 60
            return "Plays in \(playtime.int / 60) hour\(hours > 1 ? "s" : "") \(minutes != 0 ? "and \(playtime.int % 60) mins" : "")"
        } else {
            return "Plays in \(playtime.int) mins"
        }
    }

    func clearFilters() {
        filteringPlayerCount = false
        filteringPlaytime = false
        filteringGeekRating = false
        filteringMyRating = false
        showingOwned = true
        showingPreviouslyOwned = false
        showingForTrade = false
        showingWantToPlay = false
        showingWantInTrade = false
        showingWantToBuy = false
        showingPreOrdered = false
        showingOnWishlist = false
        predicate = NSPredicate(value: true)
    }

    // swiftlint: disable cyclomatic_complexity
    func buildPredicate() {
        var compoundPredicate = NSPredicate(value: true)

        if !searchText.isEmpty {
            compoundPredicate = compoundPredicate.and(NSPredicate(format: "name BEGINSWITH %@", searchText))
        }

        if filteringPlayerCount {
            compoundPredicate = compoundPredicate.and(NSPredicate(format: "%i BETWEEN {minPlayers, maxPlayers}", playerCount.int))
        }

        if filteringPlaytime {
            compoundPredicate = compoundPredicate.and(NSPredicate(format: "%i BETWEEN {minPlaytime, maxPlaytime} OR minPlaytime < %i", playtime.int, playtime.int))
        }

        if filteringGeekRating {
            switch geekRatingComparisonMode {
                case .greaterThan:
                    compoundPredicate = compoundPredicate.and(NSPredicate(format: "bayesAverageRating > %i", geekRating))
                case .lessThan:
                    compoundPredicate = compoundPredicate.and(NSPredicate(format: "bayesAverageRating < %i", geekRating))
            }
        }

        if filteringMyRating {
            switch myRatingComparisonMode {
                case .greaterThan:
                    compoundPredicate = compoundPredicate.and(NSPredicate(format: "myRating > %i", myRating))
                case .lessThan:
                    compoundPredicate = compoundPredicate.and(NSPredicate(format: "myRating < %i", myRating))
            }
        }

        var statusPredicates = [NSPredicate]()
        if showingOwned {
            statusPredicates.append(NSPredicate(format: "owned == YES"))
        }
        if showingPreviouslyOwned {
            statusPredicates.append(NSPredicate(format: "previouslyOwned == YES"))
        }
        if showingForTrade {
            statusPredicates.append(NSPredicate(format: "forTrade == YES"))
        }
        if showingWantToPlay {
            statusPredicates.append(NSPredicate(format: "wantToPlay == YES"))
        }
        if showingWantInTrade {
            statusPredicates.append(NSPredicate(format: "wantInTrade == YES"))
        }
        if showingWantToBuy {
            statusPredicates.append(NSPredicate(format: "wantToBuy == YES"))
        }
        if showingPreOrdered {
            statusPredicates.append(NSPredicate(format: "preordered == YES"))
        }
        if showingOnWishlist {
            statusPredicates.append(NSPredicate(format: "onWishlist == YES"))
        }

        self.predicate = compoundPredicate.and(NSCompoundPredicate(orPredicateWithSubpredicates: statusPredicates))
    }
    // swiftlint: enable cyclomatic_complexity

}
