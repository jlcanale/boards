//
//  BBGSearchViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/6/21.
//

import Foundation
import Combine

@MainActor
class BGGSearchViewModel: ObservableObject {
    @Published var phase = DataFetchPhase<[SearchResultItem]>.initial
    @Published var searchText: String = ""
    private var cancellable: AnyCancellable! = nil
    private let bggAPI = BGGAPI.shared

    init() {
        cancellable = $searchText
            .removeDuplicates()
            .debounce(for: 0.8, scheduler: DispatchQueue.main)
            .sink { [unowned self] searchText in
                if searchText != "" && searchText.count > 3 {
                    self.performSearch()
                }
            }
    }

    var results: [SearchResultItem] {
        self.phase.value ?? []
    }

    var noResultsText: String { "No Resutls Found" }

    func performSearch() {
        Task {
            guard !Task.isCancelled else { return }
            phase = .empty
            do {
                let fetchedData = try await bggAPI.searchForGeekItem(query: searchText)
                guard !Task.isCancelled else { return }
                phase = .success(fetchedData)
            } catch {
                guard !Task.isCancelled else { return }
                phase = .failure(error)
            }
        }
    }
}
