//
//  SearchTab.swift
//  Boards
//
//  Created by Joseph Canale on 10/6/21.
//

import SwiftUI

struct SearchTab: View {
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    @StateObject var viewModel: BGGSearchViewModel

    init() {
        self._viewModel = StateObject(wrappedValue: BGGSearchViewModel())
    }

    // TODO: Roll our own search here because we can't turn off autocomplete
    var body: some View {
        ZStack {
            List {
                ForEach(viewModel.results) { result in
                    NavigationLink {
                        GeekItemView(geekItemVM: GeekItemViewModel(objectID: result.objectID))
                    } label: {
                        SearchResultRow(searchResult: result)
                    }
                }
            }
            .overlay(overlayView)
            .listStyle(PlainListStyle())
            .searchable(text: $viewModel.searchText)
            .navigationBarTitle("Search")

            if viewModel.searchText == "" && viewModel.results.isEmpty {
                EmptyPlaceholderView(text: "Tap on search to get started", image: Image(systemSymbol: .magnifyingglass))
            }
        }

    }

    @ViewBuilder
    private var overlayView: some View {
        switch viewModel.phase {
            case .empty:
                ProgressView()
            case .success where viewModel.results.isEmpty:
                EmptyPlaceholderView(text: viewModel.noResultsText, image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription, retryAction: retryAction)
            default:
                EmptyView()
        }
    }

    func retryAction() {
        viewModel.performSearch()
    }
}

struct SearchTab_Previews: PreviewProvider {
    static var previews: some View {
        SearchTab()
    }
}
