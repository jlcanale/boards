//
//  SearchResultRow.swift
//  Boards
//
//  Created by Joseph Canale on 10/6/21.
//

import SwiftUI

struct SearchResultRow: View {
    let searchResult: SearchResultItem
    var body: some View {
        Text("\(searchResult.name) (\(String(searchResult.yearPublished)))")
            .lineLimit(2)
            .minimumScaleFactor(0.5)
    }
}

struct SearchResultRow_Previews: PreviewProvider {
    static var previews: some View {
        List(SearchResultItem.sampleSearch) { result in
            SearchResultRow(searchResult: result)
        }.listStyle(PlainListStyle())
    }
}
