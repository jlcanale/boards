//
//  NewReleaseItemRow.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI

struct NewReleaseItemRow: View {
    let newReleaseItem: NewReleaseItem
    var body: some View {
        HStack {
            AsyncImage(url: newReleaseItem.image.source2xURL) { image in
                image
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 3))
                    .frame(width: 75, height: 75)
            } placeholder: {
                Image(systemSymbol: .photo)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 75, height: 75)
                    .foregroundColor(.secondary)
            }

            VStack(alignment: .leading) {
                Text(newReleaseItem.itemName)
                    .font(.headline)
                    .lineLimit(1)
                Text("\(newReleaseItem.item?.yearPublishedString ?? "") - by \(newReleaseItem.publisherName)")
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                Text(newReleaseItem.description)
                    .font(.caption)
                    .lineLimit(2)
                    .minimumScaleFactor(0.5)
            }
        }
    }
}

struct NewReleaseItemRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            NewReleaseItemRow(newReleaseItem: NewReleaseItem.sample[0])
                .listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
                #if os(iOS)
                .listRowSeparator(.hidden)
                #endif
        }
        .listStyle(PlainListStyle())
    }
}
