//
//  CrowdfundingRow.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import SwiftUI
struct CrowdfundingItemRow: View {
    let crowdfundingItem: CrowdfundingItem
    @State private var showSafariSheet: Bool = false
    var body: some View {
        HStack {
            AsyncImage(url: crowdfundingItem.images["mediacard"]?.source2xURL) { image in
                image
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 3))
                    .frame(width: 170, height: 100)
            } placeholder: {
                Image(systemSymbol: .photo)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 170, height: 100)
                    .foregroundColor(.secondary)
            }

            VStack(alignment: .leading) {
                HStack {
                    Text(crowdfundingItem.name)
                        .font(.headline)
                        .lineLimit(1)
                        .minimumScaleFactor(0.6)
                    Button {
                        #if os(iOS)
                        showSafariSheet = true
                        #elseif os(macOS)
                        NSWorkspace.shared.open(crowdfundingItem.orderURL)
                        #endif
                    } label: {
                        switch crowdfundingItem.orderType {
                            case .kickstarter:
                                Image(systemSymbol: .kSquare)
                            case .gamefound:
                                Image(systemSymbol: .gSquare)
                            default:
                                Image(systemSymbol: .questionmarkSquare)
                        }
                    }
                    .buttonStyle(.borderless)
                }

                Text("\(Int(crowdfundingItem.progress))% / \(crowdfundingItem.pledgedText) Pledged")
                    .font(.caption)
                Text("\(crowdfundingItem.backersCount) Backers")
                    .font(.caption)

                ProgressView(value: .minimum(crowdfundingItem.progress, 100), total: 100)
                    .progressViewStyle(.linear)
                    .tint(.green)
                    .padding(.trailing)

                Text(crowdfundingItem.endsInText)
                    .font(.caption)
                    .lineLimit(1)
                    .foregroundColor(crowdfundingItem.endingSoon ? .red : .primary)

            }
        }
        #if os(iOS)
        .sheet(isPresented: $showSafariSheet, onDismiss: nil) {
            SafariView(url: crowdfundingItem.orderURL)
                .edgesIgnoringSafeArea(.bottom)
        }
        #endif
    }
}

struct CrowdfundingItemRowView_Previews: PreviewProvider {
    static var previews: some View {
        List {
            CrowdfundingItemRow(crowdfundingItem: CrowdfundingItem.sample[0])
                .listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
            #if os(iOS)
                .listRowSeparator(.hidden)
            #endif
        }
        .listStyle(PlainListStyle())
    }
}
