//
//  BrowseTab.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import SwiftUI

struct BrowseTab: View {
    #if os(iOS)
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    #endif
    @ObservedObject var browseViewModel: BrowseViewModel
    @State private var crowdfundingSortMethod: CrowdfundingSortMethod = .endingSoon

    var body: some View {
            Group {
                switch browseViewModel.fetchTaskToken.category {
                    case .hotness:
                        HotnessItemList(geekItems: browseViewModel.browseItems.map(HotnessItem.init))
                    case .crowdfunding:
                        CrowdfundingItemList(crowdfundingItems: crowdfundingItems)
                    case .newReleases:
                        NewReleasesItemList(items: browseViewModel.browseItems.map(NewReleaseItem.init))
                }
            }
            .task(id: browseViewModel.fetchTaskToken, loadTask)
            .task(id: crowdfundingSortMethod, {
                browseViewModel.phase = .success(crowdfundingItems.map(BrowseItem.init))
            })
            .overlay(overlayView)
            .refreshable(action: refreshTask)
            .navigationTitle(browseViewModel.categoryTitle)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    if browseViewModel.fetchTaskToken.category == .crowdfunding {
                        sortMethodMenu
                    }
                }

                ToolbarItem(placement: .navigationBarTrailing) {
                    if horizontalSizeClass == .compact {
                        menu
                    }
                }
            }
    }

    @ViewBuilder
    private var overlayView: some View {
        switch browseViewModel.phase {
            case .empty:
                ProgressView()
            case .success(let items) where items.isEmpty:
                EmptyPlaceholderView(text: "No Items", image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription) {
                    Task {
                        await refreshTask()
                    }
                }
            default:
                EmptyView()
        }
    }

    private var crowdfundingItems: [CrowdfundingItem] {
        if case let .success(items) = browseViewModel.phase {
            let convertedItems = items.map(CrowdfundingItem.init)
            switch crowdfundingSortMethod {
                case .endingSoon:
                    return convertedItems.sorted { $0.ends < $1.ends }
                case .backersCount:
                    return convertedItems.sorted { $1.backersCount < $0.backersCount }
                case .pledgedAmount:
                    return convertedItems.sorted { $1.pledged < $0.pledged }
                case .percentBacked:
                    return convertedItems.sorted { $1.progress < $0.progress }
            }
        } else {
            return []
        }
    }

    @Sendable
    private func loadTask() async {
        await browseViewModel.loadData()
    }

    @Sendable
    private func refreshTask() async {
        await browseViewModel.refreshTask()
    }

    private var sortMethodMenu: some View {
        Menu {
            Picker("Sort Method List", selection: $crowdfundingSortMethod) {
                ForEach(CrowdfundingSortMethod.allCases) {
                    Text($0.rawValue).tag($0)
                }
            }
        } label: {
            Image(systemSymbol: .arrowUpArrowDown)
                .imageScale(.large)
        }
    }

    private var menu: some View {
        Menu {
            Picker("Item List", selection: $browseViewModel.fetchTaskToken.category) {
                ForEach(BrowseCategory.allCases) {
                    Text($0.text).tag($0)
                }
            }
        } label: {
            Image(systemSymbol: .fiberchannel)
                .imageScale(.large)
        }
    }
}

struct BrowseTab_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = BrowseViewModel(browseItems: HotnessItem.sample.map(BrowseItem.init))
        return BrowseTab(browseViewModel: viewModel)
    }
}
