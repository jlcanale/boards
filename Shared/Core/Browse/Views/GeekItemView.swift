//
//  GeekItemView.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI
import SwiftUIKit
import SystemNotification

struct GeekItemView: View {
    @StateObject var geekItemVM: GeekItemViewModel
    @State private var showingMoreActionsSheet = false
    @StateObject private var sheetContext = SheetContext()
    @State private var selectedTab: GeekItemTab = .overview
    @StateObject private var notificationContext = SystemNotificationContext()

    var body: some View {
        ScrollView {
            if let geekItem = geekItem {
                VStack {
                    AsyncImage(url: geekItem.imageURL) { image in
                        image
                            .geekItemDetail()
                    } placeholder: {
                        Image(systemSymbol: .photo)
                            .geekItemDetail()
                            .foregroundColor(.secondary)
                    }
                    HStack {
                        Text(geekItem.name)
                            .font(.headline)
                        if geekItem.yearPublishedText != "" {
                            Text("(\(geekItem.yearPublishedText))")
                                .font(.subheadline)
                        }
                        if geekItem.versionInfo?.orderURL != nil {
                            Image(systemSymbol: .kSquare)
                                .font(.subheadline)
                                .foregroundColor(.green)
                        }
                    }.padding(0)

                    Text(geekItem.shortDescription)
                        .font(.subheadline)
                        .multilineTextAlignment(.center)
                        .padding(.horizontal)

                    GeekItemIcon(geekItem: geekItem)
                        .padding(.bottom, 30)

                    PillTabView(tabs: tabs, selectedTab: $selectedTab)
                    tabs(forItem: geekItem)

                }
                .confirmationDialog("More Actions", isPresented: $showingMoreActionsSheet) {
                    confirmationDialog(for: geekItem)
                }
            }
        }
        .sheet(sheetContext)
        .systemNotification(notificationContext)
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                Button {
                    showingMoreActionsSheet = true
                } label: {
                    Label("More Actions", systemSymbol: .ellipsisCircle)
                }
            }
        }
        .task {
            await geekItemVM.loadData()
        }
        .overlay(overlayView)
        .navigationTitle("Boardgame")
        #if os(iOS)
        .navigationBarTitleDisplayMode(.inline)
        #elseif os(macOS)
        .frame(minWidth: 750)
        #endif
    }

    private var geekItem: GeekItem? {
        if case let .success(item) = geekItemVM.phase {
            return item
        } else {
            return nil
        }
    }

    private var tabs: [GeekItemTab] {
        #if os(iOS)
        return GeekItemTab.allCases
        #elseif os(macOS)
        return [.overview, .ranks, .stats, .versions, .expansions]
        #endif
    }

    @ViewBuilder
    private func confirmationDialog(for geekItem: GeekItem) -> some View {
        let persistence = PersistenceController.shared
        if  persistence.container.viewContext.fetchBy(Boardgame.self, bggID: "\(geekItem.objectID )")?.owned ?? false == false {
            Button {
                let boardgame = Boardgame(from: geekItem, in: PersistenceController.shared.container.viewContext)
                boardgame.owned = true
                PersistenceController.shared.save()
                notificationContext.present {
                    Label("Added to Library", systemSymbol: .checkmark)
                }
            } label: {
                Label("Add to Library", systemSymbol: .booksVertical)
            }
        }

        Button {
            #if os(iOS)
            sheetContext.present(AppSheet.safari(geekItem.bggURL))
            #elseif os(macOS)
            NSWorkspace.shared.open(geekItem.bggURL)
            #endif
        } label: {
            Label("View on BGG", systemSymbol: .dieFace6)
        }

        if let orderURL = geekItem.versionInfo?.orderURL {
            Button {
                #if os(iOS)
                sheetContext.present(AppSheet.safari(orderURL))
                #elseif os(macOS)
                NSWorkspace.shared.open(orderURL)
                #endif
            } label: {
                Label("View on \(geekItem.versionInfo?.orderType?.rawValue.capitalized ?? "Crowdfunding")", systemSymbol: .dieFace6)
            }
        }
    }

    @ViewBuilder
    private func tabs(forItem geekItem: GeekItem) -> some View {
        switch selectedTab {
            case .overview:
                GeekItemOverview(geekItem: geekItem)
                    .padding(.horizontal)
            case .ranks:
                RankList(ranks: geekItem.dynamicInfo?.ranks ?? [])
            case .forums:
                ForumsList(forumListVM: ForumListViewModel(objectID: "\(geekItem.objectID)"))
            case .images:
                #if os(iOS)
                GeekImageGallery(gameImagesVM: GeekImageGalleryViewModel(objectID: "\(geekItem.objectID)"))
                    .frame(height: 900)
                #else
                EmptyView()
                #endif
            case .videos:
                GeekVideoGalleryList(objectID: "\(geekItem.objectID)")
            case .files:
                GeekFileList(geekFileListVM: GeekFileListViewModel(objectID: "\(geekItem.objectID)"))
                    .frame(height: 500)
            case .stats:
                if let stats =  geekItem.dynamicInfo?.stats {
                    GeekItemStatsList(statsVM: StatsViewModel(stats: stats))
                } else {
                    Text("No Stats Available")
                        .font(.title)
                }
            case .versions:
                PagedDataList(viewModel: BoardgameVersionListViewModel(objectID: "\(geekItem.objectID)", itemsPerPage: 10)) { item in
                    BoardgameVersionRow(boardgameVersion: item)
                }
                .listStyle(PlainListStyle())
                .frame(height: 500)
            case .expansions:
                PagedDataList(viewModel: BoardgameExpansionListViewModel(objectID: "\(geekItem.objectID)", itemsPerPage: 10)) { item in
                    BoardgameExpansionRow(boardgameExpansion: item)
                }
                .listStyle(PlainListStyle())
                .frame(height: 500)
        }
    }

    @ViewBuilder
    private var overlayView: some View {
        switch geekItemVM.phase {
            case .empty:
                ProgressView()
            case .success(let geekItem) where geekItem == nil:
                EmptyPlaceholderView(text: "No Item Found", image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription, retryAction: {})
            default:
                EmptyView()
        }
    }
}

#if os(iOS)
struct GeekItem_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            if UIDevice.current.userInterfaceIdiom == .pad {
                EmptyView()
                GeekItemView(geekItemVM: GeekItemViewModel(geekItem: .sampleBoardgame))
                    .navigationBarTitle("", displayMode: .inline)
            } else {
                GeekItemView(geekItemVM: GeekItemViewModel(geekItem: .sampleBoardgame))
                    .navigationBarTitle("", displayMode: .inline)
            }
        }
    }
}
#endif
