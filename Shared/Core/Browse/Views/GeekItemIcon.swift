//
//  GeekItemIcon.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI
import SwiftUIKit

struct GeekItemIcon: View {
    let geekItem: GeekItem

    @State private var showingRanksPopover = false
    @StateObject private var sheetContext = SheetContext()

    var body: some View {
        VStack {
            HStack(spacing: 75) {
                IconView(iconType: .playerCount, text: geekItem.playerCountText)
                IconView(iconType: .playTime, text: geekItem.playTimeText)
                IconView(iconType: .weight, text: geekItem.weightText)
            }
            HStack(spacing: 75) {
                IconView(iconType: .geekRating, text: geekItem.overallRatingText)
                IconView(iconType: .minimumAge, text: geekItem.minimumAgeText)
                IconView(iconType: .status, text: "Status")
                    .onTapGesture {
                        sheetContext.present(StatusView(statusVM: StatusViewModel(geekItem: geekItem)))
                    }
            }
        }
        .sheet(sheetContext)
    }
}

struct GeekItemIcon_Previews: PreviewProvider {
    static var previews: some View {
        GeekItemIcon(geekItem: .sampleBoardgame)
    }
}
