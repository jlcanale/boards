//
//  CrowdfundingItemList.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import SwiftUI

struct CrowdfundingItemList: View {
    let crowdfundingItems: [CrowdfundingItem]

    var body: some View {
        List {
            ForEach(crowdfundingItems) { item in
                NavigationLink(destination: GeekItemView(geekItemVM: GeekItemViewModel(objectID: item.item?.id ?? ""))) {
                    CrowdfundingItemRow(crowdfundingItem: item)
                }
            }
            #if os(iOS)
            .listRowSeparator(.hidden)
            #endif
        }
        .listStyle(PlainListStyle())
    }
}

struct CrowdfundingItemList_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            CrowdfundingItemList(crowdfundingItems: CrowdfundingItem.sample)
        }
    }
}
