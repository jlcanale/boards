//
//  HotnessItemRow.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import SwiftUI
import SFSafeSymbols

struct HotnessItemRow: View {
    let geekItem: HotnessItem
    let rowNumber: Int

    var body: some View {
        HStack {
            VStack {
                Text("\(rowNumber)")
                    .font(.subheadline)
                Image(systemSymbol: chevronImage)
                    .font(.subheadline)
                    .foregroundColor(chevronColor)
                    .padding(.leading, 1)
            }

            AsyncImage(url: geekItem.imageSets["square100"]?.source2xURL) { image in
                image
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 3))
                    .frame(width: 75, height: 75)
            } placeholder: {
                Image(systemSymbol: .photo)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 75, height: 75)
                    .foregroundColor(.secondary)
            }

            VStack(alignment: .leading) {
                Text(geekItem.name)
                    .font(.headline)
                Text(geekItem.yearPublished)
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                Text(geekItem.description)
                    .font(.body)
                    .lineLimit(2)
                    .minimumScaleFactor(0.5)
            }
        }

    }

    private var chevronImage: SFSymbol {
        guard let delta = geekItem.delta else { return .minus }
        if delta < 0 {
            return .chevronDown
        } else if delta > 0 {
            return .chevronUp
        } else {
            return .minus
        }
    }

    private var chevronColor: Color {
        guard let delta = geekItem.delta else { return .gray }
        if delta < 0 {
            return .red
        } else if delta > 0 {
            return .green
        } else {
            return .gray
        }
    }
}

struct HotnessItemRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            HotnessItemRow(geekItem: .sample[0], rowNumber: 1)
                .listRowInsets(.init(top: 0, leading: 0, bottom: 0, trailing: 0))
            #if os(iOS)
                .listRowSeparator(.hidden)
            #endif
        }
        .listStyle(PlainListStyle())
    }
}
