//
//  NewReleasesItemList.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI

struct NewReleasesItemList: View {
    let items: [NewReleaseItem]

    var body: some View {
        List {
            ForEach(items) { item in
                NavigationLink(destination: GeekItemView(geekItemVM: GeekItemViewModel(objectID: item.item?.id ?? ""))) {
                    NewReleaseItemRow(newReleaseItem: item)
                }
            }
            #if os(iOS)
            .listRowSeparator(.hidden)
            #endif
        }
        .listStyle(PlainListStyle())
    }
}

struct NewReleasesItemList_Previews: PreviewProvider {
    static var previews: some View {
        NewReleasesItemList(items: NewReleaseItem.sample)
    }
}
