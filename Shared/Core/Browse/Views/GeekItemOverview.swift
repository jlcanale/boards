//
//  GeekItemOverview.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import SwiftUI

struct GeekItemOverview: View {
    let geekItem: GeekItem
    @State private var selectedLink: GeekItemLink?

    var body: some View {
        VStack(spacing: 10) {
            ExpandableTextView(text: geekItem.fullDescription.stripHTML())
            ForEach(GeekItemLink.topCredits + GeekItemLink.classifications) { link in
                if !(geekItem.links[link]?.isEmpty ?? true) {
                    StringsDisplayView(title: link.pluralTitle, items: geekItem.links[link]?.map { $0.name } ?? [])
                        .onTapGesture {
                            selectedLink = link
                        }
                }
            }

        }
        .padding(.bottom)
        .sheet(item: $selectedLink, onDismiss: nil) { link in
            MetadataList(title: link.pluralTitle, items: geekItem.links[link])
        }
    }
}

struct GeekItemOverview_Previews: PreviewProvider {
    static var previews: some View {
        GeekItemOverview(geekItem: .sampleBoardgame)
    }
}
