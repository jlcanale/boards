//
//  HotnessItemList.swift
//  Boards
//
//  Created by Joseph Canale on 9/27/21.
//

import SwiftUI

struct HotnessItemList: View {

    let geekItems: [HotnessItem]

    var body: some View {
        List {
            ForEach(geekItems) { geekItem in
                NavigationLink(destination: GeekItemView(geekItemVM: GeekItemViewModel(objectID: geekItem.objectID))) {
                    HotnessItemRow(geekItem: geekItem, rowNumber: geekItems.firstIndex(of: geekItem)! + 1)
                }
            }
            #if os(iOS)
            .listRowSeparator(.hidden)
            #endif
        }
        .listStyle(PlainListStyle())
    }
}

struct GeekItemList_Previews: PreviewProvider {
    static var previews: some View {
        HotnessItemList(geekItems: HotnessItem.sample)
    }
}
