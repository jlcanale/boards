//
//  GeekItemStatsList.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

struct GeekItemStatsList: View {
    let statsVM: StatsViewModel
    var body: some View {
        VStack(alignment: .leading) {
            ForEach(StatsViewModel.StatType.allCases) { statType in
                Text(statType.rawValue.capitalized)
                    .font(.headline)
                    .padding(.leading)

                VStack {
                    Divider()
                }
                .padding(.horizontal)
                .padding(.leading)
                .padding(.vertical, 1)
                ForEach(statsVM.stats(forType: statType).keys.sorted(), id: \.self) { key in
                    HStack {
                        Text(statsVM.stats(forType: statType)[key]!.label)
                        Spacer()
                        Text(statsVM.stats(forType: statType)[key]!.value)
                    }

                    VStack {
                        Divider()
                    }
                }
                .padding(.horizontal)
                .padding(.leading)
                .padding(.vertical, 1)

            }
        }
    }
}

struct GeekItemStatsList_Previews: PreviewProvider {
    static var previews: some View {
        GeekItemStatsList(statsVM: StatsViewModel(stats: .sample))
    }
}
