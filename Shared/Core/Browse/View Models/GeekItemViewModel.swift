//
//  GeekItemViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import Foundation

@MainActor
class GeekItemViewModel: ObservableObject {
    @Published var phase = DataFetchPhase<GeekItem?>.empty
    let objectID: String?
    let objectType: GeekItemType

    private let bggAPI = BGGAPI.shared

    init(geekItem: GeekItem? = nil, objectID: String? = nil, objectType: GeekItemType = .thing) {
        if let geekItem = geekItem {
            self.phase = .success(geekItem)
            self.objectID = geekItem.id
        } else {
            self.phase = .empty
            self.objectID = objectID
        }
        self.objectType = objectType
    }

    init(objectID: String, objectType: GeekItemType = .thing) {
        self.phase = .empty
        self.objectID = objectID
        self.objectType = objectType
    }

    func loadData() async {
        guard !Task.isCancelled else { return }
        guard let objectID = objectID else { return }
        phase = .empty
        do {
            var geekItem = try await bggAPI.fetchGeekItem(with: objectID, type: objectType)
            let dynamicInfo = try await bggAPI.fetchDynamicInfo(forID: objectID, type: objectType)
            geekItem.dynamicInfo = dynamicInfo
            guard !Task.isCancelled else { return }
            phase = .success(geekItem)
        } catch {
            guard !Task.isCancelled else { return }
            phase = .failure(error)
        }
    }
}
