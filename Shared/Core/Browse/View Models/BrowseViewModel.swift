//
//  BrowseViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 9/28/21.
//

import Foundation

@MainActor
class BrowseViewModel: ObservableObject {
    @Published var phase = DataFetchPhase<[BrowseItem]>.empty
    @Published var fetchTaskToken: BrowseFetchTaskToken

    private let bggAPI = BGGAPI.shared
    private let cache = DiskCache<[BrowseItem]>(filename: "browseItems", expirationInterval: 1 * 60 * 24)

    init(browseItems: [BrowseItem]? = nil, selectedCategory: BrowseCategory = .hotness) {
        if let browseItems = browseItems {
            self.phase = .success(browseItems)
        } else {
            self.phase = .empty
        }

        fetchTaskToken = BrowseFetchTaskToken(category: selectedCategory, token: Date())

        Task(priority: .userInitiated) {
            try? await cache.loadFromDisk()
        }
    }

    var browseItems: [BrowseItem] {
        if case let .success(browseItems) = phase {
            return browseItems
        } else {
            return []
        }
    }

    var categoryTitle: String {
        fetchTaskToken.category.text
    }

    func loadData() async {
        await fetchData()
    }

    func refreshTask() async {
        await cache.removeValue(forKey: fetchTaskToken.key)
        fetchTaskToken.token = Date()
    }

    private func fetchData() async {
        guard !Task.isCancelled else { return }
        phase = .empty

        if let browseItems = await cache.value(forKey: fetchTaskToken.key) {
            phase = .success(browseItems)
            return
        }

        do {
            var browseItems: [BrowseItem]
            switch fetchTaskToken.category {
                case .hotness:
                    browseItems = try await bggAPI.fetchHotness().map(BrowseItem.init)
                case .crowdfunding:
                    browseItems = try await bggAPI.fetchCrowdfunding().map(BrowseItem.init)
                case .newReleases:
                    browseItems = try await bggAPI.fetchNewReleases().map(BrowseItem.init)
            }
            guard !Task.isCancelled else { return }

            await cache.setValue(browseItems, forKey: fetchTaskToken.key)
            try? await cache.saveToDisk()

            phase = .success(browseItems)
        } catch {
            guard !Task.isCancelled else { return }
            phase = .failure(error)
        }
    }

}
