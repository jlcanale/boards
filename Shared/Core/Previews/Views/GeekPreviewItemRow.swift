//
//  GeekPreviewItemRow.swift
//  Boards
//
//  Created by Joe Canale on 6/29/22.
//

import SwiftUI
import CachedAsyncImage

struct GeekPreviewItemRow: View {
    var geekPreviewItem: GeekPreviewItem
    var previewItemStatusLink: PreviewItemStatusLink

    private var thumbnailURL: URL? {
        geekPreviewItem.geekItem.item.images?["square200"]
    }

    var body: some View {
        HStack {

            thumbnailImage

            VStack(alignment: .leading, spacing: 8) {

                HStack {
                    Text(geekPreviewItem.geekItem.item.primaryName.name)
                        .lineLimit(2)
                        .minimumScaleFactor(0.4)
                        .font(.callout)
                        .padding(.horizontal, 2)

                    Text(previewItemStatusLink.preStatus?.stringValue ?? "WRONG!")
                        .font(.caption2)
                        .foregroundColor(.white)
                        .padding(.horizontal)
                        .background(Capsule().foregroundColor(previewItemStatusLink.preStatus?.color ?? .clear))
                }

                HStack {
                    StackLabelView(topLabel: "Players", bottomLabel: geekPreviewItem.geekItem.item.playerCountText)

                    Divider()

                    StackLabelView(topLabel: "PlayTime", bottomLabel: geekPreviewItem.geekItem.item.playTimeText)

                    Divider()

                    StackLabelView(topLabel: "Age", bottomLabel: "\(geekPreviewItem.geekItem.item.minAge ?? "N/A")+")

                    Divider()

                    StackLabelView(topLabel: "MSRP", bottomLabel: "\(geekPreviewItem.msrpCurrency.symbol)\(geekPreviewItem.msrp)")

                }
                .frame(height: 30)
            }
        }
    }

    private var thumbnailImage: some View {
        CachedAsyncImage(url: thumbnailURL) { image in
            image
                .resizable()
        } placeholder: {
            Rectangle()
                .fill(Color.gray)
                .overlay(
                    Image(systemSymbol: .photo)
                        .font(.system(size: 30))
                        .opacity(0.6)
                )
        }
        .frame(width: 75, height: 75)
        .clipShape(RoundedRectangle(cornerRadius: 8))
        .shadow(color: .black.opacity(0.4), radius: 5)
    }
}

struct GeekPreviewItemRow_Previews: PreviewProvider {
    static var previews: some View {
        GeekPreviewItemRow(geekPreviewItem: GeekPreviewItem.sample, previewItemStatusLink: PreviewItemStatusLink(context: PersistenceController.shared.container.viewContext))
            .previewLayout(.sizeThatFits)
    }
}
