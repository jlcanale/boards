//
//  GeekPreviewView.swift
//  Boards
//
//  Created by Joe Canale on 6/8/22.
//

import SwiftUI
import SwiftUIKit
import Foundation
import UniformTypeIdentifiers

struct GeekPreviewView: View {
    @StateObject var viewModel: GeekPreviewViewModel
    @StateObject var context = SheetContext()

    init(preview: GeekPreview) {
        self._viewModel = StateObject(wrappedValue: GeekPreviewViewModel(preview: preview))
    }

    var body: some View {
        List {

            GeekPreviewHeaderView(viewModel: viewModel)
                .frame(width: UIScreen.main.bounds.width)
                .listRowSeparator(.hidden)
                .searchable(text: $viewModel.queryString)

            HStack {
                Text("\(viewModel.items.values.flatMap {$0}.count) games")
                    .font(.title)
                    .foregroundColor(.primary.opacity(0.3))
                    .frame(maxWidth: .infinity)
            }
            .listRowSeparator(.hidden)

            ForEach(viewModel.items.keys.sorted(by: \.locationString)) { parentItem in
                Section {
                    if let previewItems = viewModel.items[parentItem] {
                        ForEach(previewItems.sorted(by: \.geekItem.item.primaryName.name)) { previewItem in
                            NavigationLink {
                                GeekPreviewItemView(geekPreviewItem: previewItem)
                            } label: {
                                GeekPreviewItemRow(geekPreviewItem: previewItem, previewItemStatusLink: viewModel.previewItemStatusLink(forId: previewItem.objectID))
                            }
                            .listRowSeparator(.hidden)
                            .swipeActions(edge: .trailing) {
                                let previewItemStatusLink = viewModel.previewItemStatusLink(forId: previewItem.objectID)
                                Button {
                                    viewModel.toggleSeen(for: previewItem.objectID)
                                } label: {
                                    Image(systemSymbol: previewItemStatusLink.seen ? .eye : .eyeSlash)
                                }
                                .tint(.purple)
                            }
                        }
                    } else {
                        Text("No games")
                    }
                } header: {
                    HStack {
                        Spacer()
                        Text(parentItem.sectionTitle)
                            .font(.headline)
                            .lineLimit(0)
                            .multilineTextAlignment(.center)
                        Spacer()
                    }

                }
            }
        }
        .listStyle(PlainListStyle())
        .overlay(overlayView)
        .refreshable(action: refreshTask)
        .sheet(context)
        .navigationBarTitleDisplayMode(.inline)
        .task(priority: .userInitiated, viewModel.loadData)
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    let filePicker = FilePicker(documentTypes: [UTType.commaSeparatedText.identifier]) { result in
                        switch result {
                            case .success(let urls):
                                guard let url = urls.first else { return }
                                viewModel.importCSV(file: url)
                            case .failure(let error):
                                print(error.localizedDescription)
                        }
                    }

                    context.present(filePicker)
                } label: {
                    Image(systemSymbol: .arrowDownDoc)
                }
            }

            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    context.present(GeekPreviewItemFilterView(viewModel: viewModel).presentationDetents([.fraction(0.6)]))
                } label: {
                    Image("Filter")
                }
            }
        }
    }

    @ViewBuilder
    private var overlayView: some View {
        switch viewModel.phase {
            case .empty:
                ProgressView()
            case .success(let items) where items.isEmpty:
                EmptyPlaceholderView(text: "No Items", image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription) {
                    Task {
                        await refreshTask()
                    }
                }
            default:
                EmptyView()
        }
    }

    @Sendable
    private func refreshTask() async {
        await viewModel.refreshTask()

    }

}

struct GeekPreviewView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            GeekPreviewView(preview: .sample)
        }
    }
}
