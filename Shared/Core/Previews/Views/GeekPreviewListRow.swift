//
//  GeekPreviewListRow.swift
//  BGGKit
//
//  Created by Joe Canale on 6/7/22.
//

import SwiftUI

struct GeekPreviewListRow: View {

    var geekPreview: GeekPreview

    var body: some View {
        HStack {
            AsyncImage(url: geekPreview.images[.thumb]) { image in
                image
                    .resizable()
            } placeholder: {
                Color.gray
                    .overlay {
                        Image(systemSymbol: .photo)
                            .foregroundColor(.black.opacity(0.5))
                            .imageScale(.large)
                    }
            }
            .cornerRadius(10)
            .aspectRatio(contentMode: .fit)
            .frame(width: 75, height: 75)

            VStack(alignment: .leading) {
                Text(geekPreview.title)
                    .font(.headline)
                    .lineLimit(2)
                Text(geekPreview.dateRangeString)
                    .font(.subheadline)
                Text(geekPreview.daysUntilEventString)
                    .font(.subheadline)
            }
            .multilineTextAlignment(.leading)

            Spacer()
        }
    }
}

struct GeekPreviewListRow_Previews: PreviewProvider {
    static var previews: some View {
        List {
            GeekPreviewListRow(geekPreview: .sample)
        }
        .listStyle(PlainListStyle())
    }
}
