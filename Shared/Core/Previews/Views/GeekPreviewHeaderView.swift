//
//  GeekPreviewHeaderView.swift
//  Boards
//
//  Created by Joe Canale on 6/8/22.
//

import SwiftUI

import MapKit

struct GeekPreviewHeaderView: View {
    @ObservedObject var viewModel: GeekPreviewViewModel

    var body: some View {
        VStack {
            Image("convention")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .overlay(alignment: .center) {
                    Text(viewModel.preview.title)
                        .multilineTextAlignment(.center)
                        .font(.system(size: 30, weight: .ultraLight, design: .monospaced))
                        .foregroundColor(.primary.opacity(0.8))
                        .padding(.horizontal, 30)
                        .background(Capsule().fill(.ultraThinMaterial))
                        .padding()
                }
                .overlay(alignment: .bottomTrailing) {
                    if viewModel.previewHasValidLocation && viewModel.location != nil {
                        Button {
                            viewModel.showingMap = true
                        } label: {
                            imageButtonLabel
                        }
                    }
                }
            Spacer()
        }
        .task(priority: .userInitiated, loadTask)
        .sheet(isPresented: $viewModel.showingMap) {
            NavigationStack {
                Map(coordinateRegion: $viewModel.region)
                    .ignoresSafeArea(.all, edges: .bottom)
                    .overlay(alignment: .bottomTrailing) {
                        if let coordinate = viewModel.location?.coordinate, let url = URL(string: "maps://?saddr=&daddr=\(coordinate.latitude),\(coordinate.longitude)"), UIApplication.shared.canOpenURL(url) {
                            Button {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                viewModel.showingMap = false
                            } label: {
                                imageButtonLabel
                            }
                        }
                    }
            }
            .presentationDetents([.medium])
            .presentationDragIndicator(.visible)

        }
    }

    private var imageButtonLabel: some View {
        Image(systemSymbol: .map)
            .foregroundColor(.primary.opacity(0.8))
            .padding(10)
            .background(Circle().fill(.ultraThinMaterial))
            .padding()
    }

    @Sendable
    private func loadTask() async {
        await viewModel.fetchLocation()
    }
}

struct VerticalLabelStyle: LabelStyle {
    func makeBody(configuration: Configuration) -> some View {
        VStack {
            configuration.icon
            configuration.title
        }
    }
}

struct GeekPreviewHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            GeekPreviewHeaderView(viewModel: GeekPreviewViewModel(preview: .sample))
        }
    }
}
