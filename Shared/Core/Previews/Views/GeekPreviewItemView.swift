//
//  GeekPreviewItemView.swift
//  Boards
//
//  Created by Joe Canale on 6/30/22.
//

import SwiftUI
import CachedAsyncImage

struct GeekPreviewItemView: View {

    @StateObject private var viewModel: GeekPreviewItemView.ViewModel

    init(geekPreviewItem: GeekPreviewItem) {
        self._viewModel = StateObject(wrappedValue: ViewModel(geekPreviewItem: geekPreviewItem))
    }

    var body: some View {
        ScrollView {
            image

            HStack {
                Text(viewModel.geekItem.primaryName.name)
                    .font(.headline)
                if let yearPublished = viewModel.geekItem.yearPublished {
                    Text("(\(yearPublished))")
                        .font(.subheadline)
                }
            }
            .padding(.vertical)

            VStack(spacing: 10) {
                Text(viewModel.geekItem.shortDescription ?? "No Description Provided")
                    .font(.callout)
                    .multilineTextAlignment(.center)

                TagsView(tags: viewModel.tags)
                    .font(.caption2)
                    .opacity(0.7)
                    .padding(.bottom)

                gameInfo
            }
            .padding(.bottom, 10)

            VStack(spacing: 10) {
                ExpandableTextView(text: viewModel.itemDescription)

                StringsDisplayView(title: "Designer", items: viewModel.links(for: .designer))
                StringsDisplayView(title: "Publisher", items: viewModel.links(for: .publisher))
                StringsDisplayView(title: "Category", items: viewModel.links(for: .category))
                StringsDisplayView(title: "Mechanics", items: viewModel.links(for: .mechanic))
            }
            .padding(.horizontal)

        }
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    viewModel.showConfirmationDialog = true
                } label: {
                    Image(systemSymbol: .ellipsisCircle)
                }
            }
        }
        .sheet(isPresented: $viewModel.showStatusSheet) { statusSheet }
        .confirmationDialog("More Options", isPresented: $viewModel.showConfirmationDialog, actions: {
            Button("View on BGG", action: {})
        })
        .navigationTitle("Preview Item")
        .navigationBarTitleDisplayMode(.inline)
    }
}

extension GeekPreviewItemView {

    private var statusSheet: some View {
        NavigationStack {

            Picker("Preview Mode", selection: $viewModel.statusType) {
                ForEach(ViewModel.StatusType.allCases) { statusType in
                    Text(statusType.description)
                }
            }
            .pickerStyle(.segmented)
            .padding()

            LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())], spacing: 30) {
                ForEach(PreviewItemStatus.selectableCases) { status in
                    StackLabelView(topLabel: status.emojiValue, bottomLabel: status.stringValue, size: .medium)
                        .padding()
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(statusStrokeStyle(for: status), lineWidth: 2)
                        )
                        .onTapGesture {
                            viewModel.set(status: status)
                        }

                }
                .navigationTitle("Status")
                .navigationBarTitleDisplayMode(.inline)
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Done") {
                        viewModel.showStatusSheet = false
                    }
                }
            }

            Button {
                viewModel.toggleSeen()
            } label: {
                Label {
                    Text(viewModel.previewItemStatusLink.seen ? "Seen" : "Not Seen")
                } icon: {
                    Image(systemSymbol: viewModel.previewItemStatusLink.seen ? .eye : .eyeSlash)
                }
            }
            .buttonStyle(.bordered)

            VStack(alignment: .leading) {
                Text("Notes")
                    .font(.headline)
                TextEditor(text: $viewModel.previewItemStatusLink.notes)
                    .shadow(color: .black.opacity(0.25), radius: 4)
            }
            .padding(.horizontal)

            Spacer()
        }
        .presentationDetents([.fraction(0.6), .large])

    }

    private func statusStrokeStyle(for status: PreviewItemStatus) -> some ShapeStyle {
        switch viewModel.statusType {
            case .pre:
                return viewModel.previewItemStatusLink.preStatus == status ? status.color : .clear
            case .post:
                return viewModel.previewItemStatusLink.postStatus == status ? status.color : .clear
        }
    }

    private var gameInfo: some View {
        Grid(horizontalSpacing: 75, verticalSpacing: 20) {
            GridRow {
                IconView(iconType: .playerCount, text: viewModel.geekItem.playerCountText)
                IconView(iconType: .playTime, text: viewModel.geekItem.playTimeText)
                IconView(iconType: .minimumAge, text: viewModel.geekItem.minAgeText)
            }

            GridRow {
                IconView(iconType: .custom(Image(systemSymbol: .calendar)), text: viewModel.geekPreviewItem.versionContainer?.item.releaseDateText ?? "NA")
                IconView(iconType: .custom(Image(systemSymbol: .dollarsignCircle)), text: "\(viewModel.geekPreviewItem.msrpCurrency.symbol)\(viewModel.geekPreviewItem.msrp)")
                IconView(iconType: .status, text: viewModel.previewItemStatusLink.preStatus?.stringValue ?? "None")
                    .onTapGesture {
                        viewModel.showStatusSheet = true
                    }
            }
        }
    }

    private func infoView(topText: String, bottomText: String) -> some View {
        VStack(spacing: 4) {
            Text(topText)
                .font(.callout)
                .fontWeight(.thin)
            Text(bottomText)
                .font(.subheadline)
        }
    }

    private var image: some View {
        CachedAsyncImage(url: viewModel.imageURL) { image in
            image
                .resizable()
        } placeholder: {
            Rectangle()
                .fill(Color.gray)
                .overlay(
                    Image(systemSymbol: .photo)
                        .font(.system(size: 30))
                        .opacity(0.6)
                )
        }
        .frame(width: 225, height: 225)
        .clipShape(RoundedRectangle(cornerRadius: 8))
        .shadow(color: .black.opacity(0.4), radius: 5)
    }
}

struct GeekPreviewItemView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            GeekPreviewItemView(geekPreviewItem: .sample)
        }
    }
}
