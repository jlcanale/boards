//
//  GeekPreviewItemRowView.swift
//  Boards
//
//  Created by Joe Canale on 7/22/22.
//

import SwiftUI

struct GeekPreviewItemRowView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct GeekPreviewItemRowView_Previews: PreviewProvider {
    static var previews: some View {
        GeekPreviewItemRowView()
    }
}
