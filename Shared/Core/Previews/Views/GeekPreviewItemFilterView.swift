//
//  GeekPreviewItemFilterView.swift
//  Boards
//
//  Created by Joe Canale on 7/20/22.
//

import SwiftUI

struct GeekPreviewItemFilterView: View {
    @ObservedObject var viewModel: GeekPreviewViewModel
    @State var statusFilters: [PreviewItemStatus]
    @State var seenFilter = GeekPreviewViewModel.SeenFilter.all
    @Environment(\.dismiss) var dismiss

    init(viewModel: GeekPreviewViewModel) {
        _viewModel = ObservedObject(wrappedValue: viewModel)
        _statusFilters = State(wrappedValue: viewModel.statusFilters)
        _seenFilter = State(wrappedValue: viewModel.seenFilter)
    }

    var body: some View {
        NavigationStack {
            List {
                Section("Status") {
                    row(for: nil)
                        .onTapGesture {
                            statusFilters = []
                        }
                    ForEach(PreviewItemStatus.allCases) { status in
                        row(for: status)
                            .onTapGesture {
                                if statusFilters.contains(status) {
                                    statusFilters.removeAll(status)
                                } else {
                                    statusFilters.append(status)
                                }
                            }
                    }
                }

                Section("Seen") {
                    Picker("Seen Filter", selection: $seenFilter) {
                        ForEach(GeekPreviewViewModel.SeenFilter.allCases) { seenFilter in
                            Text(seenFilter.stringValue)
                        }
                    }
                    .pickerStyle(.segmented)
                }
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Apply") {
                        viewModel.statusFilters = statusFilters
                        viewModel.seenFilter = seenFilter
                        dismiss()
                    }
                }

                ToolbarItem(placement: .navigationBarLeading) {
                    Button("Clear") {
                        viewModel.statusFilters = []
                        viewModel.seenFilter = .all
                        dismiss()
                    }
                }
            }
        }
        .navigationBarTitleDisplayMode(.inline)

    }

    func row(for status: PreviewItemStatus?) -> some View {
        HStack {
            Text(status?.stringValue ?? "All")
            Spacer()
            if let status = status {
                if statusFilters.contains(status) {
                    Image(systemSymbol: .checkmark)
                        .renderingMode(.template)
                        .foregroundColor(.blue)
                }
            } else {
                if statusFilters.isEmpty {
                    Image(systemSymbol: .checkmark)
                        .renderingMode(.template)
                        .foregroundColor(.blue)
                }
            }
        }
        .contentShape(Rectangle())
    }
}
