//
//  GeekPreviewListing.swift
//  BGGKit
//
//  Created by Joe Canale on 6/7/22.
//

import SwiftUI

struct GeekPreviewTab: View {
    @StateObject var viewModel: GeekPreviewListViewModel

    init(viewModel: GeekPreviewListViewModel = GeekPreviewListViewModel()) {
        self._viewModel = StateObject(wrappedValue: viewModel)

    }

    var body: some View {
        ZStack {
            if viewModel.isGrouped {
                List {
                    ForEach(Array(viewModel.groupedPreviews.keys), id: \.self) { key in
                        Section(key) {
                            ForEach(viewModel.groupedPreviews[key]!) { preview in
                                NavigationLink(value: preview) {
                                    GeekPreviewListRow(geekPreview: preview)
                                }
                            }
                        }
                    }
                }
            } else {
                List(viewModel.previews) { preview in
                    NavigationLink(value: preview) {
                        GeekPreviewListRow(geekPreview: preview)
                    }
                }
            }

        }

        .navigationDestination(for: GeekPreview.self) { preview in
            GeekPreviewView(preview: preview)
        }
        .overlay(overlayView)
        .task(id: viewModel.fetchTaskToken, loadTask)
        .listStyle(PlainListStyle())
        .navigationTitle("Geek Previews")
    }

    @ViewBuilder
    private var overlayView: some View {
        switch viewModel.phase {
            case .empty:
                ProgressView()
            case .success(let items) where items.isEmpty:
                EmptyPlaceholderView(text: "No Items", image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription, retryAction: refreshTask)
            default:
                EmptyView()
        }
    }

    @Sendable
    private func loadTask() async {
        await viewModel.loadData()
    }

    private func refreshTask() {
        Task {
            await viewModel.refreshTask()
        }
    }
}

struct GeekPreviewListing_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            GeekPreviewTab(viewModel: GeekPreviewListViewModel(geekpreviews: [.sample]))
        }
    }
}
