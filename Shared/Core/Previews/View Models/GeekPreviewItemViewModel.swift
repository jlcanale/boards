//
//  GeekPreviewItemViewModel.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 7/3/22.
//

import Foundation
import SwiftUI
import Combine
import CoreData
import SwiftUIKit

extension GeekPreviewItemView {
    class ViewModel: ObservableObject {

        enum StatusType: String, CaseIterable, Identifiable, CustomStringConvertible {
            var id: Self { self }
            case pre
            case post

            var description: String {
                "\(self.rawValue.capitalized) Convention"
            }
        }

        @Published var geekPreviewItem: GeekPreviewItem
        @Published var statusType: StatusType = .pre
        @Published var preStatus: PreviewItemStatus?
        @Published var postStatus: PreviewItemStatus?
        @Published var itemDescription = "No Description"

        @Published var showStatusSheet = false
        @Published var showConfirmationDialog = false
        @Published var sheetContext = SheetContext()

        private var subscribers: Set<AnyCancellable> = []
        private var persistance = PersistenceController.shared
        @Published var previewItemStatusLink: PreviewItemStatusLink

        init(geekPreviewItem: GeekPreviewItem) {
            self.geekPreviewItem = geekPreviewItem

            let fetchRequest = PreviewItemStatusLink.makeFetchRequest()
            let context = persistance.container.viewContext
            fetchRequest.predicate = NSPredicate(format: "geekItemId == %@", geekPreviewItem.objectID )

            let results: [PreviewItemStatusLink]? = try? context.fetch(fetchRequest)
            if let fetchedStatusLink = results?.first {
                self.previewItemStatusLink = fetchedStatusLink
                // status = fetchedStatusLink.status
            } else {
                self.previewItemStatusLink = PreviewItemStatusLink(context: context)
                self.previewItemStatusLink.geekItemId = geekPreviewItem.objectID
            }

            Task {
                let description = (try? await BGGAPI.shared.fetchDescription(forID: geekPreviewItem.objectID).stripHTML()) ?? "No Description"

                DispatchQueue.main.async {
                    self.itemDescription = description
                }
            }
        }

        var tags: [String] {
            let mechanics = geekItem.links?[.mechanic]?.prefix(2).map { $0.name } ?? []
            let categories = geekItem.links?[.category]?.prefix(2).map { $0.name } ?? []

            return mechanics + categories
        }

        func links(for link: GeekItemLink) -> [String] {
            geekItem.links?[link]?.map { $0.name } ?? []
        }

        func set(status: PreviewItemStatus) {
            switch self.statusType {
                case .pre:
                    previewItemStatusLink.preStatus = status
                case .post:
                    previewItemStatusLink.postStatus = status
            }
            objectWillChange.send()
            self.persistance.save()
        }

        func toggleSeen() {
            previewItemStatusLink.seen.toggle()
            objectWillChange.send()
            self.persistance.save()
        }

        var geekItem: GeekPreviewItem.GeekItem {
            geekPreviewItem.geekItem.item
        }

        var imageURL: URL? {
            geekItem.images?["square200"]
        }
    }
}
