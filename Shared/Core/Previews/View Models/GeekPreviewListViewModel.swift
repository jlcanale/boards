//
//  GeekPreviewListViewModel.swift
//  BGGKit
//
//  Created by Joe Canale on 6/7/22.
//

import Foundation

public class GeekPreviewListViewModel: ObservableObject {
    @Published internal var phase = DataFetchPhase<[GeekPreview]>.empty
    @Published public var fetchTaskToken: FetchTaskToken
    @Published public var isGrouped: Bool = false

    private let bggAPI = BGGAPI.shared
    private let cache = DiskCache<[GeekPreview]>(filename: "geekPreviews", expirationInterval: 1 * 60 * 24)

    public init(geekpreviews: [GeekPreview]? = nil) {
        if let geekpreviews = geekpreviews {
            self.phase = .success(geekpreviews)
        } else {
            self.phase = .empty
        }
        fetchTaskToken = FetchTaskToken(token: Date(), key: "geekPreviews")
    }

    var previews: [GeekPreview] {
        if case let .success(previews) = phase {
            return previews.sorted(by: \.daysUntilEvent) { $0 > $1 }
        } else {
            return []
        }
    }

    var groupedPreviews: [String: [GeekPreview]] {
        var result = [String: [GeekPreview]]()
        for preview in previews {
            let key = preview.location
            if var previewArray = result[key] {
                previewArray.append(preview)
                result[key] = previewArray
            } else {
                result[key] = [preview]
            }
        }
        return result
    }

    @MainActor
    public func loadData() async {
        await fetchData()
    }

    public func refreshTask() async {
        bggAPI.invalidatePreviewCache()
        fetchTaskToken.token = Date()
    }

    @MainActor
    private func fetchData() async {
        guard !Task.isCancelled else { return }

        if let previews = await cache.value(forKey: fetchTaskToken.key) {
            phase = .success(previews)
            return
        }

        phase = .empty
        do {
            let previews = try await bggAPI.fetchPreviews(withFullDetails: true)
            guard !Task.isCancelled else { return }

            phase = .success(previews)
        } catch {
            guard !Task.isCancelled else { return }
            phase = .failure(error)
        }
    }
}
