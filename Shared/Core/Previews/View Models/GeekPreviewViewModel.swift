//
//  GeekPreviewViewModel.swift
//  BGGKit
//
//  Created by Joe Canale on 6/10/22.
//

import Foundation
import MapKit
import SwiftCSV

public class GeekPreviewViewModel: ObservableObject {
    @Published var preview: GeekPreview
    @Published var phase = DataFetchPhase<[GeekPreviewParentItem: [GeekPreviewItem]]>.empty
    @Published var location: CLLocation?
    @Published var showingMap = false
    @Published var region: MKCoordinateRegion = MKCoordinateRegion()
    @Published var queryString = ""
    @Published var previewItemStatusLinks: [PreviewItemStatusLink]

    var statusFilters: [PreviewItemStatus] = []
    var seenFilter: SeenFilter = .all

    private let bggAPI = BGGAPI.shared
    private let pagingData = PagingData(itemsPerPage: 25, maxPageLimit: 1000)
    private let persistence = PersistenceController.shared

    init(preview: GeekPreview) {
        self.preview = preview
        self.phase = .empty

        let context = PersistenceController.shared.container.viewContext
        let fetchRequest = PreviewItemStatusLink.makeFetchRequest()

        previewItemStatusLinks = (try? context.fetch(fetchRequest)) ?? []

    }

    var items: [GeekPreviewParentItem: [GeekPreviewItem]] {
        if case let .success(items) = phase {
            var filteredItems = items

            applyFilters(&filteredItems)

            return filteredItems
        } else {
            return [:]
        }
    }

    func previewItemStatusLink(forId geekItemId: String) -> PreviewItemStatusLink {
        if let previewItemStatusLink = previewItemStatusLinks.first(where: { $0.geekItemId == geekItemId }) {
            return previewItemStatusLink
        } else {
            let newPreviewItemStatusLink = PreviewItemStatusLink(context: persistence.container.viewContext)
            newPreviewItemStatusLink.geekItemId = geekItemId
            previewItemStatusLinks.append(newPreviewItemStatusLink)
            persistence.save()
            return newPreviewItemStatusLink
        }
    }

    func toggleSeen(for geekItemId: String) {
        let previewItemStatusLink = previewItemStatusLink(forId: geekItemId)
        previewItemStatusLink.seen.toggle()
        persistence.save()
        objectWillChange.send()
    }

    private func shouldFilter(_ geekPreviewItem: GeekPreviewItem) -> Bool {

        if !queryString.isEmpty && !geekPreviewItem.geekItem.item.primaryName.name.localizedCaseInsensitiveContains(queryString) {
            return true
        }

        guard let statusLink = previewItemStatusLinks.first(where: { $0.geekItemId == geekPreviewItem.objectID }) else { return true }

        if !statusFilters.isEmpty && !statusFilters.contains(statusLink.preStatus ?? .notPrioritized) {
            return true
        }

        switch seenFilter {
            case .seen:
                if !statusLink.seen { return true }
            case .notSeen:
                if statusLink.seen { return true }
            default:
                break
        }

        return false
    }

    private func applyFilters( _ items: inout [GeekPreviewParentItem: [GeekPreviewItem]]) {
        guard !queryString.isEmpty || seenFilter != .all || !statusFilters.isEmpty else { return }

        for (key, value) in items {
            let filteredValues = value.filter { !shouldFilter($0) }

            if filteredValues.isEmpty {
                items.removeValue(forKey: key)
            } else {
                items[key] = filteredValues
            }
        }
        return
    }

    @MainActor
    func fetchLocation() async {
        guard previewHasValidLocation else { return }
        guard let location = try? await CLGeocoder().geocodeAddressString(preview.location).first?.location else { return  }
        self.location = location
        self.region = MKCoordinateRegion(center: location.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
    }

    @MainActor @Sendable
    public func loadData() async {
        await fetchPreviewItems()
    }

    public func refreshTask() async {
        await fetchPreviewItems(cacheMode: .invalidate)
    }

    @MainActor @Sendable
    private func fetchPreviewItems(cacheMode: BGGAPI.CacheMode = .normal) async {
        guard !Task.isCancelled else { return }

        phase = .empty
        do {
            let previewItems = try await bggAPI.fetchPreviewItemsForPreview(with: preview.id, cacheMode: cacheMode)
            let parentItems = try await bggAPI.fetchPreviewParentItemsForPreview(with: preview.id, cacheMode: cacheMode)
            guard !Task.isCancelled else { return }

            var dictionary = [GeekPreviewParentItem: [GeekPreviewItem]]()
            for parentItem in parentItems {
                dictionary[parentItem] = previewItems.filter { parentItem.previewItemIDs.contains($0.id) }
            }
            dictionary = dictionary.filter { !$0.value.isEmpty }

            phase = .success(dictionary)
        } catch {
            guard !Task.isCancelled else { return }
            phase = .failure(error)
        }
    }

    var previewHasValidLocation: Bool {
        !preview.location.isEmpty && preview.location != "Potentially everywhere!"
    }

    func geocodeLocation() async -> CLLocation? {
        guard previewHasValidLocation else { return nil }
        guard let placemark = try? await CLGeocoder().geocodeAddressString(preview.location).first else { return nil }
        return placemark.location
    }

    func importCSV(file: URL) {
        guard let csv = try? CSV<Named>(url: file) else { return }
        let context = PersistenceController.shared.container.viewContext

        for row in csv.rows {
            guard let bggID = row["BGGId"] else { continue }
            let status = row["Priority"] == nil ? .notPrioritized : PreviewItemStatus(rawValue: row["Priority"]!)
            let notes = row["Notes"] ?? ""
            print("Updating \(bggID): \(status?.stringValue ?? "None")")

            if let statusLink = previewItemStatusLinks.first(where: { $0.geekItemId == bggID }) {
                statusLink.preStatus = status
                statusLink.notes = notes
            } else {
                let newStatusLink = PreviewItemStatusLink(context: context)
                newStatusLink.geekItemId = bggID
                newStatusLink.preStatus = status
                newStatusLink.notes = notes
                previewItemStatusLinks.append(newStatusLink)
            }
        }
        PersistenceController.shared.save()
    }
}

extension GeekPreviewViewModel {
    public enum SeenFilter: CaseIterable, Identifiable {
        public var id: Self { self }
        case all
        case seen
        case notSeen

        public var stringValue: String {
            switch self {
                case .all:
                    return "All"
                case .seen:
                    return "Seen"
                case .notSeen:
                    return "Not Seen"
            }
        }
    }
}
