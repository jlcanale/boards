//
//  BoardsApp.swift
//  Shared
//
//  Created by Joseph Canale on 9/21/21.
//

import SwiftUI
import CoreSpotlight

@main
struct BoardsApp: App {
    let persistenceController = PersistenceController.shared
    let appState = AppState.shared

    var body: some Scene {
        WindowGroup {
            MainView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .environmentObject(SettingsStore.shared)
                .onContinueUserActivity(CSSearchableItemActionType, perform: handleSpotlight)
        }
        .commands {
            SidebarCommands()
        }
    }

    func handleSpotlight(_ userActivity: NSUserActivity) {
        if let bggID = userActivity.userInfo?[CSSearchableItemActivityIdentifier] as? String {
            appState.pageToNavigateTo = .boardgameDetails(bggID)
        }
    }
}
