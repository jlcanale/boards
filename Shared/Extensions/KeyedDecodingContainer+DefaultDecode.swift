//
//  KeyedDecodingContainer+DefaultDecode.swift
//  BGGKit
//
//  Created by Joseph Canale on 6/2/20.
//

import Foundation

protocol StringCreatable {
    init?(_ text: String)
}

extension Int {
    var string: String {
        return String(self)
    }
}

extension DateFormatter {
    static let geekDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
}

extension Int: StringCreatable {}

extension Double: StringCreatable {}

extension KeyedDecodingContainer {
    func decode<T: StringCreatable>(key: KeyedDecodingContainer<K>.Key, defaultValue: T) -> T {
        if let decodedString = try? self.decode(String.self, forKey: key) {
            return T.init(decodedString) ?? defaultValue
        } else {
            return defaultValue
        }
    }

    func decodeIfPresent<T: Decodable>(key: KeyedDecodingContainer<K>.Key) throws -> T? {
        return try self.decodeIfPresent(T.self, forKey: key)
    }

    func decode<T: Decodable>(key: KeyedDecodingContainer<K>.Key) throws -> T {
        return try self.decode(T.self, forKey: key)
    }
}
