//
//  PreviewProviderExtensions.swift
//  Boards
//
//  Created by Joseph Canale on 10/7/21.
//

import SwiftUI
import CoreData

extension PreviewProvider {
    static var context: NSManagedObjectContext {
        PersistenceController.preview.container.viewContext
    }
}
