//
//  ArrayExtensions.swift
//  Boards
//
//  Created by Joe Canale on 5/21/22.
//

import Foundation

extension Array {
    func item(at index: Int) -> Element? {
        guard index < count else { return nil }
        return self[index]
    }
}
