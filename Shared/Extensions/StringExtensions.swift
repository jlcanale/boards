//
//  String+HTML.swift
//  
//
//  Created by Joseph Canale on 1/12/21.
//

import Foundation

internal extension String {
    func stripHTML() -> String {
        var returnString = self
        returnString = returnString.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        returnString = returnString.replacingOccurrences(of: "&[^;]+;", with: "", options: .regularExpression, range: nil)
        return returnString
    }

    var thousandsFormatted: String {
        guard let inputInt = Double(self) else { return self }
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 3
        return formatter.string(from: NSNumber(value: inputInt)) ?? self
    }

    private var anagramPrimes: [Double] { [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 97, 101, 103] }

    func anagramFactorization() -> Double { self.reduce(1.0) { $0 * anagramPrimes[Int($1.lowercased().charactersArray[0].asciiValue!) - 97] } }
}
