//
//  IntExtensions.swift
//  Boards
//
//  Created by Joseph Canale on 10/4/21.
//

import Foundation

extension Int {
    public var thousandsFormatted: String {
        let abs = Swift.abs(self)
        if abs == 0 {
            return "0"
        } else if abs >= 0 && abs < 1000 {
            return "\(abs)"
        } else if abs >= 1000 && abs < 1000000 {
            return String(format: "%ik", abs / 1000)
        }
        return String(format: "%ikk", abs / 100000)
    }

    public var boolValue: Bool {
        return self == 0 ? false : true
    }
}

extension Int32 {
    public init?(_ value: Int?) {
        guard let value = value else { return nil }
        self.init(value)
    }

    public var int: Int {
        return Int(self)
    }
}
