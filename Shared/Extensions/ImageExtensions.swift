//
//  ImageExtensions.swift
//  Boards
//
//  Created by Joseph Canale on 9/23/21.
//

import SwiftUI

extension Image {
    func geekItemRow() -> some View {
        self
            .resizable()
            .scaledToFit()
            .clipShape(RoundedRectangle(cornerRadius: 8))
            .frame(width: 112, height: 63)
    }

    func geekItemDetail() -> some View {
        self
            .resizable()
            .scaledToFit()
            .clipShape(RoundedRectangle(cornerRadius: 8))
            .frame(width: 246, height: 205)
    }
}
