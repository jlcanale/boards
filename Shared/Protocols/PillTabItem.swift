//
//  PillTabItem.swift
//  Boards
//
//  Created by Joseph Canale on 10/20/21.
//

import Foundation

protocol PillTabItem: Identifiable, Equatable {
    var title: String { get }
}
