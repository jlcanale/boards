//
//  RankRepresentable.swift
//  Boards (iOS)
//
//  Created by Joseph Canale on 10/12/21.
//

import Foundation

public protocol RankRepresentable {
    var prettyName: String { get }
    var rank: String { get }
    var bayesAverage: String { get }

    var bayesAverageInt: Int { get }
}
extension RankRepresentable {
    public var bayesAverageInt: Int {
        let formatter = NumberFormatter()
        return formatter.number(from: bayesAverage)?.intValue ?? 0
    }
}
