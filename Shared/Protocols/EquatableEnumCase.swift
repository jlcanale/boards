//
//  EquatableEnumCase.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 4/14/22.
//

import Foundation

protocol EquatableEnumCase {
    func isSameCase(as other: Self) -> Bool
}

extension EquatableEnumCase {
    func isSameCase(as other: Self) -> Bool {
        let mirrorSelf = Mirror(reflecting: self)
        let mirrorOther = Mirror(reflecting: other)
        if let caseSelf = mirrorSelf.children.first?.label, let caseOther = mirrorOther.children.first?.label {
            return (caseSelf == caseOther) // Avoid nil comparation, because (nil == nil) returns true
        } else { return false}
    }
}
