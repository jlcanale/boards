//
//  GameInfo.swift
//  Boards
//
//  Created by Joe Canale on 6/29/22.
//

import Foundation

protocol GameInfo {
    var minPlayers: String? { get }
    var maxPlayers: String? { get }
    var minPlaytime: String? { get }
    var maxPlaytime: String? { get }
}

extension GameInfo {
    public var playerCountText: String {
        if let minPlayers = minPlayers, let maxPlayers = maxPlayers, maxPlayers != minPlayers {
            return "\(minPlayers)-\(maxPlayers)"
        } else if let minPlayers = minPlayers {
            return "\(minPlayers)"
        } else if let maxPlayers = maxPlayers {
            return "\(maxPlayers)"
        } else {
            return ""
        }
    }

    public var playTimeText: String {
        if let minPlaytime = minPlaytime, let maxPlaytime = maxPlaytime, maxPlaytime != minPlaytime {
            return "\(minPlaytime)-\(maxPlaytime) Min"
        } else if let minPlaytime = minPlaytime {
            return "\(minPlaytime) Min"
        } else if let maxPlayers = maxPlaytime {
            return "\(maxPlayers) Min"
        } else {
            return ""
        }
    }
}
