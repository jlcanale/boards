//
//  Tool.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 4/18/22.
//

import Foundation
import SwiftUI

protocol Tool {
    static var title: String { get }
    static var shortDescription: String { get }
    static var content: AnyView { get }
}

extension Tool {
    var id: String { Self.title }
}

protocol ToolContent {
    var parent: Tool { get }
}
