//
//  Metadata.swift
//  Boards
//
//  Created by Joseph Canale on 10/12/21.
//

import Foundation

public protocol Metadata {
    var name: String { get }
}
