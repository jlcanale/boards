//
//  InMemoryCache.swift
//  Boards
//
//  Created by Joseph Canale on 10/14/21.
//

import Foundation

// TODO: Fix this conformance.
extension NSCache: @unchecked Sendable {}

actor InMemoryCache<V>: Cache, Sendable {
    let cache: NSCache<NSString, CacheEntry<V>> = .init()
    let keysTracker: KeysTracker<V> = .init()

    let expirationInterval: TimeInterval

    init(expirationInterval: TimeInterval) {
        self.expirationInterval = expirationInterval
    }
}

actor DiskCache<V: Codable>: Cache {
    let cache: NSCache<NSString, CacheEntry<V>> = .init()
    let keysTracker: KeysTracker<V> = .init()

    let filename: String
    let expirationInterval: TimeInterval

    init(filename: String, expirationInterval: TimeInterval) {
        self.filename = filename
        self.expirationInterval = expirationInterval
    }

    private var saveLocationURL: URL {
        FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0].appendingPathComponent("\(filename).cache")
    }

    func saveToDisk() throws {
        let entries = keysTracker.keys.compactMap(entry)
        let data = try JSONEncoder().encode(entries)
        try data.write(to: saveLocationURL)
    }

    func loadFromDisk() throws {
        let data = try Data(contentsOf: saveLocationURL)
        let entries = try JSONDecoder().decode([CacheEntry<V>].self, from: data)
        entries.forEach { insert($0) }
    }
}
