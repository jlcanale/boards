//
//  RandomType.swift
//  Boards
//
//  Created by Joe Canale on 5/17/22.
//

import Foundation

enum RandomType: CaseIterable, CustomStringConvertible, Identifiable {
    var id: Self { self }

    case number
    case word
    case letters
    case color

    var description: String {
        switch self {
            case .number:
                return "Number"
            case .word:
                return "Word"
            case .letters:
                return "Letters"
            case .color:
                return "Color"
        }
    }
}
