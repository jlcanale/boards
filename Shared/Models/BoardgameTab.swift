//
//  BoardgameTabs.swift
//  Boards
//
//  Created by Joseph Canale on 10/8/21.
//

import Foundation

enum BoardgameTab: String, CaseIterable, PillTabItem {
    case overview
    case ranks
    case forums
    case images
    case videos
    case files
    case stats
    case versions
    case expansions

    var title: String { self.rawValue.capitalized }
    var id: Self { self }
}
