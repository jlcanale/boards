//
//  PagedDataListViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

@MainActor
class PagedDataListViewModel<T: Identifiable>: ObservableObject {
    @Published var phase = DataFetchPhase<[T]>.empty
    let objectID: String?
    let bggAPI = BGGAPI.shared

    var noDataText: String { "No Data" }
    let pagingData: PagingData

    init(objectID: String, itemsPerPage: Int, userInfo: [String: Sendable]? = nil) {
        self.phase = .empty
        self.objectID = objectID
        self.pagingData = PagingData(itemsPerPage: itemsPerPage, maxPageLimit: 1000, userInfo: userInfo)
    }

    init(items: [T]) {
        self.phase = .success(items)
        self.objectID = nil
        self.pagingData = PagingData(itemsPerPage: 1, maxPageLimit: 1000)
    }

    var isFetchingNextPage: Bool {
        if case .fetchingNextPage = phase {
            return true
        }
        return false
    }

    var items: [T] {
        phase.value ?? []
    }

    func loadFirstPage() async {
        guard !Task.isCancelled else { return }
        phase = .empty
        do {
            await pagingData.reset()
            let items = try await pagingData.loadNextPage(dataFetchProvider: loadPage(page:userInfo:))

            if Task.isCancelled { return }
            phase = .success(items)
        } catch {
            if Task.isCancelled { return }
            print(error.localizedDescription)
            phase = .failure(error)
        }
    }

    func loadNextPage() async {
        guard !Task.isCancelled else { return }

        let items = self.phase.value ?? []
        phase = .fetchingNextPage(items)

        do {
            let nextItems = try await pagingData.loadNextPage(dataFetchProvider: loadPage(page:userInfo:))
            if Task.isCancelled { return }

            phase = .success(items + nextItems)
        } catch {
            print(error.localizedDescription)
        }
    }

    @Sendable
    func loadPage(page: Int, userInfo: [String: Sendable]?) async throws -> [T] {
        fatalError("\(#function) must be overridden")
    }
}
