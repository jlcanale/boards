//
//  AppSheet.swift
//  Boards
//
//  Created by Joseph Canale on 10/2/21.
//

import SwiftUI
import SwiftUIKit

enum AppSheet: SheetProvider {
    case safari(URL)

    var sheet: AnyView {
        switch self {
            case .safari(let url):
                return SafariView(url: url).edgesIgnoringSafeArea(.bottom).any()
        }
    }
}
