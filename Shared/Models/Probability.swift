//
//  Probability.swift
//  Boards
//
//  Created by Joe Canale on 6/2/22.
//

import Foundation

// swiftlint: disable identifier_name
struct Probability {
    /// Returns the expected value  when rolling the specified number of die
    /// - Parameters:
    ///   - count: The number of dice to roll
    ///   - die: The type of die to roll
    /// - Returns: The expected value when rolling ``count`` ``die``
    static func expectedValue(rolling count: Int, _ die: DieType) -> Double {
        return expectedValue(rolling: .init(expression: die, count: count))
    }

    /// Returns the expected value when rolling an array of dice.
    /// - Parameter dice: An array of ``DieType`` containing the dice you want to roll.
    /// - Returns: The expected value of rolling the ``dice`` array
    static func expectedValue(rolling dice: [DieType]) -> Double {
        return dice.reduce(0) { $0 + $1.expectedValue }
    }

    static func probabilityOfRolling(exactly: Int, using diceCount: Int, _ dieType: DieType) -> Double {
        let maxPossibleValue = dieType.rawValue * diceCount
        guard exactly <= maxPossibleValue else { return 0 }
        guard exactly > 0 else { return 0 }

        var partialSum: Double = 0
        for k in 0...((exactly - diceCount) / dieType.rawValue) {
            partialSum += pow(-1, Double(k)) * combinations(n: diceCount, choose: k) * combinations(n: exactly - (dieType.rawValue * k) - 1, choose: exactly - (dieType.rawValue * k) - diceCount)
        }
        return partialSum
    }

    static func probabilityOfRolling(atLeast: Int, using diceCount: Int, _ dieType: DieType) -> Double {
        let maxPossibleValue = dieType.rawValue * diceCount
        guard atLeast <= maxPossibleValue else { return 0 }
        guard atLeast > 0 else { return 0 }

        let winningNumbers = (1...maxPossibleValue).filter { $0 >= atLeast }
        var totalProbability: Double = 0

        for number in winningNumbers {
            totalProbability += probabilityOfRolling(exactly: number, using: diceCount, dieType)
        }

        return (1 / pow(Double(dieType.rawValue), Double(diceCount))) * totalProbability
    }

    static func probabilityOfRolling(atLeast: Int, equalTo successes: [Int], using dieCount: Int, _ dieType: DieType) -> Double {
        guard successes.allSatisfy({ $0 <= dieType.rawValue }) else { return 0}
        return pow(Double(successes.count) * dieType.anyValueProbability, Double(dieCount))
    }

    static func combinations(n: Int, choose k: Int) -> Double {
        return factorial(n) / (factorial(k) * factorial(n - k))
    }

    static func factorial(_ n: Int) -> Double {
        assert(n >= 0, "Factorial is not defined for negative numbers.")
        guard n > 0 else { return 1 }
        return (1...n).map(Double.init).reduce(1.0, *)
    }

    static func binomialProbability(events: Int, successes: Int, successProbability: Double) -> Double {
        return combinations(n: events, choose: successes) * pow(successProbability, Double(successes)) * pow(1 - successProbability, Double(events - successes))
    }
}
