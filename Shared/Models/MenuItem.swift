//
//  MenuItem.swift
//  Boards
//
//  Created by Joseph Canale on 10/20/21.
//

import Foundation
import SFSafeSymbols

public enum MenuItem: CaseIterable, Hashable {
    case search
    case library
    case previews
    case settings
    case tools
    case browse(BrowseCategory)

    public var text: String {
        switch self {
            case .search:
                return "Search"
            case .library:
                return "Library"
            case .settings:
                return "Settings"
            case .previews:
                return "Previews"
            case .tools:
                return "Tools"
            case .browse(let browseCategory):
                return browseCategory.text
        }
    }

    public var systemSymbol: SFSymbol {
        switch self {
            case .search:
                return .magnifyingglass
            case .library:
                return .booksVertical
            case .settings:
                return .gear
            case .previews:
                return .binoculars
            case .tools:
                return .wrenchAndScrewdriver
            case .browse(let browseCategory):
                return browseCategory.systemSymbol
        }
    }

    public static var allCases: [MenuItem] {
        return [.search, .library, .previews, .settings, .tools] + BrowseCategory.menuItems
    }
}

extension MenuItem: Identifiable {

    public var id: String {
        switch self {
            case .search:
                return "search"
            case .library:
                return "library"
            case .previews:
                return "previews"
            case .settings:
                return "settings"
            case .tools:
                return "tools"
            case .browse(let browseCategory):
                return browseCategory.rawValue
        }
    }

    public init?(id: MenuItem.ID?) {
        switch id {
            case MenuItem.search.id:
                self = .search
            case MenuItem.library.id:
                self = .library
            case MenuItem.previews.id:
                self = .previews
            case MenuItem.tools.id:
                self = .tools
            case MenuItem.settings.id:
                self = .settings
            default:
                if let id = id, let browseCategory = BrowseCategory(rawValue: id) {
                    self = .browse(browseCategory)
                } else {
                    return nil
                }
        }
    }
}
