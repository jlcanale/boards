//
//  DieType.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 5/15/22.
//

import Foundation

// swiftlint: disable identifier_name
enum DieType: Int, CaseIterable, CustomStringConvertible, Identifiable {
    var id: Self { self }

    case d4 = 4
    case d6 = 6
    case d8 = 8
    case d10 = 10
    case d12 = 12
    case d20 = 20
    case d100 = 100

    var description: String {
        return "d\(rawValue)"
    }

    var rollRange: ClosedRange<Int> {
        return 1...rawValue
    }

    var anyValueProbability: Double {
        1 / Double(rawValue)
    }

    var expectedValue: Double {
        var expectedValue = 0.0

        for value in 1...rawValue {
            expectedValue += (Double(value) * (1/Double(rawValue)))
        }

        return expectedValue
    }
}
