//
//  GeekUserManager.swift
//  Boards
//
//  Created by Joseph Canale on 10/15/21.
//

import Foundation

class GeekUserManager: ObservableObject {

    var username: String {
        get {
            keychainManager.string(forKey: .bggUsername) ?? ""
        }
        set {
            keychainManager.setString(newValue, forKey: .bggUsername)
            objectWillChange.send()
        }
    }

    var password: String {
        get {
            keychainManager.string(forKey: .bggPassword) ?? ""
        }
        set {
            keychainManager.setString(newValue, forKey: .bggPassword)
            objectWillChange.send()
        }
    }

    var sessionToken: BGGAPI.SessionID? {
        get {
            keychainManager.string(forKey: .bggSession)
        }
        set {
            keychainManager.setString(newValue, forKey: .bggSession)
            objectWillChange.send()
        }
    }

    private let bggAPI = BGGAPI.shared
    private let keychainManager = KeychainManager.shared

    func login() async -> Bool {
        guard !username.isEmpty, !password.isEmpty else { return false }

        if await isLoggedIn() {
            return true
        }

        if let sessionToken = try? await bggAPI.login(using: BGGCredential(username: username, password: password)) {
            self.sessionToken = sessionToken
            return true
        }

        return false
    }

    func isLoggedIn() async -> Bool {
        guard let sessionToken = sessionToken else { return false }

        if let success = try? await bggAPI.loginStatus(sessionID: sessionToken), success == true {
            return true
        }

        self.sessionToken = nil
        return false

    }

    func logout() async {
        guard let sessionToken = sessionToken else { return }

        try? await bggAPI.logout(sessionID: sessionToken)
        self.sessionToken = nil
    }
}
