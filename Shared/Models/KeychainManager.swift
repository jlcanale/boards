//
//  KeychainManager.swift
//  Boards
//
//  Created by Joseph Canale on 10/15/21.
//

import Foundation
import Valet

class KeychainManager {

    enum Key: String {
        case bggUsername = "bggusername"
        case bggPassword = "bggpassword"
        case bggSession = "SessionID"
    }

    static let shared = KeychainManager()

    private let valet = Valet.iCloudValet(with: Identifier(nonEmpty: "Boards")!, accessibility: .whenUnlocked)

    private init() {}

    func setString(_ value: String?, forKey key: KeychainManager.Key) {
        if let value = value {
            try? valet.setString(value, forKey: key.rawValue)
        } else {
            valet.setNilValueForKey(key.rawValue)
        }
    }

    func string(forKey key: KeychainManager.Key) -> String? {
        try? valet.string(forKey: key.rawValue)
    }

}
