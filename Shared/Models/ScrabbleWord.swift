//
//  ScrabbleWord.swift
//  Boards
//
//  Created by Joe Canale on 5/18/22.
//

import Foundation

struct ScrabbleWord {

    struct Placement: Hashable {
        let id = UUID()
        let scrabbleTile: ScrabbleTile
        var spaceType: ScrabbleSpaceType
    }

    private(set) var word: [Placement]

    var string: String {
        var outputString = ""
        for tile in word {
            outputString.append(tile.scrabbleTile.description)
        }
        return outputString
    }

    init(string: String) {
        var tempWord = [Placement]()
        for character in string {
            tempWord.append(Placement(scrabbleTile: .init(letter: character), spaceType: .normal))
        }
        self.word = tempWord
    }

    func score() -> Int {
        var workingScore = 0
        var wordMultiplier = 1
        for tile in word {
            let letterMuliplier = tile.spaceType == .doubleLetter ? 2 : tile.spaceType == .tripleLetter ? 3 : 1
            wordMultiplier *= tile.spaceType == .doubleWord ? 2 : tile.spaceType == .tripleWord ? 3 : 1
            workingScore += (tile.scrabbleTile.value * letterMuliplier)
        }

        workingScore *= wordMultiplier
        return workingScore
    }

    subscript(index: Int) -> Placement? {
        get {
            guard index < word.count else { return nil }
            return word[index]
        }
        set(newValue) {
            guard let newValue = newValue else { return }
            word[index] = newValue
        }
    }

}
