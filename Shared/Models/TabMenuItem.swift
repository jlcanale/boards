//
//  TabMenuItem.swift
//  Boards
//
//  Created by Joseph Canale on 10/26/21.
//

import Foundation
import SFSafeSymbols

enum TabMenuItem: String, CaseIterable, Identifiable {

    var id: Self { self }

    case library
    case search
    case browse
    case previews
    case tools
    case settings

    var text: String { rawValue.capitalized }
    var systemSymbol: SFSymbol {
        switch self {
            case .library:
                return .booksVertical
            case .search:
                return .magnifyingglass
            case .browse:
                return .eyes
            case .previews:
                return .binoculars
            case .tools:
                return .wrenchAndScrewdriver
            case .settings:
                return .gear
        }
    }

    init(menuItem: MenuItem.ID?) {
        switch MenuItem(id: menuItem) {
            case .search:
                self = .search
            case .library:
                self = .library
            case .previews:
                self = .previews
            case .settings:
                self = .settings
            case .tools:
                self = .tools
            default:
                self = .browse
        }
    }

    func menuItemId(category: BrowseCategory?) -> MenuItem.ID {
        switch self {
            case .library:
                return MenuItem.library.id
            case .search:
                return MenuItem.search.id
            case .previews:
                return MenuItem.previews.id
            case .tools:
                return MenuItem.tools.id
            case .browse:
                return MenuItem.browse(category ?? .hotness).id
            case .settings:
                return MenuItem.settings.id
        }
    }
}
