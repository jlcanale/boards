//
//  Dawg.swift
//  LetterTreeGenerator
//
//  Created by Joe Canale on 5/19/22.
//

import Foundation

public class Dawg: CustomStringConvertible {
    private var previousWord = ""
    private var uncheckedNodes = [UncheckedNode]()
    private var minimizedNodes = [Node: Node]()

    /// The root node of the Dawg
    public var root = Node()

    /// Minimizes the Dawg down to 0.
    /// This should be called after your lexicon has been inserted to maximize performance.
    public func finish() {
        minimize(downTo: 0)
        root.numberOfReachableNodes()
    }

    /// Finds all words in the Dawg that can be made using the given letters
    /// - Parameter letters: The letters to be used in forming words.  This can include a ? to indicate any letter.
    /// - Returns: An array of words that can be formed by using the letters.
    public func validWords(using letters: [Character]) -> [String] {
        return search(node: root, partialWord: "", remainingLetters: letters)
    }

    /// Finds all words in the Dawg that match the specified pattern.
    /// - Parameter pattern: The pattern to be matched.  This can include a ? to indicate any letter.
    /// - Returns: An array of words matching the pattern.
    public func words(matching pattern: String) -> [String] {
        return search(node: root, partialWord: "", remainingLetters: pattern)
    }

    private func search(node: Node, partialWord: String, remainingLetters: String) -> [String] {
        if remainingLetters.isEmpty {
            if node.isTerminal {
                return [partialWord]
            } else {
                return []
            }
        }

        let nextCharacter = remainingLetters.first!
        let remainingLetters = String(remainingLetters.dropFirst())
        var foundWords = [String]()

        for edge in node.edges.filter({ $0.key == nextCharacter || nextCharacter == "?" }) {
            foundWords.append(contentsOf: search(node: edge.value, partialWord: partialWord.appending(String(edge.key)), remainingLetters: remainingLetters))
        }
        return foundWords
    }

    /// Inserts a word into the Dawg
    /// 
    /// Words must be inserted in alphabetical order, or a fatal error is thrown.
    /// - Parameter word: The word to be inserted.
    public func insert(word: String) {
        guard word != previousWord else { return }
        guard word > previousWord else { fatalError("Error: Words must be inserted in alphabetical order!") }

        let commonPrefix = commonPrefix(word: word)
        minimize(downTo: commonPrefix)

        var node = self.uncheckedNodes.last?.child ?? root

        for letter in word.dropFirst(commonPrefix) {
            let nextNode = Node()
            node.edges[letter] = nextNode
            uncheckedNodes.append(UncheckedNode(parent: node, letter: letter, child: nextNode))
            node = nextNode
        }
        node.isTerminal = true
        previousWord = word
    }

    /// Check if the Dawg contains the given word.
    /// - Parameter word: The word to search for.
    /// - Returns: True if the word is found, false otherwise.
    public func contains(word: String) -> Bool {
        var node = root
        for letter in word {
            guard node.edges.contains(where: { $0.key == letter}) else { return false }

            for (label, child) in node.edges.sorted(by: { $0.key < $1.key }) where label == letter {
                node = child
                break
            }
        }

        return true
    }

    /// The total number of nodes in the Dawg.
    public var nodeCount: Int { minimizedNodes.count }

    /// The total number of edges in the Dawg
    public var edgeCount: Int {
        var count = 0

        for node in minimizedNodes {
            count += node.value.edges.count
        }

        return count
    }

    public var description: String {
        var stack = [root]
        var done = Set<Int>()
        var returnString = ""

        while !stack.isEmpty {
            let node = stack.popLast()!
            if done.contains(node.id) { continue }
            done.insert(node.id)
            returnString += "\(node.id): (\(node))"
            for (label, child) in node.edges {
                returnString += "     \(label) goto \(child)"
                stack.append(child)
            }
        }
        return returnString
    }

    private func search(node: Node, partialWord: String, remainingLetters: [Character]) -> [String] {
        // If there are no more letters left and we are not at a terminal node,
        // we are in the middle of the word, so we need to return nil, else return
        // the partial word as a valid word.
        if remainingLetters.isEmpty {
            if node.isTerminal {
                return [partialWord]
            } else {
                return []
            }
        }

        var foundWords = node.isTerminal ? [partialWord] : []

        for i in 0..<remainingLetters.count {
            let possibleNextLetter = remainingLetters[i]

            if possibleNextLetter == "?" {
                var nextRemainingLetters = remainingLetters
                nextRemainingLetters.remove(at: i)

                for (letter, nextNode) in node.edges {
                    let newPartialWord = partialWord.appending("\(letter)")
                    let returnWords = search(node: nextNode, partialWord: newPartialWord, remainingLetters: nextRemainingLetters)
                    if !returnWords.isEmpty {
                        foundWords.append(contentsOf: returnWords)
                    }
                }
            } else {
                guard let nextNode = node.edges[possibleNextLetter] else { continue }
                let newPartialWord = partialWord.appending("\(possibleNextLetter)")
                var nextRemainingLetters = remainingLetters
                nextRemainingLetters.remove(at: i)
                let returnWords = search(node: nextNode, partialWord: newPartialWord, remainingLetters: nextRemainingLetters)
                if !returnWords.isEmpty {
                    foundWords.append(contentsOf: returnWords)
                }
            }
        }

        return foundWords
    }

    private func commonPrefix(word: String) -> Int {
        let wordArray = Array(word)
        let previousWordArray = Array(previousWord)
        let smallerWordLength = min(wordArray.count, previousWordArray.count)
        for commonPrefix in 0..<smallerWordLength {
            if wordArray[commonPrefix] != previousWordArray[commonPrefix] {
                return commonPrefix
            }
        }
        return smallerWordLength
    }

    private func minimize(downTo: Int) {
        for i in stride(from: self.uncheckedNodes.count - 1, to: downTo - 1, by: -1) {
            let node = uncheckedNodes[i]
            if minimizedNodes.keys.contains(node.child) {
                node.parent.edges[node.letter] = minimizedNodes[node.child]
            } else {
                minimizedNodes[node.child] = node.child
            }
            _ = uncheckedNodes.popLast()
        }
    }
}

// MARK: - Node Implementation
extension Dawg {
    public class Node: Hashable, CustomStringConvertible {
        private static var nextID = 0
        public var isTerminal: Bool = false
        public var id: Int
        public var edges: [Character: Node] = [:]
        public var count = 0

        public init() {
            self.id = Self.nextID
            Self.nextID += 1
        }

        @discardableResult
        public func numberOfReachableNodes() -> Int {
            guard self.count == 0 else { return self.count }

            var localCount = 0
            if isTerminal {
                count += 1
            }

            for (_, node) in self.edges {
                localCount += node.numberOfReachableNodes()
            }

            self.count = localCount

            return self.count
        }

        public var description: String {
            var array = [String]()

            if self.isTerminal {
                array.append("1")
            } else {
                array.append("0")
            }

            for (key, value) in edges {
                array.append("\(key)")
                array.append("\(value.id)")
            }
            return array.joined(separator: "_")
        }

        public static func == (lhs: Dawg.Node, rhs: Dawg.Node) -> Bool { lhs.description == rhs.description }

        public func hash(into hasher: inout Hasher) { hasher.combine(description) }
    }
}

// MARK: - UnchedkedNode Implementation
extension Dawg {
    fileprivate class UncheckedNode {
        var parent: Node
        var letter: Character
        var child: Node

        init(parent: Node, letter: Character, child: Node) {
            self.parent = parent
            self.letter = letter
            self.child = child
        }
    }
}
