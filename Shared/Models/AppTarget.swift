//
//  AppTarget.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 4/15/22.
//

import Foundation

enum AppTarget: String, CaseIterable {
    case app
    case siriExtension
}
