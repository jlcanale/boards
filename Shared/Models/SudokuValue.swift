//
//  SudokuValue.swift
//  Boards
//
//  Created by Joe Canale on 5/22/22.
//

import Foundation

struct SudokuValue: Equatable {
    var value: Int?
    var isStartingValue: Bool = true

    static func == (lhs: SudokuValue, rhs: SudokuValue) -> Bool { lhs.value == rhs.value }

}

extension SudokuValue: ExpressibleByIntegerLiteral, ExpressibleByNilLiteral {
    init(integerLiteral value: IntegerLiteralType) {
        self.value = value
        self.isStartingValue = true
    }

    init(nilLiteral: ()) {
        self.value = nil
        self.isStartingValue = false
    }
}
