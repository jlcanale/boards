//
//  SudokuSolver.swift
//  Boards
//
//  Created by Joe Canale on 5/22/22.
//

import Foundation

struct SudokuSolver {

    var board: [[SudokuValue]] = []

    @discardableResult
    public mutating func solve() -> Bool {
        guard let emptyCell = findFirstUnassignedCell() else { return true }

        for guess in 1...9 {
            if checkIfValid(value: guess, atRow: emptyCell.row, andColumn: emptyCell.column) {
                board[emptyCell.row][emptyCell.column].isStartingValue = false
                board[emptyCell.row][emptyCell.column].value = guess

                if solve() {
                    return true
                }
                board[emptyCell.row][emptyCell.column].value = nil
            }
        }
        return false
    }

    private func checkIfValid(value: Int, atRow row: Int, andColumn column: Int) -> Bool {
        checkIf(value: value, isValidInRow: row) && checkIf(value: value, isValidInColumn: column) && checkIf(value: value, isValidInBoxRow: row, column: column)
    }

    private func checkIf(value: Int, isValidInRow row: Int) -> Bool {
        validate(value: value)
        validate(index: row)

        for i in 0..<9 {
            if board[row][i].value == value {
                return false
            }
        }

        return true
    }

    private func checkIf(value: Int, isValidInColumn column: Int) -> Bool {
        validate(value: value)
        validate(index: column)

        for i in 0..<9 {
            if board[i][column].value == value {
                return false
            }
        }

        return true
    }

    // The upper left box is 0, the lower right box is 8
    private func checkIf(value: Int, isValidInBoxRow row: Int, column: Int) -> Bool {
        validate(value: value)
        validate(index: row)
        validate(index: column)

        let rowToCheck = ((row / 3) * 3) + 1
        let columnToCheck = ((column / 3) * 3) + 1

        for i in -1...1 {
            for j in -1...1 {
                if board[rowToCheck + i][columnToCheck + j].value == value {
                    return false
                }
            }
        }
        return true
    }

    private func findFirstUnassignedCell() -> (row: Int, column: Int)? {
        for i in 0..<9 {
            for j in 0..<9 {
                if board[i][j].value == nil {
                    return (row: i, column: j)
                }
            }
        }
        return nil
    }

    private func validate(index: Int) {
        guard (0..<9).contains(index) else { preconditionFailure("Invalid value: \(index).  Value must be between 0 and 8") }
    }

    private func validate(value: Int) {
        guard (1...9).contains(value) else { preconditionFailure("Invalid value: \(value).  Value must be between 1 and 9") }
    }

}

extension SudokuSolver: CustomStringConvertible {
    var description: String {

        var returnString = "\n----Board State----\n"

        returnString += "┏━┯━┯━┳━┯━┯━┳━┯━┯━┓\n"

        for row in 0...8 {
            for column in 0...8 {
                switch column {
                    case 0, 3, 6:
                        returnString += "┃\(string(atRow: row, column: column))"
                    case 8:
                        returnString += "│\(string(atRow: row, column: column))┃\n"
                    default:
                        returnString += "│\(string(atRow: row, column: column))"
                }
            }

            switch row {
                case 2, 5:
                    returnString += "┣━┿━┿━╋━┿━┿━╋━┿━┿━┫\n"
                case 8:
                    returnString += "┗━┷━┷━┻━┷━┷━┻━┷━┷━┛\n"
                default:
                    returnString += "┠─┼─┼─╂─┼─┼─╂─┼─┼─┨\n"
            }
        }

        return returnString
    }

    private func string(atRow row: Int, column: Int) -> String {
        guard let value = board[row][column].value else { return " "}

        return "\(value)"
    }
}
