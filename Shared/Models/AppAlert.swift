//
//  AppAlert.swift
//  Boards
//
//  Created by Joseph Canale on 10/16/21.
//

import SwiftUI
import SwiftUIKit

enum AppAlert: AlertProvider {
    case error(errorMessage: String)

    var alert: Alert {
        switch self {
            case .error(let message):
                return Alert(title: Text("Error"), message: Text(message))
        }
    }
}
