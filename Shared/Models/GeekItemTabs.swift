//
//  GeekItemTab.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import SwiftUI

enum GeekItemTab: String, CaseIterable, PillTabItem {
    case overview
    case ranks
    case forums
    case images
    case videos
    case files
    case stats
    case versions
    case expansions

    var title: String { self.rawValue.capitalized}
    var id: Self { self }
}
