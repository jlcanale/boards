//
//  ComparisonMode.swift
//  Boards
//
//  Created by Joseph Canale on 10/20/21.
//

import Foundation

enum ComparisonMode: Identifiable, CaseIterable {
    var id: Self { self }
    case greaterThan
    case lessThan

    var text: String {
        switch self {
            case .greaterThan:
                return "Greater Than"
            case .lessThan:
                return "Less Than"
        }
    }
}
