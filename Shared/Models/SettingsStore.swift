//
//  SettingsStore.swift
//  Boards
//
//  Created by Joseph Canale on 10/18/21.
//

import Foundation
import SwiftUI

class SettingsStore: ObservableObject {
    @AppStorage("collectionSyncMethod") var collectionSyncMethod: CollectionSyncMethod = .merge {
        didSet {
            objectWillChange.send()
        }
    }

    @AppStorage("toolScorerDefaultPlayerCount") var toolScorerDefaultPlayerCount: Int = 4
    @AppStorage("toolScorerDefaultScoreCategoryCount") var toolScorerDefaultScoreCategoryCount: Int = 5

    @Published var geekUserManager = GeekUserManager()

    static let shared = SettingsStore()

    private init() {}
}
