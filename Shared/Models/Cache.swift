//
//  Cache.swift
//  Boards
//
//  Created by Joseph Canale on 10/14/21.
//

import Foundation

// swiftlint: disable type_name
protocol Cache: Actor {
    associatedtype V
    var expirationInterval: TimeInterval { get }

    var cache: NSCache<NSString, CacheEntry<V>> { get }
    var keysTracker: KeysTracker<V> { get }

    func setValue(_ value: V?, forKey key: String)
    func value(forKey key: String) -> V?

    func removeValue(forKey key: String)
    func removeAllValues()
}

extension Cache {
    func removeValue(forKey key: String) {
        keysTracker.keys.remove(key)
        cache.removeObject(forKey: key as NSString)
    }

    func removeAllValues() {
        keysTracker.keys.removeAll()
        cache.removeAllObjects()
    }

    func setValue(_ value: V?, forKey key: String) {
        if let value = value {
            let expiredTimeStamp = Date().addingTimeInterval(expirationInterval)
            let cacheEntry = CacheEntry(key: key, value: value, expiredTimestamp: expiredTimeStamp)
            insert(cacheEntry)
        } else {
            removeValue(forKey: key)
        }
    }

    func value(forKey key: String) -> V? {
        entry(forKey: key)?.value
    }

    func entry(forKey key: String) -> CacheEntry<V>? {
        guard let entry = cache.object(forKey: key as NSString) else { return nil }
        guard !entry.isExpired() else { return nil }
        return entry
    }

    func insert(_ entry: CacheEntry<V>) {
        keysTracker.keys.insert(entry.key)
        cache.setObject(entry, forKey: entry.key as NSString)
    }
}
