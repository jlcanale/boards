//
//  ScrabbleSpaceType.swift
//  Boards
//
//  Created by Joe Canale on 5/18/22.
//

import Foundation
import SwiftUI

enum ScrabbleSpaceType {
    case normal
    case doubleLetter
    case doubleWord
    case tripleLetter
    case tripleWord

    var color: Color {
        switch self {
            case .normal:
                return Color("Normal")
            case .doubleLetter:
                return Color("DoubleLetterScore")
            case .doubleWord:
                return Color("DoubleWordScore")
            case .tripleLetter:
                return Color("TripleLetterScore")
            case .tripleWord:
                return Color("TripleWordScore")
        }
    }

    var abbreviation: String {
        switch self {
            case .normal:
                return ""
            case .doubleLetter:
                return "DL"
            case .doubleWord:
                return "DW"
            case .tripleLetter:
                return "TL"
            case .tripleWord:
                return "TW"
        }
    }

    var next: ScrabbleSpaceType {
        switch self {
            case .normal:
                return .doubleLetter
            case .doubleLetter:
                return .doubleWord
            case .doubleWord:
                return .tripleLetter
            case .tripleLetter:
                return .tripleWord
            case .tripleWord:
                return .normal
        }
    }
}
