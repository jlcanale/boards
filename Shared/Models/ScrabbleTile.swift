//
//  ScrabbleTile.swift
//  Boards
//
//  Created by Joe Canale on 5/18/22.
//

import Foundation

struct ScrabbleTile: Hashable, ExpressibleByStringLiteral {

    let letter: Character

    var description: String { letter == "?" ? "" : letter.uppercased() }

    var value: Int {
        switch letter.uppercased() {
            case "Q", "Z":
                return 10
            case "J", "X":
                return 8
            case "K":
                return 5
            case "F", "H", "V", "W", "Y":
                return 4
            case "B", "C", "M", "P":
                return 3
            case "D", "G":
                return 2
            case "A", "E", "I", "L", "N", "O", "R", "S", "T", "U":
                return 1
            default:
                return 0
        }

    }

    public init(letter: Character) {
        self.letter = letter
    }

    public init(stringLiteral value: String) {
        if let character = value.first, character.isLetter {
            self.letter = character.uppercased().first!
        } else {
            self.letter = "?"
        }
    }

    static var allTiles: [ScrabbleTile] {
        var tiles = [ScrabbleTile]()
        for characterCode in 65...90 {
            let character = Character(UnicodeScalar(characterCode)!)
            tiles.append(.init(letter: character))
        }
        tiles.append(.init(letter: "?"))
        return tiles
    }

    static var standardBag: [ScrabbleTile] {
        var bag = [ScrabbleTile]()
        bag.append(contentsOf: Array(repeating: "A", count: 9))
        bag.append(contentsOf: Array(repeating: "B", count: 2))
        bag.append(contentsOf: Array(repeating: "C", count: 2))
        bag.append(contentsOf: Array(repeating: "D", count: 4))
        bag.append(contentsOf: Array(repeating: "E", count: 12))
        bag.append(contentsOf: Array(repeating: "F", count: 2))
        bag.append(contentsOf: Array(repeating: "G", count: 3))
        bag.append(contentsOf: Array(repeating: "H", count: 2))
        bag.append(contentsOf: Array(repeating: "I", count: 9))
        bag.append(contentsOf: Array(repeating: "J", count: 1))
        bag.append(contentsOf: Array(repeating: "K", count: 1))
        bag.append(contentsOf: Array(repeating: "L", count: 4))
        bag.append(contentsOf: Array(repeating: "M", count: 2))
        bag.append(contentsOf: Array(repeating: "N", count: 6))
        bag.append(contentsOf: Array(repeating: "O", count: 8))
        bag.append(contentsOf: Array(repeating: "P", count: 2))
        bag.append(contentsOf: Array(repeating: "Q", count: 1))
        bag.append(contentsOf: Array(repeating: "R", count: 6))
        bag.append(contentsOf: Array(repeating: "S", count: 4))
        bag.append(contentsOf: Array(repeating: "T", count: 6))
        bag.append(contentsOf: Array(repeating: "U", count: 4))
        bag.append(contentsOf: Array(repeating: "V", count: 2))
        bag.append(contentsOf: Array(repeating: "W", count: 2))
        bag.append(contentsOf: Array(repeating: "X", count: 1))
        bag.append(contentsOf: Array(repeating: "Y", count: 2))
        bag.append(contentsOf: Array(repeating: "Z", count: 1))
        return bag
    }

}
