//
//  SimpleViewModel.swift
//  Boards
//
//  Created by Joseph Canale on 10/5/21.
//

import Foundation

// swiftlint: disable type_name
protocol SimpleListViewModelData {
    associatedtype T
    var items: [T] { get }
    func fetchData() async throws -> [T]
}

@MainActor
class SimpleListViewModel<T: Identifiable>: ObservableObject {
    @Published var phase = DataFetchPhase<[T]>.empty
    let objectID: String?

    let bggAPI = BGGAPI.shared

    var noDataText: String { "No Data" }

    init(objectID: String) {
        self.phase = .empty
        self.objectID = objectID
    }

    init(items: [T]) {
        self.phase = .success(items)
        self.objectID = nil
    }

    var items: [T] {
        self.phase.value ?? []
    }

    func loadData() async {
        guard !Task.isCancelled else { return }
        guard objectID != nil else { return }
        phase = .empty
        do {
            let fetchedData = try await fetchData()
            guard !Task.isCancelled else { return }
            phase = .success(fetchedData)
        } catch {
            guard !Task.isCancelled else { return }
            phase = .failure(error)
        }
    }

    func fetchData() async throws -> [T] { fatalError("\(#function) must be overridden!") }
}
