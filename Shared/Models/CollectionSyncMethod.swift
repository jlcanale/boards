//
//  CollectionSyncMethod.swift
//  Boards
//
//  Created by Joseph Canale on 10/18/21.
//

import Foundation

enum CollectionSyncMethod: String, Identifiable, CaseIterable {
    var id: Self { self }

    case merge
    case delete

    var explanation: String {
        switch self {
            case .delete:
                return "Delete all items in my library that aren't in my BGG Collection."
            case .merge:
                return "Keep items in my library that aren't in my BGG Collection, and merge the items that are."

        }
    }
}
