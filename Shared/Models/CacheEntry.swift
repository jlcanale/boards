//
//  CacheEntry.swift
//  Boards
//
//  Created by Joseph Canale on 10/14/21.
//

import Foundation

final class CacheEntry<V: Sendable>: Sendable {
    let key: String
    let value: V
    let expiredTimestamp: Date

    init(key: String, value: V, expiredTimestamp: Date) {
           self.key = key
           self.value = value
           self.expiredTimestamp =  expiredTimestamp
    }

    func isExpired(after date: Date = .now) -> Bool {
        date > expiredTimestamp
    }
}

extension CacheEntry: Codable where V: Codable {}
