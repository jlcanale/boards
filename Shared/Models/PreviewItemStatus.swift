//
//  GeekPreviewItemStatus.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 7/3/22.
//

import Foundation
import SwiftUI

public enum PreviewItemStatus: String, CaseIterable, Identifiable {

    public var id: Self { return self }

    case mustHave = "1"
    case interested = "2"
    case undecided = "3"
    case notInterested = "4"
    case notPrioritized = "N/A"

    static var selectableCases: [PreviewItemStatus] {
        [.mustHave, .interested, .undecided, .notInterested]
    }

    var color: Color {
        switch self {
            case .mustHave:
                return .green
            case .interested:
                return .blue
            case .undecided:
                return .gray
            case .notInterested:
                return .red
            case .notPrioritized:
                return .clear
        }
    }

    var stringValue: String {
        switch self {
            case .mustHave:
                return "Must Have"
            case .interested:
                return "Interested"
            case .undecided:
                return "Undecided"
            case .notInterested:
                return "Not Interested"
            case .notPrioritized:
                return "Not Priorotized"
        }
    }

    var emojiValue: String {
        switch self {
            case .mustHave:
                return "😍"
            case .interested:
                return "🤔"
            case .undecided:
                return "🫤"
            case .notInterested:
                return "🤮"
            case .notPrioritized:
                return "❓"
        }
    }
}
