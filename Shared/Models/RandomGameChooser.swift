//
//  RandomGameChooser.swift
//  Boards (iOS)
//
//  Created by Joe Canale on 4/14/22.
//

import Foundation

class RandomGameChooser {
    static func choose(playerCount: Int? = nil) -> Boardgame? {
        let fetchRequest = Boardgame.makeFetchRequest()
        let context = PersistenceController.shared.container.viewContext

        if let playerCount = playerCount {
            fetchRequest.predicate = NSPredicate(format: "(minPlayers <= %i) AND (maxPlayers >= %i)", playerCount, playerCount)
        }

        let boardgames = (try? context.fetch(fetchRequest)) ?? []
        return boardgames.randomElement()
    }
}
