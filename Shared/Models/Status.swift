//
//  Status.swift
//  Boards (iOS)
//
//  Created by Joseph Canale on 10/12/21.
//

import Foundation

enum Status: CaseIterable {
    case owned
    case previouslyOwned
    case forTrade
    case wantToPlay
    case wantInTrade
    case wantToBuy
    case preordered
    case onWishlist

    var title: String {
        switch self {
            case .owned:
                return "Owned"
            case .previouslyOwned:
                return "Previously Owned"
            case .forTrade:
                return "For Trade"
            case .wantToPlay:
                return "Want to Play"
            case .wantInTrade:
                return "Want in Trade"
            case .wantToBuy:
                return "Want to Buy"
            case .preordered:
                return "Pre-Ordered"
            case .onWishlist:
                return "On Wishlist"
        }
    }
}
