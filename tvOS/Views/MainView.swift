//
//  MainView.swift
//  Boards (tvOS)
//
//  Created by Joseph Canale on 10/12/21.
//

import SwiftUI
import CoreData
import SFSafeSymbols

struct MainView: View {
    var body: some View {
        TabView {
            Text("Library Detail")
                .tabItem { Text("Library") }

            NavigationView {
                BrowseGrid(category: .hotness)
            }
            .tabItem { Text("Hotness") }

            Text("Crowdfunding Detail")
                .tabItem { Text("Crowdfunding") }

            Text("New Releases Detail")
                .tabItem { Text("New Releases") }

            Text("Search Detail")
                .tabItem { Image(systemSymbol: .magnifyingglass) }

        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
