//
//  BrowseGrid.swift
//  Boards (tvOS)
//
//  Created by Joseph Canale on 10/13/21.
//

import SwiftUI
import SwifterSwift

struct BrowseGrid: View {

    @StateObject var browseVM: BrowseViewModel
    let config = [GridItem(.adaptive(minimum: 350))]

    init(category: BrowseCategory) {
        _browseVM = StateObject(wrappedValue: BrowseViewModel(selectedCategory: category))
    }

    var body: some View {
        ScrollView {
            LazyVGrid(columns: config, alignment: .center) {
                ForEach(hotnessItems) { item in
                    NavigationLink {
                        Text(someLongText)
                    } label: {
                        HotnessCard(geekItem: item, rowNumber: hotnessItems.firstIndex(of: item)! + 1)
                    }
                    .buttonStyle(.card)
                }
            }
        }
        .overlay(overlayView)
        .task(id: browseVM.fetchTaskToken, loadTask)
        .navigationBarHidden(true)
    }

    private var someLongText: String {
        var returnString = ""
        for _ in 1...50 {
            returnString += String.loremIpsum()
        }

        return returnString
    }

    private var hotnessItems: [HotnessItem] {
        if case let .success(hotnessItems) = browseVM.hotnessPhase {
            return hotnessItems
        } else {
            return []
        }
    }

    @Sendable
    private func loadTask() async {
        await browseVM.loadData()
    }

    private func refreshTask() {
        browseVM.fetchTaskToken = BrowseFetchTaskToken(category: .hotness, token: Date())
    }

    @ViewBuilder
    private var overlayView: some View {
        switch browseVM.hotnessPhase {
            case .empty:
                ProgressView()
            case .success(let geekItems) where geekItems.isEmpty:
                EmptyPlaceholderView(text: "No Items", image: nil)
            case .failure(let error):
                RetryView(text: error.localizedDescription, retryAction: refreshTask)
            default:
                EmptyView()
        }
    }
}
