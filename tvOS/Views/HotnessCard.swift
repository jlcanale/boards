//
//  HotnessCard.swift
//  Boards
//
//  Created by Joseph Canale on 10/13/21.
//

import SwiftUI
import SFSafeSymbols

struct HotnessCard: View {
    let geekItem: HotnessItem
    let rowNumber: Int
    var body: some View {
        VStack {
            AsyncImage(url: geekItem.imageSets["square100"]?.source2xURL) { image in
                image
                    .resizable()
                    .scaledToFit()
                    .clipShape(RoundedRectangle(cornerRadius: 4))
                    .frame(width: 225)

            } placeholder: {
                ProgressView()
                    .frame(width: 225, height: 225)
            }
            .padding(.top)

            HStack(spacing: 0) {
                Text("\(rowNumber)")
                    .font(.subheadline)
                Image(systemSymbol: chevronImage)
                    .font(.subheadline)
                    .foregroundColor(chevronColor)
                    .padding(.leading, 1)

                Text(geekItem.name)
                    .font(.subheadline)
                    .padding(.leading)
            }
            Text(geekItem.description)
                .font(.caption)
                .padding(.horizontal)
                .minimumScaleFactor(0.5)
            Spacer()
        }
        .frame(width: 350, height: 450)
        .background(RoundedRectangle(cornerRadius: 8).background(.thinMaterial))

    }

    private var chevronImage: SFSymbol {
        guard let delta = geekItem.delta else { return .minus }
        if delta < 0 {
            return .chevronDown
        } else if delta > 0 {
            return .chevronUp
        } else {
            return .minus
        }
    }

    private var chevronColor: Color {
        guard let delta = geekItem.delta else { return .gray }
        if delta < 0 {
            return .red
        } else if delta > 0 {
            return .green
        } else {
            return .white.opacity(0.7)
        }
    }
}

struct HotnessCard_Previews: PreviewProvider {
    static var previews: some View {
        HotnessCard(geekItem: .sample[0], rowNumber: 1)
    }
}
