//
//  BrowseItemTests.swift
//  Tests iOS
//
//  Created by Joseph Canale on 10/25/21.
//

import XCTest
@testable import Boards

class BrowseItemTests: XCTestCase {

    func testHotnessInit() {
        let _ = BrowseItem(HotnessItem.sample[0])
        XCTAssert(true)
    }

    func testCrowdfundingInit() {
        let _ = BrowseItem(CrowdfundingItem.sample[0])
        XCTAssert(true)
    }

    func testNewReleaseInit() {
        let _ = BrowseItem(NewReleaseItem.sample[0])
        XCTAssert(true)
    }
}
